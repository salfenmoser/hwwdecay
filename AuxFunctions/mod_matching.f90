module mod_matching
  use mod_types; use mod_consts_dp; use common_def
  use mod_datacuts  
  implicit none
  private
  
  public ::  match4to3, match4to2, match3to2
  
  
contains
  


  subroutine supermatch3(amp1,amp2,amp3,xxx)
    implicit none 
    integer, intent(in) :: amp1, amp2, amp3
    integer,  intent(out) :: xxx
    
    
    xxx = amp1+amp2+amp3
    
    if ( (amp1.eq.2).and.(amp2.eq.4).and.(amp3.eq.8))  xxx = 14
    if ( (amp1.eq.8).and.(amp2.eq.2).and.(amp3.eq.4))  xxx = 10014
    if ( (amp1.eq.4).and.(amp2.eq.8).and.(amp3.eq.2))  xxx = 1014
    
    !--  z5
    
    if ( (amp1.eq.2).and.(amp2.eq.12).and.(amp3.eq.32)) xxx = 46
    if ( (amp1.eq.2).and.(amp2.eq.32).and.(amp3.eq.12)) xxx = 46
    if ( (amp1.eq.32).and.(amp2.eq.2).and.(amp3.eq.12)) xxx = 46
    
    
    if ( (amp1.eq.12).and.(amp2.eq.2).and.(amp3.eq.32)) xxx = 1046
    if ( (amp1.eq.12).and.(amp2.eq.32).and.(amp3.eq.2)) xxx = 1046
    if ( (amp1.eq.32).and.(amp2.eq.12).and.(amp3.eq.2)) xxx = 1046
    
    
    
    if ( (amp1.eq.4).and.(amp2.eq.10).and.(amp3.eq.32)) xxx = 1046
    if ( (amp1.eq.4).and.(amp2.eq.32).and.(amp3.eq.10)) xxx = 1046
    if ( (amp1.eq.32).and.(amp2.eq.4).and.(amp3.eq.10)) xxx = 1046
    
    if ( (amp1.eq.10).and.(amp2.eq.4).and.(amp3.eq.32)) xxx = 10046
    if ( (amp1.eq.10).and.(amp2.eq.32).and.(amp3.eq.4)) xxx = 10046
    if ( (amp1.eq.32).and.(amp2.eq.10).and.(amp3.eq.4)) xxx = 10046
    
    
    if ( (amp1.eq.6).and.(amp2.eq.8).and.(amp3.eq.32)) xxx = 46
    if ( (amp1.eq.6).and.(amp2.eq.32).and.(amp3.eq.8)) xxx = 46
    if ( (amp1.eq.32).and.(amp2.eq.6).and.(amp3.eq.8)) xxx = 46
    
    if ( (amp1.eq.8).and.(amp2.eq.6).and.(amp3.eq.32)) xxx =  10046
    if ( (amp1.eq.8).and.(amp2.eq.32).and.(amp3.eq.6)) xxx =  10046
    if ( (amp1.eq.32).and.(amp2.eq.8).and.(amp3.eq.6)) xxx =  10046
    
    
    if ( (amp1.eq.2).and.(amp2.eq.4).and.(amp3.eq.40))  xxx = 46
    if ( (amp1.eq.40).and.(amp2.eq.2).and.(amp3.eq.4))  xxx = 10046
    if ( (amp1.eq.4).and.(amp2.eq.40).and.(amp3.eq.2))  xxx = 1046
    
    
    if ( (amp1.eq.2).and.(amp2.eq.36).and.(amp3.eq.8))  xxx = 46
    if ( (amp1.eq.8).and.(amp2.eq.2).and.(amp3.eq.36))  xxx = 10046
    if ( (amp1.eq.36).and.(amp2.eq.8).and.(amp3.eq.2))  xxx = 1046
    
    
    if ( (amp1.eq.34).and.(amp2.eq.4).and.(amp3.eq.8))  xxx = 46
    if ( (amp1.eq.8).and.(amp2.eq.34).and.(amp3.eq.4))  xxx = 10046
    if ( (amp1.eq.4).and.(amp2.eq.8).and.(amp3.eq.34))  xxx = 1046
    
    
    !-- z4

    if ( (amp1.eq.2).and.(amp2.eq.12).and.(amp3.eq.16)) xxx = 30
    if ( (amp1.eq.2).and.(amp2.eq.16).and.(amp3.eq.12)) xxx = 30
    if ( (amp1.eq.16).and.(amp2.eq.2).and.(amp3.eq.12)) xxx = 30
    
    
    if ( (amp1.eq.12).and.(amp2.eq.2).and.(amp3.eq.16)) xxx = 1030
    if ( (amp1.eq.12).and.(amp2.eq.16).and.(amp3.eq.2)) xxx = 1030
    if ( (amp1.eq.16).and.(amp2.eq.12).and.(amp3.eq.2)) xxx = 1030
    
    
    
    if ( (amp1.eq.4).and.(amp2.eq.10).and.(amp3.eq.16)) xxx = 1030
    if ( (amp1.eq.4).and.(amp2.eq.16).and.(amp3.eq.10)) xxx = 1030
    if ( (amp1.eq.16).and.(amp2.eq.4).and.(amp3.eq.10)) xxx = 1030
    
    if ( (amp1.eq.10).and.(amp2.eq.4).and.(amp3.eq.16)) xxx = 10030
    if ( (amp1.eq.10).and.(amp2.eq.16).and.(amp3.eq.4)) xxx = 10030
    if ( (amp1.eq.16).and.(amp2.eq.10).and.(amp3.eq.4)) xxx = 10030
    
    
    if ( (amp1.eq.6).and.(amp2.eq.8).and.(amp3.eq.16)) xxx = 30
    if ( (amp1.eq.6).and.(amp2.eq.16).and.(amp3.eq.8)) xxx = 30
    if ( (amp1.eq.16).and.(amp2.eq.6).and.(amp3.eq.8)) xxx = 30
    
    if ( (amp1.eq.8).and.(amp2.eq.6).and.(amp3.eq.16)) xxx =  10030
    if ( (amp1.eq.8).and.(amp2.eq.16).and.(amp3.eq.6)) xxx =  10030
    if ( (amp1.eq.16).and.(amp2.eq.8).and.(amp3.eq.6)) xxx =  10030
    
    
    if ( (amp1.eq.2).and.(amp2.eq.4).and.(amp3.eq.24))  xxx = 30
    if ( (amp1.eq.24).and.(amp2.eq.2).and.(amp3.eq.4))  xxx = 10030
    if ( (amp1.eq.4).and.(amp2.eq.24).and.(amp3.eq.2))  xxx = 1030
    
    
    if ( (amp1.eq.2).and.(amp2.eq.20).and.(amp3.eq.8))  xxx = 30
    if ( (amp1.eq.8).and.(amp2.eq.2).and.(amp3.eq.20))  xxx = 10030
    if ( (amp1.eq.20).and.(amp2.eq.8).and.(amp3.eq.2))  xxx = 1030
    
    
    if ( (amp1.eq.18).and.(amp2.eq.4).and.(amp3.eq.8))  xxx = 30
    if ( (amp1.eq.8).and.(amp2.eq.18).and.(amp3.eq.4))  xxx = 10030
    if ( (amp1.eq.4).and.(amp2.eq.8).and.(amp3.eq.18))  xxx = 1030
    
    
    return 
    
  end subroutine supermatch3
  

  subroutine supermatch2(amp1,amp2,xxx)
    implicit none 
    integer, intent(in) :: amp1, amp2
    integer,  intent(out) :: xxx
    
    xxx = amp1 + amp2
    
    

    if ( (amp1.eq.2).and.(amp2.eq.12))  xxx = 14
    if ( (amp1.eq.12).and.(amp2.eq.2) ) xxx = 1014
    
    if ( (amp1.eq.4).and.(amp2.eq.10) ) xxx = 1014
    if ( (amp1.eq.10).and.(amp2.eq.4) ) xxx = 10014
    
    if ( (amp1.eq.8).and.(amp2.eq.6)  ) xxx = 10014
    if ( (amp1.eq.6).and.(amp2.eq.8) ) xxx =  14
    
    !-- z4
    
    if ( (amp1.eq.2).and.(amp2.eq.28))  xxx = 30
    if ( (amp1.eq.28).and.(amp2.eq.2) ) xxx = 1030
    
    if ( (amp1.eq.4).and.(amp2.eq.26) ) xxx = 1030
    if ( (amp1.eq.26).and.(amp2.eq.4) ) xxx = 10030
    
    if ( (amp1.eq.8).and.(amp2.eq.22)  ) xxx = 10030
    if ( (amp1.eq.22).and.(amp2.eq.8) ) xxx =  30
    
    
    if ( (amp1.eq.18).and.(amp2.eq.12))  xxx =30
    if ( (amp1.eq.12).and.(amp2.eq.18) ) xxx = 1030
    
    if ( (amp1.eq.20).and.(amp2.eq.10) ) xxx = 1030
    if ( (amp1.eq.10).and.(amp2.eq.20) ) xxx = 10030
    
    if ( (amp1.eq.24).and.(amp2.eq.6)  ) xxx = 10030
    if ( (amp1.eq.6).and.(amp2.eq.24) ) xxx =  30
    
    
    !-- z5
    

    if ( (amp1.eq.2).and.(amp2.eq.44))  xxx = 46
    if ( (amp1.eq.44).and.(amp2.eq.2) ) xxx = 1046
    
    if ( (amp1.eq.4).and.(amp2.eq.42) ) xxx = 1046
    if ( (amp1.eq.42).and.(amp2.eq.4) ) xxx = 10046
    
    if ( (amp1.eq.8).and.(amp2.eq.38)  ) xxx = 10046
    if ( (amp1.eq.38).and.(amp2.eq.8) ) xxx =  46
    
    
    if ( (amp1.eq.34).and.(amp2.eq.12))  xxx = 46
    if ( (amp1.eq.12).and.(amp2.eq.34) ) xxx = 1046
    
    if ( (amp1.eq.38).and.(amp2.eq.10) ) xxx = 1046
    if ( (amp1.eq.10).and.(amp2.eq.38) ) xxx = 10046
    
    if ( (amp1.eq.40).and.(amp2.eq.6)  ) xxx = 10046
    if ( (amp1.eq.6).and.(amp2.eq.40) ) xxx =  46
    
    
    
    
    
    
    
  end subroutine supermatch2
  



          
  subroutine  match4to3(amp3cut,amp4cut,listofprops,equal)
    implicit none
    integer, intent(in) :: amp3cut(1:3), amp4cut(1:4) 
    integer, intent(out) :: listofprops(1:2), equal
    integer :: i,i3 
    integer :: amp4to3(1:4,1:3), auxarray(1:3), pos, x12, x23, x34, x41
    
    equal = 0
    
    
    call supermatch2(amp4cut(1),amp4cut(2),x12) 
    call supermatch2(amp4cut(2),amp4cut(3),x23) 
    call supermatch2(amp4cut(3),amp4cut(4),x34) 
    call supermatch2(amp4cut(4),amp4cut(1),x41) 
    
    
    amp4to3(1,1:3) = (/x12,amp4cut(3),amp4cut(4)/)
    amp4to3(2,1:3) = (/amp4cut(1),x23,amp4cut(4)/)
    amp4to3(3,1:3) = (/amp4cut(1), amp4cut(2),x34/)
    amp4to3(4,1:3) = (/x41,amp4cut(2),amp4cut(3)/)
    
    
    do i=1,4
       
       pos = 0
       
       do i3=1,3
          if (amp3cut(1).eq.amp4to3(i,i3))  then 
             pos = i3
             auxarray = cshift(amp4to3(i,1:3),pos-1)
             exit
          endif
       enddo
       
       
       if(pos.ne.0.and.all(auxarray.eq.amp3cut)) equal = 1
       
       if (equal.eq.1) exit
                 
    enddo

    
    ! now fixing the relative position ! the first entry matches the propagator that matches 
    ! first entry in amp3cut and the second entry is the propagator of list4 that isn't cut
    
    
    if ( (i.eq.1).and.(pos.eq.1)) listofprops = (/1,2/)
    if ( (i.eq.1).and.(pos.eq.2)) listofprops = (/3,2/) 
    if ( (i.eq.1).and.(pos.eq.3)) listofprops = (/4,2/) 
    
    if ( (i.eq.2).and.(pos.eq.1)) listofprops = (/1,3/)
    if ( (i.eq.2).and.(pos.eq.2)) listofprops = (/2,3/) 
    if ( (i.eq.2).and.(pos.eq.3)) listofprops = (/4,3/) 
    
    
    if ( (i.eq.3).and.(pos.eq.1)) listofprops = (/1,4/)
    if ( (i.eq.3).and.(pos.eq.2)) listofprops = (/2,4/) 
    if ( (i.eq.3).and.(pos.eq.3)) listofprops = (/3,4/) 
    
    
    if ( (i.eq.4).and.(pos.eq.1)) listofprops = (/4,1/)
    if ( (i.eq.4).and.(pos.eq.2)) listofprops = (/2,1/) 
    if ( (i.eq.4).and.(pos.eq.3)) listofprops = (/3,1/) 
    
    
    
    return 
  end subroutine match4to3




  
  subroutine  match4to2(amp2cut,amp4cut,listofprops,equal)
    implicit none
    integer, intent(in) :: amp2cut(1:2), amp4cut(1:4) 
    integer, intent(out) :: listofprops(1:3), equal
    integer i,i2, x123, x412, x234, x341, x12, x34, x23, x41
    integer amp4to2(1:6,1:2), auxarray(1:2), pos
    
    equal = 0
    
    call supermatch3(amp4cut(1),amp4cut(2),amp4cut(3),x123)    
    call supermatch3(amp4cut(4),amp4cut(1),amp4cut(2),x412)    
    call supermatch3(amp4cut(2),amp4cut(3),amp4cut(4),x234)    
    call supermatch3(amp4cut(3),amp4cut(4),amp4cut(1),x341)    
    
    call supermatch2(amp4cut(1),amp4cut(2),x12)
    call supermatch2(amp4cut(3),amp4cut(4),x34)
    call supermatch2(amp4cut(2),amp4cut(3),x23)
    call supermatch2(amp4cut(4),amp4cut(1),x41)
    
    
    amp4to2(1,1:2) = (/x123,amp4cut(4)/)  
    amp4to2(2,1:2) = (/x12,x34/)
    amp4to2(3,1:2) = (/x412,amp4cut(3)/)
    amp4to2(4,1:2) = (/amp4cut(1),x234/)
    amp4to2(5,1:2) = (/x41,x23/)
    amp4to2(6,1:2) = (/x341, amp4cut(2)/)
    
    
    do i=1,6
       
       pos = 0
       
       do i2=1,2
          if (amp2cut(1).eq.amp4to2(i,i2))  then 
             pos = i2
             auxarray = cshift(amp4to2(i,1:2),pos-1)
             exit
          endif
       enddo
       
       
       if(pos.ne.0.and.all(auxarray.eq.amp2cut)) equal = 1
       
       
       if (equal.eq.1) exit
       
    enddo
    
    
    ! now fixing the relative position ! the first entry matches the propagator that matches 
    ! first entry in amp2cut and the second & third  entries are propagators of list4 that aren't cut
    
    
    if ( (i.eq.1).and.(pos.eq.1)) listofprops = (/1,2,3/)
    if ( (i.eq.1).and.(pos.eq.2)) listofprops = (/4,2,3/) 
    
    
    if ( (i.eq.2).and.(pos.eq.1)) listofprops = (/1,2,4/)
    if ( (i.eq.2).and.(pos.eq.2)) listofprops = (/3,2,4/) 
    
    
    
    if ( (i.eq.3).and.(pos.eq.1)) listofprops = (/4,1,2/)
    if ( (i.eq.3).and.(pos.eq.2)) listofprops = (/3,1,2/) 
    
    
    if ( (i.eq.4).and.(pos.eq.1)) listofprops = (/1,3,4/)
    if ( (i.eq.4).and.(pos.eq.2)) listofprops = (/2,3,4/) 
    
    
    
    if ( (i.eq.5).and.(pos.eq.1)) listofprops = (/4,1,3/)
    if ( (i.eq.5).and.(pos.eq.2)) listofprops = (/2,1,3/) 
    
    
    if ( (i.eq.6).and.(pos.eq.1)) listofprops = (/3,1,4/)
    if ( (i.eq.6).and.(pos.eq.2)) listofprops = (/2,1,4/) 
    
    
    return 
  
  end subroutine match4to2




  subroutine  match3to2(amp2cut,amp3cut,listofprops,equal)
    implicit none
    integer, intent(in) :: amp2cut(1:2), amp3cut(1:3) 
    integer, intent(out) :: listofprops(1:2), equal
    integer i,i2, x12, x23, x31
    integer amp3to2(1:3,1:2), auxarray(1:2), pos
    
    equal = 0
    
    call supermatch2(amp3cut(1),amp3cut(2),x12)
    call supermatch2(amp3cut(2),amp3cut(3),x23)
    call supermatch2(amp3cut(3),amp3cut(1),x31)
    
    amp3to2(1,1:2) = (/x12,amp3cut(3)/)
    amp3to2(2,1:2) = (/amp3cut(1),x23/)
    amp3to2(3,1:2) = (/x31,amp3cut(2)/)
    
    
    
    do i=1,3
       
       pos = 0
       
       do i2=1,2
          if (amp2cut(1).eq.amp3to2(i,i2))  then 
             pos = i2
             auxarray = cshift(amp3to2(i,1:2),pos-1)
             exit
          endif
       enddo
       
       
       if(pos.ne.0.and.all(auxarray.eq.amp2cut)) equal = 1
       
       
       if (equal.eq.1) exit
       
       
    enddo
    
    
    ! now fixing the relative position ! the first entry matches the propagator that matches 
    ! first entry in amp3cut and the second entry is the propagator of list4 that isn't cut
    
    
    amp3to2(1,1:2) = (/x12,amp3cut(3)/)
    amp3to2(2,1:2) = (/amp3cut(1),x23/)
    amp3to2(3,1:2) = (/x31,amp3cut(2)/)
    
    
    if ( (i.eq.1).and.(pos.eq.1)) listofprops = (/1,2/)
    if ( (i.eq.1).and.(pos.eq.2)) listofprops = (/3,2/) 
    
    
    if ( (i.eq.2).and.(pos.eq.1)) listofprops = (/1,3/)
    if ( (i.eq.2).and.(pos.eq.2)) listofprops = (/2,3/) 
    
    
    if ( (i.eq.3).and.(pos.eq.1)) listofprops = (/3,1/)
    if ( (i.eq.3).and.(pos.eq.2)) listofprops = (/2,1/) 
    
    
    return 
  
  end subroutine match3to2

end module mod_matching
