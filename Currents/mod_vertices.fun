test_suite mod_vertices

setup
end setup

teardown
end teardown

test levicevitaTest

  assert_equal(levicevita(1,2,3), 1)
  assert_equal(levicevita(1,3,2), -1)
  assert_equal(levicevita(1,2,2), 0)
  assert_equal(levicevita(1,2,4), 0)

end test levicevitaTest

test deltaTest
  assert_equal(delta(1,2), 0)
  assert_equal(delta(45,45), 1)
end test deltaTest


end test_suite mod_vertices
