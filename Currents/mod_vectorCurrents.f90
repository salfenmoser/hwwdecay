module mod_vectorCurrents

use mod_types
use mod_consts_dp
use common_def
  
use mod_auxfunctions
use mod_vertices
use mod_propagators

implicit none
  
public :: W3, W2H, HW2, H3

public :: W4, W4_1, W4_2
public :: W3H, W3H_1, W3H_2, HW3, HW3_1
public :: W2H2, W2H2_1, W2H2_2, W2H2_3, H2W2, H2W2_1, H2W2_2, H2W2_3
public :: H4, H4_1, H4_2

public :: W5, W5_1, W5_2, W5_3, W5_4
public :: W4H, W4H_1, W4H_1b, W4H_2, W4H_3, W4H_4, W4H_5, W4H_6, HW4
public :: W3H2, W3H2_1, W3H2_2, W3H2_3, W3H2_4, H2W3
public :: W2H3, W2H3_1, W2H3_2, W2H3_3, W2H3_4, W2H3_5, H3W2
public :: H5, H5_1, H5_2, H5_3

public :: W6, W6_1, W6_2, W6_3

contains

!----------------------------------------------------------------------------------------------
! 3 particle CURRENTS
!----------------------------------------------------------------------------------------------
function W3(i3,p1,p2) 
  integer :: i3
  complex(dp), intent(in) :: p1(3,dimP), p2(3,dimP)
  complex(dp) ::  W3(dimP) 
  W3(:) = V_w3(i3,p1(1,:),p1(2,:),int(p1(3,1)),p2(1,:),p2(2,:),int(p2(3,1)))
end function W3

function W2H(i3,p1)
  integer :: i3
  complex(dp), intent(in) :: p1(3,dimP)
  complex(dp) :: W2H(dimP)
  W2H(:) = V_hw2(i3, int(p1(3,1))) * p1(2,:)
  !print *, 'colors here = ', i3, int(p1(3,1))
end function W2H

function HW2(p1,p2)   
  complex(dp), intent(in) :: p1(3,dimP), p2(3,dimP)
  complex(dp) ::  HW2
  HW2 =  sc(p2(2,:), W2H(int(p2(3,1)), p1))
end function HW2

function H3()
  complex(dp) :: H3
  H3 = V_h3()
end function

!----------------------------------------------------------------------------------------------
! 4 particle CURRENTS
!----------------------------------------------------------------------------------------------
! W -> 3W; W1 emitted directly, W2,3 after another splitting
function W4_1(i4,p1,p2,p3) 
  integer :: i4,ix, j
  complex(dp), intent(in) :: p1(3,dimP), p2(3,dimP), p3(3,dimP)
  complex(dp) ::  W4_1(dimP), partx(3,dimP)
  do j=1,dimP
  W4_1(j) = czero
  end do
 
  ! internal W
  do ix = 1,3
  
  partx(1,:) = - p2(1,:) - p3(1,:)
  partx(2,:) = WP(partx(1,:),W3(ix,p2,p3))
  partx(3,1) = ix*cone
  do j = 2,dimP
  partx(3,j) = czero
  end do  
  
  partx(1,:) = - partx(1,:)
  W4_1 = W4_1 + W3(i4,p1,partx)
  end do
  
  ! internal H
  W4_1 = W4_1 + W2H(i4,p1) * HP(partx(1,:)) * HW2(p2, p3)
  !print *, 'internal higgs contribution:', W2H(i4,p1) * HP(partx(1,:)) * HW2(p2, p3)
  
end function W4_1

! W -> 3W, 4vertex
function W4_2(i4,p1,p2,p3)
  integer :: i4
  complex(dp), intent(in) :: p1(3,dimP),p2(3,dimP),p3(3,dimP)
  complex(dp) ::  W4_2(dimP)
  W4_2 = V_w4(i4, p1(2,:),int(p1(3,1)), p2(2,:),int(p2(3,1)), p3(2,:),int(p3(3,1)))
end function W4_2

! W -> 3W, all combinations
function W4(i4,p1,p2,p3)
  integer :: i4
  complex(dp), intent(in) :: p1(3,dimP),p2(3,dimP),p3(3,dimP)
  complex(dp) ::  W4(dimP)
  W4 = W4_1(i4,p1,p2,p3) + W4_1(i4,p2,p1,p3) + W4_1(i4,p3,p1,p2) + W4_2(i4,p1,p2,p3)
  !print *, '----w4---'
  !print *, W4_1(i4,p1,p2,p3) 
  ! print *, W4_1(i4,p2,p1,p3)
  ! print *, W4_1(i4,p3,p1,p2)
  ! print *, W4_2(i4,p1,p2,p3)
  ! print *, '-----------'
  !W4 = W4_1(i4,p2,p1,p3)
end function W4

! W4 -> W1W2H, internal W->W1,W2
function W3H_1(i4, p1,p2)
  integer :: i4, ix
  complex(dp), intent(in) :: p1(3,dimP),p2(3,dimP)
  complex(dp) :: W3H_1(dimP), partx(3,dimP)
W3H_1(:) = czero
partx(3,:) = czero
 
! internal W->W1,W2
do ix = 1,3
partx(1,:) = - p1(1,:) - p2(1,:)
partx(2,:) = WP(partx(1,:),W3(ix,p1,p2))
partx(3,1) = cone*ix
  
partx(1,:) = - partx(1,:)
W3H_1 = W3H_1 + W2H(i4,partx)
end do

end function W3H_1

! W -> W1,W2,H3, internal W->W2,H3
function W3H_2(i4, p1,p2,p3)
  integer :: i4, ix
  complex(dp), intent(in) :: p1(3,dimP),p2(3,dimP),p3(3,dimP)
  complex(dp) :: W3H_2(dimP), partx(3,dimP)
  W3H_2(:) = czero
  partx(3,:) = czero
  
  ! internal W->W2,H3
  ix = int(p2(3,1))
  partx(1,:) = - p2(1,:) - p3(1,:)
  partx(2,:) = WP(partx(1,:),W2H(ix,p2))
  partx(3,1) = cone*ix
  
  partx(1,:) = - partx(1,:)
  W3H_2 = W3H_2 + W3(i4,p1,partx)
end function W3H_2

! W -> W1,W2,H3, all combinations
function W3H(i4, p1,p2,p3)
  integer :: i4
  complex(dp), intent(in) :: p1(3,dimP),p2(3,dimP),p3(3,dimP)
  complex(dp) :: W3H(dimP)
  W3H = W3H_1(i4, p1,p2) + W3H_2(i4, p1,p2,p3) + W3H_2(i4, p2,p1,p3)
end function W3H

! H -> 3W, internal W->W2,W3
function HW3_1(p1,p2,p3) 
  complex(dp), intent(in) :: p1(3,dimP),p2(3,dimP),p3(3,dimP)
  complex(dp) ::  HW3_1
  HW3_1 = sc(p1(2,:), W3H_1(int(p1(3,1)), p2, p3))
end function HW3_1

! H -> 3W, all combinations
function HW3(p1,p2,p3) 
  complex(dp), intent(in) :: p1(3,dimP),p2(3,dimP),p3(3,dimP)
  complex(dp) ::  HW3
  HW3 = HW3_1(p1,p2,p3) + HW3_1(p2,p1,p3) + HW3_1(p3,p1,p2)
end function HW3

! W -> W1,H2,H3, internal W-> W1,H2
function W2H2_1(i4, p1, p2)
  integer :: i4, ix, j
  complex(dp), intent(in) :: p1(3,dimP),p2(3,dimP)
  complex(dp) :: W2H2_1(dimP), partx(3,dimP)
  W2H2_1(:) = czero
  partx(3,:) = czero
  
! internal W->W1,H2
do ix = 1,3
  partx(1,:) = - p1(1,:) - p2(1,:)
  partx(3,1) = cone*ix
  partx(2,:) = WP(partx(1,:),W2H(ix,p1))

  partx(1,:) = - partx(1,:)
  W2H2_1 = W2H2_1 + W2H(i4, partx)
end do

end function W2H2_1

!  W -> W1,H2,H3, internal H-> H2,H3
function W2H2_2(i4, p1,p2,p3)
  integer :: i4
  complex(dp), intent(in) :: p1(3,dimP),p2(3,dimP),p3(3,dimP)
  complex(dp) :: W2H2_2(dimP)
  W2H2_2 = H3() * HP( p2(1,:) + p3(1,:) ) * W2H(i4, p1)
end function W2H2_2

! W -> W1,H2,H3, 4-vertex
function W2H2_3(i4, p1)
  integer :: i4
  complex(dp), intent(in) :: p1(3,dimP)
  complex(dp) :: W2H2_3(dimP)
  W2H2_3 = V_h2w2(i4,int(p1(3,1))) * p1(2,:)
end function W2H2_3

! W -> W1,H2,H3, all combinations
function W2H2(i4, p1,p2,p3)
  integer :: i4
  complex(dp), intent(in) :: p1(3,dimP),p2(3,dimP),p3(3,dimP)
  complex(dp) :: W2H2(dimP)
  W2H2 = W2H2_1(i4, p1, p2) + W2H2_1(i4, p1, p3) + W2H2_2(i4, p1,p2,p3) + W2H2_3(i4, p1)
  !W2H2 = W2H2_1(i4, p1, p3)
end function W2H2

! H -> W1,W2,H3, internal W-> W1,H3
function H2W2_1(p1,p2,p3)
  complex(dp), intent(in) :: p1(3,dimP), p2(3,dimP), p3(3,dimP)
  complex(dp) :: H2W2_1
  H2W2_1 = sc( p1(2,:) , W2H2_1(int(p1(3,1)), p2, p3) )
  !print *, 'W current = ',  W2H2_1(int(p1(3,1)), p2, p3) 
end function H2W2_1

! H -> W1,W2,H3, internal H-> H2,H3
function H2W2_2(p1, p2)
  complex(dp), intent(in) :: p1(3,dimP), p2(3,dimP)
  complex(dp) :: H2W2_2
  H2W2_2 =  HW2(p1,p2) * HP(p1(1,:) + p2(1,:)) * H3()
end function H2W2_2

! H -> W1,W2,H3, 4vertex
function H2W2_3(p1,p2)
  complex(dp), intent(in) :: p1(3,dimP), p2(3,dimP)
  complex(dp) :: H2W2_3
  H2W2_3 = sc( p1(2,:) , W2H2_3(int(p1(3,1)), p2) )
end function H2W2_3

! H -> W1,W2,H3, all combinations
function H2W2(p1,p2,p3)
  complex(dp), intent(in) :: p1(3,dimP), p2(3,dimP), p3(3,dimP)
  complex(dp) :: H2W2
  H2W2 = H2W2_1(p1,p2,p3) + H2W2_1(p2,p1,p3) + H2W2_2(p1,p2) + H2W2_3(p1,p2)
  ! print *, 'H2W2 contributions: '
  ! print *, '1. = ', H2W2_1(p1,p2,p3)
  ! print *, '2. = ', H2W2_1(p2,p1,p3)
  ! print *, '3. = ', H2W2_2(p1,p2)
  ! print *, '4. = ', H2W2_3(p1,p2)
  ! print *, 'propagator:', HP(p1(1,:) + p2(1,:))
  ! print *, 'three vertices :', HW2(p1,p2), H3()
  ! print *, 'eps2= ', p2(2,:)
  ! print *, 'eps1 with vert =', W2H(int(p2(3,1)), p1)
 ! H2W2 = H2W2_2(p1,p2)
end function H2W2

! H -> H1,H2,H3, internal H->H1,H2
function H4_1(p1,p2)
  complex(dp), intent(in) :: p1(3,dimP), p2(3,dimP)
  complex(dp) ::  H4_1
  H4_1 = H3() * HP(p1(1,:) + p2(1,:)) * H3()
end function H4_1

! H -> H1,H2,H3, 4vertex
function H4_2()
  complex :: H4_2
  H4_2 = V_h4()
end function H4_2

! H -> H1,H2,H3, all combinations
function H4(p1,p2,p3)
  complex(dp), intent(in) :: p1(3,dimP), p2(3,dimP), p3(3,dimP)
  complex(dp) :: H4
  H4 = H4_1(p1,p2) + H4_1(p1,p3) + H4_1(p2,p3) + H4_2()
  ! print *, 'H4 contributions: '
  ! print *, H4_1(p1,p2)
  ! print *, H4_1(p1,p3)
  ! print *, H4_1(p2,p3)
  ! print *, H4_2()
end function H4

!----------------------------------------------------------------------------------------------
! 5 particle CURRENTS
!----------------------------------------------------------------------------------------------
! W -> 4W; W1 emitted directly, internal W3 orHW3 current
function W5_1(i5,p1,p2,p3,p4) 
  integer :: i5,ix, j
  complex(dp), intent(in) :: p1(3,dimP), p2(3,dimP), p3(3,dimP), p4(3,dimP)
  complex(dp) ::  W5_1(dimP), partx(3,dimP)
  do j = 1,dimP
  W5_1(j) = czero
  end do
 
  ! internal W, colour structure checked 12/04
  do ix = 1,3
  
  partx(1,:) = - p2(1,:) - p3(1,:) - p4(1,:)
  partx(2,:) = WP(partx(1,:),W4(ix,p2,p3,p4))
  partx(3,1) = cone*ix
  do j = 2, dimP
  partx(3,j) = czero
  end do
  
  partx(1,:) = - partx(1,:)
  W5_1 = W5_1 + W3(i5,partx,p1)

  end do

  ! internal H, checked 12/04
  W5_1 = W5_1 + p1(2,:) *  V_hw2(i5, int(p1(3,1))) * HP(partx(1,:)) *HW3(p2,p3,p4)
  
end function W5_1

! W -> 4W; two internal W2 currents W->1,2 and W->3,4; colour structure checked 12/05
function W5_2(i5,p1,p2,p3,p4) 
  integer :: i5, ix1, ix2, j
  complex(dp), intent(in) :: p1(3,dimP), p2(3,dimP), p3(3,dimP), p4(3,dimP)
  complex(dp) ::  W5_2(dimP), partx1(3,dimP), partx2(3,dimP)
  do j = 1,dimP
  W5_2 = czero
  end do
 
  ! internal W->1,2
  do ix1 = 1,3
   partx1(1,:) = - p1(1,:) - p2(1,:)
   partx1(2,:) = WP(partx1(1,:),W3(ix1,p1,p2))
   partx1(3,1) = cone*ix1
   do j = 2,dimP
   partx1(3,j) = czero
   end do
   partx1(1,:) = - partx1(1,:)
  
   do ix2 = 1,3
    partx2(1,:) = - p3(1,:) - p4(1,:)
    partx2(2,:) = WP(partx2(1,:),W3(ix2,p3,p4))
    partx2(3,1) = cone*ix2
    do j = 2,dimP
    partx2(3,j) = czero
    end do
    partx2(1,:) = - partx2(1,:)
    
    W5_2 = W5_2 + W3(i5,partx1,partx2)
   end do
  end do
  
end function W5_2

! W -> 4W; internal W->W1,W2, H->W3,W4 currents; colour structure checked 12/05
function W5_3(i5,p1,p2,p3,p4) 
  integer :: i5,ix1, j
  complex(dp), intent(in) :: p1(3,dimP), p2(3,dimP), p3(3,dimP), p4(3,dimP)
  complex(dp) ::  W5_3(dimP), partx1(3,dimP), partx2(3,dimP)
do j = 1,dimP
  W5_3 = czero
  partx2(2,j) = czero
  partx2(3,j) = czero
end do

  ! internal W->1,2
do ix1 = 1,3
   partx1(1,:) = - p1(1,:) - p2(1,:)
   partx1(2,:) = WP(partx1(1,:),W3(ix1,p1,p2))
   partx1(3,1) = ix1*cone
   do j = 2,dimP
   partx1(3,j) = czero
   end do
   partx1(1,:) = - partx1(1,:)
   
   partx2(1,:) = - p3(1,:) - p4(1,:)

   W5_3 = W5_3 + W2H(i5, partx1) * HP(partx2(1,:)) * HW2(p3, p4)
end do
  
end function W5_3

! W -> 4W; W1, W2 emitted directly, internal W->W3,W4; colour structure checked 12/05
function W5_4(i5,p1,p2,p3,p4) 
  integer :: i5,ix, j
  complex(dp), intent(in) :: p1(3,dimP), p2(3,dimP), p3(3,dimP), p4(3,dimP)
  complex(dp) ::  W5_4(dimP), partx(3,dimP)
  do j = 1,dimP
  W5_4(j) = czero
  end do
 
  ! internal W->3,4
  do ix = 1,3
   partx(1,:) = - p3(1,:) - p4(1,:)
   partx(2,:) = WP(partx(1,:),W3(ix,p3,p4))
   partx(1,:) = - partx(1,:)

   W5_4 = W5_4 + V_w4(i5, p1(2,:),int(p1(3,1)), p2(2,:),int(p2(3,1)), partx(2,:),ix)
   end do
  
end function W5_4

! W -> 4W, all combinations; colour structure checked 12/04
function W5(i5,p1,p2,p3,p4)
  integer :: i5
  complex(dp), intent(in) :: p1(3,dimP),p2(3,dimP),p3(3,dimP),p4(3,dimP)
  complex(dp) ::  W5(dimP)
  W5 = W5_1(i5,p1,p2,p3,p4) + W5_1(i5,p2,p1,p3,p4) + W5_1(i5,p3,p1,p2,p4) + W5_1(i5,p4,p1,p2,p3)
  W5 = W5 + W5_2(i5,p1,p2,p3,p4) + W5_2(i5,p1,p3,p2,p4) + W5_2(i5,p1,p4,p2,p3)
  W5 = W5 + W5_3(i5,p1,p2,p3,p4) + W5_3(i5,p1,p3,p2,p4) + W5_3(i5,p1,p4,p2,p3)
  W5 = W5 + W5_3(i5,p2,p3,p1,p4) + W5_3(i5,p2,p4,p1,p3) + W5_3(i5,p3,p4,p1,p2)
  W5 = W5 + W5_4(i5,p1,p2,p3,p4) + W5_4(i5,p1,p3,p2,p4) + W5_4(i5,p1,p4,p2,p3)
  W5 = W5 + W5_4(i5,p2,p3,p1,p4) + W5_4(i5,p2,p4,p1,p3) + W5_4(i5,p3,p4,p1,p2)
end function W5

! last is higgs, internal W->2,3,4
function W4H_1(i5,p1,p2,p3,p4)
  integer :: i5, ix, j
  complex(dp), intent(in) :: p1(3,dimP), p2(3,dimP), p3(3,dimP), p4(3,dimP)
  complex(dp) ::  W4H_1(dimP), partx(3,dimP)
  do j = 1,dimP
  W4H_1(j) = czero
  partx(3,j) = czero
  end do
 
  !print *, '----------------'
  ! internal W
  do ix = 1,3
  partx(1,:) = - p2(1,:) - p3(1,:) - p4(1,:)
  partx(2,:) = WP(partx(1,:),W3H(ix,p2,p3,p4))
  partx(3,1) = cone*ix
  
  partx(1,:) = - partx(1,:)
  W4H_1 = W4H_1 + W3(i5,partx,p1)
  end do
end function W4H_1

! last is higgs, internal H->2,3,4
function W4H_1b(i5,p1,p2,p3,p4)
  integer :: i5, ix, j
  complex(dp), intent(in) :: p1(3,dimP), p2(3,dimP), p3(3,dimP), p4(3,dimP)
  complex(dp) ::  W4H_1b(dimP), partx(3,dimP)

  do j = 1,dimP
  W4H_1b(j) = czero
  partx(3,j) = czero
  partx(2,j) = czero
  end do
  partx(1,:) = - p2(1,:) - p3(1,:) - p4(1,:)
  W4H_1b = W4H_1b + p1(2,:) *  V_hw2(i5, int(p1(3,1))) * HP(partx(1,:)) * H2W2(p2,p3,p4)

end function W4H_1b

function W4H_2(i5,p1,p2,p3)
  integer :: i5, ix, j
  complex(dp), intent(in) :: p1(3,dimP), p2(3,dimP), p3(3,dimP)
  complex(dp) ::  W4H_2(dimP), partx(3,dimP)
  do j = 1,dimP
  W4H_2(j) = czero
  partx(3,j) = czero
  end do
 
  ! internal W
  do ix = 1,3
  partx(1,:) = - p1(1,:) - p2(1,:) - p3(1,:)
  partx(2,:) = WP(partx(1,:),W4(ix,p1,p2,p3))
  partx(3,1) = cone*ix
  
  partx(1,:) = - partx(1,:)
  W4H_2 = W4H_2 + W2H(i5,partx)
  end do
end function W4H_2

!last is higgs
function W4H_3(i5,p1,p2,p3,p4) 
  integer :: i5,ix, j
  complex(dp), intent(in) :: p1(3,dimP), p2(3,dimP), p3(3,dimP), p4(3,dimP)
  complex(dp) ::  W4H_3(dimP), partx(3,dimP)
  do j = 1,dimP
  W4H_3(j) = czero
  partx(3,j) = czero
  end do
 
  ! internal W->3,4
  do ix = 1,3
   partx(1,:) = - p3(1,:) - p4(1,:)
   partx(2,:) = WP(partx(1,:),W2H(ix,p3))
   partx(3,1) = ix*cone
   partx(1,:) = - partx(1,:)

   W4H_3 = W4H_3 + V_w4(i5, p1(2,:),int(p1(3,1)), p2(2,:),int(p2(3,1)), partx(2,:),ix)
   !print *, 'four vertex each : ', V_w4(i5, p1(2,:),int(p1(3,1)), p2(2,:),int(p2(3,1)), partx(2,:),ix)
  end do
  
end function W4H_3

function W4H_4(i5,p1,p2,p3) 
  integer :: i5
  complex(dp), intent(in) :: p1(3,dimP), p2(3,dimP), p3(3,dimP)
  complex(dp) ::  W4H_4(dimP), px(dimP)
   px(:) = - p2(1,:) - p3(1,:)
   W4H_4 = p1(2,:) * V_h2w2(i5, int(p1(3,1))) * HP(px) * HW2(p2, p3)
end function W4H_4

! last is higgs
! internal W->1,2; W->3,4
function W4H_5(i5,p1,p2,p3,p4) 
  integer :: i5, ix1, ix2, j
  complex(dp), intent(in) :: p1(3,dimP), p2(3,dimP), p3(3,dimP), p4(3,dimP)
  complex(dp) ::  W4H_5(dimP), partx1(3,dimP), partx2(3,dimP)
  do j = 1,dimP
    W4H_5 = czero
    partx1(3,j) = czero
    partx2(3,j) = czero
  end do
 
  ! internal W->1,2
  do ix1 = 1,3
   partx1(1,:) = - p1(1,:) - p2(1,:)
   partx1(2,:) = WP(partx1(1,:),W3(ix1,p1,p2))
   partx1(3,1) = cone*ix1
   partx1(1,:) = - partx1(1,:)
  
  ix2 = p3(3,1)
  partx2(1,:) = - p3(1,:) - p4(1,:)
  partx2(2,:) = WP(partx2(1,:),W2H(ix2,p3))
  partx2(3,1) = cone*ix2
  partx2(1,:) = - partx2(1,:)
    
  W4H_5 = W4H_5 + W3(i5,partx1,partx2)
  end do
  
end function W4H_5

! last is higgs
! internal W -> 3,4; internal H -> 1,2
function W4H_6(i5,p1,p2,p3,p4) 
  integer :: i5,ix1, j
  complex(dp), intent(in) :: p1(3,dimP), p2(3,dimP), p3(3,dimP), p4(3,dimP)
  complex(dp) ::  W4H_6(dimP), partx1(3,dimP), partx2(3,dimP)
do j = 1,dimP
  W4H_6 = czero
  partx2(2,j) = czero
  partx2(3,j) = czero
end do

  ! internal W->3,4
do ix1 = 1,3
   partx1(1,:) = - p3(1,:) - p4(1,:)
   partx1(2,:) = WP(partx1(1,:),W2H(ix1,p3))
   partx1(3,1) = ix1*cone
   do j = 2,dimP
   partx1(3,j) = czero
end do
   partx1(1,:) = - partx1(1,:)
   
   partx2(1,:) = - p1(1,:) - p2(1,:)

   W4H_6 = W4H_6 + W2H(i5, partx1) * HP(partx2(1,:)) * HW2(p1, p2)
end do
  
end function W4H_6

! last is Higgs
function W4H(i5,p1,p2,p3,p4)
  integer :: i5
  complex(dp), intent(in) :: p1(3,dimP),p2(3,dimP),p3(3,dimP),p4(3,dimP)
  complex(dp) ::  W4H(dimP)
  W4H(:) = czero

  W4H = W4H_1(i5,p1,p2,p3,p4) + W4H_1(i5,p2,p1,p3,p4) + W4H_1(i5,p3,p1,p2,p4)
  W4H = W4H + W4H_1b(i5,p1,p2,p3,p4) + W4H_1b(i5,p2,p1,p3,p4) + W4H_1b(i5,p3,p1,p2,p4)
  W4H = W4H + W4H_2(i5,p1,p2,p3)
  W4H = W4H + W4H_3(i5,p1,p2,p3,p4) + W4H_3(i5,p1,p3,p2,p4) + W4H_3(i5,p2,p3,p1,p4)
  W4H = W4H + W4H_4(i5,p1,p2,p3) + W4H_4(i5,p2,p1,p3) + W4H_4(i5,p3,p2,p1)
  W4H = W4H + W4H_5(i5,p1,p2,p3,p4) + W4H_5(i5,p1,p3,p2,p4) + W4H_5(i5,p2,p3,p1,p4)
  W4H = W4H + W4H_6(i5,p1,p2,p3,p4) + W4H_6(i5,p1,p3,p2,p4) + W4H_6(i5,p2,p3,p1,p4)
end function W4H

function HW4(p1,p2,p3,p4)
  integer :: j
  complex(dp), intent(in) :: p1(3,dimP),p2(3,dimP),p3(3,dimP),p4(3,dimP)
  complex(dp) ::  HW4, partH(3,dimP)
  partH(1,:) = - p1(1,:) - p2(1,:) - p3(1,:) - p4(1,:)
  do j = 1, dimP
  partH(2,j) = czero
  partH(3,j) = czero
  end do
  
  HW4 = sc( W4H(int(p1(3,1)), p2, p3, p4, partH), p1(2,:) )
end function HW4

!last are higgs
function W3H2_1(i5,p1,p2,p3,p4)
  integer :: i5, ix, j
  complex(dp), intent(in) :: p1(3,dimP), p2(3,dimP), p3(3,dimP), p4(3,dimP)
  complex(dp) ::  W3H2_1(dimP), partx(3,dimP)
  do j = 1,dimP
  W3H2_1(j) = czero
  partx(3,j) = czero
  end do
 
  ! internal W
  do ix = 1,3
  partx(1,:) = - p2(1,:) - p3(1,:) - p4(1,:)
  partx(2,:) = WP(partx(1,:),W2H2(ix,p2,p3,p4))
  partx(3,1) = cone*ix
  
  partx(1,:) = - partx(1,:)
  W3H2_1 = W3H2_1 + W3(i5,partx,p1)
  end do
end function W3H2_1

!last is higgs
function W3H2_2(i5,p1,p2,p3)
  integer :: i5, ix, j
  complex(dp), intent(in) :: p1(3,dimP), p2(3,dimP), p3(3,dimP)
  complex(dp) ::  W3H2_2(dimP), partx(3,dimP)
  do j = 1,dimP
  W3H2_2(j) = czero
  partx(3,j) = czero
  end do
 
  ! internal W
  do ix = 1,3
  partx(1,:) = - p1(1,:) - p2(1,:) - p3(1,:)
  partx(2,:) = WP(partx(1,:),W3H(ix,p1,p2,p3))
  partx(3,1) = cone*ix
  
  partx(1,:) = - partx(1,:)
  W3H2_2 = W3H2_2 + W2H(i5,partx)
  end do
end function W3H2_2

!last are higgs
function W3H2_3(i5,p1,p2,p3,p4) 
  integer :: i5,ix1, ix2, j
  complex(dp), intent(in) :: p1(3,dimP), p2(3,dimP), p3(3,dimP), p4(3,dimP)
  complex(dp) ::  W3H2_3(dimP), partx1(3,dimP), partx2(3,dimP)
  do j = 1,dimP
  W3H2_3(j) = czero
  partx1(3,j) = czero
  partx2(3,j) = czero
  end do
 
  ! internal W->1,3
  do ix1 = 1,3
   partx1(1,:) = - p1(1,:) - p3(1,:)
   partx1(2,:) = WP(partx1(1,:),W2H(ix1,p1))
   partx1(3,1) = cone*ix1
   
   partx1(1,:) = - partx1(1,:)
  
   do ix2 = 1,3
    partx2(1,:) = - p2(1,:) - p4(1,:)
    partx2(2,:) = WP(partx2(1,:),W2H(ix2,p2))
    partx2(3,1) = cone*ix2
    
    partx2(1,:) = - partx2(1,:)
    
    W3H2_3 = W3H2_3 + W3(i5,partx1,partx2)
   end do
  end do
  
end function W3H2_3

function W3H2_4(i5,p1,p2) 
  integer :: i5,ix, j
  complex(dp), intent(in) :: p1(3,dimP), p2(3,dimP)
  complex(dp) ::  W3H2_4(dimP), partx(3,dimP)
  do j = 1,dimP
  W3H2_4(j) = czero
  partx(3,j) = czero
  end do
 
  ! internal W->1,2
  do ix = 1,3
   partx(1,:) = - p1(1,:) - p2(1,:)
   partx(2,:) = WP(partx(1,:),W3(ix,p1,p2))
   partx(3,1) = cone*ix
   
   partx(1,:) = - partx(1,:)

   W3H2_4 = W3H2_4 + V_h2w2(i5,ix) * partx(2,:)
  end do
end function W3H2_4

!last are higgs
function W3H2(i5,p1,p2,p3,p4)
  integer :: i5
  complex(dp), intent(in) :: p1(3,dimP), p2(3,dimP), p3(3,dimP), p4(3,dimP)
  complex(dp) ::  W3H2(dimP)
  W3H2 = W3H2_1(i5,p1,p2,p3,p4) + W3H2_1(i5,p2,p1,p3,p4) + W3H2_2(i5,p1,p2,p3) + W3H2_2(i5,p1,p2,p4)
  W3H2 = W3H2 + W3H2_3(i5,p1,p2,p3,p4) + W3H2_3(i5,p1,p2,p4,p3) + W3H2_4(i5,p1,p2)
end function W3H2

!last is higgs
function H2W3(p1,p2,p3,p4)
integer :: j
  complex(dp), intent(in) :: p1(3,dimP),p2(3,dimP),p3(3,dimP),p4(3,dimP)
  complex(dp) ::  H2W3, partH(3,dimP)
  partH(1,:) = - p1(1,:) - p2(1,:) - p3(1,:) - p4(1,:)
  do j = 1,dimP
  partH(2,j) = czero
  partH(3,j) = czero
  end do
  
  H2W3 = sc( W3H2(int(p1(3,1)), p2, p3, p4, partH), p1(2,:) )
end function H2W3

! internal H -> H2, H3, H4
function W2H3_1(i5,p1,p2,p3,p4) 
  integer :: i5
  complex(dp), intent(in) :: p1(3,dimP), p2(3,dimP), p3(3,dimP), p4(3,dimP)
  complex(dp) ::  W2H3_1(dimP), px(dimP)
  px(:) = - p2(1,:) - p3(1,:) - p4(1,:)
  W2H3_1 = p1(2,:) *  V_hw2(i5, int(p1(3,1))) * HP(px(:)) *H4(p2,p3,p4)
end function W2H3_1

! internal H -> H2, H3
function W2H3_2(i5,p1,p2,p3) 
  integer :: i5
  complex(dp), intent(in) :: p1(3,dimP), p2(3,dimP), p3(3,dimP)
  complex(dp) ::  W2H3_2(dimP), px(dimP)
  px(:) = - p2(1,:) - p3(1,:)
  W2H3_2 = p1(2,:) *  V_h2w2(i5, int(p1(3,1))) * HP(px(:)) * H3()
end function W2H3_2

! internal W -> W1, H2, H3
function W2H3_3(i5,p1,p2,p3)
  integer :: i5, ix, j
  complex(dp), intent(in) :: p1(3,dimP), p2(3,dimP), p3(3,dimP)
  complex(dp) ::  W2H3_3(dimP), partx(3,dimP)

do j = 1,dimP
  W2H3_3(j) = czero
  partx(3,j) = czero
end do
   
! internal W->1,2,3
ix = i5
partx(1,:) = - p1(1,:) - p2(1,:) - p3(1,:)
partx(2,:) = WP(partx(1,:), W2H2(ix,p1,p2,p3))
partx(3,1) = cone*ix
     
W2H3_3 = V_hw2(i5,ix) * partx(2,:)

end function W2H3_3

function W2H3_4(i5,p1,p2)
  integer :: i5, ix
  complex(dp), intent(in) :: p1(3,dimP), p2(3,dimP)
  complex(dp) ::  W2H3_4(dimP), partx(3,dimP)
  W2H3_4(:) = czero
   
! internal W->1,2
do ix = 1,3
    partx(1,:) = - p1(1,:) - p2(1,:)
    partx(2,:) = WP(partx(1,:), W2H(ix,p1))
    ! print *, 'difference plus minus?'
    ! print *, WP(partx(1,:), W2H(ix,p1))
    ! print *, 'minus: ', WP(-partx(1,:), W2H(ix,p1))
  
    W2H3_4 = W2H3_4 + V_h2w2(i5,ix) * partx(2,:)
end do

end function W2H3_4

! 3-vertex, internal W-> W1, H2, internal H -> H3, H4
function W2H3_5(i5,p1,p2,p3,p4)
  integer :: i5
  complex(dp), intent(in) :: p1(3,dimP), p2(3,dimP), p3(3,dimP), p4(3,dimP)
  complex(dp) ::  W2H3_5(dimP), partx1(3,dimP), partx2(3,dimP)
  W2H3_5(:) = czero
  partx1(3,:) = czero
   
! internal W->W1,H2
partx1(1,:) = - p1(1,:) - p2(1,:)
partx1(2,:) = WP(partx1(1,:), W2H(i5,p1))
partx1(3,1) = cone*i5
! print *, 'prop momentum = ', partx1(1,:)
! print *, 'prop current = ', W2H(i5,p1)
! print *, 'propagated = ', partx1(2,:)
     
partx2(1,:) = - p3(1,:) - p4(1,:)
W2H3_5 = V_hw2(i5,i5) * partx1(2,:) * HP(partx2(1,:)) * V_h3()

end function W2H3_5

!last are higgs
function W2H3(i5,p1,p2,p3,p4)
  integer :: i5
  complex(dp), intent(in) :: p1(3,dimP), p2(3,dimP), p3(3,dimP), p4(3,dimP)
  complex(dp) ::  W2H3(dimP)
  logical :: WSEcalc

  W2H3 = czero
  WSECalc = .false.
  
  if (WSEcalc) then
    !print *, '3_3:', W2H3_3(i5,p1,p3,p4)
    W2H3 = W2H3 + W2H3_3(i5,p1,p3,p4)
  else 
  W2H3 = W2H3_1(i5,p1,p2,p3,p4) + W2H3_2(i5,p1,p2,p3) + W2H3_2(i5,p1,p2,p4) + W2H3_2(i5,p1,p3,p4)
  W2H3 = W2H3 + W2H3_3(i5,p1,p2,p3) + W2H3_3(i5,p1,p2,p4) + W2H3_3(i5,p1,p3,p4)
  W2H3 = W2H3 + W2H3_4(i5,p1,p2) + W2H3_4(i5,p1,p3) + W2H3_4(i5,p1,p4)
  W2H3 = W2H3 + W2H3_5(i5,p1,p2,p3,p4) + W2H3_5(i5,p1,p3,p2,p4) + W2H3_5(i5,p1,p4,p2,p3)

  ! print *, 'contributions : '
  ! print *,  '1:', W2H3_1(i5,p1,p2,p3,p4)
  ! print *, '-------'
  ! print *, '2_1:', W2H3_2(i5,p1,p2,p3)
  ! print *, '2_2:', W2H3_2(i5,p1,p2,p4)
  ! print *, '2_3:', W2H3_2(i5,p1,p3,p4)
  ! print *, 'momentum int higgs = ',  - p4(1,:) - p3(1,:)
  ! print *, '-------'
  ! print *, '3_1:', W2H3_3(i5,p1,p2,p3)
  ! print *, '3_2:', W2H3_3(i5,p1,p2,p4)
  ! print *, '3_3:', W2H3_3(i5,p1,p3,p4)
  ! print *, '-------'
  ! print *, '4_1:', W2H3_4(i5,p1,p2)
  ! print *, '4_2:', W2H3_4(i5,p1,p3)
  ! print *, '4_3:', W2H3_4(i5,p1,p4)
  ! print *, '-------'
  ! print *, '5_1:', W2H3_5(i5,p1,p2,p3,p4)
  ! print *, '5_2:', W2H3_5(i5,p1,p3,p2,p4)
  ! print *, '5_3:', W2H3_5(i5,p1,p4,p2,p3)

  ! W2H3 = W2H3_2(i5,p1,p2,p3) + W2H3_2(i5,p1,p2,p4) + W2H3_2(i5,p1,p3,p4)
  ! W2H3 = W2H3_4(i5,p1,p2) + W2H3_4(i5,p1,p3) + W2H3_4(i5,p1,p4)
  !W2H3 = W2H3_5(i5,p1,p3,p2,p4) + W2H3_3(i5,p1,p2,p4)
  !print *, 'sum = ', W2H3
  end if
end function W2H3

! checked 03/08/18 for internal higgs only
!last two are higgs, p1, p2 relates to w
function H3W2(p1,p2,p3,p4)
  complex(dp), intent(in) :: p1(3,dimP),p2(3,dimP),p3(3,dimP),p4(3,dimP)
  complex(dp) ::  H3W2, partH(3,dimP)
partH(1,:) = - p1(1,:) - p2(1,:) - p3(1,:) - p4(1,:)
partH(2,:) = czero
partH(3,:) = czero

H3W2 = sc( W2H3(int(p1(3,1)), p2, p3, p4, partH), p1(2,:) )
! print *, 'W2H3 = ', W2H3(int(p1(3,1)), p2, p3, p4, partH)
!print *, 'pol vector = ', p1(2,:)
!print *, 'scalar prduct = ', H3W2
end function H3W2

function H5_1(p2, p3, p4)
  complex(dp), intent(in) :: p2(3,dimP), p3(3,dimP), p4(3,dimP)
  complex(dp) ::  H5_1
  H5_1 = H3() * HP( p2(1,:) + p3(1,:) + p4(1,:) ) * H4(p2, p3, p4)
end function H5_1

function H5_2(p1, p2, p3, p4)
  complex(dp), intent(in) :: p1(3,dimP), p2(3,dimP), p3(3,dimP), p4(3,dimP)
  complex(dp) ::  H5_2
  H5_2 = H3() * HP( p1(1,:) + p2(1,:) ) * H3() * HP( p3(1,:) + p4(1,:) ) * H3()
end function H5_2

function H5_3(p3, p4)
  complex(dp), intent(in) :: p3(3,dimP), p4(3,dimP)
  complex(dp) ::  H5_3
  H5_3 = V_h4() * HP(p3(1,:) + p4(1,:) ) * H3()
end function H5_3

function H5(p1,p2,p3,p4)
  complex(dp), intent(in) :: p1(3,dimP), p2(3,dimP), p3(3,dimP), p4(3,dimP)
  complex(dp) :: H5
  H5 = H5_1(p2, p3, p4) + H5_1(p1, p3, p4) + H5_1(p1, p2, p4) + H5_1(p1, p2, p3) + &
         H5_2(p1, p2, p3, p4) + H5_2(p1, p3, p2, p4) + H5_2(p1, p4, p2, p3) + &
           H5_3(p3, p4) + H5_3(p2, p4) + H5_3(p2, p3) + H5_3(p1, p4) + H5_3(p1, p3) + H5_3(p1, p2)
end function H5

!----------------------------------------------------------------------------------------------
! 6particle CURRENTS
!----------------------------------------------------------------------------------------------

!W1 emitted directly
function W6_1(i6,p1,p2,p3,p4,p5)
  integer :: i6,ix
  complex(dp), intent(in) :: p1(3,dimP), p2(3,dimP), p3(3,dimP), p4(3,dimP), p5(3,dimP)
  complex(dp) ::  W6_1(dimP), partx(3,dimP)
  W6_1(:) = czero
  partx(3,:) = czero
 
  ! internal W
  do ix = 1,3
  partx(1,:) = - p2(1,:) - p3(1,:) - p4(1,:) - p5(1,:)
  partx(2,:) = WP(partx(1,:),W5(ix,p2,p3,p4,p5))
  partx(3,1) = cone*ix
  
  partx(1,:) = - partx(1,:)
  W6_1 = W6_1 + W3(i6,p1,partx)
  end do
  
  ! internal H, checked 12/04
  W6_1 = W6_1 + p1(2,:) *  V_hw2(i6, int(p1(3,1))) * HP(partx(1,:)) * HW4(p2,p3,p4,p5)
end function W6_1

!W1,2 together
function W6_2(i6,p1,p2,p3,p4,p5)
  integer :: i6,ix1,ix2
  complex(dp), intent(in) :: p1(3,dimP), p2(3,dimP), p3(3,dimP), p4(3,dimP), p5(3,dimP)
  complex(dp) ::  W6_2(dimP), partx1(3,dimP), partx2(3,dimP)
  W6_2(:) = czero
  partx1(3,:) = czero
  partx2(3,:) = czero

  ! internal W->2W
  do ix1 = 1,3
  partx1(1,:) = - p1(1,:) - p2(1,:)
  partx1(2,:) = WP(partx1(1,:),W3(ix1,p1,p2))
  partx1(3,1) = cone*ix1
  
  partx1(1,:) = - partx1(1,:)
  
   do ix2 = 1,3
   partx2(1,:) = - p3(1,:) - p4(1,:) - p5(1,:)
   partx2(2,:) = WP(partx2(1,:),W4(ix2,p3,p4,p5))
   partx2(3,:) = cone*ix1
   
   partx2(1,:) = -partx2(1,:)
   
   W6_2 = W6_2 + W3(i6,partx1,partx2)
   end do

  ! internal H
  W6_2 = W6_2 + partx1(2,:) *  V_hw2(i6, int(partx1(3,1))) * HP(partx1(1,:)) *HW3(p3,p4,p5)
  end do
  
  !internal W->3W(3,4,5)
  do ix1 = 1,3
  partx1(1,:) = - p3(1,:) - p4(1,:) - p5(1,:)
  partx1(2,:) = WP(partx1(1,:),W4(ix1,p3,p4,p5))
  partx1(3,1) = cone*ix1
  
  partx1(1,:) = - partx1(1,:)
  partx2(1,:) = p1(1,:) + p2(1,:)

  W6_2 = W6_2 + partx1(2,:) *  V_hw2(i6, int(partx1(3,1))) * HP(partx2(1,:)) *HW2(p1,p2)
  end do
end function W6_2

!4vertex
function W6_3(i6,p1,p2,p3,p4,p5)
  integer :: i6,ix1, ix2
  complex(dp), intent(in) :: p1(3,dimP), p2(3,dimP), p3(3,dimP), p4(3,dimP), p5(3,dimP)
  complex(dp) ::  W6_3(dimP), partx1(3,dimP), partx2(3,dimP)
  W6_3(:) = czero
  partx1(3,:) = czero
  partx2(3,:) = czero
 
  ! internal W->2,3 and W->4,5
  do ix1 = 1,3
   partx1(1,:) = - p2(1,:) - p3(1,:)
   partx1(2,:) = WP(partx1(1,:),W3(ix1,p2,p3))
   partx1(3,1) = cone*ix1
   partx1(1,:) = - partx1(1,:)
    do ix2 = 1,3
    partx2(1,:) = - p4(1,:) - p5(1,:)
    partx2(2,:) = WP(partx2(1,:),W3(ix2,p4,p5))
    partx2(3,1) = cone*ix2
    partx2(1,:) = - partx2(1,:)

    W6_3 = W6_3 + W4_2(i6,p1,partx1,partx2)
    end do
   end do
   
  ! internal H->2,3 and H->4,5
  partx1(1,:) = p2(1,:) + p3(1,:)
  partx2(1,:) = p4(1,:) + p5(1,:)
  W6_3 = W6_3 + p1(2,:) *  V_h2w2(i6,int(p1(3,1))) * HP(partx1(1,:)) *HW2(p2,p3) * HP(partx2(1,:)) * HW2(p4,p5)
end function W6_3

! W -> 5W, all combinations
function W6(i6,p1,p2,p3,p4,p5)
  integer :: i6
  complex(dp), intent(in) :: p1(3,dimP),p2(3,dimP),p3(3,dimP),p4(3,dimP),p5(3,dimP)
  complex(dp) ::  W6(dimP)
W6 = W6_1(i6,p1,p2,p3,p4,p5) + W6_1(i6,p2,p1,p3,p4,p5) + W6_1(i6,p3,p1,p2,p4,p5) + W6_1(i6,p4,p1,p2,p3,p5) + W6_1(i6,p5,p1,p2,p3,p4)
W6 = W6+W6_2(i6,p1,p2,p3,p4,p5)+W6_2(i6,p1,p3,p2,p4,p5)+W6_2(i6,p1,p4,p2,p3,p5)+W6_2(i6,p1,p5,p2,p3,p4)+W6_2(i6,p2,p3,p1,p4,p5)
W6 = W6+W6_2(i6,p2,p4,p1,p3,p5)+W6_2(i6,p2,p5,p1,p3,p4)+W6_2(i6,p3,p4,p1,p2,p5)+W6_2(i6,p3,p5,p1,p2,p4)+W6_2(i6,p4,p5,p1,p2,p3)
W6 = W6+W6_3(i6,p1,p2,p3,p4,p5)+W6_3(i6,p1,p2,p4,p3,p5)+W6_3(i6,p1,p2,p5,p3,p4)+W6_3(i6,p2,p1,p3,p4,p5)+W6_3(i6,p2,p1,p4,p3,p5)
W6 = W6+W6_3(i6,p2,p1,p5,p3,p4)+W6_3(i6,p3,p1,p2,p4,p5)+W6_3(i6,p3,p1,p4,p2,p5)+W6_3(i6,p3,p1,p5,p2,p4)+W6_3(i6,p4,p1,p2,p3,p5)
W6 = W6+W6_3(i6,p4,p1,p3,p2,p5)+W6_3(i6,p4,p1,p5,p2,p3)+W6_3(i6,p5,p1,p2,p3,p4)+W6_3(i6,p5,p1,p3,p2,p4)+W6_3(i6,p5,p1,p4,p2,p3)
end function W6

end module