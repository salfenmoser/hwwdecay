use mod_auxfunctions
use mod_consts_dp
use mod_kinematics


test_suite mod_amplitudes

  real(dp) :: ECM = 250
  complex(dp) :: P5(5,4)
  complex(dp) :: ePol1in(3,4), ePol2in(3,4), ePol3in(3,4), ePol4in(3,4), ePol5in(3,4)
  complex(dp) :: pa1(3,dimP), pa2(3,dimP), pa3(3,dimP), pa4(3,dimP), pa5(3,dimP)
  integer :: pol1, pol2, pol3, pol4, pol5, c1, c2, c3, c4, c5
  

setup

call kinematicsN(ECM, 2, P5)

call spinonepol(P5(1,:), ePol1in)
call spinonepol(P5(2,:), ePol2in)
call spinonepol(P5(3,:), ePol3in)
call spinonepol(P5(4,:), ePol4in)
call spinonepol(P5(4,:), ePol5in)

pa1(1,1:4) = P5(1,:)
pa2(1,1:4) = P5(2,:)
pa3(1,1:4) = P5(3,:)
pa4(1,1:4) = P5(4,:)
pa5(1,1:4) = P5(5,:)

end setup

teardown
  ! This code runs immediately after each test
end teardown

!test funit_assertions
!  integer, dimension(2) :: a = (/ 1, 2 /)
!  integer, dimension(2) :: b = (/ 1, 2 /)

!  assert_array_equal(a,b)
!  assert_real_equal(0.9999999e0, 1.0e0)
!  assert_equal_within(1e-7, 0.0, 1e-6)
!  assert_equal(1, 5 - 4)
!  assert_false(5 < 4)
!  assert_true(4 == 4)
!end test

test W3CurrentDoubleColorVanishes
  pol1 = 1
  pol2 = 1
  pol3 = 1
  pol4 = 1
  pol5 = 1

  c1 = 1
  c2 = 1
  c3 = 1
  c4 = 2
  c5 = 2

pa1(2,1:4) = ePol1in(pol1,:)
pa2(2,1:4) = ePol2in(pol2,:)
pa3(2,1:4) = ePol3in(pol3,:)
pa4(2,1:4) = ePol4in(pol4,:)
pa5(2,1:4) = ePol5in(pol5,:)

pa1(3,1) = c1
pa2(3,1) = c2
pa3(3,1) = c3
pa4(3,1) = c4
pa5(3,1) = c5

assert_equal_within(abs(J3(pa1, pa2, pa3)), zero, 1e-10)
assert_equal_within(abs(J3(pa1, pa2, pa4)), zero, 1e-10)

end test W3CurrentDoubleColorVanishes


test W4CurrentSingleColorVanishes
  pol1 = 1
  pol2 = 1
  pol3 = 1
  pol4 = 1
  pol5 = 1

  c1 = 1
  c2 = 1
  c3 = 1
  c4 = 2
  c5 = 2

pa1(2,1:4) = ePol1in(pol1,:)
pa2(2,1:4) = ePol2in(pol2,:)
pa3(2,1:4) = ePol3in(pol3,:)
pa4(2,1:4) = ePol4in(pol4,:)
pa5(2,1:4) = ePol5in(pol5,:)

pa1(3,1) = c1
pa2(3,1) = c2
pa3(3,1) = c3
pa4(3,1) = c4
pa5(3,1) = c5

assert_equal_within(abs(J4(pa1, pa2, pa3, pa4)), zero, 1e-10)

end test W3CurrentSingleColorVanishes


test W5CurrentFourEqualColorsVanishes
  pol1 = 1
  pol2 = 1
  pol3 = 1
  pol4 = 1
  pol5 = 1

  c1 = 1
  c2 = 1
  c3 = 1
  c4 = 1
  c5 = 2

pa1(2,1:4) = ePol1in(pol1,:)
pa2(2,1:4) = ePol2in(pol2,:)
pa3(2,1:4) = ePol3in(pol3,:)
pa4(2,1:4) = ePol4in(pol4,:)
pa5(2,1:4) = ePol5in(pol5,:)

pa1(3,1) = c1
pa2(3,1) = c2
pa3(3,1) = c3
pa4(3,1) = c4
pa5(3,1) = c5

assert_equal_within(abs(J5(pa1, pa2, pa3, pa4, pa5)), zero, 1e-10)

end test W5CurrentFourEqualColorsVanishes

end test_suite
