module mod_amplitudes

  use mod_types
  use mod_consts_dp
  use common_def
  
  use mod_auxfunctions
  use mod_kinematics
  use mod_vectorCurrents
  
implicit none
  
public :: J3, J4, J5

contains

! particle type is automatically identified, and scalar current is returned for 3, 4 or 5 particles.

function J3(part1,part2,part3)
  complex(dp), intent(in) :: part1(3,dimP), part2(3,dimP), part3(3,dimP)
  complex(dp) ::  J3
  logical :: is1W, is2W, is3W
J3 = czero
is1W = .true.
is2W = .true.
is3W = .true.
  
if (part1(3,1) == 0) then
    is1W = .false.
  end if
if (part2(3,1) == 0) then
    is2W = .false.
  end if
if (part3(3,1) == 0) then
    is3W = .false.
  end if 
  
if (is1W) then
  if (is2W) then
    if (is3W) then
      J3 = sc(W3(int(part3(3,1)),part1,part2), part3(2,:))
    else if (is3W .eqv. .false.) then
      J3 = HW2(part1,part2)
    end if
  else if (is2W .eqv. .false.) then
    if (is3W) then
      J3 = HW2(part1,part3)
    else if (is3W .eqv. .false.) then
      print *, 'theory does not contain HHW interaction'
    end if
  end if
    
else if (is1W .eqv. .false.) then
  if (is2W) then
    if (is3W) then
      J3 = HW2(part2,part3)
    else if (is3W .eqv. .false.) then
      print *, 'theory does not contain HHW interaction'
    end if
  else if (is2W .eqv. .false.) then
    if (is3W) then
      print *, 'theory does not contain HHW interaction'
    else if (is3W .eqv. .false.) then
      J3 = H3()
    end if
  end if
end if

end function J3

function J4(part1,part2,part3,part4)
  complex(dp), intent(in) :: part1(3,dimP), part2(3,dimP), part3(3,dimP), part4(3,dimP)
  complex(dp) ::  J4, W_4(dimP)
  logical :: is1W, is2W, is3W, is4W
J4 = czero
is1W = .true.
is2W = .true.
is3W = .true.
is4W = .true.
  
if (part1(3,1) == 0) then
    is1W = .false.
  end if
if (part2(3,1) == 0) then
    is2W = .false.
  end if
if (part3(3,1) == 0) then
    is3W = .false.
  end if
if (part4(3,1) == 0) then
    is4W = .false.
  end if

if (is1W) then
  if (is2W) then
  
    if (is3W) then
      if (is4W) then
      W_4 =  W4(int(part4(3,1)),part1,part2,part3)
      J4 = sc(W_4, part4(2,:))
      else if (is4W .eqv. .false.) then
        J4 = HW3(part1,part2,part3)
      end if
    else if (is3W .eqv. .false.) then
      if (is4W) then
        J4 = HW3(part1,part2,part4)
      else if (is4W .eqv. .false.) then
        !print *, 'right current'
        J4 = H2W2(part1,part2,part4)
      end if
    end if

  else if (is2W .eqv. .false.) then
    if (is3W) then
      if (is4W) then
        J4 = HW3(part1,part3,part4)
      else if (is4W .eqv. .false.) then
        J4 = H2W2(part1,part3,part4)
      end if
    else if (is3W .eqv. .false.) then
      if (is4W) then
        J4 = H2W2(part1,part4,part3)
      else if (is4W .eqv. .false.) then
        print *, 'theory does not contain HHHW interaction'
      end if
    end if
  end if
    
else if (is1W .eqv. .false.) then
  if (is2W) then
    if (is3W) then
      if (is4W) then
        J4 = HW3(part2,part3,part4)
      else if (is4W .eqv. .false.) then
        J4 = H2W2(part2,part3,part4)
      end if
    else if (is3W .eqv. .false.) then
      if (is4W) then
        J4 = H2W2(part2,part4,part3)
      else if (is4W .eqv. .false.) then
        print *, 'theory does not contain HHHW interaction'
      end if
    end if
    
  else if (is2W .eqv. .false.) then
    if (is3W) then
      if (is4W) then
        J4 = H2W2(part3,part4,part2)
      else if (is4W .eqv. .false.) then
        print *, 'theory does not contain HHHW interaction'
      end if
    else if (is3W .eqv. .false.) then
      if (is4W) then
        print *, 'theory does not contain HHHW interaction'
      else if (is4W .eqv. .false.) then
        J4 = H4(part1,part2,part3)
      end if
    end if
  end if
end if

end function J4

function J5(part1,part2,part3,part4,part5)
  complex(dp), intent(in) :: part1(3,dimP), part2(3,dimP), part3(3,dimP), part4(3,dimP), part5(3,dimP)
  complex(dp) ::  J5
  logical :: is1W, is2W, is3W, is4W, is5W
J5 = czero
is1W = .true.
is2W = .true.
is3W = .true.
is4W = .true.
is5W = .true.
  
if (part1(3,1) == 0) then
    is1W = .false.
  end if
if (part2(3,1) == 0) then
    is2W = .false.
  end if
if (part3(3,1) == 0) then
    is3W = .false.
  end if
if (part4(3,1) == 0) then
    is4W = .false.
  end if
if (part5(3,1) == 0) then
    is5W = .false.
  end if
  
if (is1W) then
  if (is2W) then
  
    if (is3W) then
      if (is4W) then
        if (is5W) then
          J5 = sc(W5(int(part5(3,1)),part1,part2,part3,part4), part5(2,:))
        else if (is5W .eqv. .false.) then
          J5 = HW4(part1,part2,part3,part4)
        end if
      else if (is4W .eqv. .false.) then
        if (is5W) then
          J5 = HW4(part1,part2,part3,part5)
        else if (is5W .eqv. .false.) then
          J5 = H2W3(part1,part2,part3,part4)
        end if
      end if
      
    else if (is3W .eqv. .false.) then
      if (is4W) then
        if (is5W) then
          J5 = HW4(part1,part2,part4,part5)
        else if (is5W .eqv. .false.) then
          J5 = H2W3(part1,part2,part4,part3)
        end if
      else if (is4W .eqv. .false.) then
        if (is5W) then
          J5 = H2W3(part1,part2,part5,part3)
        else if (is5W .eqv. .false.) then
          J5 = H3W2(part1,part2,part3,part4)
        end if
      end if
    end if

  else if (is2W .eqv. .false.) then
    if (is3W) then
      if (is4W) then
        if (is5W) then
          J5 = HW4(part1,part3,part4,part5)
        else if (is5W .eqv. .false.) then
          J5 = H2W3(part1,part3,part4,part2)
        end if
      else if (is4W .eqv. .false.) then
        if (is5W) then
          J5 = H2W3(part1,part3,part5,part2)
        else if (is5W .eqv. .false.) then
          J5 = H3W2(part1,part3,part2,part4)
        end if
      end if
    else if (is3W .eqv. .false.) then
      if (is4W) then
        if (is5W) then
          J5 = H2W3(part1,part4,part5,part2)
        else if (is5W .eqv. .false.) then
          J5 = H3W2(part1,part4,part2,part3)
        end if
      else if (is4W .eqv. .false.) then
        if (is5W) then
          J5 = H3W2(part1,part5,part2,part3)
        else if (is5W .eqv. .false.) then
          print *, 'theory does not contain H4-W interaction'
        end if
      end if
    end if
  end if
    
else if (is1W .eqv. .false.) then
  if (is2W) then
    if (is3W) then
      if (is4W) then
        if (is5W) then
          J5 = HW4(part2,part3,part4,part5)
        else if (is5W .eqv. .false.) then
          J5 = H2W3(part2,part3,part4,part1)
        end if
      else if (is4W .eqv. .false.) then
        if (is5W) then
          J5 = H2W3(part2,part3,part5,part1)
        else if (is5W .eqv. .false.) then
          J5 = H3W2(part2,part3,part1,part4)
        end if
      end if
    else if (is3W .eqv. .false.) then
      if (is4W) then
        if (is5W) then
          J5 = H2W3(part2,part4,part5,part1)
        else if (is5W .eqv. .false.) then
          J5 = H3W2(part2,part4,part1,part3)
        end if
      else if (is4W .eqv. .false.) then
        if (is5W) then
          J5 = H3W2(part2,part5,part1,part3)
        else if (is5W .eqv. .false.) then
          print *, 'theory does not contain H4-W interaction'
        end if
      end if
    end if
    
  else if (is2W .eqv. .false.) then
    if (is3W) then
      if (is4W) then
        if (is5W) then
          J5 = H2W3(part3,part4,part5,part1)
        else if (is5W .eqv. .false.) then
          J5 = H3W2(part3,part4,part1,part2)
        end if
      else if (is4W .eqv. .false.) then
        if (is5W) then
          J5 = H3W2(part3,part5,part1,part2)
        else if (is5W .eqv. .false.) then
          print *, 'theory does not contain H4-W interaction'
        end if
      end if
    else if (is3W .eqv. .false.) then
      if (is4W) then
        if (is5W) then
          J5 = H3W2(part4,part5,part1,part2)
        else if (is5W .eqv. .false.) then
          print *, 'theory does not contain H4-W interaction'
        end if
      else if (is4W .eqv. .false.) then
        if (is5W) then
          print *, 'theory does not contain H4-W interaction'
        else if (is5W .eqv. .false.) then
          J5 = H5(part1,part2,part3,part4)
        end if
      end if
    end if
  end if
end if

end function J5
  
end module 
