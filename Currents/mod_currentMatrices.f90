module mod_currentMatrices

  use mod_types
  use mod_consts_dp
  use common_def
  
  use mod_auxfunctions
  use mod_vertices
  use mod_vectorCurrents

  implicit none
  public :: W3_M, W4_M, W5_M, W6_M

contains

function W3_M(part1,part2)
  integer :: ix
  complex(dp), intent(in) :: part1(3,dimP), part2(3,dimP)
  complex(dp) ::  W3_M(dimP,3) 
  do ix = 1,3
  W3_M(:,ix) = W3(ix,part1,part2)
  end do
end function W3_M

function W4_M(part1,part2,part3)
  integer :: ix
  complex(dp), intent(in) :: part1(3,dimP), part2(3,dimP), part3(3,dimP)
  complex(dp) ::  W4_M(dimP,3) 
  do ix = 1,3
  W4_M(:,ix) = W4(ix,part1,part2,part3)
  end do
end function W4_M

function W5_M(part1,part2,part3,part4)
  integer :: ix
  complex(dp), intent(in) :: part1(3,dimP), part2(3,dimP), part3(3,dimP), part4(3,dimP)
  complex(dp) ::  W5_M(dimP,3) 
  do ix = 1,3
  W5_M(:,ix) = W5(ix,part1,part2,part3,part4)
  end do
end function W5_M

function W6_M(part1,part2,part3,part4,part5)
  integer :: ix
  complex(dp), intent(in) :: part1(3,dimP), part2(3,dimP), part3(3,dimP), part4(3,dimP), part5(3,dimP)
  complex(dp) ::  W6_M(dimP,3) 
  do ix = 1,3
  W6_M(:,ix) = W6(ix,part1,part2,part3,part4,part5)
  end do
end function W6_M
    
end module
