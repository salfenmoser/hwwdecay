module mod_propagators

  use mod_types
  use mod_consts_dp
  use common_def
  
  use mod_auxfunctions
  use mod_vertices

  implicit none

  public :: HP, WP
  public :: WPnum

contains

! higgs propagator
! set to zero if denominator smaller than limit value as defined in BasicDef/mod_const_dp.f90
function HP(p)
  complex(dp), intent(in) :: p(dimP)
  complex(dp) ::  HP
  !logical :: HWWOffShellW
  
HP = czero
!HWWOffShellW = .true.

  if ( abs(sc(p,p) - mhsq) > limitProp .and. abs(sc(p,p)) > limitProp) then
    HP = ci / (sc(p,p) - mhsq)
  !else if (abs(sc(p,p) - mhsq) > limitProp .and. HWWOffShellW) then
    !print *, 'calculating H prop with inverse = ', sq(p) - mhsq
    !HP = ci / (sq(p) - mhsq)
  end if
end function HP

! W propagator
! set to zero if denominator smaller than limit value as defined in BasicDef/mod_const_dp.f90
! matrix structure will be implemented as two scalar products
! J, p go in same direction
function WP(p,J)
  complex(dp), intent(in) :: p(dimP), J(dimP)
  complex(dp) ::  WP(dimP), massSq
WP(:) = czero
massSq = mwsq

if ( abs(sc(p,p) - mwsq) > limitProp .and. abs(sc(p,p)) > limitProp ) then
  !print *, 'calc W prop'
  if (abs(sc(p,p) - mwsq) < upperLimitProp) then
    massSq = sc(p,p)
    !print *, 'only for offshell continuation'
  end if
  !WP(:) = ( -ci / (sc(p,p) - massSq) ) * ( J(:) - p(:) * sc(p,J) / massSq )
  WP(:) = ( -ci / (sc(p,p) - mwsq) ) * ( J(:) - p(:) * sc(p,J) / massSq )
end if
end function WP


! W propagator
! set to zero if denominator smaller than limit value as defined in BasicDef/mod_const_dp.f90
! matrix structure will be implemented as two scalar products
! J, p go in same direction
function WPnum(p,J)
  complex(dp), intent(in) :: p(dimP), J(dimP)
  complex(dp) ::  WPnum(dimP), massSq
WPnum(:) = czero
massSq = mwsq

if ( abs(sc(p,p) - mwsq) > limitProp .and. abs(sc(p,p)) > limitProp ) then
  !print *, 'calc W prop'
  if (abs(sc(p,p) - mwsq) < upperLimitProp) then
    massSq = sc(p,p)
    !print *, 'only for offshell continuation'
    WPnum(:) = J(:) - p(:) * sc(p,J) / massSq
  end if
  WPnum(:) = J(:) - p(:) * sc(p,J) / massSq
end if
end function WPnum
    
end module
