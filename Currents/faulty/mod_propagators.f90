module mod_propagators

  use mod_types
  use mod_consts_dp
  use common_def
  
  use mod_auxfunctions
  use mod_vertices

  implicit none
  private
  public :: HP, WP

contains

! higgs propagator
! set to zero if denominator smaller than limit value as defined in BasicDef/mod_const_dp.f90
function HP(p)
  complex(dp), intent(in) :: p(5)
  complex(dp) ::  HP
HP = 0._dp
if ( (abs(sc(p,p) - mhsq) > limitProp) .and. abs(sc(p,p)) > limitProp) then
  HP = ci / (sc(p,p) - mhsq)
! else
!   print *, 'H propagator: denominator too small or zero momentum transfer'
end if
end function HP

! W propagator
! set to zero if denominator smaller than limit value as defined in BasicDef/mod_const_dp.f90
! matrix structure will be implemented as two scalar products
! J, p go in same direction
function WP(p,J)
  complex(dp), intent(in) :: p(dimP), J(dimP)
  complex(dp) ::  WP(dimP)
WP(:) = 0._dp
if ( abs(sq(p) - mwsq) > limitProp .and. abs(sq(p)) > limitProp ) then
  !print *, 'calc W prop'
  WP(:) = ( -ci / (sq(p) - mwsq) ) * ( J(:) - p(:) * sc(p,J) / sq(p) )
! else
!   print *, 'W propagator: denominator too small or zero momentum transfer'
end if
end function WP
    
end module
