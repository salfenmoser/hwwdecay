module mod_vertices

  use mod_types
  use mod_consts_dp
  use common_def
  
  use mod_auxfunctions

  implicit none
  private
  public :: levicevita, delta, V_w3, V_w4, V_hw2, V_h2w2, V_h3, V_h4

contains

! levi cevita symbol
function levicevita(i1,i2,i3) 
  integer, intent(in) :: i1,i2,i3 
  integer ::  levicevita 
  levicevita = 0  
  
  if (i1.eq.i2.or.i1.eq.i3.or.i2.eq.i3) then
  levicevita = 0
  
  else if (i1.ne.1 .and. i1.ne.2 .and. i1.ne.3) then
  levicevita = 0
  
  else if (i2.ne.1 .and. i2.ne.2 .and. i2.ne.3) then
  levicevita = 0
  
  else if (i3.ne.1 .and. i3.ne.2 .and. i3.ne.3) then
  levicevita = 0
  
  else if (i1.eq.1 .and. i2.eq.2) then
    levicevita = 1 
  else if (levicevita.eq.0 .and. i1.eq.1 .and. i2.eq.3) then
    levicevita = -1 
  else if (levicevita.eq.0 .and. i1.eq.2 .and. i2.eq.1) then
    levicevita = -1
  else if (levicevita.eq.0 .and. i1.eq.2 .and. i2.eq.3) then
    levicevita = 1
  else if (levicevita.eq.0 .and. i1.eq.3 .and. i2.eq.2) then
    levicevita = -1
  else if (levicevita.eq.0 .and. i1.eq.3 .and. i2.eq.1) then
    levicevita = 1
  end if
  
end function levicevita
  
! kronecker delta
function delta(i1,i2) 
  integer, intent(in) :: i1,i2
  integer ::  delta 
  delta = 0
  if (i1.eq.i2)   delta = 1
end function delta

!-----------------------------------------------
!Define W3, W4 vertices
!-----------------------------------------------
! 3W-vertex, all momenta going in, first argument corresponds to the particle not given
function V_w3(i3,p1,ep1,i1,p2,ep2,i2) 
  integer :: i3,i1,i2
  complex(dp), intent(in) :: p1(dimP), p2(dimP), ep1(dimP), ep2(dimP)
  complex(dp) ::  V_w3(dimP) 
  complex(dp) :: p3(dimP), p12(dimP), p23(dimP), p31(dimP) 

p3 = - p1 - p2 
p12 =  p1  - p2 
p23 =  p2  - p3
p31 =  p3  - p1
    
V_w3(:) = g_coupling * levicevita(i3,i1,i2) * ( sc(ep1,ep2)*p12(:) + sc(ep1,p23)*ep2(:) + sc(ep2,p31)*ep1(:) )
    
end function V_w3

!-----------------------------------------------
! 4W-vertex, all momenta going in, first argument corresponds to the particle not given.
function V_w4(i4,ep1,i1,ep2,i2,ep3,i3) 
  integer :: i3,i1,i2,i4, sumInd, j
  complex(dp), intent(in) :: ep1(dimP), ep2(dimP), ep3(dimP)
  complex(dp) :: V_w4(dimP)
  do j = 1,dimP
  V_w4(j) = czero
  end do
  do sumInd = 1, 3
    V_w4(:) =  V_w4(:) - ci * g_coupling**2 * ( &
                        levicevita(sumInd,i1,i2) * levicevita(sumInd,i3,i4) * (  & 
                                sc(ep1,ep3)*ep2(:) - sc(ep2,ep3)*ep1(:) )     & 
                        + levicevita(sumInd,i1,i3) * levicevita(sumInd,i4,i2) * (  & 
                                sc(ep2,ep3)*ep1(:) - sc(ep1,ep2)*ep3(:) )     & 
                        + levicevita(sumInd,i1,i4) * levicevita(sumInd,i2,i3) * (  & 
                                sc(ep1,ep2)*ep3(:) - sc(ep1,ep3)*ep2(:) ) )
  end do
end function V_w4

!-----------------------------------------------
!Define H2, H3 vertices
!-----------------------------------------------
! HWW-vertex, all momenta going in, first argument corresponds to higgs (not given)
function V_hw2(i1,i2) 
  integer :: i1,i2
  complex(dp) ::  V_hw2
  V_hw2 = two * ci * g_coupling * mw * delta(i1,i2)
end function V_hw2

!-----------------------------------------------
! HHWW-vertex, all momenta going in, 4: unknown higgs; 3: known higgs; 1,2: W
function V_h2w2(i1,i2) 
  integer :: i1,i2
  complex(dp) ::  V_h2w2
  V_h2w2 = two * ci * g_coupling**2 * delta(i1,i2)
end function V_h2w2

! HHH-vertex
function V_h3()
  complex(dp) ::  V_h3
  V_h3 = - three * ci * g_coupling * mhsq / mw
end function V_h3

! HHH-vertex
function V_h4()
  complex(dp) ::  V_h4
  V_h4 = - three * ci * g_coupling**2 * mhsq / mwsq
end function V_h4
    
    end module 
