
!----------------------------------------------------------------------------------------------
! H1 CURRENTS
!----------------------------------------------------------------------------------------------

! H -> 4W; 3vertex, W1 emitted directly, internal W->2,3,4; colour structure checked 12/05
function HW4_W4(part1,part2,part3,part4) 
  integer :: ix
  complex(dp), intent(in) :: part1(3,4), part2(3,4), part3(3,4), part4(3,4)
  complex(dp) ::  HW4_W4, partx(3,4)
  HW4_W4 = czero
 
  ! internal W
  do ix = 1,3
  
  partx(1,1:4) = - part2(1,:) - part3(1,:) - part4(1,:)
  partx(2,1:4) = WP(partx(1,:),W4(ix,part2,part3,part4))
  partx(3,1:4) = (/cone*ix,czero,czero,czero/)
  
  partx(1,1:4) = - partx(1,1:4)
  HW4_W4 = HW4_W4 + HW2(part1,partx)
  end do
  
end function HW4_W4

! H -> 4W; 3vertex, W->W1,W2 and W->W3,W4; colour structure checked 12/04
function HW4_W3W3(part1,part2,part3,part4) 
  integer :: ix1, ix2
  complex(dp), intent(in) :: part1(3,4), part2(3,4), part3(3,4), part4(3,4)
  complex(dp) ::  HW4_W3W3, partx1(3,4), partx2(3,4)
  HW4_W3W3 = czero
 
  do ix1 = 1,3
  partx1(1,1:4) = - part1(1,:) - part2(1,:)
  partx1(2,1:4) = WP(partx1(1,:),W3(ix1,part1,part2))
  partx1(3,1:4) = (/cone*ix1,czero,czero,czero/)
  partx1(1,1:4) = - partx1(1,1:4)
  
  ix2 = ix1
  partx2(1,1:4) = - part3(1,:) - part4(1,:)
  partx2(2,1:4) = WP(partx2(1,:),W3(ix2,part3,part4))
  partx2(3,1:4) = (/cone*ix2,czero,czero,czero/)
  partx2(1,1:4) = - partx2(1,1:4)
  
    HW4_W3W3 = HW4_W3W3 + HW2(partx1,partx2)
  end do
  
end function HW4_W3W3

! H -> 4W; 4vertex, W1,W2 emitted directly, internal H->3,4;
function HW4_h2w2(part1,part2,part3,part4) 
  complex(dp), intent(in) :: part1(3,4), part2(3,4), part3(3,4), part4(3,4)
  complex(dp) ::  HW4_h2w2, px(4)
  HW4_h2w2 = czero
  px(:) = - part3(1,:) - part4(1,:)  
  HW4_h2w2 =  V_h2w2(int(part1(3,1)),int(part2(3,1))) * sc(part1(2,:),part2(2,:)) * HP(px) * HW2(part3,part4)
end function HW4_h2w2

! H -> 4W; 3Hvertex, H-> H(->W1+W2) + H(->W3+W4)
function HW4_H2H2(part1,part2,part3,part4) 
  complex(dp), intent(in) :: part1(3,4), part2(3,4), part3(3,4), part4(3,4)
  complex(dp) ::  HW4_H2H2, px1(4), px2(4)
  HW4_H2H2 = czero
  px1(:) = - part1(1,:) - part2(1,:)
  px2(:) = - part3(1,:) - part4(1,:)
  HW4_H2H2 =  V_h3() * HP(px1) * HW2(part1,part2) * HP(px2) * HW2(part3,part4)
end function HW4_H2H2

! H -> 4W
function HW4_(part1,part2,part3,part4) 
  complex(dp), intent(in) :: part1(3,4), part2(3,4), part3(3,4), part4(3,4)
  complex(dp) ::  HW4_
  HW4_ = HW4_W4(part1,part2,part3,part4) + HW4_W4(part2,part1,part3,part4) + HW4_W4(part3,part1,part2,part4) + HW4_W4(part4,part1,part2,part3) + HW4_W3W3(part1,part2,part3,part4) + HW4_W3W3(part1,part3,part2,part4) + HW4_W3W3(part1,part4,part2,part3) + HW4_h2w2(part1,part2,part3,part4) + HW4_h2w2(part1,part3,part2,part4) + HW4_h2w2(part1,part4,part2,part3) + HW4_h2w2(part2,part3,part1,part4) + HW4_h2w2(part2,part4,part1,part3) + HW4_h2w2(part3,part4,part1,part2) + HW4_H2H2(part1,part2,part3,part4) + HW4_H2H2(part1,part3,part2,part4) + HW4_H2H2(part1,part4,part2,part3)
end function HW4_

! H -> 5W, HWW-vertex with W1 directly
function HW5_W4(part1,part2,part3,part4,part5)
  integer :: ix
  complex(dp), intent(in) :: part1(3,4), part2(3,4), part3(3,4), part4(3,4), part5(3,4)
  complex(dp) ::  HW5_W4, partx(3,4)
  HW5_W4 = czero
 
  ! internal W
  do ix = 1,3
  
  partx(1,1:4) = - part2(1,:) - part3(1,:) - part4(1,:) - part5(1,:)
  partx(2,1:4) = WP(partx(1,:),W5(ix,part2,part3,part4,part5))
  partx(3,1:4) = (/cone*ix,czero,czero,czero/)
  
  partx(1,1:4) = - partx(1,1:4)
  HW5_W4 = HW5_W4 + HW2(part1,partx)
  end do
end function HW5_W4

! H -> 5W; 3-vertex, W/H->W1,W2 and W/H->W3,W4,W5
function HW5_W3W4_H2H3(part1,part2,part3,part4,part5) 
  integer :: ix1, ix2
  complex(dp), intent(in) :: part1(3,4), part2(3,4), part3(3,4), part4(3,4), part5(3,4)
  complex(dp) ::  HW5_W3W4_H2H3, partx1(3,4), partx2(3,4)
  HW5_W3W4_H2H3 = czero
 
 !internal W,W
  do ix1 = 1,3
  partx1(1,1:4) = - part1(1,:) - part2(1,:)
  partx1(2,1:4) = WP(partx1(1,:),W3(ix1,part1,part2))
  partx1(3,1:4) = (/cone*ix1,czero,czero,czero/)
  partx1(1,1:4) = - partx1(1,1:4)
  
  ix2 = ix1
  partx2(1,1:4) = - part3(1,:) - part4(1,:) - part5(1,:)
  partx2(2,1:4) = WP(partx2(1,:),W4(ix2,part3,part4,part5))
  partx2(3,1:4) = (/cone*ix2,czero,czero,czero/)
  partx2(1,1:4) = - partx2(1,1:4)
  
  HW5_W3W4_H2H3 = HW5_W3W4_H2H3 + HW2(partx1,partx2)
  end do
  
 !internal H,H
  HW5_W3W4_H2H3 = HW5_W3W4_H2H3 +  V_h3() * HP(partx1(1,:)) * HW2(part1,part2) * HP(partx2(1,:)) * HW3(part3,part4,part5)

end function HW5_W3W4_H2H3

! H -> 5W; 4vertex, W1,W2 emitted directly, internal H->3,4,5;
function HW5_H3(part1,part2,part3,part4,part5) 
  complex(dp), intent(in) :: part1(3,4), part2(3,4), part3(3,4), part4(3,4), part5(3,4)
  complex(dp) ::  HW5_H3, px(4)
  HW5_H3 = czero
  px(:) = - part3(1,:) - part4(1,:) -part5(1,:)
  HW5_H3 =  V_h2w2(int(part1(3,1)),int(part2(3,1))) * sc(part1(2,:),part2(2,:)) * HP(px) * HW3(part3,part4,part5)
end function HW5_H3

! H -> 5W; 4vertex, W1 emitted directly, internal W->2,3; H->4,5;
function HW5_H2W3(part1,part2,part3,part4,part5) 
  integer :: ix1
  complex(dp), intent(in) :: part1(3,4), part2(3,4), part3(3,4), part4(3,4), part5(3,4)
  complex(dp) ::  HW5_H2W3, partx1(3,4), px2(4)
  HW5_H2W3 = czero
  px2(:) = - part4(1,:) -part5(1,:)
  
  ! internal W
  do ix1 = 1,3
  partx1(1,1:4) = - part2(1,:) - part3(1,:)
  partx1(2,1:4) = WP(partx1(1,:),W3(ix1,part2,part3))
  HW5_H2W3 = HW5_H2W3 +  V_h2w2(int(part1(3,1)),ix1) * sc(part1(2,:),partx1(2,:)) * HP(px2) * HW2(part4,part5)
  end do
end function HW5_H2W3

! H -> 5W
function HW5(part1,part2,part3,part4,part5) 
  complex(dp), intent(in) :: part1(3,4), part2(3,4), part3(3,4), part4(3,4), part5(3,4)
  complex(dp) ::  HW5
  HW5 = HW5_W4(part1,part2,part3,part4,part5) + HW5_W4(part2,part1,part3,part4,part5) + HW5_W4(part3,part1,part2,part4,part5) + HW5_W4(part4,part1,part2,part3,part5) + HW5_W4(part5,part1,part2,part3,part4) + & 
    HW5_W3W4_H2H3(part1,part2,part3,part4,part5) + HW5_W3W4_H2H3(part1,part3,part2,part4,part5) + HW5_W3W4_H2H3(part1,part4,part2,part3,part5) + HW5_W3W4_H2H3(part1,part5,part2,part3,part4) + HW5_W3W4_H2H3(part2,part3,part1,part4,part5) + HW5_W3W4_H2H3(part2,part4,part1,part3,part5) + HW5_W3W4_H2H3(part2,part5,part1,part3,part4) + HW5_W3W4_H2H3(part3,part4,part1,part2,part5) + HW5_W3W4_H2H3(part3,part5,part1,part2,part4) + HW5_W3W4_H2H3(part4,part5,part1,part2,part3) + &
        HW5_H3(part1,part2,part3,part4,part5) + HW5_H3(part1,part3,part2,part4,part5) + HW5_H3(part1,part4,part2,part3,part5) + HW5_H3(part1,part5,part2,part3,part4) + HW5_H3(part2,part3,part1,part4,part5) + HW5_H3(part2,part4,part1,part3,part5) + HW5_H3(part2,part5,part1,part3,part4) + HW5_H3(part3,part4,part1,part2,part5) + HW5_H3(part3,part5,part1,part2,part4) + HW5_H3(part4,part5,part1,part2,part3) + &
            (HW5_H2W3(part1,part2,part3,part4,part5) + HW5_H2W3(part1,part2,part4,part3,part5)+ HW5_H2W3(part1,part2,part5,part3,part4) + HW5_H2W3(part1,part3,part4,part2,part5) + HW5_H2W3(part1,part3,part5,part2,part4) + HW5_H2W3(part1,part4,part5,part2,part3)) +       (HW5_H2W3(part2,part1,part3,part4,part5) + HW5_H2W3(part2,part1,part4,part3,part5) + HW5_H2W3(part2,part1,part5,part3,part4) + HW5_H2W3(part2,part3,part4,part1,part5) + HW5_H2W3(part2,part3,part5,part1,part4) + HW5_H2W3(part2,part4,part5,part1,part3)) + (HW5_H2W3(part3,part2,part1,part4,part5) + HW5_H2W3(part3,part2,part4,part1,part5)+ HW5_H2W3(part3,part2,part5,part1,part4) + HW5_H2W3(part3,part1,part4,part2,part5) + HW5_H2W3(part3,part1,part5,part2,part4) + HW5_H2W3(part3,part4,part5,part2,part1)) +   (HW5_H2W3(part4,part2,part3,part1,part5) + HW5_H2W3(part4,part2,part1,part3,part5) + HW5_H2W3(part4,part2,part5,part3,part1) + HW5_H2W3(part4,part3,part1,part2,part5) + HW5_H2W3(part4,part3,part5,part2,part1) + HW5_H2W3(part4,part1,part5,part2,part3)) + (HW5_H2W3(part5,part2,part3,part4,part1) + HW5_H2W3(part5,part2,part4,part3,part1)+ HW5_H2W3(part5,part2,part1,part3,part4) + HW5_H2W3(part5,part3,part4,part2,part1) + HW5_H2W3(part5,part3,part1,part2,part4) + HW5_H2W3(part5,part4,part1,part2,part3))
end function HW5
