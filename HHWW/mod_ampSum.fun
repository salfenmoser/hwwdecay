use mod_auxfunctions
use mod_consts_dp
use mod_kinematics
use mod_vertices


test_suite mod_ampSum

  complex(dp) :: P3(3,dimP)

  complex(dp) :: pa1(3,dimP), pa2(3,dimP), paH(3,dimP)
  integer :: pol1, pol2, c1, c2, D_S
  

setup

pol1 = 1
pol2 = 1

c1 = 2
c2 = 2

call particlesHWW(pol1, pol2, c1, c2, paH, pa1, pa2)

end setup

teardown
  ! This code runs immediately after each test
end teardown

test givesZeroForImproperChoicesIfDemanded
  complex(dp) :: l(dimP), triangleCurrents, bubbleCurrents, singleCutCurrents
  integer :: i, j

l = four * paH(1,:)

do D_S = 4,6
do i = 1,6
triangleCurrents = getTriangleAmps(D_S, l, paH, pa1, pa2, i, .true.)
assert_equal_within(abs(triangleCurrents), zero, limitAssert)
do j = 1,3
  bubbleCurrents = czero
  bubbleCurrents = getBubbleAmps(D_S, l, paH, pa1, pa2, i, j, .true.)
  assert_equal_within(abs(bubbleCurrents), zero, limitAssert)
end do
end do
end do

do D_S = 4,6
do i = 1,6
do j = 1,3
  singleCutCurrents = getTadpoleAmps(D_S, l, paH, pa1, pa2, i, j, .true.)
  assert_equal_within(abs(singleCutCurrents), zero, limitAssert) 
end do
end do
end do

end test givesZeroForImproperChoicesIfDemanded


test givesNotZeroForImproperChoicesIfNotDemanded
  complex(dp) :: l(dimP), triangleCurrents, bubbleCurrents, singleCutCurrents
  integer :: i, j

l = four * paH(1,:)

do D_S = 4,6
do i = 1,6
triangleCurrents = getTriangleAmps(D_S, l, paH, pa1, pa2, i, .false.)
assert_false(abs(triangleCurrents) < 1e-10)
do j = 1,3
  bubbleCurrents = getBubbleAmps(D_S, l, paH, pa1, pa2, i, j, .false.)
  assert_false(abs(bubbleCurrents) < 1e-10)
end do
end do
end do

do D_S = 4,6
do i = 1,6
do j = 1,3
  singleCutCurrents = getTadpoleAmps(D_S, l, paH, pa1, pa2, i, j, .false.)
  assert_false(abs(singleCutCurrents) < 1e-10)
end do
end do
end do
end test givesNotZeroForImproperChoicesIfNotDemanded

test triangleMomentumIndependet
complex(dp) :: l(dimP), triangleCurrents
integer :: i, j

do D_S = 4,6
l(:) = czero
do i = 3,4
triangleCurrents = getTriangleAmps(D_S, l, paH, pa1, pa2, i, .false.)
assert_equal_within(abs(triangleCurrents), abs(V_hw2(1,1)**two * V_h3()), limitAssert)
end do

l = pa1(1,:)
do i = 5, 6
triangleCurrents = getTriangleAmps(D_S, l, paH, pa1, pa2, i, .false.)
assert_equal_within(abs(triangleCurrents), abs(V_hw2(1,1)**three), limitAssert)
end do
end do
end test triangleMomentumIndependet



end test_suite
