module mod_ampSumHHWW

    use mod_types
    use mod_consts_dp
    use common_def
    
    use mod_auxfunctions     
    use mod_kinematics
    use mod_amplitudes
  
    use mod_diagKinHHWW
    
implicit none
  
public :: getBoxCurrents
logical :: printWarningBox = .false.
  
  
contains
  
!---------------------------------------------------------
! triple cuts 
! --------------------------------------------------------
!pH1 = Higgs, pW1 = W1, pW2 = W2
! dimension=D_S, momentum l, diagram i, boolean if only calulation if momenta are chosen adequately (on shell)
function getBoxCurrents(D_S, l, pH1, pH2, pW1, pW2, i, properMom)
  integer, intent(in) :: D_S, i
  complex(dp), intent(in) :: pH1(3,dimP), pH2(3,dimP), pW1(3,dimP), pW2(3,dimP), l(dimP)
  logical, intent(in) :: properMom ! if true: only returns non-zero result if proper momenta are chosen
  logical :: properMom
  integer :: epx1, cx1, epx2, cx2, epx3, cx3, epx4, cx4, sumCx1, sumCx2, sumCx3, sumCx4, sumEx1, sumEx2, sumEx3, sumEx4
  complex(dp) :: getBoxCurrents, diag(9,28)
  complex(dp) :: pax1In(3,dimP), pax2In(3,dimP), pax3In(3,dimP), pax4In(3,dimP), pax1Out(3,dimP), pax2Out(3,dimP), pax3Out(3,dimP), pax4Out(3,dimP)
  complex(dp) :: ePx1In(dimP,dimE), ePx2In(dimP,dimE), ePx3In(dimP,dimE), ePx4In(dimP,dimE)
  complex(dp) :: J3_1, J3_2, J3_3, J3_4
  complex(dp) :: d(9,4)

getBoxCurrents = czero
properMom = .true.
d = getDenHHWW(l, pH1, pH2, pW1, pW2)

if ((abs(d(i,1)) > limitProp) .or. (abs(d(i,2)) > limitProp) .or. (abs(d(i,3)) > limitProp) .or. (abs(d(i,4)) > limitProp)) then
  if (printWarningBox) then
    print *, "WARNING: TRYING TO COMPUTE TRIPLE CUT CURRENTS WITH IMPROPERLY CHOSEN MOMENTA"
  end if
  properMom = .false.
end if

if  ((properMom .eqv. .true.) .or. (properMom .eqv. .false.)) then

diag = diagHHWW(l, pH1(1,:), pH2(1,:), pW1(1,:), pW2(1,:))

! define momenta of internal particles, in and outgoing
pax1In(1,:) = diag(i,1:6)
pax1Out(1,:) = - pax1In(1,:)
pax2In(1,:) = diag(i,8:13)
pax2Out(1,:) = - pax2In(1,:)
pax3In(1,:) = diag(i,15:20)
pax3Out(1,:) = - pax3In(1,:)
pax4In(1,:) = diag(i,22:27)
pax4Out(1,:) = - pax4In(1,:)

! set polarization matrix of internal particles to zero, assuming higgs for now
ePx1In(:,:) = czero
ePx2In = ePx1In
ePx3In = ePx1In
ePx4In = ePx1In

! define upper sum limit of polarization and color sum for three internal particles, assuming higgs for now
sumCx1 = 1
sumCx2 = 1
sumCx3 = 1
sumCx4 = 1
  
sumEx1 = 1
sumEx2 = 1
sumEx3 = 1
sumEx4 = 1

! if not higgs, define properties accordingly
if (diag(i,7) == mwsq) then
  sumCx1 = 3
  sumEx1 = D_S - 1
  call spinonepol_6d(pax1In(1,:),ePx1In)
end if
if (diag(i,14) == mwsq) then
  sumCx2 = 3
  sumEx2 = D_S - 1
  call spinonepol_6d(pax2In(1,:),ePx2In)
end if
if (diag(i,21) == mwsq) then
  sumCx3 = 3
  sumEx3 = D_S - 1
  call spinonepol_6d(pax3In(1,:),ePx3In)
end if
if (diag(i,28) == mwsq) then
  sumCx4 = 3
  sumEx4 = D_S - 1
  call spinonepol_6d(pax4In(1,:),ePx4In)
end if

! sum over internal particles
do cx1 = 1,sumCx1
  do cx2 = 1,sumCx2
    do cx3 = 1,sumCx3
      do cx4 = 1,sumCx4

      do epx1 = 1,sumEx1
        do epx2 = 1,sumEx2
          do epx3 = 1,sumEx3
            do epx4 = 1,sumEx4
    
      ! define polarization states, non vanishing only if not higgs
      pax1In(2,:) = ePx1In(epx1,:)
      pax1Out(2,:) = ePx1In(epx1,:)
      pax2In(2,:) = ePx2In(epx2,:)
      pax2Out(2,:) = ePx2In(epx2,:)
      pax3In(2,:) = ePx3In(epx3,:)
      pax3Out(2,:) = ePx3In(epx3,:)
      pax4In(2,:) = ePx4In(epx4,:)
      pax4Out(2,:) = ePx4In(epx4,:)
    
      ! define color states, non vanishing only if not higgs
      pax1In(3,:) = czero
      pax2In(3,:) = czero
      pax3In(3,:) = czero
      pax4In(3,:) = czero
          
      if (diag(i,7) == mwsq) then
        pax1In(3,1) = cone*cx1
      end if
      if (diag(i,14) == mwsq) then
        pax2In(3,1) = cone*cx2
      end if
      if (diag(i,21) == mwsq) then
        pax3In(3,1) = cone*cx3
      end if
      if (diag(i,28) == mwsq) then
        pax4In(3,1) = cone*cx4
      end if
  
      pax1Out(3,:) = pax1In(3,:)
      pax2Out(3,:) = pax2In(3,:)
      pax3Out(3,:) = pax3In(3,:)
      pax4Out(3,:) = pax4In(3,:)
    
      ! calculate three 3particle currents
      J3_1 = J3(pH1,pax1Out,pax4In)
      if (i == 1 .or. i == 4 .or. i == 8) then
        J3_2 = J3(pW1,pax1In,pax2Out)
        J3_3 = J3(pW2,pax2In,pax3Out)
        J3_4 = J3(pH2,pax3In,pax4Out)
      else if (i == 2 .or. i == 5 .or. i == 9) then
        J3_2 = J3(pW2,pax1In,pax2Out)
        J3_3 = J3(pW1,pax2In,pax3Out)
        J3_4 = J3(pH2,pax3In,pax4Out)
      else if (i == 3 .or. i == 6 .or. i == 7) then
        J3_2 = J3(pW1,pax1In,pax2Out)
        J3_3 = J3(pH2,pax2In,pax3Out)
        J3_4 = J3(pW2,pax3In,pax4Out)
      end if
          
      getBoxCurrents = getBoxCurrents + J3_1 * J3_2 * J3_3 * J3_4
      print *, 'getBoxCurrents = ', getBoxCurrents
      print *, 'currents = ', J3_1, J3_2, J3_3, J3_4

       end do
      end do
     end do
    end do
  
    end do
    end do
   end do
  end do

end if
end function getBoxCurrents

  
end module