module mod_box

  use mod_types
  use mod_consts_dp
  use common_def
  
  use mod_auxfunctions     
  use mod_kinematics
  use mod_amplitudes

  use mod_diagKinHHWW
  use mod_ampSumHHWW
  
implicit none

public :: D_ijkl, getBoxCutCoefficients_i, getBoxCutCoefficient0_i, getBoxCutFunction_i
public :: d_Test

logical :: demandOnShellExternal = .false., fromScratch = .false.
integer :: unitOffsetD = 30

contains

!---------------------------------------------------------
! triple cuts 
! --------------------------------------------------------

function d_Test()
  complex(dp) :: d_Test(5)
  d_Test(:) = czero
end function d_Test

! function C_ijkTest(D_S, l, ph1, pw1, pw2, i, c)
!   integer, intent(in) :: D_S, i
!   complex(dp), intent(in) ::  l(dimP), ph1(3,dimP), pw1(3,dimP), pw2(3,dimP), c(10)
!   complex(dp) :: lt_sq, v(dimP,dimP), V3(dimP), l_eps(10,dimP)
!   complex(dp) C_ijkTest

! C_ijkTest = czero
! call getTriangleKinematics(ph1, pw1, pw2, i, v, V3, lt_sq)
! if (D_S == 4) then
!   v(5,:) = czero
! end if

! C_ijkTest = c(10) + c(1)*sc(l,v(3,:)) + c(2)*sc(l,v(4,:)) + c(3)*(sc(l,v(3,:))**two - sc(l,v(4,:))**two)
! C_ijkTest = C_ijkTest + c(4)*sc(l,v(3,:))*sc(l,v(4,:)) + c(5)*sc(l,v(3,:))**three + c(6)*sc(l,v(4,:))**three 
! C_ijkTest = C_ijkTest + c(7)*sc(l,v(5,:))**two + c(8)*(sc(l,v(5,:))**two)*sc(l,v(3,:)) + c(9)*(sc(l,v(5,:))**two)*sc(l,v(4,:))

! end function C_ijkTest

! box cut function depending on dimension D_S, momentum l, particles h1, h2, w1,w2, diagram i
! nothing to subtract from higher order cuts
function D_ijkl(D_S, l, ph1, ph2, pw1, pw2, i)
  integer, intent(in) :: i, D_S
  complex(dp), intent(in) :: ph1(3,dimP), ph2(3,dimP), pw1(3,dimP), pw2(3,dimP), l(dimP)
  complex(dp) :: D_ijkl
  D_ijkl = ci**4 * getBoxCurrents(D_S, l, ph1, ph2, pw1, pw2, i, demandOnShellExternal)
end function D_ijkl


! five box cut coefficients for dimension D_S, diagram i
! reduction is performed with five properly chosen momenta l
function getBoxCutCoefficients_i(D_S, ph1, ph2, pw1, pw2, i, test)
  integer, intent(in) :: D_S, i
  complex(dp), intent(in) ::  ph1(3,dimP), ph2(3,dimP), pw1(3,dimP), pw2(3,dimP)
  logical, intent(in) :: test
  integer :: m, n, unitInt
  complex(dp) :: getBoxCutCoefficients_i(5), coeff(5), auxD(5)
  complex(dp) :: lt_sq, v(dimP,dimP), V4(dimP), lAux(5,dimP)
  character*5 :: strDim, strDiag
  character*70 :: documentName
  logical :: fileExists

coeff(:) = czero
getBoxCutCoefficients_i(:) = czero
auxD(:) = czero

call getBoxCutKinematics(ph1, ph2, pw1, pw2, i, v, V4, lt_sq)

lAux(:,:) = czero
lAux = getBoxCutMomenta(D_S, ph1, ph2, pw1, pw2, i)

do m = 1,5
  auxD(m) = D_ijkl(D_S, lAux(m,:), ph1, ph2, pw1, pw2, i)
  print *, 'auxD,m = ', m, auxD(m)
  print *, 'momentum = ', lAux(m,:)
end do

! coeff(10) corresponds to c_0
coeff(5) = ( auxD(1) + auxD(2) )/two
coeff(1) = ( auxD(1) - auxD(2) ) / (two * sqrt(lt_sq))
if (D_S > 4) then
coeff(2) = ( two*auxD(3) + two*auxD(4) - auxD(5) - three*coeff(5)) / lt_sq
coeff(3) = sqrt(two/lt_sq**three) * (auxD(3) - auxD(4) - sqrt(two*lt_sq)*coeff(1))
coeff(4) = two * ( auxD(5) - auxD(3) - auxD(4) + coeff(5)) / lt_sq**two
end if

! set to zero if smaller than limit value
do m = 1,5
  if (abs(REAL(coeff(m))) < limitCoeff ) then
    coeff(m) = cmplx(zero,AIMAG(coeff(m)),kind=dp)
  end if
  if (abs(AIMAG(coeff(m))) < limitCoeff ) then
    coeff(m) = cmplx(REAL(coeff(m)),zero,kind=dp)
  end if
  end do

getBoxCutCoefficients_i(:) = coeff(:)

if (.NOT. test) then
! write coefficients in file, if not done yet
write (strDim, '(I1)') D_S
write (strDiag, '(I1)') i
unitInt = unitOffsetD+D_S-4

documentName = trim('diagrams/coefficients/boxCutCoefficient_'//trim(strDiag)//'_'//trim(strDim)//'.dat')

INQUIRE(FILE=documentName, EXIST=fileExists)
if (.NOT. fileExists ) then
  open(unit = unitInt, file = documentName, status='new')
  write(unitInt, FMT, advance='no') coeff(:)
  close(unit = unitInt)
end if
end if

end function getBoxCutCoefficients_i

function getBoxCutCoefficient0_i(D_S, ph1, ph2, pw1, pw2, i)
  integer, intent(in) :: i, D_S
  complex(dp), intent(in) ::  ph1(3,dimP), ph2(3,dimP), pw1(3,dimP), pw2(3,dimP)
  complex(dp) :: boxCutCoefficients(5)
  complex(dp) :: getBoxCutCoefficient0_i
  boxCutCoefficients(:) = czero
  boxCutCoefficients(:) = getBoxCutCoefficients_i(D_S, ph1, ph2, pw1, pw2, i, .false.)
getBoxCutCoefficient0_i = boxCutCoefficients(5)
end function getBoxCutCoefficient0_i

! returns the C_ijk function for any momentum l. required for subtraction scheme for double cut coefficients
function getBoxCutFunction_i(D_S, l, ph1, ph2, pw1, pw2, i)
  integer, intent(in) :: i, D_S
  complex(dp), intent(in) :: l(dimP), ph1(3,dimP), ph2(3,dimP), pw1(3,dimP), pw2(3,dimP)
  complex(dp) :: getBoxCutFunction_i, d_Func, d_i(5)
  real(dp) :: d_iREAL(5), d_iIMAG(5)
  complex(dp) :: v(dimP,dimP), V4(dimP), lt_sq
  integer :: m, unitInt
  character*5 :: strDim, strDiag
  character*70 :: documentName
  logical :: fileExists

call getBoxCutKinematics(ph1, ph2, pw1, pw2, i, v, V4, lt_sq)
if (D_S == 4) then
  v(5,:) = czero
end if

! check if file exists
write (strDim, '(I1)') D_S
write (strDiag, '(I1)') i
unitInt = unitOffsetD+D_S-4
documentName = trim('diagrams/coefficients/boxCutCoefficient_'//trim(strDiag)//'_'//trim(strDim)//'.dat')
!print*, 'inquiring existence of C file: ', documentName
INQUIRE(FILE=documentName, EXIST=fileExists)

! if so: getTriangleCoefficients_i from file
if (fileExists .and. (fromScratch .eqv. .false.)) then
  !print*, documentName, 'coefficient file found, no re-calculation'
  open(unit = unitInt, file = documentName, action='read', status='old')
  do m = 1,5
    d_i(m) = czero
    read(unitInt, *) d_iREAL(m)
    read(unitInt, *) d_iIMAG(m)
    d_i(m) = d_iREAL(m) + ci*d_iIMAG(m)
  end do
  close(unit = unitInt)

! if not: get from reduction
else
  !print*, documentName, 'coefficient file not found, start calculation'
  d_i = getBoxCutCoefficients_i(D_S, ph1, ph2, pw1, pw2, i, .false.)
end if

d_Func = d_i(5) + d_i(1)*sc(l,v(4,:)) + d_i(2)*sc(l,v(5,:))**two + d_i(3)*sc(l,v(4,:))*sc(l,v(5,:))**two + d_i(4)*sc(l,v(5,:))**four

getBoxCutFunction_i = d_Func
end function getBoxCutFunction_i

end module