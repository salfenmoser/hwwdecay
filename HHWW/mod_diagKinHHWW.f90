module mod_diagKinHHWW

    use mod_types
    use mod_consts_dp
    use common_def
    
    use mod_auxfunctions     
    use mod_kinematics
    use mod_vertices
    
    implicit none
    
  public :: diagHHWW, getDenHHWW
  public :: getBoxCutKinematics, getBoxCutMomenta

  
  contains

! contains 6 rows, each row represents one diagram;
! each row contains propagator momentum and mass for the three propagators
! row 1,2: WWW loop, row 3,4: HWH loop, row 5,6: WHW loop
! entry 1-6:momentum of first propagator, entry 7: mass squared of first propagator, etc.
function diagHHWW(l,p1,p2,k1,k2)
  integer :: i
  complex(dp), intent(in) :: l(dimP), p1(dimP), p2(dimP), k1(dimP), k2(dimP)
  complex(dp) ::  diagHHWW(9,28)

  do i = 1,3
    diagHHWW(i,1:6) = l
    diagHHWW(i,22:27) = l - p1

    diagHHWW(i,7) = mwsq
    diagHHWW(i,14) = mwsq
    diagHHWW(i,21) = mwsq
    diagHHWW(i,28) = mwsq
  end do
  
  diagHHWW(1,8:13) = l + k1
  diagHHWW(1,15:20) = l + k1 + k2
  diagHHWW(2,8:13) = l + k2
  diagHHWW(2,15:20) = l + k1 + k2
  diagHHWW(3,8:13) = l + k1
  diagHHWW(3,15:20) = l + k1 + p2

  do i = 4,5
    diagHHWW(i,1:6) = l
    diagHHWW(i,15:20) = l + k1 + k2
    diagHHWW(i,22:27) = l - p1

    diagHHWW(i,7) = mwsq
    diagHHWW(i,14) = mhsq
    diagHHWW(i,21) = mwsq
    diagHHWW(i,28) = mwsq
  end do

  diagHHWW(4,8:13) = l + k1
  diagHHWW(5,8:13) = l + k2

  do i = 6,7
    diagHHWW(i,1:6) = l
    diagHHWW(i,8:13) = l + k1
    diagHHWW(i,15:20) = l + k1 + p2
    diagHHWW(i,22:27) = l - p1
  end do

  diagHHWW(6,7) = mwsq
  diagHHWW(6,14) = mhsq
  diagHHWW(6,21) = mhsq
  diagHHWW(6,28) = mwsq

  diagHHWW(7,7) = mhsq
  diagHHWW(7,14) = mwsq
  diagHHWW(7,21) = mwsq
  diagHHWW(7,28) = mhsq

  do i = 8,9
    diagHHWW(i,1:6) = l
    diagHHWW(i,15:20) = l + k1 + k2
    diagHHWW(i,22:27) = l - p1

    diagHHWW(i,7) = mhsq
    diagHHWW(i,14) = mwsq
    diagHHWW(i,21) = mhsq
    diagHHWW(i,28) = mhsq
  end do

  diagHHWW(8,8:13) = l + k1
  diagHHWW(9,8:13) = l + k2


end function diagHHWW

! returns the three denominators of the six different diagrams
function getDenHHWW(l, h1, h2, w1, w2)
  integer :: i, k
  complex(dp), intent(in) :: l(dimP), h1(3,dimP), h2(3,dimP), w1(3,dimP), w2(3,dimP)
  complex(dp) :: getDenHHWW(9,4)
  complex(dp) :: diag(9,28)

diag = diagHHWW(l, h1(1,:), h2(1,:), w1(1,:), w2(1,:))
do i = 1,9
getDenHHWW(i,1) = sc(diag(i,1:6), diag(i,1:6)) - diag(i,7)
getDenHHWW(i,2) = sc(diag(i,8:13), diag(i,8:13)) - diag(i,14)
getDenHHWW(i,3) = sc(diag(i,15:20), diag(i,15:20)) - diag(i,21)
getDenHHWW(i,4) = sc(diag(i,22:27), diag(i,22:27)) - diag(i,28)
end do
end function getDenHHWW
  
! construction of Van Neerven-Vermaseren basis from particle 1,2,3, for diagram i for triple cut
! v contains 6d basis, v(1), v(2) parallel to k1,k2; v(3), v(4) in transversal space
! V3 and lt_sq required for construction of momenta
subroutine getBoxCutKinematics(h1, h2, w1, w2, i, v, V4, lt_sq)
  complex(dp), intent(in) ::  h1(3,dimP), h2(3,dimP), w1(3,dimP), w2(3,dimP)
  integer, intent(in) :: i
  complex(dp), intent(out) :: v(dimP,dimP), V4(dimP), lt_sq
  complex(dp) :: diag(9,28), l(dimP), vAux(4,4)

l(:) = czero
diag = diagHHWW(l, h1(1,:), h2(1,:), w1(1,:),w2(1,:))
    
! construct physical and transverse space to get l_transverse, symmetric in w1,w2
! since give2to4vect is symmetric in w1, w2, no need to differentiate between diagrams
! could also use give2to4vect(1,2) and shift all indices in diag(i,:) by -4 or give2to4vect(3,1) and shift by +4
if (i == 1 .or. i == 4 .or. i == 8) then
  call give3to4vect(w1(1,1:4), w2(1,1:4), h2(1,1:4), vAux)
  call extendBasisD(vAux, v)
   
else if (i == 2 .or. i == 5 .or. i == 9) then
  call give3to4vect(w2(1,1:4), w1(1,1:4), h2(1,1:4), vAux)
  call extendBasisD(vAux, v)

else if (i == 3 .or. i == 6 .or. i == 7) then
  call give3to4vect(w1(1,1:4), h2(1,1:4), w2(1,1:4), vAux)
  call extendBasisD(vAux, v)
end if

V4(:) = - 0.5_dp * ( sq(diag(i,8:13)) - diag(i,14) + diag(i,7) ) * v(1,:)
V4(:) = V4(:) - 0.5_dp * ( sq(diag(i,15:20)) - diag(i,21) + diag(i,7)) * v(2,:)
V4(:) = V4(:) - 0.5_dp * ( sq(diag(i,22:27)) - diag(i,28) + diag(i,7)) * v(3,:)
lt_sq = diag(i,7) - sq(V4)
return
end subroutine getBoxCutKinematics


function getBoxCutMomenta(D_S, pH1, pH2, pW1, pW2, i)
  integer, intent(in) :: i, D_S
  complex(dp), intent(in) :: pH1(3,dimP), pH2(3,dimP), pW1(3,dimP), pW2(3,dimP)
  complex(dp) :: v(dimP,dimP), V4(dimP), lt_sq
  complex(dp) :: getBoxCutMomenta(5,dimP)

getBoxCutMomenta(:,:) = czero
call getBoxCutKinematics(pH1, pH2, pW1, pW2, i, v, V4, lt_sq)

getBoxCutMomenta(1,:) = V4 + sqrt(lt_sq) * v(4,:)
getBoxCutMomenta(2,:) = V4 - sqrt(lt_sq) * v(4,:)
getBoxCutMomenta(3,:) = V4 + sqrt(lt_sq) * ( v(4,:) + v(5,:) )/sqrt(two)
getBoxCutMomenta(4,:) = V4 - sqrt(lt_sq) * ( v(4,:) + v(5,:) )/sqrt(two)
getBoxCutMomenta(5,:) = V4 + sqrt(lt_sq) * v(5,:)

end function getBoxCutMomenta
  
end module
  