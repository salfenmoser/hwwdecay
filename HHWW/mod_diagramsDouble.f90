module mod_bubble

  use mod_types
  use mod_consts_dp
  use common_def
  
  use mod_auxfunctions     
  use mod_kinematics
  use mod_amplitudes

  use mod_diagramKinematics
  use mod_ampSum
  use mod_triangle
  use mod_numeratorFunctions
  
implicit none

public :: B_ij, getBubbleCoefficients_ij, getBubbleCoefficient0_ij, getBubbleFunction_ij
public :: B_ijTest, b_Test

integer :: unitOffsetB = 40

contains


function b_Test()
  complex(dp) :: b_Test(10)
  b_Test(:) = czero
end function b_Test

! double cut function depending on dimension D_S, momentum l, particles higgs, w1,w2, diagram i, cut j
! need to subtract triangle coefficients adequately
function B_ij(D_S, l, pa1, pa2, pa3, i, j, test, testB)
  integer, intent(in) :: i, j, D_S
  complex(dp), intent(in) :: pa1(3,dimP), pa2(3,dimP), pa3(3,dimP), l(dimP), testB(10)
  logical, intent(in) :: test
  complex(dp) :: B_ij, d_l(6,3), d_lM2(6,3), d_lM1(6,3), doubleCurrents, tripleC(6), tripleC_shiftedByK1(6), tripleC_shiftedByK2(6)
  integer :: k
  complex(dp) :: lt_sq, v(dimP,dimP), V3(dimP), x1, q(dimP)

if (test) then
  B_ij = B_ijTest(D_S, l, pa1, pa2, pa3, i, j, testB)
else
d_l = getDenominators(l, pa1, pa2, pa3)
d_lM2 = getDenominators(l-pa3(1,:), pa1, pa2, pa3)
d_lM1 = getDenominators(l-pa2(1,:), pa1, pa2, pa3)

doubleCurrents = czero
doubleCurrents = ci**2 * getBubbleAmps(D_S, l, pa1, pa2, pa3, i, j, demandOnShellExternal)
B_ij = czero

!print *, 'doubleCurrents = ', doubleCurrents

! first cut: d0=d1=0
if (j == 1) then

  !subtraction prescription depends on structure, how diagrams add up
if (i == 1) then
  tripleC(i) = getTriangleFunction_i(D_S, l, pa1, pa2, pa3, 1)
  tripleC_shiftedByK2(2) = getTriangleFunction_i(D_S, l - pa3(1,:) , pa1, pa2, pa3, 2)
  !tripleC_shiftedByK2(2) = getTriangleFunction_i(D_S, l - pa3(1,:) , pa1, pa2, pa3, 1)
  B_ij = doubleCurrents - tripleC(i)/d_l(1,3) - tripleC_shiftedByK2(2)/d_lM2(2,1)
else if (i == 3) then
  tripleC(i) = getTriangleFunction_i(D_S, l , pa1, pa2, pa3, i)
  tripleC_shiftedByK2(i+3) = getTriangleFunction_i(D_S, l - pa3(1,:), pa1, pa2, pa3, i+3)
  B_ij = doubleCurrents - tripleC(i)/d_l(i,3) - tripleC_shiftedByK2(i+3)/d_lM2(i+3,1)
else if (i == 5) then
  tripleC(i) = getTriangleFunction_i(D_S, l , pa1, pa2, pa3, i)
  tripleC_shiftedByK2(i-1) = getTriangleFunction_i(D_S, l - pa3(1,:), pa1, pa2, pa3, i-1)
  B_ij = doubleCurrents - tripleC(i)/d_l(i,3) - tripleC_shiftedByK2(i-1)/d_lM2(i-1,1)

else if (i == 2) then
  tripleC(i) = getTriangleFunction_i(D_S, l , pa1, pa2, pa3, i)
  tripleC_shiftedByK1(i-1) = getTriangleFunction_i(D_S, l - pa2(1,:), pa1, pa2, pa3, i-1)
  B_ij = doubleCurrents - tripleC(i)/d_l(i,3) - tripleC_shiftedByK1(i-1)/d_lM1(i-1,1)
else if (i == 4) then
  tripleC(i) = getTriangleFunction_i(D_S, l , pa1, pa2, pa3, i)
  tripleC_shiftedByK1(i+1) = getTriangleFunction_i(D_S, l - pa2(1,:), pa1, pa2, pa3, i+1)
  B_ij = doubleCurrents - tripleC(i)/d_l(i,3) - tripleC_shiftedByK1(i+1)/d_lM1(i+1,1)
else if (i == 6) then
  tripleC(i) = getTriangleFunction_i(D_S, l , pa1, pa2, pa3, i)
  tripleC_shiftedByK1(i-3) = getTriangleFunction_i(D_S, l - pa2(1,:), pa1, pa2, pa3, i-3)
  B_ij = doubleCurrents - tripleC(i)/d_l(i,3) - tripleC_shiftedByK1(i-3)/d_lM1(i-3,1) 
end if

! second cut: d0=d2=0
else if (j == 2) then
  do k = 1,6
  tripleC(k) = getTriangleFunction_i(D_S, l, pa1, pa2, pa3, k)
  end do

  if (i == 1) then
    B_ij = doubleCurrents - tripleC(1)/d_l(1,2) - tripleC(2)/d_l(2,2)
    B_ij = B_ij - tripleC(5)/d_l(5,2) - tripleC(6)/d_l(6,2)
    ! print *, 'subtract:', - tripleC(1)/d_l(1,2) - tripleC(2)/d_l(2,2)
    ! print *, 'subtract:', - tripleC(5)/d_l(5,2) - tripleC(6)/d_l(6,2)
  else if (i == 3) then
    B_ij = doubleCurrents - tripleC(3)/d_l(3,2) - tripleC(4)/d_l(4,2)
    !print *, 'subtract:', - tripleC(3)/d_l(3,2) - tripleC(4)/d_l(4,2)
  end if
end if

!print *, 'B_ij = ', B_ij

end if 

end function B_ij

function B_ijTest(D_S, l, pa1, pa2, pa3, i, j, b)
  integer, intent(in) :: i, j, D_S
  complex(dp), intent(in) ::  l(dimP), pa1(3,dimP), pa2(3,dimP), pa3(3,dimP), b(10)
  complex(dp) :: x1, q(dimP), lt_sq, vec(dimP,dimP), V2(dimP), lPlus(dimP), lMinus(dimP), t, v(dimP,dimP)
  complex(dp) B_ijTest

B_ijTest = czero
call getBubbleKinematics(pa1, pa2, pa3, i, j, q, x1, lt_sq, vec)
if (D_S == 4) then
  v(5,:) = czero
end if

B_ijTest = b(10) + b(1)*sc(l, vec(2,:)) + b(2)*sc(l, vec(3,:)) + b(3)*sc(l, vec(4,:))
B_ijTest = B_ijTest + b(4)*(sc(l, vec(2,:))**two - sc(l, vec(4,:))**two) + b(5)*(sc(l, vec(3,:))**two - sc(l, vec(4,:))**two)
B_ijTest = B_ijTest + b(6)*sc(l, vec(2,:))*sc(l, vec(3,:)) + b(7)*sc(l, vec(3,:))*sc(l, vec(4,:))
B_ijTest = B_ijTest + b(8)*sc(l, vec(2,:))*sc(l, vec(4,:)) + b(9)*sc(l, vec(5,:))**two

end function B_ijTest



! ten double cut coefficients for dimension D_S, diagram i, cut j
! reduction is performed with ten properly chosen momenta l
function getBubbleCoefficients_ij(D_S, pa1, pa2, pa3, i, j, test, testB)
  integer, intent(in) :: i, j, D_S
  complex(dp), intent(in) ::  pa1(3,dimP), pa2(3,dimP), pa3(3,dimP), testB(10)
  logical, intent(in) :: test
  integer :: n, m, i_ind, unitInt
  complex(dp) :: getBubbleCoefficients_ij(10), Bcoeff(10), B1(3), B2(2), auxB, B3(4), BEps
  complex(dp) :: x1, q(dimP), lt_sq, vec(dimP,dimP), V2(dimP)
  complex(dp) :: t, x, y
  complex(dp) :: l1(3,3,2,dimP), l2(2,2,2,dimP), l_Aux(4,dimP), l_Eps(dimP)
  character*5 :: strDim, strDiag, strCut
  character*70 :: documentName
  logical :: fileExists

getBubbleCoefficients_ij(:) = (/czero,czero,czero,czero,czero,czero,czero,czero,czero,czero/)

if ( j == 3 ) then
else if ( j == 2 .and. (i /= 1 .and. i /= 3) ) then
else

call getBubbleKinematics(pa1, pa2, pa3, i, j, q, x1, lt_sq, vec)

B1(:) = czero
l1(:,:,:,:) = czero
l2(:,:,:,:) = czero
l_Aux(:,:) = czero
l_Eps(:) = czero

call getBubbleMomenta(D_S, pa1, pa2, pa3, i, j, l1, l2, l_Aux, l_Eps, x, y)

do m = 1,3
  do n = 0,2
  t = exp(2*pi*ci * n / three)  
  auxB = (B_ij(D_S, l1(m,n+1,1,:), pa1, pa2, pa3, i, j, test, testB) + &
       B_ij(D_S, l1(m,n+1,2,:), pa1, pa2, pa3, i, j, test, testB))/ two
  B1(m) = B1(m) + (one/three) * auxB * t**(m-1) !corresponds to 1: b_0(eff), 3: b_1, 2: b_4(eff)   
end do
end do
  
B2(:) = czero
do m = 1,2
  do n = 0,1
  t = exp(2*pi*ci * n / two)
  auxB = (B_ij(D_S, l2(m,n+1,1,:), pa1, pa2, pa3, i, j, test, testB) - & 
        B_ij(D_S, l2(m,n+1,2,:), pa1, pa2, pa3, i, j, test, testB)) / (two * sqrt(lt_sq - t**2))
  B2(m) = B2(m) + (one/two) * auxB * t**(m-1) !corresponds to b_2, b_6    
end do
end do

Bcoeff(1) = B1(3)
Bcoeff(2) = B2(1)
Bcoeff(6) = B2(2)

B3(:) = czero
do m= 1,4
  B3(m) =  B_ij(D_S, l_Aux(m,:), pa1, pa2, pa3, i, j, test, testB)
end do
  
Bcoeff(3) = ( (B3(1) - B3(3))/two - x*Bcoeff(1) )/ y
Bcoeff(8) = ( (B3(1) - B3(2))/two - x*Bcoeff(1) ) / (x*y)
Bcoeff(5) = ( B1(1) + Bcoeff(3)*y + x*y * Bcoeff(8) + x* Bcoeff(1) + (x**two - y**two)*B1(2) - B3(1) ) / (three*y**two)
Bcoeff(4) = B1(2) + Bcoeff(5)
Bcoeff(10) = B1(1) - lt_sq * Bcoeff(5)
Bcoeff(7) = (B3(4) + (x**two - y**two) * Bcoeff(5) + Bcoeff(4)*x**2 - Bcoeff(3)*x - y*Bcoeff(2) - Bcoeff(10) ) / (x*y)

Bcoeff(9) = czero
!access b9
if (D_S > 4) then
  Beps = B_ij(D_S, l_Eps, pa1, pa2, pa3, i, j, test, testB)
  Bcoeff(9) = (Beps - Bcoeff(10))/lt_sq
end if

do m = 1,10
  if (abs(REAL(Bcoeff(m))) < limitCoeff ) then
    Bcoeff(m) = cmplx(zero,AIMAG(Bcoeff(m)),kind=dp)
  end if
  if (abs(AIMAG(Bcoeff(m))) < limitCoeff ) then
    Bcoeff(m) = cmplx(REAL(Bcoeff(m)),zero,kind=dp)
  end if
end do

getBubbleCoefficients_ij(:) = Bcoeff(:)

if (.NOT. test) then
! write coefficients in file
write (strDim, '(I1)') D_S
write (strDiag, '(I1)') i
write (strCut, '(I1)') j
unitInt = unitOffsetB+D_S-4

documentName = trim('diagrams/coefficients/bubbleCoefficient_'//trim(strDiag)//'_'//trim(strCut)//'_'//trim(strDim)//'.dat')

INQUIRE(FILE=documentName, EXIST=fileExists)
if (.NOT. fileExists ) then
  open(unit = unitInt, file = documentName, status='new')
  write(unitInt, FMT, advance='no') Bcoeff(:)
  close(unit = unitInt)
end if
end if

end if
end function getBubbleCoefficients_ij

function getBubbleCoefficient0_ij(D_S, pa1, pa2, pa3, i, j)
  integer, intent(in) :: i, j, D_S
  complex(dp), intent(in) ::  pa1(3,dimP), pa2(3,dimP), pa3(3,dimP)
  complex(dp) :: bubbleCoefficients(10)
  complex(dp) :: getBubbleCoefficient0_ij
bubbleCoefficients(:) = czero
bubbleCoefficients(:) = getBubbleCoefficients_ij(D_S, pa1, pa2, pa3, i, j, .false., b_Test())
getBubbleCoefficient0_ij = bubbleCoefficients(10)
end function getBubbleCoefficient0_ij

! returns the B_ij function for any momentum l. required for subtraction scheme for single cut coefficients
function getBubbleFunction_ij(D_S, l, pa1, pa2, pa3, i, j)
  integer, intent(in) :: i, j, D_S
  complex(dp), intent(in) :: l(dimP), pa1(3,dimP), pa2(3,dimP), pa3(3,dimP)
  complex(dp) :: getBubbleFunction_ij, b_Func, bCoeff_ij(10), v(dimP,dimP), q(dimP), x1, lt_sq, b_ijIMAG(10), b_ijREAL(10)
  integer :: m, unitInt
  complex(dp) :: den(6,3), l_eff(dimP), den_eff, l_zero(dimP), alpha1, diag(6,21)
  character*5 :: strDim, strDiag, strCut
  character*70 :: documentName
  logical :: fileExists, sqDen

sqDen = .false.

call getBubbleKinematics(pa1, pa2, pa3, i, j, q, x1, lt_sq, v)
bCoeff_ij = getBubbleCoefficients_ij(D_S, pa1, pa2, pa3, i, j, .false., b_Test())

do m = 1,dimP
l_zero(m) = czero
end do

diag = diagramsHWW(l_zero, pa1(1,:), pa2(1,:), pa3(1,:))

! check if file exists
write (strDim, '(I1)') D_S
write (strDiag, '(I1)') i
write (strCut, '(I0)') j
unitInt = unitOffsetB+D_S-4
documentName = trim('diagrams/coefficients/bubbleCoefficient_'//trim(strDiag)//'_'//trim(strCut)//'_'//trim(strDim)//'.dat')
!print*, 'inquiring existence of B file: ', documentName
INQUIRE(FILE=documentName, EXIST=fileExists)

! if so: getTriangleCoefficients_i from file
if (fileExists .and. (fromScratch .eqv. .false.)) then
  !print*, 'B coefficient file found, no re-calculation'
  open(unit = unitInt, file = trim(documentName), status='old')
  do m = 1,10
    bCoeff_ij(m) = czero
    read(unitInt, *) b_ijREAL(m)
    read(unitInt, *) b_ijIMAG(m)
    bCoeff_ij(m) = b_ijREAL(m) + ci*b_ijIMAG(m)
  end do
  close(unit = unitInt)

! if not: get from reduction
else
  !print*, 'B coefficient file not found, start calculation'
  bCoeff_ij = getBubbleCoefficients_ij(D_S, pa1, pa2, pa3, i, j, .false., b_Test())
end if

den = getDenominators(l, pa1, pa2, pa3)

if (j == 1) then

b_Func = bCoeff_ij(10) + bCoeff_ij(1)*sc(l, v(2,:)) + bCoeff_ij(2)*sc(l, v(3,:)) + bCoeff_ij(3)*sc(l, v(4,:))
b_Func = b_Func + bCoeff_ij(4)*(sc(l, v(2,:))**2 - sc(l,v(4,:))**2) + bCoeff_ij(5)*(sc(l, v(3,:))**2 - sc(l,v(4,:))**2)
b_Func = b_Func + bCoeff_ij(6)*sc(l, v(2,:))*sc(l, v(3,:)) + bCoeff_ij(7)*sc(l, v(3,:))*sc(l, v(4,:))
b_Func = b_Func + bCoeff_ij(8)*sc(l, v(2,:))*sc(l, v(4,:)) + bCoeff_ij(9)*sc(l, v(5,:))**2

! in case of squared denominator contribution
if (sqDen) then
  print *, 'bFunc before: ', b_Func
  den = getDenominators(l, pa1, pa2, pa3)
  alpha1 = bCoeff_ij(1)/( -two * sc(v(2,:), diag(i,1:6) - diag(i,15:20)) )
  b_Func = b_Func - alpha1 * (den(i,2) - den(i,1)) * sc(v(1,:),diag(i,1:6) - diag(i,15:20))
  print *, 'bFunc after: ', b_Func
end if 

else if (j == 2) then
b_Func = bCoeff_ij(10) + bCoeff_ij(1)*sc(l, v(2,:)) + bCoeff_ij(2)*sc(l, v(3,:)) + bCoeff_ij(3)*sc(l, v(4,:))
b_Func = b_Func + bCoeff_ij(4)*(sc(l, v(2,:))**2 - sc(l,v(4,:))**2) + bCoeff_ij(5)*(sc(l, v(3,:))**2 - sc(l,v(4,:))**2)
b_Func = b_Func + bCoeff_ij(6)*sc(l, v(2,:))*sc(l, v(3,:)) + bCoeff_ij(7)*sc(l, v(3,:))*sc(l, v(4,:))
b_Func = b_Func + bCoeff_ij(8)*sc(l, v(2,:))*sc(l, v(4,:)) + bCoeff_ij(9)*sc(l, v(5,:))**2


if (sqDen) then
  !print *, 'bFunc before: ', b_Func
  den = getDenominators(l, pa1, pa2, pa3)
  alpha1 = bCoeff_ij(1)/( -two * sc(v(2,:), diag(i,8:13) - diag(i,1:6)) )
  b_Func = b_Func - alpha1 * (den(i,1) - den(i,3)) * sc(v(1,:),diag(i,8:13) - diag(i,1:6))
  !print *, 'bFunc after: ', b_Func
end if

! else if (j == 3) then
! b_Func = bCoeff_ij(10) + bCoeff_ij(1)*sc(l, v(2,:)) + bCoeff_ij(2)*sc(l, v(3,:)) + bCoeff_ij(3)*sc(l, v(4,:))
! b_Func = b_Func + bCoeff_ij(4)*(sc(l, v(2,:))**2 - sc(l,v(4,:))**2) + bCoeff_ij(5)*(sc(l, v(3,:))**2 - sc(l,v(4,:))**2)
! b_Func = b_Func + bCoeff_ij(6)*sc(l, v(2,:))*sc(l, v(3,:)) + bCoeff_ij(7)*sc(l, v(3,:))*sc(l, v(4,:))
! b_Func = b_Func + bCoeff_ij(8)*sc(l, v(2,:))*sc(l, v(4,:)) + bCoeff_ij(9)*sc(l, v(5,:))**2

! if (sqDen) then
!   den = getDenominators(l, pa1, pa2, pa3)
!   alpha1 = bCoeff_ij(1)/( -two * sc(v(2,:), diag(i,15:20) - diag(i,8:13)) )
!   b_Func = b_Func - alpha1 * (den(i,3) - den(i,2)) * sc(v(1,:),diag(i,15:20) - diag(i,8:13))
! end if 

end if

getBubbleFunction_ij = b_Func

end function getBubbleFunction_ij

end module