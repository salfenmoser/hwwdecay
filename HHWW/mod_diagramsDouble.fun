use mod_auxfunctions
use mod_consts_dp
use mod_kinematics
use mod_vertices
use mod_numeratorFunctions


test_suite mod_bubble
  complex(dp) :: pa1(3,dimP), pa2(3,dimP), paH(3,dimP)
  integer :: pol1, pol2, c1, c2

!------------------------------------------------------------------------------------------------------------
setup

pol1 = 1
pol2 = 1
c1 = 1
c2 = 1
call particlesHWW(pol1, pol2, c1, c2, paH, pa1, pa2)

end setup
!------------------------------------------------------------------------------------------------------------

teardown
  ! This code runs immediately after each test
end teardown

!----------------------------------------------------------------------------------
test BReduction
integer :: D_S, i, j, k
complex(dp) :: b(10), bubbleCoeff(10)

b(:) = czero
bubbleCoeff(:) = czero

b(1) = one - ci
b(2) = two + two*ci
b(3) = -one/two
b(4) = czero
b(5) = three/four
b(6) = ci
b(7) = -ci
b(8) = four*ci
b(9) = -five
b(10) = mh

do D_S = 4,6
do i = 1,6
j = 1
bubbleCoeff(:) = czero
bubbleCoeff = getBubbleCoefficients_ij(D_S, paH, pa1, pa2, i, j, .true., b)
! print *, bubbleCoeff(9)
! print *, '--------'

do k = 1, 8
assert_equal_within(REAL(bubbleCoeff(k)), REAL(b(k)), limitAssert)
assert_equal_within(aimag(bubbleCoeff(k)), AIMAG(b(k)), limitAssert)
end do

if (D_S > 4) then
assert_equal_within(REAL(bubbleCoeff(9)), REAL(b(9)), limitAssert)
assert_equal_within(aimag(bubbleCoeff(9)), AIMAG(b(9)), limitAssert)
end if

assert_equal_within(REAL(bubbleCoeff(10)), REAL(b(10)), limitAssert)
assert_equal_within(aimag(bubbleCoeff(10)), AIMAG(b(10)), limitAssert)

end do
end do

do D_S = 4,6
do i = 1,3, 2
j = 2
bubbleCoeff(:) = czero
bubbleCoeff = getBubbleCoefficients_ij(D_S, paH, pa1, pa2, i, j, .true., b)
  
do k = 1, 8
assert_equal_within(REAL(bubbleCoeff(k)), REAL(b(k)), limitAssert)
assert_equal_within(aimag(bubbleCoeff(k)), AIMAG(b(k)), limitAssert)
end do
  
if (D_S > 4) then
assert_equal_within(REAL(bubbleCoeff(9)), REAL(b(9)), limitAssert)
assert_equal_within(aimag(bubbleCoeff(9)), AIMAG(b(9)), limitAssert)
end if
  
assert_equal_within(REAL(bubbleCoeff(10)), REAL(b(10)), limitAssert)
assert_equal_within(aimag(bubbleCoeff(10)), AIMAG(b(10)), limitAssert)
  
end do
end do

end test BReduction
!-------------------------------------------------------------------------------------------------

test momentaBubble1
  integer :: i, j, D_S, m, n, k
  complex(dp) :: l1(3,3,2,dimP), l2(2,2,2,dimP), lAux(4,dimP), lEps(dimP), den(6,3)
  complex(dp) :: x, y

do i = 1,6
do D_S = 4, 6

j = 1
call getBubbleMomenta(D_S, paH, pa1, pa2, i, j, l1, l2, lAux, lEps, x, y)

do m = 1, 3
do n = 1, 3

  den = getDenominators(l1(m,n,1,:), paH, pa1, pa2)
  assert_equal_within(abs(den(i, 1)), zero, limitAssert)
  assert_equal_within(abs(den(i, 2)), zero, limitAssert)
  den = getDenominators(l1(m,n,2,:), paH, pa1, pa2)
  assert_equal_within(abs(den(i, 1)), zero, limitAssert)
  assert_equal_within(abs(den(i, 2)), zero, limitAssert)
  ! if (i==3 .AND. m==1 .AND. n ==1 ) then
  !   print *, den(i,:)
  ! end if
end do
end do

do m = 1, 2
  do n = 1, 2
    
    den = getDenominators(l2(m,n,1,:), paH, pa1, pa2)
    assert_equal_within(abs(den(i, 1)), zero, limitAssert)
    assert_equal_within(abs(den(i, 2)), zero, limitAssert)
    den = getDenominators(l2(m,n,2,:), paH, pa1, pa2)
    assert_equal_within(abs(den(i, 1)), zero, limitAssert)
    assert_equal_within(abs(den(i, 2)), zero, limitAssert)
end do
end do

do m = 1, 4
  den = getDenominators(lAux(m,:), paH, pa1, pa2)
  assert_equal_within(abs(den(i, 1)), zero, limitAssert)
  assert_equal_within(abs(den(i, 2)), zero, limitAssert)
end do

if (D_S > 4) then
den = getDenominators(lEps, paH, pa1, pa2)
assert_equal_within(abs(den(i, 1)), zero, limitAssert)
assert_equal_within(abs(den(i, 2)), zero, limitAssert)
end if

end do
end do

end test momentaBubble1
!------------------------------------------------------------------------------------------------------------


test momentaBubble2
  integer :: i, j, D_S, m, n, k
  complex(dp) :: l1(3,3,2,dimP), l2(2,2,2,dimP), lAux(4,dimP), lEps(dimP), den(6,3)
  complex(dp) :: x, y

do i = 1, 3, 2
do D_S = 4, 6

j = 2
call getBubbleMomenta(D_S, paH, pa1, pa2, i, j, l1, l2, lAux, lEps, x, y)

do m = 1, 3
do n = 1, 3

  den = getDenominators(l1(m,n,1,:), paH, pa1, pa2)
  assert_equal_within(abs(den(i, 1)), zero, limitAssert)
  assert_equal_within(abs(den(i, 3)), zero, limitAssert)
  den = getDenominators(l1(m,n,2,:), paH, pa1, pa2)
  assert_equal_within(abs(den(i, 1)), zero, limitAssert)
  assert_equal_within(abs(den(i, 3)), zero, limitAssert)
end do
end do

do m = 1, 2
  do n = 1, 2
    
    den = getDenominators(l2(m,n,1,:), paH, pa1, pa2)
    assert_equal_within(abs(den(i, 1)), zero, limitAssert)
    assert_equal_within(abs(den(i, 3)), zero, limitAssert)
    den = getDenominators(l2(m,n,2,:), paH, pa1, pa2)
    assert_equal_within(abs(den(i, 1)), zero, limitAssert)
    assert_equal_within(abs(den(i, 3)), zero, limitAssert)
end do
end do

do m = 1, 4
  den = getDenominators(lAux(m,:), paH, pa1, pa2)
  assert_equal_within(abs(den(i, 1)), zero, limitAssert)
  assert_equal_within(abs(den(i, 3)), zero, limitAssert)
end do

if (D_S > 4) then
den = getDenominators(lEps, paH, pa1, pa2)
assert_equal_within(abs(den(i, 1)), zero, limitAssert)
assert_equal_within(abs(den(i, 3)), zero, limitAssert)
end if

end do
end do

end test momentaBubble2

!------------------------------------------------------------------------------------------------------------

test bubble1NumeratorFunction
  integer :: D_S, j, m, n, k, i
  complex(dp) :: numFunc(6,12), numFuncMink1(6,12), numFuncMink2(6,12), bubbleFunc(6)
  complex(dp) :: numSum(6), den(6,6,3), denMink1(6,6,3), denMink2(6,6,3)
  complex(dp) :: l1(3,3,2,dimP), l2(2,2,2,dimP), lAux(4,dimP), lEps(dimP), x, y


do D_S = 4,6

do m = 1,3
do n = 1,3
do k = 1,2
numSum(:) = czero
numFunc(:,:) = czero
bubbleFunc(:) = czero

do i = 3,6
call getBubbleMomenta(D_S, paH, pa1, pa2, i, 1, l1, l2, lAux, lEps, x, y)
den(i,:,:) = getDenominators(l1(m,n,k,:), paH, pa1, pa2)
denMink1(i,:,:) = getDenominators(l1(m,n,k,:) - pa1(1,:), paH, pa1, pa2)
denMink2(i,:,:) = getDenominators(l1(m,n,k,:) - pa2(1,:), paH, pa1, pa2)

bubbleFunc(i) = getBubbleAmps(D_S, l1(m,n,k,:), paH, pa1, pa2, i, 1, .true.)

do j = 3,12
numFunc(i,j) = numerator(paH, pa1, pa2, l1(m,n,k,:), j, D_S)
numFuncMink2(i,j) = numerator(paH, pa1, pa2, l1(m,n,k,:) - pa2(1,:), j, D_S)
numFuncMink1(i,j) = numerator(paH, pa1, pa2, l1(m,n,k,:) - pa1(1,:), j, D_S)

!print *, 'numFunc j = ', j, numFunc(i,j)
end do

end do

numSum(3) = numFunc(3,3)/den(3,3,3) + numFuncMink2(3,6)/denMink2(3,6,1) + numFunc(3,9)
! print *, numFunc(3,3)/den(3,3,3)
! print *, numFuncMink1(3,6)/denMink1(3,6,1)
! print *, numFunc(3,9)
! print *, numSum(3)
! print *, bubbleFunc(3)
assert_equal_within(abs(numSum(3)/bubbleFunc(3)), 1, limitAssert)

numSum(4) = numFunc(4,4)/den(4,4,3) + numFuncMink1(4,5)/denMink1(4,5,1) + numFunc(4,10)
! print *, numFunc(4,4)/den(4,4,3)
! print *, numFuncMink1(4,5)/denMink1(4,5,1)
! print *, numFunc(4,10)
! print *, numSum(4)
! print *, bubbleFunc(4)
assert_equal_within(abs(numSum(4)/bubbleFunc(4)), 1, limitAssert)

numSum(5) = numFunc(5,5)/den(5,5,3) + numFuncMink2(5,4)/denMink2(5,4,1) + numFunc(5,11)
assert_equal_within(abs(numSum(5)/bubbleFunc(5)), 1, limitAssert)

numSum(6) = numFunc(6,6)/den(6,6,3) + numFuncMink1(6,3)/denMink1(6,3,1) + numFunc(6,12)
assert_equal_within(abs(numSum(6)/bubbleFunc(6)), 1, limitAssert)
end do
end do
end do

do m = 1,2
do n = 1,2
do k = 1,2
  numSum(:) = czero
  numFunc(:,:) = czero
  bubbleFunc(:) = czero
  
do i = 3,6
  call getBubbleMomenta(D_S, paH, pa1, pa2, i, 1, l1, l2, lAux, lEps, x, y)
  den(i,:,:) = getDenominators(l2(m,n,k,:), paH, pa1, pa2)
  denMink1(i,:,:) = getDenominators(l2(m,n,k,:) - pa1(1,:), paH, pa1, pa2)
  denMink2(i,:,:) = getDenominators(l2(m,n,k,:) - pa2(1,:), paH, pa1, pa2)
  
  bubbleFunc(i) = getBubbleAmps(D_S, l2(m,n,k,:), paH, pa1, pa2, i, 1, .true.)
    
  do j = 3,12
  numFunc(i,j) = numerator(paH, pa1, pa2, l2(m,n,k,:), j, D_S)
  numFuncMink2(i,j) = numerator(paH, pa1, pa2, l2(m,n,k,:) - pa2(1,:), j, D_S)
  numFuncMink1(i,j) = numerator(paH, pa1, pa2, l2(m,n,k,:) - pa1(1,:), j, D_S)
  end do
    
  end do
    
  numSum(3) = numFunc(3,3)/den(3,3,3) + numFuncMink2(3,6)/denMink2(3,6,1) + numFunc(3,9)
  assert_equal_within(abs(numSum(3)/bubbleFunc(3)), 1, limitAssert)
    
  numSum(4) = numFunc(4,4)/den(4,4,3) + numFuncMink1(4,5)/denMink1(4,5,1) + numFunc(4,10)
  assert_equal_within(abs(numSum(4)/bubbleFunc(4)), 1, limitAssert)
    
  numSum(5) = numFunc(5,5)/den(5,5,3) + numFuncMink2(5,4)/denMink2(5,4,1) + numFunc(5,11)
  assert_equal_within(abs(numSum(5)/bubbleFunc(5)), 1, limitAssert)
    
  numSum(6) = numFunc(6,6)/den(6,6,3) + numFuncMink1(6,3)/denMink1(6,3,1) + numFunc(6,12)
  assert_equal_within(abs(numSum(6)/bubbleFunc(6)), 1, limitAssert)
end do
end do
end do

do m = 1,4
numSum(:) = czero
numFunc(:,:) = czero
bubbleFunc(:) = czero
  
do i = 3,6
  call getBubbleMomenta(D_S, paH, pa1, pa2, i, 1, l1, l2, lAux, lEps, x, y)
  den(i,:,:) = getDenominators(lAux(m,:), paH, pa1, pa2)
  denMink1(i,:,:) = getDenominators(lAux(m,:) - pa1(1,:), paH, pa1, pa2)
  denMink2(i,:,:) = getDenominators(lAux(m,:) - pa2(1,:), paH, pa1, pa2)
  
  bubbleFunc(i) = getBubbleAmps(D_S, lAux(m,:), paH, pa1, pa2, i, 1, .true.)
    
  do j = 3,12
  numFunc(i,j) = numerator(paH, pa1, pa2, lAux(m,:), j, D_S)
  numFuncMink2(i,j) = numerator(paH, pa1, pa2, lAux(m,:) - pa2(1,:), j, D_S)
  numFuncMink1(i,j) = numerator(paH, pa1, pa2, lAux(m,:) - pa1(1,:), j, D_S)
  end do
    
end do
    
numSum(3) = numFunc(3,3)/den(3,3,3) + numFuncMink2(3,6)/denMink2(3,6,1) + numFunc(3,9)
assert_equal_within(abs(numSum(3)/bubbleFunc(3)), 1, limitAssert)
    
numSum(4) = numFunc(4,4)/den(4,4,3) + numFuncMink1(4,5)/denMink1(4,5,1) + numFunc(4,10)
assert_equal_within(abs(numSum(4)/bubbleFunc(4)), 1, limitAssert)
  
numSum(5) = numFunc(5,5)/den(5,5,3) + numFuncMink2(5,4)/denMink2(5,4,1) + numFunc(5,11)
assert_equal_within(abs(numSum(5)/bubbleFunc(5)), 1, limitAssert)
    
numSum(6) = numFunc(6,6)/den(6,6,3) + numFuncMink1(6,3)/denMink1(6,3,1) + numFunc(6,12)
assert_equal_within(abs(numSum(6)/bubbleFunc(6)), 1, limitAssert)
end do

if (D_S > 4) then
numSum(:) = czero
numFunc(:,:) = czero
bubbleFunc(:) = czero

do i = 3,6
  call getBubbleMomenta(D_S, paH, pa1, pa2, i, 1, l1, l2, lAux, lEps, x, y)
  den(i,:,:) = getDenominators(lEps, paH, pa1, pa2)
  denMink1(i,:,:) = getDenominators(lEps - pa1(1,:), paH, pa1, pa2)
  denMink2(i,:,:) = getDenominators(lEps - pa2(1,:), paH, pa1, pa2)
  
  bubbleFunc(i) = getBubbleAmps(D_S, lEps, paH, pa1, pa2, i, 1, .true.)
    
  do j = 3,12
  numFunc(i,j) = numerator(paH, pa1, pa2, lEps, j, D_S)
  numFuncMink2(i,j) = numerator(paH, pa1, pa2, lEps - pa2(1,:), j, D_S)
  numFuncMink1(i,j) = numerator(paH, pa1, pa2, lEps - pa1(1,:), j, D_S)
  end do
    
end do
    
numSum(3) = numFunc(3,3)/den(3,3,3) + numFuncMink2(3,6)/denMink2(3,6,1) + numFunc(3,9)
assert_equal_within(abs(numSum(3)/bubbleFunc(3)), 1, limitAssert)
    
numSum(4) = numFunc(4,4)/den(4,4,3) + numFuncMink1(4,5)/denMink1(4,5,1) + numFunc(4,10)
assert_equal_within(abs(numSum(4)/bubbleFunc(4)), 1, limitAssert)
  
numSum(5) = numFunc(5,5)/den(5,5,3) + numFuncMink2(5,4)/denMink2(5,4,1) + numFunc(5,11)
assert_equal_within(abs(numSum(5)/bubbleFunc(5)), 1, limitAssert)
    
numSum(6) = numFunc(6,6)/den(6,6,3) + numFuncMink1(6,3)/denMink1(6,3,1) + numFunc(6,12)
assert_equal_within(abs(numSum(6)/bubbleFunc(6)), 1, limitAssert)
end if

end do

end test bubble1NumeratorFunction


!------------------------------------------------------------------------------------------------------------

! test bubble2NumeratorFunctionDiag1
!   integer :: D_S, j, m, n, k
!   complex(dp) :: numFunc(12), bubbleFunc, den(6,3), numSum
!   complex(dp) :: l1(3,3,2,dimP), l2(2,2,2,dimP), lAux(4,dimP), lEps(dimP), x, y

! numFunc(:) = czero
! call getBubbleMomenta(D_S, paH, pa1, pa2, 1, 2, l1, l2, lAux, lEps, x, y)

! do D_S = 4,4

! do m = 1,1
! do n = 1,1
!   do k = 1,1
! numSum = czero
! numFunc(:) = czero
! bubbleFunc = czero

! numFunc(1) = numerator(paH, pa1, pa2, l1(m,n,k,:), 1, D_S)
! numFunc(2) = numerator(paH, pa1, pa2, l1(m,n,k,:), 2, D_S)
! numFunc(5) = numerator(paH, pa1, pa2, l1(m,n,k,:), 5, D_S)
! numFunc(6) = numerator(paH, pa1, pa2, l1(m,n,k,:), 6, D_S)
! numFunc(7) = numerator(paH, pa1, pa2, l1(m,n,k,:), 7, D_S)
! bubbleFunc = getBubbleAmps(D_S, l1(m,n,k,:), paH, pa1, pa2, 1, 2, .true.)
! den = getDenominators(l1(m,n,k,:), paH, pa1, pa2)

! numSum = numFunc(1)/den(1,2) + numFunc(2)/den(2,2) + numFunc(5)/den(5,2) + numFunc(6)/den(6,2) + numFunc(7)

! print *, '--------------'
! print *, numFunc(1)/den(1,2)
! print *, numFunc(2)/den(2,2)
! print *, numFunc(5)/den(5,2) ! passt
! print *, numFunc(6)/den(6,2) !passt
! print *, numFunc(7) !passt
! print *, 'bubble func = ', bubbleFunc

! !assert_equal_within(abs(numSum/bubbleFunc), 1, limitAssert)
!   end do
! end do
! end do
! print *, '--------------'


! do m = 1,2
!   do n = 1,2
!     do k = 1,2
!   numSum = czero
!   numFunc(:) = czero
!   bubbleFunc = czero
  
!   numFunc(3) = numerator(paH, pa1, pa2, l2(m,n,k,:), 3, D_S)
!   numFunc(4) = numerator(paH, pa1, pa2, l2(m,n,k,:), 4, D_S)
!   numFunc(8) = numerator(paH, pa1, pa2, l2(m,n,k,:), 8, D_S)
!   bubbleFunc = getBubbleAmps(D_S, l2(m,n,k,:), paH, pa1, pa2, 3, 2, .true.)
!   den = getDenominators(l2(m,n,k,:), paH, pa1, pa2)
!   numSum = numFunc(3)/den(3,2) + numFunc(4)/den(4,2) + numFunc(8)
!   assert_equal_within(abs(numSum/bubbleFunc), 1, limitAssert)
!     end do
!   end do
!   end do

! do m = 1,4
! numSum = czero
! numFunc(:) = czero
! bubbleFunc = czero

! numFunc(3) = numerator(paH, pa1, pa2, lAux(m,:), 3, D_S)
! numFunc(4) = numerator(paH, pa1, pa2, lAux(m,:), 4, D_S)
! numFunc(8) = numerator(paH, pa1, pa2, lAux(m,:), 8, D_S)
! bubbleFunc = getBubbleAmps(D_S, lAux(m,:), paH, pa1, pa2, 3, 2, .true.)
! den = getDenominators(lAux(m,:), paH, pa1, pa2)

! numSum = numFunc(3)/den(3,2) + numFunc(4)/den(4,2) + numFunc(8)
! assert_equal_within(abs(numSum/bubbleFunc), 1, limitAssert)
! end do

! if (D_S > 4) then
! numSum = czero
! numFunc(:) = czero
! bubbleFunc = czero

! numFunc(3) = numerator(paH, pa1, pa2, lEps, 3, D_S)
! numFunc(4) = numerator(paH, pa1, pa2, lEps, 4, D_S)
! numFunc(8) = numerator(paH, pa1, pa2, lEps, 8, D_S)
! bubbleFunc = getBubbleAmps(D_S, lEps, paH, pa1, pa2, 3, 2, .true.)
! den = getDenominators(lEps, paH, pa1, pa2)

! numSum = numFunc(3)/den(3,2) + numFunc(4)/den(4,2) + numFunc(8)
! print *, numSum
! !print *, bubbleFunc
! assert_equal_within(abs(numSum/bubbleFunc), 1, limitAssert)
! end if

! end do

! end test bubble2NumeratorFunctionDiag1
!------------------------------------------------------------------------------------------------------------


test bubble2NumeratorFunctionDiag3
  integer :: D_S, j, m, n, k
  complex(dp) :: numFunc(12), bubbleFunc, den(6,3), numSum
  complex(dp) :: l1(3,3,2,dimP), l2(2,2,2,dimP), lAux(4,dimP), lEps(dimP), x, y

numFunc(:) = czero
call getBubbleMomenta(D_S, paH, pa1, pa2, 3, 2, l1, l2, lAux, lEps, x, y)

do D_S = 4,6

do m = 1,3
do n = 1,3
  do k = 1,2
numSum = czero
numFunc(:) = czero
bubbleFunc = czero

numFunc(3) = numerator(paH, pa1, pa2, l1(m,n,k,:), 3, D_S)
numFunc(4) = numerator(paH, pa1, pa2, l1(m,n,k,:), 4, D_S)
numFunc(8) = numerator(paH, pa1, pa2, l1(m,n,k,:), 8, D_S)
bubbleFunc = getBubbleAmps(D_S, l1(m,n,k,:), paH, pa1, pa2, 3, 2, .true.)
den = getDenominators(l1(m,n,k,:), paH, pa1, pa2)
numSum = numFunc(3)/den(3,2) + numFunc(4)/den(4,2) + numFunc(8)
assert_equal_within(abs(numSum/bubbleFunc), 1, limitAssert)
  end do
end do
end do

do m = 1,2
  do n = 1,2
    do k = 1,2
  numSum = czero
  numFunc(:) = czero
  bubbleFunc = czero
  
  numFunc(3) = numerator(paH, pa1, pa2, l2(m,n,k,:), 3, D_S)
  numFunc(4) = numerator(paH, pa1, pa2, l2(m,n,k,:), 4, D_S)
  numFunc(8) = numerator(paH, pa1, pa2, l2(m,n,k,:), 8, D_S)
  bubbleFunc = getBubbleAmps(D_S, l2(m,n,k,:), paH, pa1, pa2, 3, 2, .true.)
  den = getDenominators(l2(m,n,k,:), paH, pa1, pa2)
  numSum = numFunc(3)/den(3,2) + numFunc(4)/den(4,2) + numFunc(8)
  assert_equal_within(abs(numSum/bubbleFunc), 1, limitAssert)
    end do
  end do
  end do

do m = 1,4
numSum = czero
numFunc(:) = czero
bubbleFunc = czero

numFunc(3) = numerator(paH, pa1, pa2, lAux(m,:), 3, D_S)
numFunc(4) = numerator(paH, pa1, pa2, lAux(m,:), 4, D_S)
numFunc(8) = numerator(paH, pa1, pa2, lAux(m,:), 8, D_S)
bubbleFunc = getBubbleAmps(D_S, lAux(m,:), paH, pa1, pa2, 3, 2, .true.)
den = getDenominators(lAux(m,:), paH, pa1, pa2)

numSum = numFunc(3)/den(3,2) + numFunc(4)/den(4,2) + numFunc(8)
assert_equal_within(abs(numSum/bubbleFunc), 1, limitAssert)
end do

if (D_S > 4) then
numSum = czero
numFunc(:) = czero
bubbleFunc = czero

numFunc(3) = numerator(paH, pa1, pa2, lEps, 3, D_S)
numFunc(4) = numerator(paH, pa1, pa2, lEps, 4, D_S)
numFunc(8) = numerator(paH, pa1, pa2, lEps, 8, D_S)
bubbleFunc = getBubbleAmps(D_S, lEps, paH, pa1, pa2, 3, 2, .true.)
den = getDenominators(lEps, paH, pa1, pa2)

numSum = numFunc(3)/den(3,2) + numFunc(4)/den(4,2) + numFunc(8)
print *, numSum
!print *, bubbleFunc
assert_equal_within(abs(numSum/bubbleFunc), 1, limitAssert)
end if

end do

end test bubble2NumeratorFunctionDiag3

end test_suite
