use mod_auxfunctions
use mod_consts_dp
use mod_kinematics
use mod_numeratorFunctions


test_suite mod_triangle
  complex(dp) :: pa1(3,dimP), pa2(3,dimP), paH(3,dimP)
  integer :: pol1, pol2, c1, c2
  
!------------------------------------------------------------------------------------------------------------
setup

pol1 = 1
pol2 = 1
c1 = 2
c2 = 2
call particlesHWW(pol1, pol2, c1, c2, paH, pa1, pa2)

end setup

!------------------------------------------------------------------------------------------------------------
teardown
end teardown
!------------------------------------------------------------------------------------------------------------

test CReduction
integer :: D_S, i, k
complex(dp) :: c(10), triangleCoeff(10)

c(:) = czero
triangleCoeff(:) = czero

c(1) = one
c(2) = two
c(3) = -one/two
c(4) = czero
c(5) = three/four
c(6) = ci
c(7) = -ci
c(8) = four*ci
c(9) = -five
c(10) = mh

do D_S = 4,6
do i = 1,4
  triangleCoeff(:) = czero
triangleCoeff = getTriangleCoefficients_i(D_S, paH, pa1, pa2, i, .true., c)
!print *, triangleCoeff(7:9)

do k = 1,6
assert_equal_within(REAL(triangleCoeff(k)), REAL(c(k)), limitAssert)
assert_equal_within(aimag(triangleCoeff(k)), AIMAG(c(k)), limitAssert)
end do

if (D_S > 4) then
do k = 7,9
assert_equal_within(REAL(triangleCoeff(k)), REAL(c(k)), limitAssert)
assert_equal_within(aimag(triangleCoeff(k)), AIMAG(c(k)), limitAssert)
end do
end if

assert_equal_within(REAL(triangleCoeff(10)), REAL(c(10)), limitAssert)
assert_equal_within(aimag(triangleCoeff(10)), AIMAG(c(10)), limitAssert)

end do
end do

end test CReduction
!------------------------------------------------------------------------------------------------------------

test triangleNumeratorFunction
  integer :: i, D_S, m, n
  complex(dp) :: numFunc, triangleFunc, triangleMomenta4D(7,7,dimP), triangleMomenta5_6D(4,dimP)

do D_S = 4,6
do i = 3,6

triangleMomenta4D(:,:,:) = czero
triangleMomenta4D = getTriangleMomenta4D(D_S, paH, pa1, pa2, i)

do m = 1,7
do n = 1,7
numFunc = numerator(paH, pa1, pa2, triangleMomenta4D(m,n,:), i, D_S)
triangleFunc = C_ijk(D_S, triangleMomenta4D(m,n,:), paH, pa1, pa2, i, .false., c_Test())
assert_equal_within(abs(numFunc/triangleFunc), 1, limitAssert)
end do
end do

if (D_S > 4) then
triangleMomenta5_6D(:,:) = czero
triangleMomenta5_6D = getTriangleMomenta5_6D(D_S, paH, pa1, pa2, i)

do m = 1,4
  numFunc = numerator(paH, pa1, pa2, triangleMomenta5_6D(m,:), i, D_S)
  triangleFunc = C_ijk(D_S, triangleMomenta5_6D(m,:), paH, pa1, pa2, i, .false., c_Test())
  assert_equal_within(abs(numFunc/triangleFunc), 1, limitAssert)
end do
end if

end do
end do

end test triangleNumeratorFunction

!------------------------------------------------------------------------------------------------------------
test momentaTriple
  integer :: i, D_S, m, n, j
  complex(dp) :: triangleMomenta4D(7,7,dimP), triangleMomenta5_6D(4,dimP), den(6,3)

triangleMomenta4D(:,:,:) = czero
triangleMomenta5_6D(:,:) = czero

do i = 1,6
D_S = 4
triangleMomenta4D = getTriangleMomenta4D(D_S, paH, pa1, pa2, i)

do m = 1, 7
do n = 1, 7
  den = getDenominators(triangleMomenta4D(m,n,:), paH, pa1, pa2)
  do j = 1, 3
    assert_equal_within(abs(den(i, j)), zero, limitAssert)
  end do
end do
end do

do D_S = 5,6
triangleMomenta5_6D = getTriangleMomenta5_6D(D_S, paH, pa1, pa2, i)

do m = 1,4
  den = getDenominators(triangleMomenta5_6D(m,:), paH, pa1, pa2)
  do j = 1, 3
    assert_equal_within(abs(den(i, j)), zero, limitAssert)
  end do
end do
end do

end do

end test momentaTriple

end test_suite