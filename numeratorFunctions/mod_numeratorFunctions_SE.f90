module mod_numeratorFunctionsSE

use mod_types
use mod_consts_dp
use common_def
  
use mod_auxfunctions
use mod_vertices
use mod_propagators
use mod_kinematics

implicit none
  
public :: d1W, d2W, d3W, d4W
public :: d1H, d2H, d3H, d4H
public ::numeratorSE

contains


function d1W(w, l)
  complex(dp), intent(in) ::w(3,dimP), l(dimP)
  complex(dp) :: d1W

d1W = czero
end function d1W

function d2W(w, l)
  complex(dp), intent(in) ::w(3,dimP), l(dimP)
  complex(dp) :: d2W
  complex(dp) ::polWPlus(dimE,dimE), polWMinus(dimE,dimE)
  integer :: pol

d2W = czero
d2W = d2W + V_hw2(1,1)**2 * ( sq(w(2,:)) - (sc(w(2,:), l-w(1,:))**2)/mwsq )

!print *, 'd2W = ', d2W
! call spinonepol_6d(w(1,:),polWPlus)
! call spinonepol_6d(w(1,:),polWMinus)
! pol = 3
! print *, 'scalar product pol+ * l = ', sc(polWPlus(pol,:), l)
! print *, 'scalar product pol- * l = ', sc(polWMinus(pol,:), l)
end function d2W

function d3W(w, l)
  complex(dp), intent(in) ::w(3,dimP), l(dimP)
  complex(dp) :: d3W

d3W = czero
end function d3W

function d4W(w, l)
  complex(dp), intent(in) ::w(3,dimP), l(dimP)
  complex(dp) :: d4W

d4W = czero
d4W = ci * V_h2w2( int(w(3,1)), int(w(3,1)) ) * sq(w(2,:))
end function d4W


function d1H(h, l, D_S)
  complex(dp), intent(in) ::h(3,dimP), l(dimP)
  integer, intent(in) :: D_S
  complex(dp) :: d1H
  integer :: i1

d1H = czero
do i1 = 1,3
d1H = d1H - V_hw2(i1, i1)**2 * ( D_S - sq(l-h(1,:))/mwsq - sq(l)/mwsq + (sc(l, l-h(1,:))**2)/mwsq**2 )
end do
end function d1H


function d2H(h, l)
  complex(dp), intent(in) ::h(3,dimP), l(dimP)
  complex(dp) :: d2H

d2H = czero
d2H = - V_h3()**2
end function d2H


function d3H(h, l, D_S)
  complex(dp), intent(in) ::h(3,dimP), l(dimP)
  integer, intent(in) :: D_S
  complex(dp) :: d3H
  integer :: i1

d3H = czero
do i1 = 1,3
d3H = d3H - ci * V_h2w2(i1,i1) * (D_S - sq(l)/mwsq)
end do
end function d3H


function d4H(h, l)
  complex(dp), intent(in) ::h(3,dimP), l(dimP)
  complex(dp) :: d4H

d4H = czero
d4H = ci * V_h4()
end function d4H

function numeratorSE(particle, l, i, D_S)
  complex(dp), intent(in) :: particle(3,dimP), l(dimP)
  integer, intent(in) :: i, D_S
  complex(dp) :: numeratorSE
  logical :: isW
numeratorSE = czero

isW = .true.
if (int(particle(3,1)) == 0 ) isW = .false.

!print *, isW, i

if (isW .and. i == 1) then
  numeratorSE = d1W(particle, l)
else if (isW .and. i == 2) then
  numeratorSE = d2W(particle, l)
else if (isW .and. i == 3) then
    numeratorSE = d3W(particle, l)
else if (isW .and. i == 4) then
  numeratorSE = d4W(particle, l)
else if (i == 1) then
  numeratorSE = d1H(particle, l, D_S)
else if (i == 2) then
  numeratorSE = d2H(particle, l)
else if (i == 3) then
  numeratorSE = d3H(particle, l, D_S)
else if (i == 4) then
  numeratorSE = d4H(particle, l)
end if

end function numeratorSE

end module