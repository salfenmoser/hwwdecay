module mod_numeratorFunctions

use mod_types
use mod_consts_dp
use common_def
  
use mod_auxfunctions
use mod_vertices
use mod_propagators

implicit none
  
public :: d3, d4, d5, d6
public :: d8, d9, d10, d11, d12
public ::numerator

contains

! function d1(h, w1, w2, l, D_S)
!   complex(dp), intent(in) :: h(3,dimP), w1(3,dimP), w2(3,dimP), l(dimP)
!   integer, intent(in) :: D_S
!   integer :: i1, i2, i3
!   complex(dp) :: a(2,dimP), d1

! d1 = czero

! do i1 = 1,3
! do i2 = 1,3

! a(1,:) = V_hw2(i1,i1) * l
! a(2,:) = V_w3(i2,-l+h(1,:),-WP(a(1,:)),i1,w2(1,:),w2(2,:),int(w2(3,1)))
! d1 = d1 + V_w3(i1,-l-w1(1,:),-WP(a(2,:)),i2,w1(1,:),w1(2,:),int(w1(3,1))) 
! end do
! end do
! d1 = d1 * V_hw2(i2,i2)
! end function d1

function d3(h, w1, w2, l)
  complex(dp), intent(in) :: h(3,dimP), w1(3,dimP), w2(3,dimP), l(dimP)
  complex(dp) :: d3

d3 = czero
d3 = ci * V_h3() * V_hw2(int(w1(3,1)),int(w2(3,1)))**two
d3 = d3 * (sc(w1(2,:),w2(2,:)) - sc(w1(2,:), l+w1(1,:))*sc(w2(2,:), l+w1(1,:))/mwsq)
end function d3

function d4(h, w1, w2, l)
  complex(dp), intent(in) :: h(3,dimP), w1(3,dimP), w2(3,dimP), l(dimP)
  complex(dp) :: d4

d4 = czero
d4 = ci * V_h3() * V_hw2(int(w1(3,1)),int(w2(3,1)))**two
d4 = d4 * (sc(w1(2,:),w2(2,:)) - sc(w1(2,:), l+w2(1,:))*sc(w2(2,:), l+w2(1,:))/mwsq)
end function d4

function d5(h, w1, w2, l)
  complex(dp), intent(in) :: h(3,dimP), w1(3,dimP), w2(3,dimP), l(dimP)
  complex(dp) :: d5

d5 = czero
d5 = -ci * V_hw2(int(w1(3,1)),int(w2(3,1)))**three * ( sc(w1(2,:),w2(2,:)) - ( sc(w1(2,:),l) * sc(w2(2,:), l) &
  + sc(w1(2,:), l-h(1,:)) * sc(w2(2,:), l-h(1,:)) )/mwsq + ( sc(w2(2,:), l-h(1,:)) * sc(l, l-h(1,:)) * sc(w1(2,:),l) )/mwsq**two )
end function d5

function d6(h, w1, w2, l)
  complex(dp), intent(in) :: h(3,dimP), w1(3,dimP), w2(3,dimP), l(dimP)
  complex(dp) :: d6

d6 = czero
d6 = -ci * V_hw2(int(w1(3,1)),int(w2(3,1)))**three * ( sc(w1(2,:),w2(2,:)) - ( sc(w1(2,:),l) * sc(w2(2,:), l) &
  + sc(w1(2,:), l-h(1,:)) * sc(w2(2,:), l-h(1,:)) )/mwsq + ( sc(w1(2,:), l-h(1,:)) * sc(l, l-h(1,:)) * sc(w2(2,:),l) )/mwsq**two )
end function d6

function d7(h, w1, w2, l, D_S)
  complex(dp), intent(in) :: h(3,dimP), w1(3,dimP), w2(3,dimP), l(dimP)
  integer, intent(in) :: D_S
  integer :: i1, i2
  complex(dp) :: a, d7

d7 = czero
a = (1 - D_S + sq(l-h(1,:))/mwsq + sq(l)/mwsq - (sc(l,l-h(1,:))**2)/mwsq**2)

do i1 = 1,3
  do i2 = 1,3

d7 = d7 + sc(w1(2,:), w2(2,:)) * a * (levicevita(i1,i2,int(w2(3,1)))*levicevita(i1,int(w1(3,1)),i2) - &
      levicevita(i1,i2,int(w1(3,1)))*levicevita(i1,i2,int(w2(3,1))) )
d7 = d7 + levicevita(i1,i2,int(w2(3,1)))*levicevita(i1,int(w1(3,1)),i2) * (-sc(w1(2,:),l-h(1,:))*sc(w2(2,:),l-h(1,:))/mwsq &
      - sc(w1(2,:),l)*sc(w2(2,:),l)/mwsq + sc(l,l-h(1,:))*sc(l,w1(2,:))*sc(l-h(1,:), w2(2,:))/mwsq**2)
d7 = d7 + levicevita(i1,i2,int(w1(3,1)))*levicevita(i1,i2,int(w2(3,1))) * ( sc(w1(2,:),l-h(1,:))*sc(w2(2,:),l-h(1,:))/mwsq &
      + sc(w1(2,:),l)*sc(w2(2,:),l)/mwsq - sc(l,l-h(1,:))*sc(l,w2(2,:))*sc(l-h(1,:), w1(2,:))/mwsq**2)
  end do
end do
d7 = d7 * V_hw2(i2,i2) * g_Coupling**2
end function d7

function d8(h, w1, w2, l)
  complex(dp), intent(in) :: h(3,dimP), w1(3,dimP), w2(3,dimP), l(dimP)
  complex(dp) :: d8

d8 = czero
d8 = - V_h3() * V_h2w2(int(w1(3,1)),int(w2(3,1))) * sc(w1(2,:),w2(2,:))
end function d8

function d9(h, w1, w2, l)
  complex(dp), intent(in) :: h(3,dimP), w1(3,dimP), w2(3,dimP), l(dimP)
  complex(dp) :: d9

d9 = czero
d9 = V_h2w2(int(w1(3,1)), int(w2(3,1))) * V_hw2(int(w1(3,1)),int(w2(3,1))) * &
        ( sc(w1(2,:),w2(2,:)) - sc(w1(2,:), l+w1(1,:))*sc(w2(2,:), l+w1(1,:))/mwsq )
end function d9

function d10(h, w1, w2, l)
  complex(dp), intent(in) :: h(3,dimP), w1(3,dimP), w2(3,dimP), l(dimP)
  complex(dp) :: d10

d10 = czero
d10 = V_h2w2(int(w1(3,1)), int(w2(3,1))) * V_hw2(int(w1(3,1)),int(w2(3,1))) * &
        ( sc(w1(2,:),w2(2,:)) - sc(w1(2,:), l+w2(1,:))*sc(w2(2,:), l+w2(1,:))/mwsq )
end function d10

function d11(h, w1, w2, l)
  complex(dp), intent(in) :: h(3,dimP), w1(3,dimP), w2(3,dimP), l(dimP)
  complex(dp) :: d11

d11 = czero
d11 = V_h2w2(int(w1(3,1)), int(w2(3,1))) * V_hw2(int(w1(3,1)),int(w2(3,1))) * &
          ( sc(w1(2,:),w2(2,:)) - sc(w1(2,:), l)*sc(w2(2,:), l)/mwsq )
end function d11

function d12(h, w1, w2, l)
  complex(dp), intent(in) :: h(3,dimP), w1(3,dimP), w2(3,dimP), l(dimP)
  complex(dp) :: d12

d12 = czero
d12 = V_h2w2(int(w1(3,1)), int(w2(3,1))) * V_hw2(int(w1(3,1)),int(w2(3,1))) * &
          ( sc(w1(2,:),w2(2,:)) - sc(w1(2,:), l)*sc(w2(2,:), l)/mwsq )
end function d12

function numerator(h, w1, w2, l, i, D_S)
  complex(dp), intent(in) :: h(3,dimP), w1(3,dimP), w2(3,dimP), l(dimP)
  integer, intent(in) :: i, D_S
  complex(dp) :: numerator
numerator = czero

if (i == 3) then
  numerator = d3(h, w1, w2, l)
else if (i == 4) then
  numerator = d4(h, w1, w2, l)
else if (i == 5) then
  numerator = d5(h, w1, w2, l)
else if (i == 6) then
  numerator = d6(h, w1, w2, l)
else if (i == 7) then
  numerator = d7(h, w1, w2, l, D_S)
else if (i == 8) then
  numerator = d8(h, w1, w2, l)
else if (i == 9) then
  numerator = d9(h, w1, w2, l)
else if (i == 10) then
  numerator = d10(h, w1, w2, l)
else if (i == 11) then
  numerator = d11(h, w1, w2, l)
else if (i == 12) then
    numerator = d12(h, w1, w2, l)
end if


end function numerator

end module