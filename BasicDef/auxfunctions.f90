module mod_auxfunctions
  use mod_types; use mod_consts_dp
  
  implicit none
  private
  
  public :: dilog2, trilog

contains

! ----- logs and polylogs

  function trilog(z)
    real(dp) :: z, trilog, x
    complex(8) :: xli3
    
    trilog = real(xli3(dcmplx(z,0.0d0)),kind=dp)
    
  end function trilog

  FUNCTION dilog2(xIn)
    IMPLICIT NONE
    real(dp), intent(in):: xIn
    real(dp):: x,z,z2
    complex(dp):: dilog2, Li2tmp,Const
    real(dp):: Fact
    real(dp), parameter:: Pi26=1.64493406684822643647241516665_dp
    real(dp), parameter:: Pi23=3.28986813369645287294483033329_dp
    real(dp), parameter:: B1= -0.25_dp
    real(dp), parameter:: B2=  2.7777777777777778d-02
    real(dp), parameter:: B4= -2.7777777777777778d-04
    real(dp), parameter:: B6=  4.7241118669690098d-06
    real(dp), parameter:: B8= -9.1857730746619635d-08
    real(dp), parameter:: B10= 1.8978869988970999d-09
    real(dp), parameter:: B12=-4.0647616451442255d-11
    real(dp), parameter:: B14= 8.9216910204564525d-13
    real(dp), parameter:: B16=-1.9939295860721075d-14
    real(dp), parameter:: B18= 4.5189800296199181d-16
    
    Const=cmplx(0.0_dp,0.0_dp)
    Fact =1.0_dp
    
    x = xIn
    if ( x.gt.1.0_dp.and.x.lt.2.0_dp  ) then
       Fact = -1.0_dp
       Const = Pi23-0.5_dp*log(x)**2-pi*log(x)*ci
       x = 1.0_dp/x
       write(6,*) 'error'
       stop
    elseif(x.gt.2.0_dp) then
       Fact = -1.0_dp
       Const = Pi23-0.5_dp*log(x)**2-pi*log(x)*ci
       x = 1.0_dp/x
    elseif ( x.lt.(-1.0_dp) ) then
       Fact = -1.0_dp
       Const =-Pi26-0.5_dp*log(-x)**2
       x = 1.0_dp/x
    elseif ( x.eq.1.0_dp) then
       dilog2 = Pi26
       return
    endif
    
    if ( x.gt.0.5_dp ) then
       Fact = -1.0_dp*Fact
       Const = Const + Pi26 - log(x)*log(1.0_dp-x)
       x = 1.0_dp-x
    endif
    
    z = -log(1.0_dp-x)
    Li2tmp = z
    z2 = z*z
    Li2tmp = Li2tmp + B1  * z2
    z = z*z2
    Li2tmp = Li2tmp + B2  * z
    z = z*z2
    Li2tmp = Li2tmp + B4  * z
    z = z*z2
    Li2tmp = Li2tmp + B6  * z
    z = z*z2
    Li2tmp = Li2tmp + B8  * z
    z = z*z2
    Li2tmp = Li2tmp + B10 * z
    z = z*z2
    Li2tmp = Li2tmp + B12 * z
    z = z*z2
    Li2tmp = Li2tmp + B14 * z
    z = z*z2
    Li2tmp = Li2tmp + B16 * z
    z = z*z2
    Li2tmp = Li2tmp + B18 * z
    
    dilog2 = Fact*Li2tmp + Const
    
    return
  END FUNCTION dilog2

  

end module mod_auxfunctions
