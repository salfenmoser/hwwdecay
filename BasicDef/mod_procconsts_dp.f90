module mod_procconsts_dp
  use mod_types; use common_def; use mod_consts_dp
  implicit none
  private

  !---- to monitor performance 
  real(dp), public :: resmax 

  ! --- overall couplings and color
  real(dp), public, parameter :: couplfac = gwsq/two/cosW2

  ! --- the masses and width of the vector bosons
  real(dp), public, parameter :: mv = mz
  real(dp), public, parameter :: mvsq = mzsq
  real(dp), public, parameter :: gaV = gaz

  ! --- the number of generations
  real(dp), public, parameter :: ng = two

  ! --- the RR/LL part 
  integer, public, parameter :: LL = 1
  integer, public, parameter :: RR = 2
  
  ! --- invariant mass cut
  real(dp), public, parameter :: sthresh = four*mzsq

  ! --- lepton decay phase space, zero width approximation for now
  ! --- put back the inverse propagator, as we have the propagator in the ME
  real(dp), public, parameter :: PSW_V_dc = one/8.0_dp/pi/(two*mV*gaV)

  ! --- choose fixed initial state channel for partonic runs
  integer, public, parameter :: fix_ch_1 = up
  integer, public, parameter :: fix_ch_2 = -up
  
  ! -- observables
    ! --- generic declarations for histograms
  integer, public, parameter :: Nobs = 5  ! total number of observabels
  integer, public, parameter :: NumOfBlocks = 1  ! replicas of histograms, to check pole cancellation. Here we don't do it
  integer, public, parameter :: Nnoncum = 3 ! total number of non cumulative histograms (must be in the first position)
  integer, public, parameter :: Nbinmax = 250  ! maximal number of bins 

  !--- the observables, for now, are: 
  !-- 1: total rate 
  !-- 2: m4l
  !-- 3: cumulative m4l

  character*6, public ::  OBS_CODE(Nobs) = (/'RATE',&
       'm4l','pTZ','cm4l','cmpTZ'/)

  real(dp), public, parameter :: rate_low = zero  ! these are fictitious numbers, just to get the rate
  real(dp), public, parameter :: rate_high = one  ! 
  real(dp), public, parameter :: rate_step = one  !

  real(dp), public, parameter :: m4l_low = 10.0_dp
  real(dp), public, parameter :: m4l_high = 1000.0_dp
  real(dp), public, parameter :: m4l_step = 5.0_dp

  real(dp), public, parameter :: pTZ_low = 0.0_dp
  real(dp), public, parameter :: pTZ_high = 200.0_dp
  real(dp), public, parameter :: pTZ_step = 1.0_dp

  real(dp), public, parameter :: LowLimits(Nobs)=(/rate_low,&
       m4l_low,pTZ_low,m4l_low,pTZ_low/)

  real(dp), public, parameter :: UpLimits(Nobs)=(/rate_high,&
       m4l_high,pTZ_high,m4l_high,pTZ_high/)

  real(dp), public, parameter :: Step(Nobs) = (/rate_step,&
       m4l_step,pTZ_step*5.0_dp,m4l_step,pTZ_step/)
  
  integer, public, parameter :: Nbins(Nobs) = (UpLimits(:) - LowLimits(:))/Step(:)

  integer, public :: evalcount ! count the number of evaluations
  integer, public :: sumcount = 0
  integer, public :: evalcountmax  ! maximum number of evaluations 
  
  real(dp), public :: auxweight = one ! a useful parameter to account for 
                                      ! some global factors that are used 
                                      ! in finalresults instead of FLO

  real(dp), public :: HistoGlobalArray(Nobs*NumOfBlocks,Nbinmax) = zero ! second argument is just large enough 
  real(dp), public :: HistoGlobalArray_aux(Nobs*NumOfBlocks,Nbinmax) = zero  ! second argument is just large enough 
  real(dp), public :: ErrorGlobalArray(Nobs*NumOfBlocks,Nbinmax) = 1d9 ! second argument is just large enough 
  real(dp), public :: ErrorGlobalArray_aux(Nobs*NumOfBlocks,Nbinmax) ! second argument is just large enough 

  real(dp), public :: Intaver(Nobs*NumOfBlocks,Nbinmax)  ! second argument is just large enough 
  real(dp), public :: Intaver_aux(Nobs*NumOfBlocks,Nbinmax)  ! second argument is just large enough 

  real(dp), public :: Intaversq(Nobs*NumOfBlocks,Nbinmax)  ! second argument is just large enough 
  real(dp), public :: Intaversq_new(Nobs*NumOfBlocks,Nbinmax)  ! second argument is just large enough 
  real(dp), public :: Error_aux(Nobs*NumOfBlocks,Nbinmax)  ! second argument is just large enough 

  real(dp), public :: HistoGlobalArray_Markus(Nobs*NumOfBlocks,Nbinmax) = zero ! second argument is just large enough 

end module mod_procconsts_dp
