module common_def
  use mod_types
  implicit none
  private

  real(dp), public :: s,shad

  integer, public :: epsilon
  integer, public :: numofjets

  real(dp), public :: musq
  real(qp), public :: musq_qp

  real(dp), public :: prefactorLO,prefactorNLO

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

  real(dp), public :: buff
  real(dp), public :: cbuff
  real(dp), public :: onet

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  
  real(dp), public, parameter :: switchbuff = 1d-7
  real(dp), public, parameter :: planebuff = 1d-8

  real(dp), public :: ptjetcut

end  module common_def
