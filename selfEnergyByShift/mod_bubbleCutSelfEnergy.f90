module mod_bubbleCutSelfEnergy

    use mod_types
    use mod_consts_dp
    use common_def
    
    use mod_auxfunctions     
    use mod_kinematics
    use mod_triangle
    use mod_bubble
    use mod_tadpole
    
    implicit none
    
  public :: get_hHLoopCoefficient, get_hWLoopCoefficient, get_wWHLoopCoefficient, get_wWWLoopCoefficient, polarizationFactorWShift

  contains

function get_hHLoopCoefficient(h, w1, w2, xi, dim)
  integer, intent(in) :: dim
  complex(dp), intent(in) :: h(3, dimP), w1(3, dimP), w2(3, dimP), xi
  complex(dp) :: hShifted(3, dimP), w1Shifted(3, dimP)
  complex(dp) :: kappa(dimP), cutCoeff(10)
  complex(dp) :: get_hHLoopCoefficient

call HShiftMomentum(h(1,:), w1(1,:), kappa)
hShifted = h
w1Shifted = w1
hShifted(1,:) = h(1,:) + xi*kappa(:)
w1Shifted(1,:) = w1(1,:) - xi*kappa(:)

call system('rm ~/Documents/thesis/fortran/UnitarityMethodCalculation/diagrams/coefficients/*')
print *, 'calculating H Loop Self Energy Coefficient of higgs boson'

cutCoeff = getBubbleCoefficients_ij(dim, hShifted, w1Shifted, w2, 3, 2, .false., b_Test())
!cutCoeff = getBubbleCoefficients_ij(dim, hShifted, w1Shifted, w2, 3, 2)
get_hHLoopCoefficient = cutCoeff(10)
end function get_hHLoopCoefficient


function get_hWLoopCoefficient(h, w1, w2, xi, dim)
  integer, intent(in) :: dim
  complex(dp), intent(in) :: h(3, dimP), w1(3, dimP), w2(3, dimP), xi
  complex(dp) :: hShifted(3, dimP), w1Shifted(3, dimP)
  complex(dp) :: kappa(dimP), cutCoeff(10)
  complex(dp) :: get_hWLoopCoefficient

call HShiftMomentum(h(1,:), w1(1,:), kappa)
hShifted = h
w1Shifted = w1
hShifted(1,:) = h(1,:) + xi*kappa(:)
w1Shifted(1,:) = w1(1,:) - xi*kappa(:)

call system('rm ~/Documents/thesis/fortran/UnitarityMethodCalculation/diagrams/coefficients/*')
print *, 'calculating W Loop Self Energy Coefficient of higgs boson'

cutCoeff = getBubbleCoefficients_ij(dim, hShifted, w1Shifted, w2, 1, 2, .false., b_Test())
!cutCoeff = getBubbleCoefficients_ij(dim, hShifted, w1Shifted, w2, 1, 2)
get_hWLoopCoefficient = cutCoeff(10)
end function get_hWLoopCoefficient


function get_wWHLoopCoefficient(h, w1, w2, xi, dim, pol1, pol2)
  integer, intent(in) :: dim, pol1, pol2
  complex(dp), intent(in) :: h(3, dimP), w1(3, dimP), w2(3, dimP), xi
  complex(dp) :: ep1(6, dimE), ep2(6, dimE), w1Shifted(3, dimP), w2Shifted(3, dimP)
  complex(dp) :: kappa(dimP), cutCoeff(10)
  complex(dp) :: get_wWHLoopCoefficient

call WShiftMomentum(kappa)
w1Shifted = w1
w2Shifted = w2
w1Shifted(1,:) = w1(1,:) + xi*kappa(:)
w2Shifted(1,:) = w2(1,:) - xi*kappa(:)

call spinonepol_6d(w1Shifted(1,:),ep1)
call spinonepol_6d(w2Shifted(1,:),ep2)
w1Shifted(2,:) = ep1(pol1, :)
w2Shifted(2,:) = ep2(pol2, :)

call system('rm ~/Documents/thesis/fortran/UnitarityMethodCalculation/diagrams/coefficients/*')
print *, 'calculating WH Loop Self Energy Coefficient of W boson'

cutCoeff = getBubbleCoefficients_ij(dim, h, w1Shifted, w2Shifted, 3, 1, .false., b_Test())
!cutCoeff = getBubbleCoefficients_ij(dim, h, w1Shifted, w2Shifted, 3, 1)
get_wWHLoopCoefficient = cutCoeff(10)
end function get_wWHLoopCoefficient


function get_wWWLoopCoefficient(h, w1, w2, xi, dim, pol1, pol2)
  integer, intent(in) :: dim, pol1, pol2
  complex(dp), intent(in) :: h(3, dimP), w1(3, dimP), w2(3, dimP), xi
  complex(dp) :: ep1(6, dimE), ep2(6, dimE), w1Shifted(3, dimP), w2Shifted(3, dimP)
  complex(dp) :: kappa(dimP), cutCoeff(10)
  complex(dp) :: get_wWWLoopCoefficient

call WShiftMomentum(kappa)
w1Shifted = w1
w2Shifted = w2
w1Shifted(1,:) = w1(1,:) + xi*kappa(:)
w2Shifted(1,:) = w2(1,:) - xi*kappa(:)
  
call spinonepol_6d(w1Shifted(1,:),ep1)
call spinonepol_6d(w2Shifted(1,:),ep2)
w1Shifted(2,:) = ep1(pol1, :)
w2Shifted(2,:) = ep2(pol2, :)

call system('rm ~/Documents/thesis/fortran/UnitarityMethodCalculation/diagrams/coefficients/*')
print *, 'calculating WW Loop Self Energy Coefficient of W boson'

cutCoeff = getBubbleCoefficients_ij(dim, h, w1Shifted, w2Shifted, 1, 1, .false., b_Test())
!cutCoeff = getBubbleCoefficients_ij(dim, h, w1Shifted, w2Shifted, 1, 1)
get_wWWLoopCoefficient = cutCoeff(10)
end function get_wWWLoopCoefficient



function polarizationFactorWShift(w1, w2, xi, pol1, pol2)
  integer, intent(in) :: pol1, pol2
  complex(dp), intent(in) :: w1(3, dimP), w2(3, dimP), xi
  complex(dp) :: ep1(6, dimE), ep2(6, dimE), w1Shifted(3, dimP), w2Shifted(3, dimP), kappa(dimP)
  complex(dp) :: polarizationFactorWShift

call WShiftMomentum(kappa)
w1Shifted = w1
w2Shifted = w2
w1Shifted(1,:) = w1(1,:) + xi*kappa(:)
w2Shifted(1,:) = w2(1,:) - xi*kappa(:)
    
call spinonepol_6d(w1Shifted(1,:),ep1)
call spinonepol_6d(w2Shifted(1,:),ep2)
w1Shifted(2,:) = ep1(pol1, :)
w2Shifted(2,:) = ep2(pol2, :)

polarizationFactorWShift = sc(w1Shifted(2,:), w2Shifted(2,:)) - sc(w1Shifted(2,:), w1Shifted(1,:))*sc(w2Shifted(2,:), w1Shifted(1,:)) / sc(w1Shifted(1,:), w1Shifted(1,:))
end function polarizationFactorWShift

  
end module
  