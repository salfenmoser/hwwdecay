use mod_auxfunctions
use mod_consts_dp
use mod_kinematics
use mod_vertices
use mod_numeratorFunctions



test_suite mod_diagramKinematics
  complex(dp) :: pa1(3,dimP), pa2(3,dimP), paH(3,dimP)
  integer :: pol1, pol2, c1, c2
  
setup

pol1 = 1
pol2 = 1
c1 = 2
c2 = 2
call particlesHWW(pol1, pol2, c1, c2, paH, pa1, pa2)

end setup

teardown
  ! This code runs immediately after each test
end teardown


test triangleKinematicsOrthogonality
integer :: i, r, s
complex(dp) :: v(dimP,dimP), V3(dimP), lt_sq

do i = 1,6
call getTriangleKinematics(paH, pa1, pa2, i, v, V3, lt_sq)

if (MOD(i,2) == 1) then
  assert_equal_within(abs(sc(v(1,:), pa1(1,:)) - cone), zero, 1e-10)
  assert_equal_within(abs(sc(v(1,:), pa2(1,:))), zero, 1e-10)
  assert_equal_within(abs(sc(v(2,:), pa1(1,:))), zero, 1e-10)
  assert_equal_within(abs(sc(v(2,:), pa2(1,:)) - cone), zero, 1e-10)
else if (MOD(i,2) == 0) then
  assert_equal_within(abs(sc(v(1,:), pa1(1,:))), zero, 1e-10)
  assert_equal_within(abs(sc(v(1,:), pa2(1,:)) - cone), zero, 1e-10)
  assert_equal_within(abs(sc(v(2,:), pa1(1,:)) - cone), zero, 1e-10)
  assert_equal_within(abs(sc(v(2,:), pa2(1,:))), zero, 1e-10)
end if

do s = 3,5
  assert_equal_within(abs(sc(v(s,:), pa1(1,:))), zero, 1e-10)
  assert_equal_within(abs(sc(v(s,:), pa2(1,:))), zero, 1e-10)
  assert_equal_within(abs(sc(v(s,:), v(s,:)) - cone), zero, 1e-10)

  do r = 1,2
    assert_equal_within(abs(sc(v(s,:), v(r,:))), zero, 1e-10)
  end do

end do

assert_equal_within(abs(sc(v(3,:), v(4,:))), zero, 1e-10)

end do

end test triangleKinematicsOrthogonality



test bubbleKinematicsOrthogonality_cut1
integer :: i, j, r, s
complex(dp) :: q(dimP), x1, lt_sq, v(dimP,dimP)

j = 1
do i = 1,2
call getBubbleKinematics(paH, pa1, pa2, i, j, q, x1, lt_sq, v)

if (MOD(i,2) == 1) then
  assert_equal_within(abs( sc( pa1(1,:) - q(:), pa1(1,:) - q(:) ) ), zero, 1e-10)
  assert_equal_within(abs(sc(v(1,:), pa1(1,:)) - cone), zero, 1e-10)
  do s = 2,5
  assert_equal_within(abs(sc(v(s,:), pa1(1,:))), zero, 1e-10)
  end do
else if (MOD(i,2) == 0) then
  assert_equal_within(abs( sc( pa2(1,:) - q(:), pa2(1,:) - q(:) ) ), zero, 1e-10)
  assert_equal_within(abs(sc(v(1,:), pa2(1,:)) - cone), zero, 1e-10)
  do s = 2,5
  assert_equal_within(abs(sc(v(s,:), pa2(1,:))), zero, 1e-10)
  end do
end if

do s = 2,5
assert_equal_within(abs(sc(v(s,:), v(1,:))), zero, 1e-10)
do r = 2,5
  assert_equal_within(abs(sc(v(s,:), v(r,:)) - delta(r,s)), zero, 1e-10)
end do
end do

end do

end test bubbleKinematicsOrthogonality_cut1


test bubbleKinematicsOrthogonality_cut2
integer :: i, j, r, s
complex(dp) :: q(dimP), x1, lt_sq, v(dimP,dimP)

j = 2
do i = 1,6
call getBubbleKinematics(paH, pa1, pa2, i, j, q, x1, lt_sq, v)

assert_equal_within(abs( sq( paH(1,:) - q(:) ) ), zero, 1e-10)
assert_equal_within(abs(sc(v(1,:), paH(1,:)) - cone), zero, 1e-10)
do s = 2,4
  assert_equal_within(abs(sc(v(s,:), paH(1,:))), zero, 1e-10)
  assert_equal_within(abs(sc(v(s,:), v(s,:)) - cone), zero, 1e-10)
  assert_equal_within(abs(sc(v(s,:), v(1,:))), zero, 1e-10)
end do

end do

end test bubbleKinematicsOrthogonality_cut2



test diagramsHWWCorrectness
integer :: i, k
complex(dp) :: diag(6,21), lzero(dimP)
real(dp) :: absV

do k = 1, dimP
  lzero(k) = czero
  end do

diag = diagramsHWW(lzero, paH(1,:),pa1(1,:),pa2(1,:))

do i = 1,6

if (MOD(i,2) == 1) then
  absV = abs(sc( pa1(1,:) - diag(i,8:13), pa1(1,:) - diag(i,8:13)))
  assert_equal_within(absV, zero, 1e-10)
else if (MOD(i,2) == 0) then
  absV = abs( sc( pa2(1,:) - diag(i,8:13), pa2(1,:) - diag(i,8:13)) )
  assert_equal_within(absV, zero, 1e-10)
end if

end do

end test diagramsHWWCorrectness


end test_suite
