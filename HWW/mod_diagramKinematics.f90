module mod_diagramKinematics

    use mod_types
    use mod_consts_dp
    use common_def
    
    use mod_auxfunctions     
    use mod_kinematics
    use mod_vertices

    use mod_numeratorFunctions
    
    implicit none
    
  public :: diagramsHWW, getDenominators
  public :: getTriangleKinematics, getTriangleMomenta4D, getTriangleMomenta5_6D
  public :: getBubbleKinematics, getBubbleMomenta
  public :: getTadpoleMomenta

  
  contains

! contains 6 rows, each row represents one diagram;
! each row contains propagator momentum and mass for the three propagators
! row 1,2: WWW loop, row 3,4: HWH loop, row 5,6: WHW loop
! entry 1-6:momentum of first propagator, entry 7: mass squared of first propagator, etc.
function diagramsHWW(l,p,k1,k2)
  integer :: i
  complex(dp), intent(in) :: l(dimP), p(dimP), k1(dimP), k2(dimP)
  complex(dp) ::  diagramsHWW(6,21)

  do i = 1,5, 2
    diagramsHWW(i,1:6) = l
    diagramsHWW(i,8:13) = l + k1
    diagramsHWW(i,15:20) = l - p
    diagramsHWW(i,7) = mwsq
    diagramsHWW(i,14) = mwsq
    diagramsHWW(i,21) = mwsq
  end do

diagramsHWW(3,7) = mhsq
diagramsHWW(3,21) = mhsq
diagramsHWW(5,14) = mhsq
  
  do i = 2,6, 2
    diagramsHWW(i,1:6) = l
    diagramsHWW(i,8:13) = l + k2
    diagramsHWW(i,15:20) = l - p
    diagramsHWW(i,7) = mwsq
    diagramsHWW(i,14) = mwsq
    diagramsHWW(i,21) = mwsq
  end do

diagramsHWW(4,7) = mhsq
diagramsHWW(4,21) = mhsq
diagramsHWW(6,14) = mhsq

end function diagramsHWW

! returns the three denominators of the six different diagrams
function getDenominators(l, part1, part2, part3)
  integer :: i, k
  complex(dp), intent(in) :: l(dimP), part1(3,dimP), part2(3,dimP), part3(3,dimP)
  complex(dp) :: getDenominators(6,3)
  complex(dp) :: diag(6,21)

diag = diagramsHWW(l, part1(1,:), part2(1,:), part3(1,:))
do i = 1,6
getDenominators(i,1) = sc(diag(i,1:6), diag(i,1:6)) - diag(i,7)
getDenominators(i,2) = sc(diag(i,8:13), diag(i,8:13)) - diag(i,14)
getDenominators(i,3) = sc(diag(i,15:20), diag(i,15:20)) - diag(i,21)
end do
end function getDenominators
  
! construction of Van Neerven-Vermaseren basis from particle 1,2,3, for diagram i for triple cut
! v contains 6d basis, v(1), v(2) parallel to k1,k2; v(3), v(4) in transversal space
! V3 and lt_sq required for construction of momenta
subroutine getTriangleKinematics(part1, part2, part3, i, v, V3, lt_sq)
  integer, intent(in) :: i
  complex(dp), intent(in) ::  part1(3,dimP), part2(3,dimP), part3(3,dimP)
  complex(dp), intent(out) :: lt_sq, v(dimP,dimP), V3(dimP)
  complex(dp) :: diag(6,21), l(dimP), vAux(4,4)

l(:) = czero
diag = diagramsHWW(l, part1(1,:),part2(1,:),part3(1,:))
    
! construct physical and transverse space to get l_transverse, symmetric in part2,part3
! since give2to4vect is symmetric in part2, part3, no need to differentiate between diagrams
! could also use give2to4vect(1,2) and shift all indices in diag(i,:) by -4 or give2to4vect(3,1) and shift by +4
if (MOD(i,2) == 1) then
  call give2to4vect(part2(1,1:4),part3(1,1:4),vAux)
  call extendBasisD(vAux, v)
   
else if (MOD(i,2) == 0) then
  call give2to4vect(part3(1,1:4),part2(1,1:4),vAux)
  call extendBasisD(vAux, v)
end if

V3(:) = -0.5_dp*(sc(diag(i,8:13),diag(i,8:13))-sc(diag(i,1:6),diag(i,1:6)) - diag(i,14) + diag(i,7) ) * v(1,:)
V3(:) = V3(:) - 0.5_dp * ( sc(diag(i,15:20),diag(i,15:20)) - sc(diag(i,8:13),diag(i,8:13)) - diag(i,21) + diag(i,14)) * v(2,:)
lt_sq = diag(i,7) - sq(V3)
return
end subroutine getTriangleKinematics


function getTriangleMomenta4D(D_S, pa1, pa2, pa3, i)
  integer, intent(in) :: i, D_S
  complex(dp), intent(in) :: pa1(3,dimP), pa2(3,dimP), pa3(3,dimP)
  integer :: m, n
  complex(dp) :: v(dimP,dimP), V3(dimP), lt_sq, t
  complex(dp) :: getTriangleMomenta4D(7,7,dimP)

getTriangleMomenta4D(:,:,:) = czero

call getTriangleKinematics(pa1, pa2, pa3, i, v, V3, lt_sq)

!m=1,...7 corresponds to auxC -3, ... +3
do m = 1,7
  do n = 0,6
    t = exp(two*pi*ci*n/7._dp)
    getTriangleMomenta4D(m,n+1,:) = V3 + sqrt(lt_sq)*(REAL(t)*v(3,:) + AIMAG(t)*v(4,:))
  end do
end do

end function getTriangleMomenta4D


function getTriangleMomenta5_6D(D_S, pa1, pa2, pa3, i)
  integer, intent(in) :: i, D_S
  complex(dp), intent(in) :: pa1(3,dimP), pa2(3,dimP), pa3(3,dimP)
  complex(dp) :: v(dimP,dimP), V3(dimP), lt_sq, t
  complex(dp) :: getTriangleMomenta5_6D(4,dimP)
getTriangleMomenta5_6D(:,:) = czero

if (.NOT. D_S == 4) then
call getTriangleKinematics(pa1, pa2, pa3, i, v, V3, lt_sq)

getTriangleMomenta5_6D(1,:) = V3 + sqrt(lt_sq/two)*(v(3,:) + v(5,:))
getTriangleMomenta5_6D(2,:) = V3 + sqrt(lt_sq/two)*(- v(3,:) + v(5,:))
getTriangleMomenta5_6D(3,:) = V3 + sqrt(lt_sq/two)*(v(4,:) + v(5,:))
getTriangleMomenta5_6D(4,:) = V3 + sqrt(lt_sq/two)*( - v(4,:) + v(5,:))
end if

end function getTriangleMomenta5_6D


! construction of Van Neerven-Vermaseren basis from particle 1,2,3, for diagram i, cut j for double cut
! v contains 6d basis, v(1) parallel to k1 (diag 1,3,5) or k2 (diag 2,4,6); v(2), v(3), v(4) in transversal space
! x1 and lt_sq required for construction of momenta
subroutine getBubbleKinematics(part1, part2, part3, i, j, q, x1, lt_sq, v)
  integer, intent(in) :: i, j ! correspond to diag, cut
  complex(dp), intent(in) :: part1(3,dimP), part2(3,dimP), part3(3,dimP)
  complex(dp), intent(out) :: q(dimP), x1, lt_sq, v(dimP,dimP)
  complex(dp) :: l(dimP), diag(6,21), vAux(4,4)

l(:) = czero
diag = diagramsHWW(l, part1(1,:),part2(1,:),part3(1,:))

if (j == 1) then    
  q(:) = diag(i, 8:13) - diag(i, 1:6)
  x1 = ( sc(diag(i,1:6), diag(i,1:6)) - diag(i,7) - sc(diag(i,8:13), diag(i,8:13)) + diag(i,14) ) / (two * sc(q, q) )
  lt_sq = diag(i, 7) -  x1**two * sc(q, q)
      
else if (j == 2) then
  q(:) = diag(i, 1:6) - diag(i,15:20)
  x1 = ( sc(diag(i,15:20), diag(i,15:20)) - diag(i,21) - sc(diag(i,1:6), diag(i,1:6)) + diag(i,7) ) / (two * sc(q, q) )
  lt_sq = diag(i, 21) -  x1**two * sc(q, q)
  
! else if (j == 3) then
!   q(:) = diag(i, 15:20) - diag(i, 8:13)
!   x1 = ( sc(diag(i,8:13), diag(i,8:13)) - diag(i,14) - sc(diag(i,15:20), diag(i,15:20)) + diag(i,21) ) / (two * sc(q, q) )    
!   lt_sq = diag(i,14) - diag(i,21) -  sc(diag(i,8:13), diag(i,8:13)) + sc(diag(i,15:20), diag(i,15:20)) - three * x1**2 * sc(q,q)
end if

call give1to4vect(q(1:4), vAux)
call extendBasisD(vAux, v)
    
return
end subroutine getBubbleKinematics  


subroutine getBubbleMomenta(D_S, pa1, pa2, pa3, i, j, l1PlusMinus, l2PlusMinus, lAux, lEps, x, y)
  integer, intent(in) :: i, j, D_S
  complex(dp), intent(in) :: pa1(3,dimP), pa2(3,dimP), pa3(3,dimP)
  complex(dp), intent(out) :: l1PlusMinus(3,3,2,dimP), l2PlusMinus(2,2,2,dimP), lAux(4,dimP), lEps(dimP), x, y
  integer :: m, n
  complex(dp) :: v(dimP,dimP), x1, q(dimP), lt_sq, t

l1PlusMinus(:,:,:,:) = czero
l2PlusMinus(:,:,:,:) = czero
lAux(:,:) = czero
lEps(:) = czero

call getBubbleKinematics(pa1, pa2, pa3, i, j, q, x1, lt_sq, v)

do m = 1,3
  do n = 0,2
  t = exp(2*pi*ci * n / three)
  l1PlusMinus(m, n+1, 1,:) = x1*q(:) + t*v(2,:) + sqrt(lt_sq - t**2)*v(3,:)
  l1PlusMinus(m, n+1, 2,:) = x1*q(:) + t*v(2,:) - sqrt(lt_sq - t**2)*v(3,:)
end do
end do

do m = 1,2
  do n = 0,1
  t = exp(2*pi*ci * n / two)
  l2PlusMinus(m, n+1, 1,:) = x1*q(:) + t*v(2,:) + sqrt(lt_sq - t**2)*v(3,:)
  l2PlusMinus(m, n+1, 2,:) = x1*q(:) + t*v(2,:) - sqrt(lt_sq - t**2)*v(3,:) 
end do
end do

x = sqrt(lt_sq/six)
y = sqrt(lt_sq - x**two)
  
lAux(1,:) = x1*q(:) + x*v(2,:) + y*v(4,:)
lAux(2,:) = x1*q(:) - x*v(2,:) + y*v(4,:)
lAux(3,:) = x1*q(:) - x*v(2,:) - y*v(4,:)
lAux(4,:) = x1*q(:) + x*v(4,:) + y*v(3,:)

if (D_S > 4) then
lEps(:) = x1*q(:) + sqrt(lt_sq)*v(5,:)
end if

return
end subroutine getBubbleMomenta

function getTadpoleMomenta(i)
  integer, intent(in) :: i
  complex(dp) :: v(dimP, dimP), lAux(7, dimP), l_sq
  complex(dp) :: getTadpoleMomenta(7,dimP)
  integer :: k

v(:,:) = czero
do k = 1, dimP
  v(k,k) = ci
end do 
v(1,1) = cone
    
if (i == 1) then
  l_sq = mwsq
else if (i == 3) then
  l_sq = mhsq
end if
    
lAux(1,:) = sqrt(l_sq/two) * (v(1,:) + v(2,:))
lAux(2,:) = sqrt(l_sq/two) * (v(1,:) - v(2,:))
    
lAux(3,:) = sqrt(l_sq/two) * (v(1,:) + v(3,:))
lAux(4,:) = sqrt(l_sq/two) * (v(1,:) - v(3,:))
  
lAux(5,:) = sqrt(l_sq/two) * (v(1,:) + v(4,:))
lAux(6,:) = sqrt(l_sq/two) * (v(1,:) - v(4,:))

lAux(7,:) = sqrt(l_sq/three) * (v(1,:) + v(2,:) + v(3,:))

getTadpoleMomenta = lAux 
end function getTadpoleMomenta

  
end module
  