module mod_ampSum

    use mod_types
    use mod_consts_dp
    use common_def
    
    use mod_auxfunctions     
    use mod_kinematics
    use mod_amplitudes
  
    use mod_diagramKinematics
    
implicit none
  
public :: getTriangleAmps, getBubbleAmps, getTadpoleAmps
logical :: printWarningTriple = .false., printWarningDouble = .true., printWarningSingle = .false.
  
  
contains
  
!---------------------------------------------------------
! triple cuts 
! --------------------------------------------------------
!pa1 = Higgs, pa2 = W1, pa3 = W2
! dimension=D_S, momentum l, diagram i, boolean if only calulation if momenta are chosen adequately (on shell)
function getTriangleAmps(D_S, l, pa1, pa2, pa3, i, properMom)
  integer, intent(in) :: D_S, i
  complex(dp), intent(in) :: pa1(3,dimP), pa2(3,dimP), pa3(3,dimP), l(dimP)
  logical, intent(in) :: properMom ! if true: only returns non-zero result if proper momenta are chosen
  logical :: properMom
  integer :: epx1, cx1, epx2, cx2, epx3, cx3, sumCx1, sumCx2, sumCx3, sumEx1, sumEx2, sumEx3, k_ind, l_ind
  complex(dp) :: getTriangleAmps, diag(6,21)
  complex(dp) :: pax1In(3,dimP), pax2In(3,dimP), pax3In(3,dimP), pax1Out(3,dimP), pax2Out(3,dimP), pax3Out(3,dimP)
  complex(dp) :: ePx1In(dimP,dimE), ePx2In(dimP,dimE), ePx3In(dimP,dimE)
  complex(dp) :: J3_1, J3_2, J3_3
  complex(dp) :: d(6,3)

getTriangleAmps = czero
properMom = .true.
d = getDenominators(l, pa1, pa2, pa3)

if ((abs(d(i,1)) > limitProp) .or. (abs(d(i,2)) > limitProp) .or. (abs(d(i,3)) > limitProp)) then
  if (printWarningTriple) then
    print *, "WARNING: TRYING TO COMPUTE TRIPLE CUT CURRENTS WITH IMPROPERLY CHOSEN MOMENTA"
  end if
  properMom = .false.
end if

if  ((properMom .eqv. .true.) .or. (properMom .eqv. .false.)) then

diag = diagramsHWW(l, pa1(1,:), pa2(1,:), pa3(1,:))

! define momenta of internal particles, in and outgoing
pax1In(1,:) = diag(i,1:6)
pax1Out(1,:) = - pax1In(1,:)
pax2In(1,:) = diag(i,8:13)
pax2Out(1,:) = - pax2In(1,:)
pax3In(1,:) = diag(i,15:20)
pax3Out(1,:) = - pax3In(1,:)

! set polarization matrix of internal particles to zero, assuming higgs for now
ePx1In(:,:) = czero
ePx2In = ePx1In
ePx3In = ePx1In

! define upper sum limit of polarization and color sum for three internal particles, assuming higgs for now
sumCx1 = 1
sumCx2 = 1
sumCx3 = 1
  
sumEx1 = 1
sumEx2 = 1
sumEx3 = 1

! if not higgs, define properties accordingly
if (diag(i,7) == mwsq) then
  sumCx1 = 3
  sumEx1 = D_S - 1
  call spinonepol_6d(pax1In(1,:),ePx1In)
end if
if (diag(i,14) == mwsq) then
  sumCx2 = 3
  sumEx2 = D_S - 1
  call spinonepol_6d(pax2In(1,:),ePx2In)
end if
if (diag(i,21) == mwsq) then
  sumCx3 = 3
  sumEx3 = D_S - 1
  call spinonepol_6d(pax3In(1,:),ePx3In)
end if

! sum over internal particles
do cx1 = 1,sumCx1
  do cx2 = 1,sumCx2
    do cx3 = 1,sumCx3

      do epx1 = 1,sumEx1
        do epx2 = 1,sumEx2
          do epx3 = 1,sumEx3
    
      ! define polarization states, non vanishing only if not higgs
      pax1In(2,:) = ePx1In(epx1,:)
      pax1Out(2,:) = ePx1In(epx1,:)
      pax2In(2,:) = ePx2In(epx2,:)
      pax2Out(2,:) = ePx2In(epx2,:)
      pax3In(2,:) = ePx3In(epx3,:)
      pax3Out(2,:) = ePx3In(epx3,:)
    
      ! define color states, non vanishing only if not higgs
      pax1In(3,:) = czero
      pax2In(3,:) = czero
      pax3In(3,:) = czero
          
      if (diag(i,7) == mwsq) then
        pax1In(3,1) = cone*cx1
      end if
      if (diag(i,14) == mwsq) then
        pax2In(3,1) = cone*cx2
      end if
      if (diag(i,21) == mwsq) then
        pax3In(3,1) = cone*cx3
      end if
  
      pax1Out(3,:) = pax1In(3,:)
      pax2Out(3,:) = pax2In(3,:)
      pax3Out(3,:) = pax3In(3,:)
    
      ! calculate three 3particle currents
      J3_1 = J3(pa1,pax1Out,pax3In)
      if (MOD(i,2) == 1) then
        J3_2 = J3(pa2,pax1In,pax2Out)
        J3_3 = J3(pa3,pax2In,pax3Out)
      else if (MOD(i,2) == 0) then
        J3_2 = J3(pa3,pax1In,pax2Out)
        J3_3 = J3(pa2,pax2In,pax3Out)
      end if
          
      getTriangleAmps = getTriangleAmps + J3_1 * J3_2 * J3_3

       end do
      end do
     end do
    end do
   end do
  end do

end if
end function getTriangleAmps

  
  
!---------------------------------------------------------
! double cuts 
! -------------------------------------------------------- 
!pa1 = Higgs, pa2 = W1, pa3 = W2
! dimension = D_S, momentum l, diagram i, cut j, boolean if only calulation if momenta are chosen adequately (on shell)
function getBubbleAmps(D_S, l, pa1, pa2, pa3, i, j, properMom)
  integer :: epx1, cx1, epx2, cx2, epx3, cx3, sumCx1, sumCx2, sumCx3, sumEx1, sumEx2, sumEx3
  integer, intent(in) :: D_S, i, j
  complex(dp), intent(in) :: pa1(3,dimP), pa2(3,dimP), pa3(3,dimP), l(dimP)
  logical, intent(in) :: properMom ! if true: only returns non-zero result if proper momenta are chosen
  logical :: properMom

  complex(dp) :: doubleCurrents, diag(6,21)
  complex(dp) :: pax1In(3,dimP), pax2In(3,dimP), pax3In(3,dimP), ePx1In(dimP,dimE), ePx2In(dimP,dimE), ePx3In(dimP,dimE)
  complex(dp) :: pax1Out(3,dimP), pax2Out(3,dimP), pax3Out(3,dimP)
  complex(dp) :: J3_1, J4_1, d_k, d(6,3)
  complex(dp) :: getBubbleAmps
  
doubleCurrents = czero
properMom = .true.

d = getDenominators(l, pa1, pa2, pa3)
!print *, 'denominators = ', d(i,:)
if (j == 1) then
  if ((abs(d(i,1)) > limitProp) .or. (abs(d(i,2)) > limitProp))  then
    if (printWarningDouble) then
      print *, "WARNING: TRYING TO COMPUTE DOUBLE CUT CURRENTS WITH IMPROPERLY CHOSEN MOMENTA FOR CUT 1"
    end if
    properMom = .false.
  end if
else if (j == 2) then
  if ((abs(d(i,1)) > limitProp) .or. (abs(d(i,3)) > limitProp))  then
    if (printWarningDouble) then
      print *, "WARNING: TRYING TO COMPUTE DOUBLE CUT CURRENTS WITH IMPROPERLY CHOSEN MOMENTA FOR CUT 2"
    end if
      properMom = .false.
  end if
! else if (j == 3) then
!   if ((abs(d(i,2)) > limitProp) .or. (abs(d(i,3)) > limitProp))  then
!     if (printWarningDouble) then
!       print *, "WARNING: TRYING TO COMPUTE DOUBLE CUT CURRENTS WITH IMPROPERLY CHOSEN MOMENTA FOR CUT 3"
!     end if
!     properMom = .false.
!   end if
end if

if  ((properMom .eqv. .true.) .or. (properMom .eqv. .false.)) then

diag = diagramsHWW(l, pa1(1,:), pa2(1,:), pa3(1,:))
pax1In(1,:) = diag(i,1:6)
pax1Out(1,:) = - pax1In(1,:)
pax2In(1,:) = diag(i,8:13)
pax2Out(1,:) = - pax2In(1,:)
pax3In(1,:) = diag(i,15:20)
pax3Out(1,:) = - pax3In(1,:)
  
ePx1In(:,:) = czero
ePx1In = ePx1In
ePx3In = ePx1In
  
sumCx1 = 1
sumCx2 = 1
sumCx3 = 1

sumEx1 = 1
sumEx2 = 1
sumEx3 = 1
  
  
if (diag(i,7) == mwsq .and. j /= 3) then
  sumCx1 = 3
  sumEx1 = D_S - 1
  call spinonepol_6d(pax1In(1,:),ePx1In)
end if
if (diag(i,14) == mwsq .and. j /= 2) then
  sumCx2 = 3
  sumEx2 = D_S - 1
  call spinonepol_6d(pax2In(1,:),ePx2In)
end if
if (diag(i,21) == mwsq .and. j /= 1) then
  sumCx3 = 3
  sumEx3 = D_S - 1
  call spinonepol_6d(pax3In(1,:),ePx3In)
end if
  
do epx1 = 1,sumEx1
  do cx1 = 1,sumCx1
    do epx2 = 1,sumEx2
      do cx2 = 1,sumCx2
        do epx3 = 1,sumEx3
          do cx3 = 1,sumCx3
    
          pax1In(2,:) = ePx1In(epx1,:)
          pax1Out(2,:) = ePx1In(epx1,:)
          pax2In(2,:) = ePx2In(epx2,:)
          pax2Out(2,:) = ePx2In(epx2,:)
          pax3In(2,:) = ePx3In(epx3,:)
          pax3Out(2,:) = ePx3In(epx3,:)
    
          pax1In(3,:) = czero
          pax2In(3,:) = czero
          pax3In(3,:) = czero
  
          if (diag(i,7) == mwsq) then
            pax1In(3,1) = cone*cx1
          end if
          if (diag(i,14) == mwsq) then
            pax2In(3,1) = cone*cx2
          end if
          if (diag(i,21) == mwsq) then
            pax3In(3,1) = cone*cx3
          end if
  
          pax1Out(3,:) = pax1In(3,:)
          pax2Out(3,:) = pax2In(3,:)
          pax3Out(3,:) = pax3In(3,:)
          
          if (j == 1) then
            !d_k = sc(diag(i,15:20), diag(i,15:20)) - diag(i,21)
            if (MOD(i,2) == 1) then
              J3_1 = J3(pa2,pax1In,pax2Out)
              J4_1 = J4(pa1,pa3,pax1Out,pax2In)
            else if (MOD(i,2) == 0) then
              J3_1 = J3(pa3,pax1In,pax2Out)
              J4_1 = J4(pa1,pa2,pax1Out,pax2In)
            end if
          
          else if (j == 2) then
            J3_1 = J3(pa1,pax1Out,pax3In)
            J4_1 = J4(pa2,pa3,pax1In,pax3Out)
          end if

          doubleCurrents = doubleCurrents + J3_1 * J4_1
          end do
        end do
      end do
    end do  
  end do
end do

getBubbleAmps = doubleCurrents

end if
end function getBubbleAmps

  
!---------------------------------------------------------
! single cuts 
! --------------------------------------------------------
!pa1 = Higgs, pa2 = W1, pa3 = W2
! dimension = D_S, momentum l, diagram i, cut jSingle, boolean if only calulation if momenta are chosen adequately (on shell)
function getTadpoleAmps(D_S, l, pa1, pa2, pa3, i, jSingle, properMom)
  integer :: epx, cx, sumCx, sumEx, D_S
  integer, intent(in) :: i, jSingle
  complex(dp), intent(in) :: pa1(3,dimP), pa2(3,dimP), pa3(3,dimP), l(dimP)
  logical, intent(in) :: properMom ! if true: only returns non-zero result if proper momenta are chosen
  logical :: properMom

  complex(dp) :: diag(6,21)
  complex(dp) :: partxIn(3,dimP), partxOut(3,dimP), ePolxIn(dimP,dimE), J5_1, d(6,3), J5_ex
  complex(dp) :: getTadpoleAmps

getTadpoleAmps = czero
properMom = .true.
    
d = getDenominators(l, pa1, pa2, pa3)
    
if (jSingle == 1) then
  if (abs(d(i,1)) > limitProp)  then
    if (printWarningSingle) then
      print *, "WARNING: TRYING TO COMPUTE SINGLE CUT CURRENTS WITH IMPROPERLY CHOSEN MOMENTA FOR CUT 1"
    end if
   properMom = .false.
  end if
else if (jSingle == 2) then
  if (abs(d(i,2)) > limitProp)  then
    if (printWarningSingle) then
      print *, "WARNING: TRYING TO COMPUTE SINGLE CUT CURRENTS WITH IMPROPERLY CHOSEN MOMENTA FOR CUT 2"
    end if
    properMom = .false.
  end if
else if (jSingle == 3) then
  if (abs(d(i,3)) > limitProp)  then
    if (printWarningSingle) then
      print *, "WARNING: TRYING TO COMPUTE SINGLE CUT CURRENTS WITH IMPROPERLY CHOSEN MOMENTA FOR CUT 3"
    end if
    properMom = .false.
  end if
end if
    
if  ((properMom .eqv. .true.) .or. (properMom .eqv. .false.)) then
    
diag = diagramsHWW(l, pa1(1,:), pa2(1,:), pa3(1,:))
  
if (jSingle == 1) then
  partxIn(1,:) = diag(i,1:6)
  partxOut(1,:) = - partxIn(1,:)
end if

ePolxIn(:,:) = czero
  
sumCx = 1
sumEx = 1
  
if (jSingle == 1 .and. diag(i,7) == mwsq) then
  sumCx = 3
  sumEx = D_S - 1
  call spinonepol_6d(partxIn(1,:),ePolxIn)
end if
  
do epx = 1,sumEx
  do cx = 1,sumCx
    partxIn(2,:) = ePolxIn(epx,:)
    partxOut(2,:) = ePolxIn(epx,:)
    
    partxIn(3,:) = czero
          
    if (jSingle == 1 .and. diag(i,7) == mwsq) then
      partxIn(3,1) = cone*cx
    end if
    partxOut(3,:) = partxIn(3,:)
  
    J5_1 = J5(pa1, pa2, pa3, partxIn, partxOut)
    getTadpoleAmps = getTadpoleAmps + J5_1
 
    end do
  end do
end if
end function getTadpoleAmps

end module