module mod_tadpole

  use mod_types
  use mod_consts_dp
  use common_def
  
  use mod_auxfunctions     
  use mod_kinematics
  use mod_amplitudes

  use mod_diagramKinematics
  use mod_ampSum
  use mod_triangle
  use mod_bubble
  
implicit none

public :: A_ij, getTadpoleCoefficients_ij, getTadpoleCoefficient0_ij, finalCoefficients
public :: a_Test, A_ijTest

integer :: unitOffsetA = 50

contains

function a_Test()
  complex(dp) :: a_Test(5)
  a_Test(:) = czero
end function a_Test


! single cut function depending on dimension D_S, momentum l, particles higgs, w1,w2, diagram i, cut jSingle
! need to subtract triangle and bubble coefficients adequately
function A_ij(D_S, l, pa1, pa2, pa3, i, jSingle, test, testA)
  integer, intent(in) :: i, jSingle, D_S
  complex(dp), intent(in) :: pa1(3,dimP), pa2(3,dimP), pa3(3,dimP), l(dimP), testA(5)
  logical, intent(in) :: test
  complex(dp) :: d_l(6,3), d_lM1(6,3), d_lPp(6,3), d_lM2(6,3)
  complex(dp) :: A_ij, singleCurrents
  complex(dp) :: doubleB(6,3), doubleB_Pp(6,3), doubleB_M1(6,3), doubleB_M2(6,3)
  complex(dp) :: tripleC(6), tripleC_M1(6), tripleC_M2(6), tripleC_Pp(6)
  integer :: j, k, m, n

if (test) then
  A_ij = A_ijTest(l, testA)
else

d_l = getDenominators(l, pa1, pa2, pa3)
d_lM1 = getDenominators(l-pa2(1,:), pa1, pa2, pa3)
d_lM2 = getDenominators(l-pa3(1,:), pa1, pa2, pa3)
d_lPp = getDenominators(l+pa1(1,:), pa1, pa2, pa3)

singleCurrents = ci * getTadpoleAmps(D_S, l, pa1, pa2, pa3, i, jSingle, demandOnShellExternal)

do k = 1,6
tripleC(k) = getTriangleFunction_i(D_S, l, pa1, pa2, pa3, k)
tripleC_M1(k) = getTriangleFunction_i(D_S, l-pa2(1,:), pa1, pa2, pa3, k)
tripleC_M2(k) = getTriangleFunction_i(D_S, l-pa3(1,:), pa1, pa2, pa3, k)
tripleC_Pp(k) = getTriangleFunction_i(D_S, l+pa1(1,:), pa1, pa2, pa3, k)
end do

do j = 1,2
do k = 1,6
doubleB(k,j) = getBubbleFunction_ij(D_S, l, pa1, pa2, pa3, k, j)
doubleB_Pp(k,j) = getBubbleFunction_ij(D_S, l+pa1(1,:), pa1, pa2, pa3, k, j)
doubleB_M1(k,j) = getBubbleFunction_ij(D_S, l-pa2(1,:), pa1, pa2, pa3, k, j)
doubleB_M2(k,j) = getBubbleFunction_ij(D_S, l-pa3(1,:), pa1, pa2, pa3, k, j)
end do
end do

A_ij = czero

if (jSingle == 1 .and. i == 1) then
A_ij = singleCurrents
!substract first all c coefficients
A_ij = A_ij - tripleC(1)/d_l(1,2)/d_l(1,3) - tripleC_M1(1)/d_lM1(1,1)/d_lM1(1,3) - tripleC_Pp(1)/d_lPp(1,1)/d_lPp(1,2)
A_ij = A_ij - tripleC(2)/d_l(2,2)/d_l(2,3) - tripleC_M2(2)/d_lM2(2,1)/d_lM2(2,3) - tripleC_Pp(2)/d_lPp(2,1)/d_lPp(2,2)

A_ij = A_ij - tripleC_M1(3)/d_lM1(3,1)/d_lM1(3,3)
A_ij = A_ij - tripleC_M2(4)/d_lM2(4,1)/d_lM2(4,3)

A_ij = A_ij - tripleC(5)/d_l(5,2)/d_l(5,3) - tripleC_Pp(5)/d_lPp(5,1)/d_lPp(5,2)
A_ij = A_ij - tripleC(6)/d_l(6,2)/d_l(6,3) - tripleC_Pp(6)/d_lPp(6,1)/d_lPp(6,2)

!substract pink, light blue, dark blue, brown, skin, yellow, green b coefficients
A_ij = A_ij  - ( doubleB(1,1) / d_l(1,2) + doubleB_M1(1,1) / d_lM1(1,1) )
A_ij = A_ij  - ( doubleB(2,1) / d_l(2,2) + doubleB_M2(2,1) / d_lM2(2,1) )

A_ij = A_ij - doubleB_M1(3,1) / d_lM1(3,1) 
A_ij = A_ij - doubleB_M2(4,1) / d_lM2(4,1) 

A_ij = A_ij - doubleB(5,1) / d_l(5,2)
A_ij = A_ij - doubleB(6,1) / d_l(6,2)

A_ij = A_ij - ( doubleB(1,2) / d_l(1,3) + doubleB_Pp(1,2) / d_lPp(1,1) )

else if (jSingle == 1 .and. i == 3) then
A_ij = singleCurrents

!substract first all c coefficients
A_ij = A_ij - ( tripleC(3)/d_l(3,2)/d_l(3,3) + tripleC_Pp(3)/d_lPp(3,1)/d_lPp(3,2) )
A_ij = A_ij - ( tripleC(4)/d_l(4,2)/d_l(4,3) + tripleC_Pp(4)/d_lPp(4,1)/d_lPp(4,2) )
A_ij = A_ij - tripleC_M1(5)/d_lM1(5,1)/d_lM1(5,3)
A_ij = A_ij - tripleC_M2(6)/d_lM2(6,1)/d_lM2(6,3)
!print *, 'after C = ', A_ij


!substract dark blue, brown, skin, yellow, red b coefficients
A_ij = A_ij - doubleB(3,1)/d_l(3,2)
A_ij = A_ij - doubleB(4,1)/d_l(4,2)
A_ij = A_ij - doubleB_M1(5,1)/d_lM1(5,1)
A_ij = A_ij - doubleB_M2(6,1)/d_lM2(6,1)
A_ij = A_ij - doubleB(3,2)/d_l(3,3) - doubleB_Pp(3,2)/d_lPp(3,1)

end if

end if

end function A_ij


function A_ijTest(l, a)
  complex(dp), intent(in) ::  l(dimP), a(5)
  integer :: k
  complex(dp) :: v(dimP,dimP), sum
  complex(dp) :: A_ijTest

A_ijTest = czero

v(:,:) = czero
do k = 1, dimP
  v(k,k) = ci
end do 
v(1,1) = cone

A_ijTest = a(5) + a(1)*sc(l, v(1,:)) + a(2)*sc(l, v(2,:)) + a(3)*sc(l, v(3,:)) + a(4)*sc(l, v(4,:))

end function A_ijTest


! five single cut coefficients for dimension D_S, diagram i, cut jSingle
! reduction is performed with seven properly chosen momenta l
function getTadpoleCoefficients_ij(D_S, pa1, pa2, pa3, i, jSingle, test, testA)
  integer, intent(in) :: i, jSingle, D_S
  complex(dp), intent(in) ::  pa1(3,dimP), pa2(3,dimP), pa3(3,dimP), testA(5)
  logical, intent(in) :: test
  complex(dp) :: getTadpoleCoefficients_ij(5), l(dimP), v(dimP, dimP), lAux(7, dimP), A(7), l_sq, diag(6,21)
  complex(dp) :: den(6,3)
  integer :: k, unitInt
  character*5 :: strDim, strDiag, strCut
  character*70 :: documentName
  logical :: fileExists

getTadpoleCoefficients_ij(:) = (/czero,czero,czero,czero,czero/)

if ( jSingle == 3 .or. jSingle == 2) then
else if ( i /= 1 .and. i /= 3 ) then
else  

lAux = getTadpoleMomenta(i)

if (i == 1) then
  l_sq = mwsq
else if (i == 3) then
  l_sq = mhsq
end if

do k = 1,7
A(k) = A_ij(D_S, lAux(k,:), pa1, pa2, pa3, i, jSingle, test, testA)
!print *, 'A(k) = ', A(k)
end do

! 5 corresponds to 0
getTadpoleCoefficients_ij(2) = (A(1) - A(2)) / sqrt(two * l_sq)
getTadpoleCoefficients_ij(3) = (A(3) - A(4)) / sqrt(two * l_sq)
getTadpoleCoefficients_ij(4) = (A(5) - A(6)) / sqrt(two * l_sq)

getTadpoleCoefficients_ij(5) = ( (A(1) - A(7)*sqrt(three/two)) + getTadpoleCoefficients_ij(3)*sqrt(l_sq/two) ) &
      / (one - sqrt(three/two))
getTadpoleCoefficients_ij(1) = (A(1) - getTadpoleCoefficients_ij(5))*sqrt(two/l_sq) - getTadpoleCoefficients_ij(2)

do k = 1,5
  if (abs(REAL(getTadpoleCoefficients_ij(k))) < limitCoeff ) then
    getTadpoleCoefficients_ij(k) = cmplx(zero,AIMAG(getTadpoleCoefficients_ij(k)),kind=dp)
  end if
  if (abs(AIMAG(getTadpoleCoefficients_ij(k))) < limitCoeff ) then
    getTadpoleCoefficients_ij(k) = cmplx(REAL(getTadpoleCoefficients_ij(k)),zero,kind=dp)
  end if
end do

if (.NOT. test) then
! write coefficients in file
write (strDim, '(I1)') D_S
write (strDiag, '(I1)') i
write (strCut, '(I1)') jSingle
unitInt = unitOffsetA+D_S-4

documentName = trim('diagrams/coefficients/tadpoleCoefficient_'//trim(strDiag)//'_'//trim(strCut)//'_'//trim(strDim)//'.dat')

INQUIRE(FILE=documentName, EXIST=fileExists)
if (.NOT. fileExists ) then
  open(unit = unitInt, file = documentName, status='new')
  write(unitInt, FMT, advance='no') getTadpoleCoefficients_ij(:)
  close(unitInt)
end if
end if

end if
end function getTadpoleCoefficients_ij

function getTadpoleCoefficient0_ij(D_S, pa1, pa2, pa3, i, jSingle)
  integer, intent(in) :: i, jSingle, D_S
  complex(dp), intent(in) ::  pa1(3,dimP), pa2(3,dimP), pa3(3,dimP)
  complex(dp) :: tadpoleCoefficients(5)
  complex(dp) :: getTadpoleCoefficient0_ij

tadpoleCoefficients(:) = czero
tadpoleCoefficients(:) = getTadpoleCoefficients_ij(D_S, pa1, pa2, pa3, i, jSingle, .false., a_Test())
getTadpoleCoefficient0_ij = tadpoleCoefficients(5)
end function getTadpoleCoefficient0_ij

! returns final master integral coefficients in 4 dimensional limit
function finalCoefficients(pa1, pa2, pa3)
  complex(dp), intent(in) :: pa1(3,dimP), pa2(3,dimP), pa3(3,dimP)
  complex(dp) :: finalCoefficients(11)
  complex(dp) :: triangleCoefficients(10), bubbleCoefficients(10), tadpoleCoefficients(5)
  character*70 :: documentName
  integer :: D_S, i
  logical :: fileExists

finalCoefficients(:) = czero
triangleCoefficients(:) = czero
bubbleCoefficients(:) = czero
tadpoleCoefficients(:) = czero
documentName = trim('diagrams/coefficients/finalCoefficients.dat')

! check if file exists, if so, take from file
INQUIRE(FILE=documentName, EXIST=fileExists)
if (fileExists .and. (fromScratch .eqv. .false.)) then
  open(unit = 1, file = trim(documentName), status='old')
  do i = 1,9
    read(1, *) finalCoefficients(i)
  end do
  close(unit = 1)

! if not: calculate
else

print *, 'get triple cut coefficients'
do i = 1,3
  finalCoefficients(i) = getTriangleCoefficient0_i(4, pa1, pa2, pa3, (2*i-1)) 
end do

print *, 'get double and single cut coefficients, diag 1, cut 1'
finalCoefficients(4) = getBubbleCoefficient0_ij(4, pa1, pa2, pa3, 1, 1)
finalCoefficients(10) = getTadpoleCoefficient0_ij(4, pa1, pa2, pa3, 1, 1)

print *, 'get double cut coefficients, diag 2, cut 1'
finalCoefficients(5) = getBubbleCoefficient0_ij(4, pa1, pa2, pa3, 2, 1)

print *, 'get double and single cut coefficients, diag 3, cut 1'
finalCoefficients(6) = getBubbleCoefficient0_ij(4, pa1, pa2, pa3, 3, 1 )
finalCoefficients(11) = getTadpoleCoefficient0_ij(4, pa1, pa2, pa3, 3, 1)

print *, 'get double cut coefficients, diag 4, cut 1'
finalCoefficients(7) = getBubbleCoefficient0_ij(4, pa1, pa2, pa3, 4, 1 )

print *, 'get double cut coefficients, diag 1, cut 2'
finalCoefficients(8) = getBubbleCoefficient0_ij(4, pa1, pa2, pa3, 1, 2 )

print *, 'get double cut coefficients, diag 3, cut 2'
finalCoefficients(9) = getBubbleCoefficient0_ij(D_S, pa1, pa2, pa3, 3, 2 )

! write into file
open(unit = 1, file = trim(documentName), status='replace')
write(1, FMT, advance='no') finalCoefficients(1)
write(1, FMT, advance='no') finalCoefficients(2)
write(1, FMT, advance='no') finalCoefficients(3)
write(1, FMT, advance='no') finalCoefficients(4)
write(1, FMT, advance='no') finalCoefficients(5)
write(1, FMT, advance='no') finalCoefficients(6)
write(1, FMT, advance='no') finalCoefficients(7)
write(1, FMT, advance='no') finalCoefficients(8)
write(1, FMT, advance='no') finalCoefficients(9)
close(1)

end if

end function finalCoefficients


end module