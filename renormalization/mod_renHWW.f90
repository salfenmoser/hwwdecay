module mod_renHWW

    use mod_types
    use mod_consts_dp
    use common_def
    
    use mod_auxfunctions     
    use mod_kinematics
    use mod_triangle
    use mod_bubble
    use mod_tadpole

    implicit none
    
  public :: renWFromHWW, renHFromHWW

  integer :: xiLength = 7
  ! have to be chosen sufficiently far from each other, otherwise exact same results after multiplication with xi even though should be small differences
  !complex(dp) :: xi(11) = [zero, one*1e-9, two*1e-9, three*1e-9, four*1e-9, five*1e-9, six*1e-9, 7._dp*1e-9, 8._dp*1e-9, 9._dp*1e-9, one*1e-8]
  !complex(dp) :: xi(4) = [zero, one*1e-8, one*1e-7, one*1e-6]
  complex(dp) :: xi(7) = [zero, -five*1e-8, -three*1e-8, -one*1e-8, one*1e-8, three*1e-8, five*1e-8]


  contains

! returns renormalization constants (only coefficients of master integrals) for higgs boson
! delta m in first row, delta Z in second row, 
function renHFromHWW(dim, h, w1, w2, ep1, ep2)
  integer, intent(in) :: dim, ep1, ep2
  complex(dp), intent(in) :: h(3, dimP), w1(3, dimP), w2(3, dimP)
  complex(dp) :: hShifted(3, dimP), w1Shifted(3, dimP), pol1(6,dimE), pol2(6,dimE)
  complex(dp) :: kappa(dimP), B6(xiLength), B7(xiLength), A8(xiLength), A9(xiLength), pSq(xiLength), vertVal(xiLength)
  integer :: j, D_S, diag, cut
  character*70 :: docNameWrite, docNameRe, docNameIm
  complex(dp) :: renHFromHWW(3,11), renHFromHWWRe(3,11), renHFromHWWIm(3,11)
  
renHFromHWW(:,:) = czero
renHFromHWWRe(:,:) = czero
renHFromHWWIm(:,:) = czero

B6(:) = czero
B7(:) = czero
A8(:) = czero
A9(:) = czero
vertVal(:) = czero

call HShiftMomentum(h(1,:), w1(1,:), kappa)
hShifted = h
w1Shifted = w1
    
do j = 1,xiLength
  print *, 'calculate self energy for higgs boson off-shell momenta with xi = ', xi(j)
  print *, 'j = ', j
  hShifted(1,:) = h(1,:) + kappa(:) * xi(j)
  w1Shifted(1,:) = w1(1,:) - kappa(:) * xi(j)
  call spinonepol_6d(w1Shifted(1,:),pol1)
  w1Shifted(2,:) = pol1(ep1, :)

  ! print *, 'pol vec w1shifted = ', w1Shifted(2,:)
  ! print *, 'pol vec w2 = ', w2(2,:)
  ! print *, 'sc prod = ', sc(w1Shifted(2,:),w2(2,:))

  ! for transversal polarizations, on-shell pol vector is not continuous to off-shell.
  ! do only for ep=3
  pSq(j) = sq(hShifted(1,:))
  print *, 'pSq - mhsq = ', pSq(j) - mhsq
  vertVal(j) = two*g_Coupling*mw*sc(w1Shifted(2,:),w2(2,:))*ci
  print *, 'vertex factor = ', vertVal(j)

  call system('rm ~/Documents/thesis/fortran/UnitarityMethodCalculation/diagrams/coefficients/*')
  call sleep(1)

B6(j) = getBubbleCoefficient0_ij(4, hShifted, w1Shifted, w2, 1, 2)
B7(j) = getBubbleCoefficient0_ij(4, hShifted, w1Shifted, w2, 3, 2)
A8(j) = getTadpoleCoefficient0_ij(4, hShifted, w1Shifted, w2, 1, 1)
A9(j) = getTadpoleCoefficient0_ij(4, hShifted, w1Shifted, w2, 3, 1)

  print *, 'final coefficient array:'
  print *, B6(j)
  print *, B7(j)
  print *, A8(j)
  print *, A9(j)

if (j > 1) then
  B6(j) = (B6(j) - B6(1))/vertVal(j)
  B7(j) = (B7(j) - B7(1))/vertVal(j)
  A8(j) = (A8(j) - A8(1))/vertVal(j)
  A9(j) = (A9(j) - A9(1))/vertVal(j)
end if

end do
    
print *, 'write in file '
docNameWrite = 'renormalization/renormalizationHWW/hByShiftArray.dat'
open(unit = 1, file = trim(docNameWrite), status='replace')
write(1, FMT, advance='no') xi(2:xiLength)
write(1, *) ''
write(1, FMT, advance='no') B6(2:xiLength)
write(1, *) ''
write(1, FMT, advance='no') B7(2:xiLength)
write(1, *) ''
write(1, FMT, advance='no') A8(2:xiLength)
write(1, *) ''
write(1, FMT, advance='no') A9(2:xiLength)
close(1)
    
print *, 'call python function for fit'
call system('python renormalization/renormalizationHWW/hFitLin.py')
call sleep(3)
        
docNameRe = 'renormalization/renormalizationHWW/coeffValhLinRe.dat'
open(unit = 1, file = trim(docNameRe), status='old')
docNameIm = 'renormalization/renormalizationHWW/coeffValhLinIm.dat'
open(unit = 2, file = trim(docNameIm), status='old')
do j = 1,4
  read(1, *) renHFromHWWRe(1,7+j)
  read(2, *) renHFromHWWIm(1,7+j)
end do
close(1)
close(2)

renHFromHWW(1,:) = renHFromHWWRe(1,:) + ci*renHFromHWWIm(1,:)

docNameRe = 'renormalization/renormalizationHWW/coeffDerhLinRe.dat'
open(unit = 1, file = trim(docNameRe), status='old')
docNameIm = 'renormalization/renormalizationHWW/coeffDerhLinIm.dat'
open(unit = 2, file = trim(docNameIm), status='old')
do j = 1,4
  read(1, *) renHFromHWWRe(2,7+j)
  read(2, *) renHFromHWWIm(2,7+j)
end do
close(1)
close(2)

renHFromHWW(2,:) = renHFromHWWRe(2,:) + ci*renHFromHWWIm(2,:)
        
do j=1,11
  renHFromHWW(2,j) = -1*renHFromHWW(2,j)
end do

renHFromHWW(3,8) = - renHFromHWW(1,8)
renHFromHWW(3,9) = - renHFromHWW(1,9)
      
end function renHFromHWW
  
  
! returns renormalization constants (only coefficients of master integrals) for W boson
! delta m in first row, delta Z in second row 
function renWFromHWW(dim, h, w1, w2, ep1, ep2)
  integer, intent(in) :: dim, ep1, ep2
  complex(dp), intent(in) :: h(3, dimP), w1(3, dimP), w2(3, dimP)
  complex(dp) :: w1Shifted(3, dimP), w2Shifted(3, dimP), hShifted(3,dimP), pol1(6, dimE), pol2(6, dimE)
  complex(dp) :: kappa(dimP), B4(xiLength), B5(xiLength), A8(xiLength), A9(xiLength), pSq(xiLength), vertVal(xiLength), vertValTad(xiLength)
  integer :: j
  character*70 :: docNameWrite, docNameRe, docNameIm
  complex(dp) :: renWFromHWW(3,11), renWFromHWWRe(3,11), renWFromHWWIm(3,11)
  
renWFromHWW(:,:) = czero
renWFromHWWRe(:,:) = czero
renWFromHWWIm(:,:) = czero

call WShiftMomentum(kappa)
w1Shifted = w1
w2Shifted = w2
hShifted = h

B4(:) = czero
B5(:) = czero
A8(:) = czero
A9(:) = czero

! in propagator calculation, boolean HWWOffShellW has to be set to true, otherwise tadpoles will vanish

do j = 1, xiLength
  print *, 'calculate self energy for W boson off-shell momenta with xi = ', xi(j)
  print *, 'j = ', j

  w1Shifted(1,:) = w1(1,:) + kappa(:) * xi(j)
  call spinonepol_6d(w1Shifted(1,:),pol1)
  w1Shifted(2,:) = pol1(ep1, :)

  hShifted(1,:) = h(1,:) - kappa(:) * xi(j)

  !w2Shifted(1,:) = w2(1,:) + kappa(:) * xi(j)
  ! call spinonepol_6d(w2Shifted(1,:),pol2)
  ! w2Shifted(2,:) = pol2(ep2, :)

  ! print *, 'momentum k1 = ', w1Shifted(1,:)
  ! print *, 'momentum k2 = ', w2Shifted(1,:)

  pSq(j) = sq(w1Shifted(1,:))
  !print *, 'pSq - mwsq = ', pSq(j) - mwsq
  vertVal(j) = two*ci*g_Coupling*mw*sc(w1Shifted(2,:),w2Shifted(2,:))
  print *, 'vertex value = ', vertVal(j)
  vertValTad(j) = two*ci*g_Coupling*mw*sc(WPnum(w1Shifted(1,:), w1Shifted(2,:)),w2Shifted(2,:))
  print *, 'vertex value tadpole = ', vertValTad(j)

  ! print *, 'polarization vector w1 : ', w1Shifted(2,:)
  ! print *, 'polarization vector w2 : ', w2Shifted(2,:)
      
  call system('rm ~/Documents/thesis/fortran/UnitarityMethodCalculation/diagrams/coefficients/*')
  call sleep(1)

  B4(j) = getBubbleCoefficient0_ij(4, hShifted, w1Shifted, w2Shifted, 1, 1)
  B5(j) = getBubbleCoefficient0_ij(4, hShifted, w1Shifted, w2Shifted, 3, 1)
  A8(j) = getTadpoleCoefficient0_ij(4, hShifted, w1Shifted, w2Shifted, 1, 1)
  A9(j) = getTadpoleCoefficient0_ij(4, hShifted, w1Shifted, w2Shifted, 3, 1)
  
  ! B4(j) = B4(j) - getBubbleCoefficient0_ij(6, h, w1Shifted, w2Shifted, 1, 1)
  ! B5(j) = B5(j) - getBubbleCoefficient0_ij(6, h, w1Shifted, w2Shifted, 3, 1)

  print *, 'final coefficient array:'
  print *, B4(j)
  print *, B5(j)
  print *, A8(j)
  print *, A9(j)
      
if (j > 1) then
  B4(j) = (B4(j)- B4(1))/vertVal(j)
  B5(j) = (B5(j)- B5(1))/vertVal(j)
  A8(j) = (A8(j)- A8(1))/vertVal(j)
  A9(j) = (A9(j)- A9(1))/vertVal(j)

print *, 'passing to python:'
print *, B4(j)
print *, B5(j)
print *, A8(j)
print *, A9(j)
end if

end do
  
print *, 'write in file '
docNameWrite = 'renormalization/renormalizationHWW/WByShiftArray.dat'
open(unit = 1, file = trim(docNameWrite), status='replace')
write(1, FMT, advance='no') xi(2:xiLength)
write(1, *) ''
write(1, FMT, advance='no') B4(2:xiLength)
write(1, *) ''
write(1, FMT, advance='no') B5(2:xiLength)
write(1, *) ''
write(1, FMT, advance='no') A8(2:xiLength)
write(1, *) ''
write(1, FMT, advance='no') A9(2:xiLength)
close(1)
  
print *, 'call python function for fit'
call system('python renormalization/renormalizationHWW/WFitLin.py')
call sleep(3)

docNameRe = 'renormalization/renormalizationHWW/coeffValWLinRe.dat'
open(unit = 1, file = trim(docNameRe), status='old')
docNameIm = 'renormalization/renormalizationHWW/coeffValWLinIm.dat'
open(unit = 2, file = trim(docNameIm), status='old')
do j = 1,2
  read(1, *) renWFromHWWRe(1,2+2*j)
  read(2, *) renWFromHWWIm(1,2+2*j)
end do
do j = 1,2
  read(1, *) renWFromHWWRe(1,9+j)
  read(2, *) renWFromHWWRe(1,9+j)
end do
close(1)
close(2)
        
renWFromHWW(1,:) = renWFromHWWRe(1,:) + ci*renWFromHWWIm(1,:)

docNameRe = 'renormalization/renormalizationHWW/coeffDerWLinRe.dat'
open(unit = 1, file = trim(docNameRe), status='old')
docNameIm = 'renormalization/renormalizationHWW/coeffDerWLinIm.dat'
open(unit = 2, file = trim(docNameIm), status='old')
do j = 1,2
  read(1, *) renWFromHWWRe(2,2+2*j)
  read(2, *) renWFromHWWIm(2,2+2*j)
end do
do j = 1,2
  read(1, *) renWFromHWWRe(2,9+j)
  read(2, *) renWFromHWWIm(2,9+j)
end do
close(1)
close(2)

renWFromHWW(2,:) = renWFromHWWRe(2,:) + ci*renWFromHWWIm(2,:)

do j=1,9
  renWFromHWW(2,j) = -1*renWFromHWW(2,j)
end do

renWFromHWW(3,4) = - renWFromHWW(1,4)
renWFromHWW(3,6) = - renWFromHWW(1,6)

do j = 1, 3
  renWFromHWW(j,5) = renWFromHWW(j,4)
  renWFromHWW(j,7) = renWFromHWW(j,6)
end do
    
end function renWFromHWW

end module
  