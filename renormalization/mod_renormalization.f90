module mod_renormalization
    use mod_types
    use mod_consts_dp
    use common_def

    use mod_vectorCurrents
    use mod_renormConst
    use mod_triangle
    use mod_bubble
    use mod_tadpole
    
implicit none

public :: renormRes

contains

function renormRes(h, w1, w2, ep1, ep2)
  integer, intent(in) :: ep1, ep2
  complex(dp), intent(in) :: h(3,dimP), w1(3,dimP), w2(3,dimP)
  complex(dp) :: Zg(2,11), renConstW(3,11), renConstH(3,11), finalCoeff(11)
  character*50 :: docName
  complex(dp) ::renormRes(2,11)

renormRes(:,:) = czero

finalCoeff(:) = finalCoefficients(h, w1, w2)

Zg(:,:) = Z_g()
renConstW(:,:) = renW(.true., .false., 4, sq(w1(1,:)), h, w1, w2, ep1, ep2)
renConstH(:,:) = renH(.true., .false., 4, sq(h(1,:)), h, w1, w2, ep1, ep2)

renormRes(1,:) = finalCoeff(:) + sc( W2H(int(w2(3,1)),w1) , w2(2,:) ) * ( Zg(1,:) + renConstW(1,:)/two/mwsq + renConstW(2,:) + renConstH(2,:)/two )
renormRes(2,:) = sc( W2H(int(w2(3,1)),w1) , w2(2,:) ) * ( Zg(2,:) + renConstW(3,:) + renConstH(3,:)/two )

! docName = trim('renormalization/renormResult.dat')
! open(unit = 1, file = trim(docName), status='replace')
! write(1, FMT, advance='no') renormRes(1,:)
! write(1, FMT, advance='no') ''
! write(1, FMT, advance='no') renormRes(2,:)
! close(1)

end function renormRes

end module
  