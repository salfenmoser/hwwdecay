import numpy as np
from math import log10, floor
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit
from scipy.misc import derivative
import os

print '--------------- polynomial fit in python --------------------'

def round_sig(x, sig=5):
    if (x == 0):
        return 0
    return round(x, sig-int(floor(log10(abs(x))))-1)

xi = []

B6Re = []
B7Re = []
A8Re = []
A9Re = []

valRe = []
derRe = []

B6Im = []
B7Im = []
A8Im = []
A9Im = []

valIm = []
derIm = []

coeffList = [B6Re, B7Re, A8Re, A9Re, B6Im, B7Im, A8Im, A9Im]
offSet = []
scale = []

mW = 80.379
mH = 125.18

path = ''
#path = 'renormalization/renormalizationHWW/'
coefficients = open(path + 'hByShiftArray.dat', "r").readlines()

#xiInput = [10**(-9), 10**(-8), 5*10**(-8), 10**(-7), 5*10**(-7)]

denFactor = 2*mH

xiLength = 6
for i in range(xiLength):
    xi.append(eval(coefficients[2*i]))

    B6Im.append(eval(coefficients[1*2*xiLength + 2*i])*denFactor*xi[i])# - eval(coefficients[2*xiLength]))
    B7Im.append(eval(coefficients[2*2*xiLength + 2*i])*denFactor*xi[i])#) - eval(coefficients[2*2*xiLength]))
    A8Im.append(eval(coefficients[3*2*xiLength + 2*i])*denFactor*xi[i])#) - eval(coefficients[3*2*xiLength]))
    A9Im.append(eval(coefficients[4*2*xiLength + 2*i])*denFactor*xi[i])#) - eval(coefficients[4*2*xiLength]))

    B6Re.append(eval(coefficients[1*2*xiLength + 2*i+1])*denFactor*xi[i])#) - eval(coefficients[2*xiLength+1]))
    B7Re.append(eval(coefficients[2*2*xiLength + 2*i+1])*denFactor*xi[i])#) - eval(coefficients[2*2*xiLength+1]))
    A8Re.append(eval(coefficients[3*2*xiLength + 2*i+1])*denFactor*xi[i])#) - eval(coefficients[3*2*xiLength+1]))
    A9Re.append(eval(coefficients[4*2*xiLength + 2*i+1])*denFactor*xi[i])#) - eval(coefficients[4*2*xiLength+1]))

pSq = [denFactor*el for el in xi]
#print 'pSQ= ', pSq

print 'xi = ', xi
print 'B6Im = ', B6Im
print 'B7Im = ', B7Im
print 'A8Im = ', A8Im
print 'A9Im = ', A9Im

for cL in coeffList:
    offSet.append(cL[-1])
    scale.append(cL[-1])
    for i in range(xiLength):
        cL[i] = cL[i]-offSet[-1]

#################################################################################

def fitConst(x_var, *c):
    return c[0]

def fitLin(x_var, *c):
    return c[0] + c[1]*x_var

def fitQuad(x_var, *c):
    return c[0] + c[1]*x_var + c[2]*x_var**2

def coeffFit(x_var, *c):
    return fitLin(x_var, *c)
initialValues = [0, 0]

#################################################################################
fig1, axs = plt.subplots(2,2, sharex=False, figsize=(20,15))
fig1.subplots_adjust(hspace=0.5)
fig1.subplots_adjust(wspace=0.5)

plt.rc('text', usetex=True)
plt.rc('font', family='serif')

xArange = np.linspace(min(pSq), max(pSq), 10000)
initialValues = [0,0]

axs[0,0].plot(pSq,B6Re, '+', color='blue')
axs[0,1].plot(pSq,B7Re, '+', color='blue')
axs[1,0].plot(pSq,A8Re, '+', color='blue')
axs[1,1].plot(pSq,A9Re, '+', color='blue')

axs[0,0].grid(True)
axs[0,1].grid(True)
axs[1,0].grid(True)
axs[1,1].grid(True)

axs[0,0].set_xlabel(r'$2 \xi p \kappa$', fontsize=14)
axs[0,0].set_ylabel(r'$((b_{02}^{(1)})_0 (\xi) - (b_{02}^{(1)})_0 (0)) \cdot \frac{2\xi p \kappa}{2ig m_W \epsilon^{(3)}(\hat{k}_1) \epsilon^{(3)}(k_2)} $', fontsize=14)

axs[0,1].set_xlabel(r'$2 \xi p \kappa$', fontsize=14)
axs[0,1].set_ylabel(r'$(b_{02}^{(3)})_0 (\xi) - (b_{02}^{(3)})_0 (0)) \cdot \frac{2\xi p \kappa}{2ig m_W \epsilon^{(3)}(\hat{k}_1) \epsilon^{(3)}(k_2)} $', fontsize=14)

axs[1,0].set_xlabel(r'$2 \xi p \kappa$', fontsize=14)
axs[1,0].set_ylabel(r'$(a_{0}^{(W)} (\xi) - a_0^{(W)} (0)) \cdot \frac{2\xi p \kappa}{2ig m_W \epsilon^{(3)}(\hat{k}_1) \epsilon^{(3)}(k_2)} $', fontsize=14)

axs[1,1].set_xlabel(r'$2 \xi p \kappa$', fontsize=14)
axs[1,1].set_ylabel(r'$(a_{0}^{(H)} (\xi) - a_0^{(H)} (0)) \cdot \frac{2\xi p \kappa}{2ig m_W \epsilon^{(3)}(\hat{k}_1) \epsilon^{(3)}(k_2)} $', fontsize=14)

fit_res, covar = curve_fit(coeffFit, pSq, B6Re, p0=initialValues)
print 'fit results: ', fit_res
print 'covariance matrix: ', covar
axs[0,0].plot(xArange,coeffFit(xArange, *fit_res),'r-', linewidth = 1, label = r'Fitted function: $f(\xi) = $ ' + str(round_sig(fit_res[0])) + r' $ + $ ' + str(round_sig(fit_res[1])) + r' $ \cdot $ $ (2 \xi p \kappa) $')

def B6Fit(x):
    return coeffFit(x, *fit_res)
valRe.append(B6Fit(0.0) + offSet[0])
derRe.append(derivative(B6Fit, 0.0, dx=1e-8))

#print 're- rescaled B6 = ', fit_res[0]*scale[0]

fit_res, covar = curve_fit(coeffFit, pSq, B7Re, p0=initialValues)
print 'fit results: ', fit_res
print 'covariance matrix: ', covar
axs[0,1].plot(xArange,coeffFit(xArange, *fit_res),'r-', linewidth = 1, label = r'Fitted function: $f(\xi) = $ ' + str(round_sig(fit_res[0])) + r' $ + $ ' + str(round_sig(fit_res[1])) + r' $ \cdot $ $ (2 \xi p \kappa) $')

def B7Fit(x):
    return coeffFit(x, *fit_res)
valRe.append(B7Fit(0.0)+ offSet[1])
derRe.append(derivative(B7Fit, 0.0, dx=1e-8))

fit_res, covar = curve_fit(coeffFit, pSq, A8Re, p0=initialValues)
print 'fit results: ', fit_res
print 'covariance matrix: ', covar
axs[1,0].plot(xArange,coeffFit(xArange, *fit_res),'r-', linewidth = 1, label = r'Fitted function: $f(\xi) = $ ' + str(round_sig(fit_res[0])) + r' $ + $ ' + str(round_sig(fit_res[1])) + r' $ \cdot $ $ (2 \xi p \kappa) $')

def A8Fit(x):
    return coeffFit(x, *fit_res)
valRe.append(A8Fit(0.0)+ offSet[2])
derRe.append(derivative(A8Fit, 0.0, dx=1e-8))

fit_res, covar = curve_fit(coeffFit, pSq, A9Re, p0=initialValues)
print 'fit results: ', fit_res
print 'covariance matrix: ', covar
axs[1,1].plot(xArange,coeffFit(xArange, *fit_res),'r-', linewidth = 1, label = r'Fitted function: $f(\xi) = $ ' + str(round_sig(fit_res[0])) + r' $ + $ ' + str(round_sig(fit_res[1])) + r' $ \cdot $ $ (2 \xi p \kappa) $')

def A9Fit(x):
    return coeffFit(x, *fit_res)
valRe.append(A9Fit(0.0)+ offSet[3])
derRe.append(derivative(A9Fit, 0.0, dx=1e-8))

#plt.suptitle(r'Real part of B4, B5, A8, A9 coefficients as function of $p^2$ from HWW evaluation')
#plt.legend()
axs[0,0].legend(loc=1, fontsize=14)
axs[1,0].legend(loc=1, fontsize=14)
axs[0,1].legend(loc=1, fontsize=14)
axs[1,1].legend(loc=1, fontsize=14)

plt.savefig(path + 'hFitLinRe.png')
plt.show()

print 'values real : ', valRe
print 'derivatives Real : ', derRe

file = open(path + 'coeffValhLinRe.dat','w')
for element in valRe:
    file.write(str(element) + '\n')

file = open(path + 'coeffDerhLinRe.dat','w')
for element in derRe:
    file.write(str(element) + '\n')

valRe = [el/2 for el in valRe]
derRe = [el/2 for el in derRe]
print 'values real div2: ', valRe
print 'derivatives Real div2: ', derRe

################################################################################
fig2, axs = plt.subplots(2,2, sharex=False, figsize=(20,15))
fig2.subplots_adjust(hspace=0.5)
fig2.subplots_adjust(wspace=0.5)

xArange = np.linspace(min(pSq), max(pSq), 10000)
initialValues = [0,0]

plt.rc('text', usetex=True)
plt.rc('font', family='serif')

axs[0,0].plot(pSq,B6Im, '+', color='blue')
axs[0,1].plot(pSq,B7Im, '+', color='blue')
axs[1,0].plot(pSq,A8Im, '+', color='blue')
axs[1,1].plot(pSq,A9Im, '+', color='blue')

fit_res, covar = curve_fit(coeffFit, pSq, B6Im, p0=initialValues)
print 'fit results: ', fit_res
print 'covariance matrix: ', covar
axs[0,0].plot(xArange,coeffFit(xArange, *fit_res),'r-', linewidth = 1)

def B6Fit(x):
    return coeffFit(x, *fit_res)
valIm.append(B6Fit(0.0) + offSet[4])
derIm .append(derivative(B6Fit, 0.0, dx=1e-8))

fit_res, covar = curve_fit(coeffFit, pSq, B7Im, p0=initialValues)
print 'fit results: ', fit_res
print 'covariance matrix: ', covar
axs[0,1].plot(xArange,coeffFit(xArange, *fit_res),'r-', linewidth = 1)

def B7Fit(x):
    return coeffFit(x, *fit_res)
valIm.append(B7Fit(0.0) + offSet[5])
derIm.append(derivative(B7Fit, 0.0, dx=1e-8))

fit_res, covar = curve_fit(coeffFit, pSq, A8Im, p0=initialValues)
print 'fit results: ', fit_res
print 'covariance matrix: ', covar
axs[1,0].plot(xArange,coeffFit(xArange, *fit_res),'r-', linewidth = 1)

def A8Fit(x):
    return coeffFit(x, *fit_res)
valIm.append(A8Fit(0.0) + offSet[6])
derIm.append(derivative(A8Fit, 0.0, dx=1e-8))

fit_res, covar = curve_fit(coeffFit, pSq, A9Im, p0=initialValues)
print 'fit results: ', fit_res
print 'covariance matrix: ', covar
axs[1,1].plot(xArange,coeffFit(xArange, *fit_res),'r-', linewidth = 1)

def A9Fit(x):
    return coeffFit(x, *fit_res)
valIm.append(A9Fit(0.0)+ offSet[7])
derIm.append(derivative(A9Fit, 0.0, dx=1e-8))

plt.suptitle(r'Imaginary part of B4, B5, A8, A9 coefficients as function of $p^2$')
#plt.legend()
plt.savefig(path + 'hFitLinIm.png')
plt.show()

print 'values imaginary : ', valIm
print 'derivatives imaginary : ', derIm

file = open(path + 'coeffValhLinIm.dat','w')
for element in valIm:
    #print element
    file.write(str(element) + '\n')

file = open(path + 'coeffDerhLinIm.dat','w')
for element in derIm:
    file.write(str(element) + '\n')

print '--------------- END polynomial fit in python --------------------'
