import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit
from scipy.misc import derivative
import os

print '--------------- polynomial fit in python --------------------'

xi = []

B4Re = []
B5Re = []
A8Re = []
A9Re = []

valRe = []
derRe = []

B4Im = []
B5Im = []
A8Im = []
A9Im = []

valIm = []
derIm = []

coeffList = [B4Re, B5Re, A8Re, A9Re, B4Im, B5Im, A8Im, A9Im]
offSet = []
scale = []

mW = 80.398
mH = 125.0

path = ''
#path = 'renormalization/renormalizationHWW/'
coefficients = open(path + 'WByShiftArray.dat', "r").readlines()

# xiInput = [10**(-9), 10**(-8), 5*10**(-8), 10**(-7), 5*10**(-7)]

# print xiInput
# denInput = [2*mH*el for el in xiInput]
# print denInput

denFactor = -mH/2.

xiLength = 10
for i in range(xiLength):
    xi.append(eval(coefficients[2*i]))

    B4Re.append(eval(coefficients[1*2*xiLength + 2*i]))#*pSq[i])# - eval(coefficients[2*xiLength]))
    B5Re.append(eval(coefficients[2*2*xiLength + 2*i]))#*pSq[i])#) - eval(coefficients[2*2*xiLength]))
    A8Re.append(eval(coefficients[3*2*xiLength + 2*i]))#*pSq[i])#) - eval(coefficients[3*2*xiLength]))
    A9Re.append(eval(coefficients[4*2*xiLength + 2*i]))#*pSq[i])#) - eval(coefficients[4*2*xiLength]))

    B4Im.append(eval(coefficients[1*2*xiLength + 2*i+1]))#*pSq[i])#) - eval(coefficients[2*xiLength+1]))
    B5Im.append(eval(coefficients[2*2*xiLength + 2*i+1]))#*pSq[i])#) - eval(coefficients[2*2*xiLength+1]))
    A8Im.append(eval(coefficients[3*2*xiLength + 2*i+1]))#*pSq[i])#) - eval(coefficients[3*2*xiLength+1]))
    A9Im.append(eval(coefficients[4*2*xiLength + 2*i+1]))#*pSq[i])#) - eval(coefficients[4*2*xiLength+1]))

pSq = [denFactor*el for el in xi]

for cL in coeffList:
    offSet.append(cL[-1])
    scale.append(cL[-1])
    for i in range(xiLength):
        if (scale[-1] != 0):
            cL[i] = cL[i]/scale[-1]


#################################################################################

def fitConst(x_var, *c):
    return c[0]

def fitLin(x_var, *c):
    return c[0] + c[1]*x_var

def fitQuad(x_var, *c):
    return c[0] + c[1]*x_var + c[2]*x_var**2

def coeffFit(x_var, *c):
    return fitLin(x_var, *c)/x_var

initialValues = [0, 0]

#################################################################################
fig1, axs = plt.subplots(2,2, sharex=False, figsize=(20,15))
fig1.subplots_adjust(hspace=0.5)
fig1.subplots_adjust(wspace=0.5)

plt.rc('text', usetex=True)
plt.rc('font', family='serif')

xArange = np.linspace(min(pSq), max(pSq), 10000)
initialValues = [0,0]

axs[0,0].plot(pSq,B4Re, '+', color='blue')
axs[0,1].plot(pSq,B5Re, '+', color='blue')
axs[1,0].plot(pSq,A8Re, '+', color='blue')
axs[1,1].plot(pSq,A9Re, '+', color='blue')

fit_res, covar = curve_fit(coeffFit, pSq, B4Re, p0=initialValues)
print 'fit results: ', fit_res
print 'covariance matrix: ', covar
axs[0,0].plot(xArange,coeffFit(xArange, *fit_res),'r-', linewidth = 1)

def B4Fit(x):
    return fitLin(x, *fit_res)
valRe.append(B4Fit(0.0)*scale[0])
derRe.append(derivative(B4Fit, 0.0, dx=1e-8)*scale[0])


fit_res, covar = curve_fit(coeffFit, pSq, B5Re, p0=initialValues)
print 'fit results: ', fit_res
print 'covariance matrix: ', covar
axs[0,1].plot(xArange,coeffFit(xArange, *fit_res),'r-', linewidth = 1)

def B5Fit(x):
    return fitLin(x, *fit_res)
valRe.append(B5Fit(0.0)*scale[1])
derRe.append(derivative(B5Fit, 0.0, dx=1e-8)*scale[1])

fit_res, covar = curve_fit(coeffFit, pSq, A8Re, p0=initialValues)
print 'fit results: ', fit_res
print 'covariance matrix: ', covar
axs[1,0].plot(xArange,coeffFit(xArange, *fit_res),'r-', linewidth = 1)

def A8Fit(x):
    return fitLin(x, *fit_res)
valRe.append(A8Fit(0.0)*scale[2])
derRe.append(derivative(A8Fit, 0.0, dx=1e-8)*scale[2])

fit_res, covar = curve_fit(coeffFit, pSq, A9Re, p0=initialValues)
print 'fit results: ', fit_res
print 'covariance matrix: ', covar
axs[1,1].plot(xArange,coeffFit(xArange, *fit_res),'r-', linewidth = 1)

def A9Fit(x):
    return fitLin(x, *fit_res)
valRe.append(A9Fit(0.0)*scale[3])
derRe.append(derivative(A9Fit, 0.0, dx=1e-8)*scale[3])

plt.suptitle(r'Real part of B4, B5, A8, A9 coefficients as function of $p^2$ from HWW evaluation')
#plt.legend()
plt.savefig(path + 'WFitHypRe.png')
plt.show()

print 'values real : ', valRe
print 'derivatives Real : ', derRe

file = open(path + 'coeffValWHypRe.dat','w')
for element in valRe:
    file.write(str(element) + '\n')

file = open(path + 'coeffDerWHypRe.dat','w')
for element in derRe:
    file.write(str(element) + '\n')

################################################################################
fig2, axs = plt.subplots(2,2, sharex=False, figsize=(20,15))
fig2.subplots_adjust(hspace=0.5)
fig2.subplots_adjust(wspace=0.5)

xArange = np.linspace(min(pSq), max(pSq), 10000)
initialValues = [0,0]

plt.rc('text', usetex=True)
plt.rc('font', family='serif')

axs[0,0].plot(pSq,B4Im, '+', color='blue')
axs[0,1].plot(pSq,B5Im, '+', color='blue')
axs[1,0].plot(pSq,A8Im, '+', color='blue')
axs[1,1].plot(pSq,A9Im, '+', color='blue')

fit_res, covar = curve_fit(coeffFit, pSq, B4Im, p0=initialValues)
print 'fit results: ', fit_res
print 'covariance matrix: ', covar
axs[0,0].plot(xArange,coeffFit(xArange, *fit_res),'r-', linewidth = 1)

def B4Fit(x):
    return fitLin(x, *fit_res)
valIm.append(B4Fit(0.0)*scale[4])
derIm .append(derivative(B4Fit, 0.0, dx=1e-8)*scale[4])

fit_res, covar = curve_fit(coeffFit, pSq, B5Im, p0=initialValues)
print 'fit results: ', fit_res
print 'covariance matrix: ', covar
axs[0,1].plot(xArange,coeffFit(xArange, *fit_res),'r-', linewidth = 1)

def B5Fit(x):
    return fitLin(x, *fit_res)
valIm.append(B5Fit(0.0)*scale[5])
derIm.append(derivative(B5Fit, 0.0, dx=1e-8)*scale[5])

fit_res, covar = curve_fit(coeffFit, pSq, A8Im, p0=initialValues)
print 'fit results: ', fit_res
print 'covariance matrix: ', covar
axs[1,0].plot(xArange,coeffFit(xArange, *fit_res),'r-', linewidth = 1)

def A8Fit(x):
    return fitLin(x, *fit_res)
valIm.append(A8Fit(0.0)*scale[6])
derIm.append(derivative(A8Fit, 0.0, dx=1e-8)*scale[6])

fit_res, covar = curve_fit(coeffFit, pSq, A9Im, p0=initialValues)
print 'fit results: ', fit_res
print 'covariance matrix: ', covar
axs[1,1].plot(xArange,coeffFit(xArange, *fit_res),'r-', linewidth = 1)

def A9Fit(x):
    return fitLin(x, *fit_res)
valIm.append(A9Fit(0.0)*scale[7])
derIm.append(derivative(A9Fit, 0.0, dx=1e-8)*scale[7])

plt.suptitle(r'Imaginary part of B4, B5, A8, A9 coefficients as function of $p^2$')
#plt.legend()
plt.savefig(path + 'WFitHypIm.png')
plt.show()

print 'values imaginary : ', valIm
print 'derivatives imaginary : ', derIm

file = open(path + 'coeffValWHypIm.dat','w')
for element in valIm:
    file.write(str(element) + '\n')

file = open(path + 'coeffDerWHypIm.dat','w')
for element in derIm:
    file.write(str(element) + '\n')

print '--------------- END polynomial fit in python --------------------'