import numpy as np
from math import log10, floor
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit
from scipy.misc import derivative
import os

print '--------------- polynomial fit in python --------------------'

def round_sig(x, sig=5):
    if (x == 0):
        return 0
    return round(x, sig-int(floor(log10(abs(x))))-1)

def coeffFit(x_var, *c):
    return c[0] + c[1]*x_var#+ c[2]*x_var**2

initialValues = [0,0]

pSq = []

B6Re = []
B7Re = []
A8Re = []
A9Re = []
derRe = []

B6Im = []
B7Im = []
A8Im = []
A9Im = []
derIm = []

mW = 80.379
mH = 125.18

path = ''
#path = 'renormalization/renormalizationSE/'
coefficients = open(path + 'hByShiftArray.dat', "r").readlines()

xiLength = 7
for i in range(xiLength):
    pSq.append(eval(coefficients[2*i]) - mH**2)

    B6Re.append(eval(coefficients[2*xiLength + 2*i]) - eval(coefficients[2*xiLength]))
    B7Re.append(eval(coefficients[2*2*xiLength + 2*i]) - eval(coefficients[2*2*xiLength]))
    A8Re.append(eval(coefficients[3*2*xiLength + 2*i]) - eval(coefficients[3*2*xiLength]))
    A9Re.append(eval(coefficients[4*2*xiLength + 2*i]) - eval(coefficients[4*2*xiLength]))

    B6Im.append(eval(coefficients[2*xiLength + 2*i+1]) - eval(coefficients[2*xiLength+1]))
    B7Im.append(eval(coefficients[2*2*xiLength + 2*i+1]) - eval(coefficients[2*2*xiLength+1]))
    A8Im.append(eval(coefficients[3*2*xiLength + 2*i+1]) - eval(coefficients[3*2*xiLength+1]))
    A9Im.append(eval(coefficients[4*2*xiLength + 2*i+1]) - eval(coefficients[4*2*xiLength+1]))

print 'pSQ= ', pSq

print 'B6Re = ', B6Re
print 'B7Re = ', B7Re
print 'A8Re = ', A8Re
print 'A9Re = ', A9Re

print 'B6Im = ', B6Im
print 'B7Im = ', B7Im
print 'A8Im = ', A8Im
print 'A9Im = ', A9Im

#################################################################################
fig1, axs = plt.subplots(2,2, sharex=False, figsize=(20,15))
fig1.subplots_adjust(hspace=0.2)
fig1.subplots_adjust(wspace=0.2)

plt.rc('text', usetex=True)
plt.rc('font', family='serif')

xArange = np.linspace(min(pSq), max(pSq), 10000)

axs[0,0].plot(pSq,B6Re, '+', color='blue')
axs[0,1].plot(pSq,B7Re, '+', color='blue')
axs[1,0].plot(pSq,A8Re, '+', color='blue')
axs[1,1].plot(pSq,A9Re, '+', color='blue')

axs[0,0].grid(True)
axs[0,1].grid(True)
axs[1,0].grid(True)
axs[1,1].grid(True)

axs[0,0].set_xlabel(r'$2 \xi p \kappa$', fontsize=14)
axs[0,0].set_ylabel(r'$(b_{01}^{(1)})_0 (\xi) - (b_{01}^{(1)})_0 (0)$', fontsize=14)

axs[0,1].set_xlabel(r'$2 \xi p \kappa$', fontsize=14)
axs[0,1].set_ylabel(r'$(b_{01}^{(2)})_0 (\xi) - (b_{01}^{(2)})_0 (0)$', fontsize=14)

axs[1,0].set_xlabel(r'$2 \xi p \kappa$', fontsize=14)
axs[1,0].set_ylabel(r'$a_{0}^{(W)} (\xi) - a_{0}^{(W)} (0)$', fontsize=14)

axs[1,1].set_xlabel(r'$2 \xi p \kappa$', fontsize=14)
axs[1,1].set_ylabel(r'$a_{0}^{(H)} (\xi) - a_{0}^{(H)} (0)$', fontsize=14)

fit_res, covar = curve_fit(coeffFit, pSq, B6Re, p0=initialValues)
print 'fit results: ', fit_res
print 'covariance matrix: ', covar
axs[0,0].plot(xArange,coeffFit(xArange, *fit_res),'r-', linewidth = 1, label = r'Fitted function: $f(\xi) = $ ' + str(round_sig(fit_res[0])) + r' $ + $ ' + str(round_sig(fit_res[1])) + r' $ \cdot $ $ (2 \xi p \kappa) $')

def B4Fit(x):
    return coeffFit(x, *fit_res)
derRe.append(derivative(B4Fit, 0.0, dx=1e-8))

fit_res, covar = curve_fit(coeffFit, pSq, B7Re, p0=initialValues)
print 'fit results: ', fit_res
print 'covariance matrix: ', covar
axs[0,1].plot(xArange,coeffFit(xArange, *fit_res),'r-', linewidth = 1, label = r'Fitted function: $f(\xi) = $ ' + str(round_sig(fit_res[0])) + r' $ + $ ' + str(round_sig(fit_res[1])) + r' $ \cdot $ $ (2 \xi p \kappa) $')

def B5Fit(x):
    return coeffFit(x, *fit_res)
derRe.append(derivative(B5Fit, 0.0, dx=1e-8))

fit_res, covar = curve_fit(coeffFit, pSq, A8Re, p0=initialValues)
print 'fit results: ', fit_res
print 'covariance matrix: ', covar
axs[1,0].plot(xArange,coeffFit(xArange, *fit_res),'r-', linewidth = 1, label = r'Fitted function: $f(\xi) = $ ' + str(round_sig(fit_res[0])) + r' $ + $ ' + str(round_sig(fit_res[1])) + r' $ \cdot $ $ (2 \xi p \kappa) $')

def A8Fit(x):
    return coeffFit(x, *fit_res)
derRe.append(derivative(A8Fit, 0.0, dx=1e-10))

fit_res, covar = curve_fit(coeffFit, pSq, A9Re, p0=initialValues)
print 'fit results: ', fit_res
print 'covariance matrix: ', covar
axs[1,1].plot(xArange,coeffFit(xArange, *fit_res),'r-', linewidth = 1, label = r'Fitted function: $f(\xi) = $ ' + str(round_sig(fit_res[0])) + r' $ + $ ' + str(round_sig(fit_res[1])) + r' $ \cdot $ $ (2 \xi p \kappa) $')

def A9Fit(x):
    return coeffFit(x, *fit_res)
derRe.append(derivative(A9Fit, 0.0, dx=1e-10))

axs[0,0].legend(loc=1, fontsize=14)
axs[1,0].legend(loc=1, fontsize=14)
axs[0,1].legend(loc=1, fontsize=14)
axs[1,1].legend(loc=1, fontsize=14)

#plt.suptitle(r'Real part of B4, B5, A8, A9 coefficients as function of $p^2$')
#plt.legend()
plt.savefig(path + 'hFitRe.png')
plt.show()

print 'derivatives Real : ', derRe

#file = open('coeffDerhRe.dat','w')
file = open(path + 'coeffDerhRe.dat','w')
for element in derRe:
    file.write(str(element) + '\n')

################################################################################
fig2, axs = plt.subplots(2,2, sharex=False, figsize=(20,15))
fig2.subplots_adjust(hspace=0.5)
fig2.subplots_adjust(wspace=0.5)

plt.rc('text', usetex=True)
plt.rc('font', family='serif')

axs[0,0].plot(pSq,B6Im, '+', color='blue')
axs[0,1].plot(pSq,B7Im, '+', color='blue')
axs[1,0].plot(pSq,A8Im, '+', color='blue')
axs[1,1].plot(pSq,A9Im, '+', color='blue')

axs[0,0].grid(True)
axs[0,1].grid(True)
axs[1,0].grid(True)
axs[1,1].grid(True)

fit_res, covar = curve_fit(coeffFit, pSq, B6Im, p0=initialValues)
print 'fit results: ', fit_res
print 'covariance matrix: ', covar
axs[0,0].plot(xArange,coeffFit(xArange, *fit_res),'r-', linewidth = 1)

def B4Fit(x):
    return coeffFit(x, *fit_res)
derIm .append(derivative(B4Fit, 0.0, dx=1e-8))

fit_res, covar = curve_fit(coeffFit, pSq, B7Im, p0=initialValues)
print 'fit results: ', fit_res
print 'covariance matrix: ', covar
axs[0,1].plot(xArange,coeffFit(xArange, *fit_res),'r-', linewidth = 1)

def B5Fit(x):
    return coeffFit(x, *fit_res)
derIm.append(derivative(B5Fit, 0.0, dx=1e-8))

fit_res, covar = curve_fit(coeffFit, pSq, A8Im, p0=initialValues)
print 'fit results: ', fit_res
print 'covariance matrix: ', covar
axs[1,0].plot(xArange,coeffFit(xArange, *fit_res),'r-', linewidth = 1)

def A8Fit(x):
    return coeffFit(x, *fit_res)
derIm.append(derivative(A8Fit, 0.0, dx=1e-10))

fit_res, covar = curve_fit(coeffFit, pSq, A9Im, p0=initialValues)
print 'fit results: ', fit_res
print 'covariance matrix: ', covar
axs[1,1].plot(xArange,coeffFit(xArange, *fit_res),'r-', linewidth = 1)

def A9Fit(x):
    return coeffFit(x, *fit_res)
derIm.append(derivative(A9Fit, 0.0, dx=1e-10))

plt.suptitle(r'Imaginary part of B4, B5, A8, A9 coefficients as function of $p^2$')
#plt.legend()
plt.savefig(path + 'hFitIm.png')
plt.show()

print 'derivatives Im : ', derIm

file = open(path + 'coeffDerhIm.dat','w')
for element in derIm:
    file.write(str(element) + '\n')

print '--------------- END polynomial fit in python --------------------'
