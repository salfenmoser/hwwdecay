import numpy as np
from math import log10, floor
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit
from scipy.misc import derivative
import os

print '--------------- polynomial fit in python --------------------'

def round_sig(x, sig=5):
    if (x == 0):
        return 0
    return round(x, sig-int(floor(log10(abs(x))))-1)

def coeffFit(x_var, *c):
    return c[0] + c[1]*x_var#+ c[2]*x_var**2

initialValues = [0,0]#,0]

pSq = []
xi = []

B4Re = []
B5Re = []
A8Re = []
A9Re = []
derRe = []

B4Im = []
B5Im = []
A8Im = []
A9Im = []
derIm = []

mW = 80.379
mH = 125.18
kKappa = np.sqrt(-mH**2/4 + mW**2)

path = ''
#path = 'renormalization/renormalizationSE/'
coefficients = open(path + 'WByShiftArray.dat', "r").readlines()

xiLength = 7
for i in range(xiLength):
    xi.append(eval(coefficients[2*i]))

    B4Re.append(eval(coefficients[2*xiLength + 2*i]) - eval(coefficients[2*xiLength]))
    B5Re.append(eval(coefficients[2*2*xiLength + 2*i]) - eval(coefficients[2*2*xiLength]))
    A8Re.append(eval(coefficients[3*2*xiLength + 2*i]) - eval(coefficients[3*2*xiLength]))
    A9Re.append(eval(coefficients[4*2*xiLength + 2*i]) - eval(coefficients[4*2*xiLength]))

    B4Im.append(eval(coefficients[2*xiLength + 2*i+1]) - eval(coefficients[2*xiLength+1]))
    B5Im.append(eval(coefficients[2*2*xiLength + 2*i+1]) - eval(coefficients[2*2*xiLength+1]))
    A8Im.append(eval(coefficients[3*2*xiLength + 2*i+1]) - eval(coefficients[3*2*xiLength+1]))
    A9Im.append(eval(coefficients[4*2*xiLength + 2*i+1]) - eval(coefficients[4*2*xiLength+1]))

pSq = [2*kKappa*el for el in xi]
print 'pSQ= ', pSq

print 'B4Re = ', B4Re
print 'B5Re = ', B5Re
print 'A8Re = ', A8Re
print 'A9Re = ', A9Re

print 'B4Im = ', B4Im
print 'B5Im = ', B5Im
print 'A8Im = ', A8Im
print 'A9Im = ', A9Im

#################################################################################
fig1, axs = plt.subplots(2,2, sharex=False, figsize=(20,15))
fig1.subplots_adjust(hspace=0.2)
fig1.subplots_adjust(wspace=0.2)

plt.rc('text', usetex=True)
plt.rc('font', family='serif')

xArange = np.linspace(min(pSq), max(pSq), 10000)

axs[0,0].plot(pSq,B4Re, '+', color='blue')
axs[0,1].plot(pSq,B5Re, '+', color='blue')
axs[1,0].plot(pSq,A8Re, '+', color='blue')
axs[1,1].plot(pSq,A9Re, '+', color='blue')

axs[0,0].grid(True)
axs[0,1].grid(True)
axs[1,0].grid(True)
axs[1,1].grid(True)

axs[0,0].set_xlabel(r'$2 \xi p \kappa$', fontsize=14)
axs[0,0].set_ylabel(r'$(b_{01}^{(1)})_0 (\xi) - (b_{01}^{(1)})_0 (0)$', fontsize=14)

axs[0,1].set_xlabel(r'$2 \xi p \kappa$', fontsize=14)
axs[0,1].set_ylabel(r'$(b_{01}^{(2)})_0 (\xi) - (b_{01}^{(2)})_0 (0)$', fontsize=14)

axs[1,0].set_xlabel(r'$2 \xi p \kappa$', fontsize=14)
axs[1,0].set_ylabel(r'$a_{0}^{(W)} (\xi) - a_{0}^{(W)} (0)$', fontsize=14)

axs[1,1].set_xlabel(r'$2 \xi p \kappa$', fontsize=14)
axs[1,1].set_ylabel(r'$a_{0}^{(H)} (\xi) - a_{0}^{(H)} (0)$', fontsize=14)

fit_res, covar = curve_fit(coeffFit, pSq, B4Re, p0=initialValues)
print 'fit results: ', fit_res
print 'covariance matrix: ', covar
axs[0,0].plot(xArange,coeffFit(xArange, *fit_res),'r-', linewidth = 1, label = r'Fitted function: $f(\xi) = $ ' + str(round_sig(fit_res[0])) + r' $ + $ ' + str(round_sig(fit_res[1])) + r' $ \cdot $ $ (2 \xi p \kappa) $')

def B4Fit(x):
    return coeffFit(x, *fit_res)
derRe.append(derivative(B4Fit, 0.0, dx=1e-8))

fit_res, covar = curve_fit(coeffFit, pSq, B5Re, p0=initialValues)
print 'fit results: ', fit_res
print 'covariance matrix: ', covar
axs[0,1].plot(xArange,coeffFit(xArange, *fit_res),'r-', linewidth = 1, label = r'Fitted function: $f(\xi) = $ ' + str(round_sig(fit_res[0])) + r' $ + $ ' + str(round_sig(fit_res[1])) + r' $ \cdot $ $ (2 \xi p \kappa) $')

def B5Fit(x):
    return coeffFit(x, *fit_res)
derRe.append(derivative(B5Fit, 0.0, dx=1e-8))

fit_res, covar = curve_fit(coeffFit, pSq, A8Re, p0=initialValues)
print 'fit results: ', fit_res
print 'covariance matrix: ', covar
axs[1,0].plot(xArange,coeffFit(xArange, *fit_res),'r-', linewidth = 1, label = r'Fitted function: $f(\xi) = $ ' + str(round_sig(fit_res[0])) + r' $ + $ ' + str(round_sig(fit_res[1])) + r' $ \cdot $ $ (2 \xi p \kappa) $')

def A8Fit(x):
    return coeffFit(x, *fit_res)
derRe.append(derivative(A8Fit, 0.0, dx=1e-10))

fit_res, covar = curve_fit(coeffFit, pSq, A9Re, p0=initialValues)
print 'fit results: ', fit_res
print 'covariance matrix: ', covar
axs[1,1].plot(xArange,coeffFit(xArange, *fit_res),'r-', linewidth = 1, label = r'Fitted function: $f(\xi) = $ ' + str(round_sig(fit_res[0])) + r' $ + $ ' + str(round_sig(fit_res[1])) + r' $ \cdot $ $ (2 \xi p \kappa) $')

def A9Fit(x):
    return coeffFit(x, *fit_res)
derRe.append(derivative(A9Fit, 0.0, dx=1e-10))

#plt.suptitle(r'Real part of B4, B5, A8, A9 coefficients as function of $p^2$')
#plt.legend()
axs[0,0].legend(loc=1, fontsize=14)
axs[1,0].legend(loc=1, fontsize=14)
axs[0,1].legend(loc=1, fontsize=14)
axs[1,1].legend(loc=1, fontsize=14)

plt.savefig(path + 'WFitRe.png')
plt.show()

print 'derivatives Real : ', derRe

file = open(path + 'coeffDerWRe.dat','w')
for element in derRe:
    file.write(str(element) + '\n')

derRe = [el/2 for el in derRe]
print 'derivatives Real div2: ', derRe

################################################################################
fig2, axs = plt.subplots(2,2, sharex=False, figsize=(20,15))
fig2.subplots_adjust(hspace=0.5)
fig2.subplots_adjust(wspace=0.5)

plt.rc('text', usetex=True)
plt.rc('font', family='serif')

axs[0,0].plot(pSq,B4Im, '+', color='blue')
axs[0,1].plot(pSq,B5Im, '+', color='blue')
axs[1,0].plot(pSq,A8Im, '+', color='blue')
axs[1,1].plot(pSq,A9Im, '+', color='blue')

fit_res, covar = curve_fit(coeffFit, pSq, B4Im, p0=initialValues)
print 'fit results: ', fit_res
print 'covariance matrix: ', covar
axs[0,0].plot(xArange,coeffFit(xArange, *fit_res),'r-', linewidth = 1)

def B4Fit(x):
    return coeffFit(x, *fit_res)
derIm .append(derivative(B4Fit, 0.0, dx=1e-8))

fit_res, covar = curve_fit(coeffFit, pSq, B5Im, p0=initialValues)
print 'fit results: ', fit_res
print 'covariance matrix: ', covar
axs[0,1].plot(xArange,coeffFit(xArange, *fit_res),'r-', linewidth = 1)

def B5Fit(x):
    return coeffFit(x, *fit_res)
derIm.append(derivative(B5Fit, 0.0, dx=1e-8))

fit_res, covar = curve_fit(coeffFit, pSq, A8Im, p0=initialValues)
print 'fit results: ', fit_res
print 'covariance matrix: ', covar
axs[1,0].plot(xArange,coeffFit(xArange, *fit_res),'r-', linewidth = 1)

def A8Fit(x):
    return coeffFit(x, *fit_res)
derIm.append(derivative(A8Fit, 0.0, dx=1e-8))

fit_res, covar = curve_fit(coeffFit, pSq, A9Im, p0=initialValues)
print 'fit results: ', fit_res
print 'covariance matrix: ', covar
axs[1,1].plot(xArange,coeffFit(xArange, *fit_res),'r-', linewidth = 1)

def A9Fit(x):
    return coeffFit(x, *fit_res)
derIm.append(derivative(A9Fit, 0.0, dx=1e-8))

plt.suptitle(r'Imaginary part of B4, B5, A8, A9 coefficients as function of $p^2$')
#plt.legend()
plt.savefig(path + 'WFitIm.png')
plt.show()

print 'derivatives Imaginary : ', derIm

#file = open('coeffDerWIm.dat','w')
file = open(path + 'coeffDerWIm.dat','w')
for element in derIm:
    file.write(str(element) + '\n')

print '--------------- END polynomial fit in python --------------------'
