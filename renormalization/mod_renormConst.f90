module mod_renormConst

    use mod_types
    use mod_consts_dp
    use common_def
    
    use mod_vertices
    use mod_selfEnergy
    use mod_renSE
    use mod_renHWW
    
implicit none

public :: Z_g, renW, renH
complex(dp) :: intFactor = four*g_Coupling**two * ci

contains


! each of the form (2,9), containing coefficients of first row: all master integrals 3xC, 4xB, 2xA, second row: derivatives
function Z_g()
  complex(dp) :: Z_g(2,11)
Z_g(:,:) = czero
Z_g = intFactor * Z_g
end function Z_g

! first line: master integral coefficients of deltaM
! second line: master integral coefficients of deltaZ (corresponds to derivative of line 1)
! third line: coefficients of master integral derivatives (equal to first line)
function renW(fromSE, fromHWW, D, pSq, h, w1, w2, ep1, ep2)
  integer, intent(in) :: D, ep1, ep2
  complex(dp), intent(in) :: pSq, h(3, dimP), w1(3, dimP), w2(3, dimP)
  logical, intent(in) :: fromSE, fromHWW
  complex(dp) :: renW(3,11)
  integer :: k
renW(:,:) = czero

! all from self energy off-shell continuation
if (fromSE) then
  renW = renWFromSE(D, w1, ep1)

! all from HWW off-shell continuation
else if (fromHWW) then
  renW = renWFromHWW(D, h, w1, w2, ep1, ep2)

! all numerical input
else
  renW(1,4) = (three* (four*mwsq - pSq) * (four*(D-one)*mwsq**two + four*(two*D-three)*mwsq*pSq + pSq**two) )/(16._dp*(D-one)*mwsq**two)
  renW(1,6) = - (three * (two*(two*D-three)*mwsq*pSq + mhsq**two - two*mhsq*(mwsq + pSq) + mwsq**two + pSq**two) )/(four*(D-one)*pSq)
  renW(1,10) = three* ( -(two*D**two-six*D+three)/four + (D-two)*pSq/two/mwsq + (mwsq - mhsq)/four/pSq + pSq**two/8._dp/mwsq**two) /(D-one)
  renW(1,11) = -three* ( D - two + (mwsq-mhsq)/pSq) / (four*(D-one))

  renW(2,4) = - ( (16._dp*(two-D)*mwsq*pSq - three*pSq**two)/(16._dp*(D-one)*mwsq**two) + (7._dp*D-11._dp)/(four*(D-one)) )
  renW(2,6) = - ( (mhsq**two - two*mhsq*mwsq + mwsq**two)/(four*(D-one)*pSq**two) - one/four/(D-one) )
  renW(2,10) = - ( three*((D-two)/two/mwsq + (mhsq-mwsq)/pSq**two + pSq/mwsq**two)/(D-one) )
  renW(2,11) = - ( three*(mwsq - mhsq)/(four*(D-one)*pSq**two) )

  renW = intFactor * renW  
end if

  renW(3,4) = - renW(1,4)
  renW(3,6) = - renW(1,6)

do k = 1, 3
  renW(k,5) = renW(k,4)
  renW(k,7) = renW(k,6)
end do

end function renW

! first line: master integral coefficients of deltaM
! second line: master integral coefficients of deltaZ (corresponds to derivative of line 1)
! third line: coefficients of master integral derivatives (equal to first line)
function renH(fromSE, fromHWW, D, pSq, h, w1, w2, ep1, ep2)
  integer, intent(in) :: D, ep1, ep2
  complex(dp), intent(in) :: pSq, h(3,dimP), w1(3, dimP), w2(3, dimP)
  logical, intent(in) :: fromSE, fromHWW
  complex(dp) :: renH(3,11)
renH(:,:) = czero

! all from self energy off-shell continuation
if (fromSE) then
  renH = renHFromSE(D, h, w1)

! all from HWW off-shell continuation
else if (fromHWW) then
  renH = renHFromHWW(D, h, w1, w2, ep1, ep2)

! all numerical input
else
  renH(1,8) = three*(four*mwsq**two*(d-one) - four*mwsq*pSq + pSq**two)/8._dp/mwsq
  renH(1,9) = 9._dp*mhsq**two/8._dp/mwsq
  renH(1,10) = three*(mwsq*(D-one) - pSq)/four/mwsq
  renH(1,11) = three*mhsq/8._dp/mwsq

  renH(2,8) = - ( -three/two + three*pSq/four/mwsq )
  renH(2,9) = - ( -three/four/mwsq )

  renH = intFactor * renH  
end if

renH(3,8) = - renH(1,8)
renH(3,9) = - renH(1,9)

end function renH
  
end module
  