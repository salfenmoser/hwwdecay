module mod_renSE

  use mod_types
  use mod_consts_dp
  use common_def
    
  use mod_auxfunctions     
  use mod_kinematics
  use mod_selfEnergy
    
  implicit none

  public :: renWFromSE, renHFromSE

  integer :: xiLength = 7
  ! have to be chosen sufficiently far from each other, otherwise exact same results after multiplication with xi even though should be small differences
  !complex(dp) :: xi(11) = [zero, one*1e-9, two*1e-9, three*1e-9, four*1e-9, five*1e-9, six*1e-9, 7._dp*1e-9, 8._dp*1e-9, 9._dp*1e-9, one*1e-8]
  complex(dp) :: xi(7) = [zero, -five*1e-8, -three*1e-8, -one*1e-8, one*1e-8, three*1e-8, five*1e-8]

  contains

! returns renormalization constants (only coefficients of master integrals) for higgs boson
! delta m in first row, delta Z in second row, 
function renHFromSE(dim, h, w)
  integer, intent(in) :: dim
  complex(dp), intent(in) :: h(3, dimP), w(3, dimP)
  complex(dp) :: hShifted(3, dimP)
  complex(dp) :: kappa(dimP), cutCoeff(4), B6(7), B7(7), A8(7), A9(7), pSq(7)
  integer :: j
  character*70 :: docNameWrite, docNameRe, docNameIm
  complex(dp) :: renHFromSE(3,11), renHFromSERe(3,11), renHFromSEIm(3,11)

renHFromSE(:,:) = czero
call HShiftMomentum(h(1,:), w(1,:), kappa)
hShifted = h

print *, 'SELF ENERGY SHIFT RENORMALIZATION CONSTANTS'
print *, 'calculate self energy oj H boson for off-shell momenta'
do j = 1,xiLength
  hShifted(1,:) = h(1,:) + kappa(:) * xi(j)
  pSq(j) = sq(hShifted(1,:))
      
  call system('rm ~/Documents/thesis/fortran/UnitarityMethodCalculation/diagramsSE/coefficientsSE/*')
  call sleep(1)
  
  cutCoeff = finalSE(hShifted)
      
  B6(j) = cutCoeff(1)
  B7(j) = cutCoeff(2)
  A8(j) = cutCoeff(3)
  A9(j) = cutCoeff(4)
end do
  
renHFromSE(1,8) = B6(1)
renHFromSE(1,9) = B7(1)
renHFromSE(1,10) = A8(1)
renHFromSE(1,11) = A9(1)
  
print *, 'write in file '
docNameWrite = 'renormalization/renormalizationSE/hByShiftArray.dat'
open(unit = 1, file = trim(docNameWrite), status='replace')
write(1, FMT, advance='no') pSq
write(1, *) ''
write(1, FMT, advance='no') B6
write(1, *) ''
write(1, FMT, advance='no') B7
write(1, *) ''
write(1, FMT, advance='no') A8
write(1, *) ''
write(1, FMT, advance='no') A9
close(1)
  
print *, 'call python function for fit'
call system('python renormalization/renormalizationSE/hFit.py')
call sleep(3)

docNameRe = 'renormalization/renormalizationSE/coeffDerhRe.dat'
open(unit = 1, file = trim(docNameRe), status='old')
docNameIm = 'renormalization/renormalizationSE/coeffDerhIm.dat'
open(unit = 2, file = trim(docNameIm), status='old')
do j = 1,4
  read(1, *) renHFromSERe(2,7+j)
  read(2, *) renHFromSEIm(2,7+j)
end do
close(1)
close(2)

renHFromSE(2,:) = renHFromSERe(2,:) + ci*renHFromSEIm(2,:)
        
do j=1,11
  renHFromSE(2,j) = -1*renHFromSE(2,j)
end do

renHFromSE(3,8) = - renHFromSE(1,8)
renHFromSE(3,9) = - renHFromSE(1,9)
    
end function renHFromSE


! returns renormalization constants (only coefficients of master integrals) for W boson
! delta m in first row, delta Z in second row
function renWFromSE(dim, w, ep)
  integer, intent(in) :: dim, ep
  complex(dp), intent(in) :: w(3, dimP)
  complex(dp) :: wShifted(3, dimP)
  complex(dp) :: kappa(dimP), cutCoeff(2,3), B4(7), B5(7), A8(7), A9(7), pSq(7), pol(6, dimE)
  integer :: j
  character*70 :: docNameWrite, docNameRe, docNameIm
  complex(dp) :: renWFromSE(3,11),renWFromSERe(3,11), renWFromSEIm(3,11)

renWFromSE(:,:) = czero
call WShiftMomentum(kappa)
wShifted = w

print *, 'SELF ENERGY SHIFT RENORMALIZATION CONSTANTS'
print *, 'calculate self energy of W boson for off-shell momenta'
do j = 1,xiLength
  print *, 'j = ', j
  print *, 'xi = ', xi(j)

  wShifted(1,:) = w(1,:) + kappa(:) * xi(j)
  pSq(j) = sq(wShifted(1,:))

  call spinonepol_6d(wShifted(1,:),pol)
  wShifted(2,:) = pol(ep, :)
    
  call system('rm ~/Documents/thesis/fortran/UnitarityMethodCalculation/diagramsSE/coefficientsSE/*')
  call sleep(1)

  B4(j) = getBubbleCoefficients0_SE(4, wShifted, 1)
  B5(j) = getBubbleCoefficients0_SE(4, wShifted, 2)
  A8(j) = getTadpoleCoefficients0_SE(4, wShifted, 1)
  A9(j) = getTadpoleCoefficients0_SE(4, wShifted, 2)

  print *, 'coefficient array:'
  print *, B4(j)
  print *, B5(j)
  print *, A8(j)
  print *, A9(j)

end do

renWFromSE(1,4) = B4(1)
renWFromSE(1,6) = B5(1)
renWFromSE(1,10) = A8(1)
renWFromSE(1,11) = A9(1)

print *, 'write in file '
docNameWrite = 'renormalization/renormalizationSE/WByShiftArray.dat'
open(unit = 1, file = trim(docNameWrite), status='replace')
write(1, FMT, advance='no') xi
write(1, *) ''
write(1, FMT, advance='no') B4
write(1, *) ''
write(1, FMT, advance='no') B5
write(1, *) ''
write(1, FMT, advance='no') A8
write(1, *) ''
write(1, FMT, advance='no') A9
close(1)

print *, 'call python function for fit'
call system('python renormalization/renormalizationSE/WFit.py')
call sleep(3)

docNameRe = 'renormalization/renormalizationSE/coeffDerWRe.dat'
open(unit = 1, file = trim(docNameRe), status='old')
docNameIm = 'renormalization/renormalizationSE/coeffDerWIm.dat'
open(unit = 2, file = trim(docNameIm), status='old')
do j = 1,2
  read(1, *) renWFromSERe(2,2+2*j)
  read(2, *) renWFromSEIm(2,2+2*j)
end do
do j = 1,2
  read(1, *) renWFromSERe(2,9+j)
  read(2, *) renWFromSEIm(2,9+j)
end do
close(1)
close(2)

renWFromSE(2,:) = renWFromSERe(2,:) + ci*renWFromSEIm(2,:)

do j=1,11
  renWFromSE(2,j) = -1*renWFromSE(2,j)
end do

renWFromSE(3,4) = - renWFromSE(1,4)
renWFromSE(3,6) = - renWFromSE(1,6)

do j = 1, 3
  renWFromSE(j,5) = renWFromSE(j,4)
  renWFromSE(j,7) = renWFromSE(j,6)
end do
  
end function renWFromSE

end module
  