module mod_ampSumSE

    use mod_types
    use mod_consts_dp
    use common_def
    
    use mod_auxfunctions     
    use mod_kinematics
    use mod_amplitudes

    use mod_vertices
  
    use mod_diagramKinematicsSE
    
implicit none
  
public :: getDoubleCurrentsSE, getSingleCurrentsSE
logical :: printWarningDouble = .true., printWarningSingle = .true., demandProperMomSE = .false.

contains
  
  !---------------------------------------------------------
  ! double cuts 
  ! -------------------------------------------------------- 
! pa is higgs or W
! dimension = D_S, momentum l, diagram i, boolean if only calulation if momenta are chosen adequately (on shell)
  function getDoubleCurrentsSE(D_S, l, pa, i, demandProperMomSE)
    integer :: epx1, cx1, epx2, cx2, sumCx1, sumCx2, sumEx1, sumEx2
    integer, intent(in) :: D_S, i
    complex(dp), intent(in) :: pa(3,dimP), l(dimP)
    logical, intent(in) :: demandProperMomSE ! if true: only returns non-zero result if proper momenta are chosen
    logical :: properMom, isW

    complex(dp) :: diag(2,14), paOut(3,dimP)
    complex(dp) :: pax1In(3,dimP), pax2In(3,dimP), ePx1In(dimP,dimE), ePx2In(dimP,dimE)
    complex(dp) :: pax1Out(3,dimP), pax2Out(3,dimP)
    complex(dp) :: J3_1, J3_2, d(2,2)
    complex(dp) :: getDoubleCurrentsSE
  
getDoubleCurrentsSE = czero
properMom = .true.
d = getDenominatorsSE(l, pa)

if ((abs(d(i,1)) > limitProp) .or. (abs(d(i,2)) > limitProp))  then
  if (printWarningDouble) then
    print *, "WARNING: TRYING TO COMPUTE SELF ENERGY DOUBLE CUT CURRENTS WITH IMPROPERLY CHOSEN MOMENTA"
  end if
  properMom = .false.
end if

if  ((properMom .eqv. .true.) .or. (demandProperMomSE .eqv. .false.)) then

paOut = pa
paOut(1,:) = -pa(1,:)

! decide if higgs or W
isW = isParticleW(pa)

diag = diagramsSE(l, pa(1,:), isW)

! define momenta of internal particles, in and outgoing
pax1In(1,:) = diag(i,1:6)
pax1Out(1,:) = - pax1In(1,:)
pax2In(1,:) = diag(i,8:13)
pax2Out(1,:) = - pax2In(1,:)

! set polarization matrix of internal particles to zero, assuming higgs for now
ePx1In(:,:) = czero
ePx2In = ePx1In

! define upper sum limit of polarization and color sum for three internal particles, assuming higgs for now
sumCx1 = 1
sumCx2 = 1
sumEx1 = 1
sumEx2 = 1

! if not higgs, define properties accordingly
if (diag(i,7) == mwsq) then
  sumCx1 = 3
  sumEx1 = D_S - 1
  call spinonepol_6d(pax1In(1,:),ePx1In)
end if
if (diag(i,14) == mwsq) then
  sumCx2 = 3
  sumEx2 = D_S - 1
  call spinonepol_6d(pax2In(1,:),ePx2In)
end if

! print *, 'pol matrix pax1'
! print *, ePx1In
! print *, 'pol matrix pax2'
! print *, ePx2In

!print *, 'sum over : ', sumEx1, sumCx1, sumEx2, sumCx2

! sum over internal particles
do epx1 = 1,sumEx1
 do cx1 = 1,sumCx1
  do epx2 = 1,sumEx2
   do cx2 = 1,sumCx2
  
  ! define polarization states, non vanishing only if not higgs
  pax1In(2,:) = ePx1In(epx1,:)
  pax1Out(2,:) = ePx1In(epx1,:)
  pax2In(2,:) = ePx2In(epx2,:)
  pax2Out(2,:) = ePx2In(epx2,:)

  ! define color states, non vanishing only if not higgs
  pax1In(3,:) = czero
  pax2In(3,:) = czero
  
  if (diag(i,7) == mwsq) then
    pax1In(3,1) = cone*cx1
  end if
  if (diag(i,14) == mwsq) then
    pax2In(3,1) = cone*cx2
  end if
  
  ! calculate two 3particle currents
  pax1Out(3,:) = pax1In(3,:)
  pax2Out(3,:) = pax2In(3,:)
  J3_1 = J3(pa,pax1Out,pax2In)
  J3_2 = J3(paOut,pax1In,pax2Out)
          
  !print *, 'contribution to double currents ', J3_1 * J3_2
  !print *, 'expected: ', sq(pa(2,:))*V_hw2(int(pa(3,1)), int(pa(3,1)))**two
  getDoubleCurrentsSE = getDoubleCurrentsSE + J3_1 * J3_2
   end do
  end do  
 end do
end do

end if
end function getDoubleCurrentsSE

  
! !---------------------------------------------------------
! ! single cuts 
! ! --------------------------------------------------------

!pa is higgs or W
! dimension = D_S, momentum l, diagram i, boolean if only calulation if momenta are chosen adequately (on shell)
function getSingleCurrentsSE(D_S, l, pa, i, demandProperMomSE)
  integer :: epx, cx, sumCx, sumEx, D_S
  integer, intent(in) :: i
  complex(dp), intent(in) :: pa(3,dimP), l(dimP)
  logical, intent(in) :: demandProperMomSE ! if true: only returns non-zero result if proper momenta are chosen
  logical :: properMom, isW

  complex(dp) :: diag(2,14)
  complex(dp) :: partxIn(3,dimP), partxOut(3,dimP), ePolxIn(dimP,dimE), J4_1, d(2,2), paOut(3,dimP)
  complex(dp) :: getSingleCurrentsSE

  getSingleCurrentsSE = czero
properMom = .true.
    
d = getDenominatorsSE(l, pa)

if (abs(d(i,1)) > limitProp)  then
  if (printWarningSingle) then
    print *, "WARNING: TRYING TO COMPUTE SINGLE CUT CURRENTS OF SELF ENERGY WITH IMPROPERLY CHOSEN MOMENTA"
  end if
  properMom = .false.
end if
    
if  ((properMom .eqv. .true.) .or. (demandProperMomSE .eqv. .false.)) then

if (pa(3,1) .ne. 0) then
  isW = .true.
else
  isW = .false.
end if

diag = diagramsSE(l, pa(1,:), isW)

paOut = pa
paOut(1,:) = -pa(1,:)

partxIn(1,:) = diag(i,1:6)
partxOut(1,:) = - partxIn(1,:)
  
sumCx = 1
sumEx = 1
  
if (diag(i,7) == mwsq) then
  sumCx = 3
  sumEx = D_S - 1
  call spinonepol_6d(partxIn(1,:),ePolxIn)
end if
  
do epx = 1,sumEx
  do cx = 1,sumCx
    !print *, 'pol and color = ', epx, cx
    partxIn(2,:) = ePolxIn(epx,:)
    partxOut(2,:) = ePolxIn(epx,:)

    partxIn(3,:) = czero 
    if (diag(i,7) == mwsq) then
      partxIn(3,1) = cone*cx
    end if
    partxOut(3,:) = partxIn(3,:)

    !print *, '--------0------'
    J4_1 = J4(pa, partxIn, partxOut, paOut)
    !print *, '--------1------'
    getSingleCurrentsSE = getSingleCurrentsSE + J4_1
    end do
  end do
end if
end function getSingleCurrentsSE

end module