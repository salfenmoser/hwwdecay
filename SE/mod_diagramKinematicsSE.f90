module mod_diagramKinematicsSE

    use mod_types
    use mod_consts_dp
    use common_def
    
    use mod_auxfunctions     
    use mod_kinematics
    use mod_vertices
    
    implicit none
    
  public :: diagramsSE, getDenominatorsSE, getBubbleKinematicsSE, getBubbleMomentaSE, getTadpoleMomentaSE
  public :: isParticleW
  
  contains

! decides if given particle is W boson or not (then higgs)
function isParticleW(pa)
  complex(dp), intent(in) :: pa(3,dimP)
  logical :: isParticleW
 
if (pa(3,1) .ne. 0) then
  isParticleW = .true.
else
  isParticleW = .false.
end if
end function isParticleW

! contains 2 rows, each row represents one diagram;
! each row contains propagator momentum and mass for the two propagators
! isW is boolean that decides if H or W self energy diagram is returned
! row 1: WW loop, row 2: HH or WH loop
! entry 1-6:momentum of first propagator, entry 7: mass squared of first propagator, etc.
function diagramsSE(l, p, isW)
  integer :: i
  complex(dp), intent(in) :: l(dimP), p(dimP)
  logical, intent(in) :: isW
  complex(dp) ::  diagramsSE(2,14)

do i = 1, 2
  diagramsSE(i,1:6) = l
  diagramsSE(i,8:13) = l - p
end do

diagramsSE(1,7) = mwsq
diagramsSE(1,14) = mwsq
diagramsSE(2,7) = mhsq
if (isW) then
  diagramsSE(2,14) = mwsq
else
  diagramsSE(2,14) = mhsq
end if

end function diagramsSE

! returns the two denominators of the two different diagrams
function getDenominatorsSE(l, part)
  integer :: i, k
  complex(dp), intent(in) :: l(dimP), part(3,dimP)
  complex(dp) :: getDenominatorsSE(2,2)
  complex(dp) :: diag(2,14)
  logical :: isW

if (part(3,1) .ne. 0) then
  isW = .true.
else
  isW = .false.
end if

diag = diagramsSE(l, part(1,:), isW)

do i = 1,2
  getDenominatorsSE(i,1) = sq(diag(i,1:6)) - diag(i,7)
  getDenominatorsSE(i,2) = sq(diag(i,8:13)) - diag(i,14)
  ! print *, 'diagram = ', i
  ! print *, 'diag 14 = ', diag(i,14)
  ! print *, 'mhsq = ', mhsq
  ! print *, 'mwsq = ', mwsq
end do
end function getDenominatorsSE

  
! construction of Van Neerven-Vermaseren basis from particle (H or W) for diagram i
! v contains 6d basis, v(1) parallel to p; v(2), v(3), v(4) in transversal space
! x1 and lt_sq required for construction of momenta
subroutine getBubbleKinematicsSE(part, i, q, x1, lt_sq, v)
    integer, intent(in) :: i
    complex(dp), intent(in) :: part(3,dimP)
    complex(dp), intent(out) :: q(dimP), x1, lt_sq, v(dimP,dimP)
    complex(dp) :: l(dimP), diag(2,14), vAux(4,4)
    logical :: isW

l(:) = czero
isW = isParticleW(part)
diag = diagramsSE(l, part(1,:), isW)
 
q(:) = diag(i, 8:13) - diag(i, 1:6)
x1 = ( - diag(i,7) - sc(q,q) + diag(i,14) ) / (two * sc(q, q) )
lt_sq = diag(i, 7) -  x1**two * sc(q, q)
    
!q(:) = q(:)/sqrt(sc(q,q))
call give1to4vect(q(1:4), vAux)
call extendBasisD(vAux, v)
    
return
end subroutine getBubbleKinematicsSE

subroutine getBubbleMomentaSE(D_S, pa, i, l1PlusMinus, l2PlusMinus, lAux, lEps, x, y)
  integer, intent(in) :: i, D_S
  complex(dp), intent(in) :: pa(3,dimP)
  complex(dp), intent(out) :: l1PlusMinus(3,3,2,dimP), l2PlusMinus(2,2,2,dimP), lAux(4,dimP), lEps(dimP), x, y
  integer :: m, n
  complex(dp) :: v(dimP,dimP), x1, q(dimP), lt_sq, t

l1PlusMinus(:,:,:,:) = czero
l2PlusMinus(:,:,:,:) = czero
lAux(:,:) = czero
lEps(:) = czero

call getBubbleKinematicsSE(pa, i, q, x1, lt_sq, v)

do m = 1,3
  do n = 0,2
  t = exp(2*pi*ci * n / three)
  l1PlusMinus(m, n+1, 1,:) = x1*q(:) + t*v(2,:) + sqrt(lt_sq - t**2)*v(3,:)
  l1PlusMinus(m, n+1, 2,:) = x1*q(:) + t*v(2,:) - sqrt(lt_sq - t**2)*v(3,:)
end do
end do

do m = 1,2
  do n = 0,1
  t = exp(2*pi*ci * n / two)
  l2PlusMinus(m, n+1, 1,:) = x1*q(:) + t*v(2,:) + sqrt(lt_sq - t**2)*v(3,:)
  l2PlusMinus(m, n+1, 2,:) = x1*q(:) + t*v(2,:) - sqrt(lt_sq - t**2)*v(3,:) 
end do
end do

x = sqrt(lt_sq/six)
y = sqrt(lt_sq - x**two)
  
lAux(1,:) = x1*q(:) + x*v(2,:) + y*v(4,:)
lAux(2,:) = x1*q(:) - x*v(2,:) + y*v(4,:)
lAux(3,:) = x1*q(:) - x*v(2,:) - y*v(4,:)
lAux(4,:) = x1*q(:) + x*v(4,:) + y*v(3,:)

if (D_S > 4) then
!lEps(:) = x1*q(:) + x*v(2,:) + y*v(5,:)
lEps(:) = x1*q(:) + sqrt(lt_sq)*v(5,:)
end if

return
end subroutine getBubbleMomentaSE

function getTadpoleMomentaSE(i, D_S)
  integer, intent(in) :: i, D_S
  complex(dp) :: v(dimP, dimP), lAux(7, dimP), l_sq
  complex(dp) :: getTadpoleMomentaSE(7,dimP)
  integer :: k

V(:,:) = czero
do k = 1, dimP
  v(k,k) = ci
end do 
v(1,1) = cone
    
if (i == 1) then
  l_sq = mwsq
else if (i == 2) then
  l_sq = mhsq
end if
    
lAux(1,:) = sqrt(l_sq/two) * (v(1,:) + v(2,:))
lAux(2,:) = sqrt(l_sq/two) * (v(1,:) - v(2,:))
    
lAux(3,:) = sqrt(l_sq/two) * (v(1,:) + v(3,:))
lAux(4,:) = sqrt(l_sq/two) * (v(1,:) - v(3,:))
  
lAux(5,:) = sqrt(l_sq/two) * (v(1,:) + v(4,:))
lAux(6,:) = sqrt(l_sq/two) * (v(1,:) - v(4,:))

lAux(7,:) = sqrt(l_sq/three) * (v(1,:) + v(2,:) + v(3,:))

getTadpoleMomentaSE = lAux 
end function getTadpoleMomentaSE
  
end module
  