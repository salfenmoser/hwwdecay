use mod_auxfunctions
use mod_consts_dp
use mod_kinematics
use mod_vertices
use mod_numeratorFunctionsSE
use mod_diagramKinematicsSE


test_suite mod_selfEnergy
  complex(dp) :: pa1(3,dimP), pa2(3,dimP), paH(3,dimP)
  integer :: pol1, pol2, c1, c2

!------------------------------------------------------------------------------------------------------------
setup

pol1 = 1
pol2 = 1
c1 = 1
c2 = 1
call particlesHWW(pol1, pol2, c1, c2, paH, pa1, pa2)

end setup
!------------------------------------------------------------------------------------------------------------

teardown
  ! This code runs immediately after each test
end teardown

!----------------------------------------------------------------------------------
!-------------------------------------------------------------------------------------------------

test momentaBubbleH
  integer :: i, D_S, m, n, k
  complex(dp) :: l1(3,3,2,dimP), l2(2,2,2,dimP), lAux(4,dimP), lEps(dimP), den(2,2)
  complex(dp) :: x, y

do i = 1,2
do D_S = 4, 6

call getBubbleMomentaSE(D_S, paH, i, l1, l2, lAux, lEps, x, y)

do m = 1, 3
do n = 1, 3

  den = getDenominatorsSE(l1(m,n,1,:), paH)
  assert_equal_within(abs(den(i, 1)), zero, limitAssert)
  assert_equal_within(abs(den(i, 2)), zero, limitAssert)
  den = getDenominatorsSE(l1(m,n,2,:), paH)
  assert_equal_within(abs(den(i, 1)), zero, limitAssert)
  assert_equal_within(abs(den(i, 2)), zero, limitAssert)
  if (i==3 .AND. m==1 .AND. n ==1 ) then
    print *, den(i,:)
  end if
end do
end do

do m = 1, 2
  do n = 1, 2
    
    den = getDenominatorsSE(l2(m,n,1,:), paH)
    assert_equal_within(abs(den(i, 1)), zero, limitAssert)
    assert_equal_within(abs(den(i, 2)), zero, limitAssert)
    den = getDenominatorsSE(l2(m,n,2,:), paH)
    assert_equal_within(abs(den(i, 1)), zero, limitAssert)
    assert_equal_within(abs(den(i, 2)), zero, limitAssert)
end do
end do

do m = 1, 4
  den = getDenominatorsSE(lAux(m,:), paH)
  assert_equal_within(abs(den(i, 1)), zero, limitAssert)
  assert_equal_within(abs(den(i, 2)), zero, limitAssert)
end do

if (D_S > 4) then
den = getDenominatorsSE(lEps, paH)
assert_equal_within(abs(den(i, 1)), zero, limitAssert)
assert_equal_within(abs(den(i, 2)), zero, limitAssert)
end if

end do
end do

end test momentaDoubleH
! !------------------------------------------------------------------------------------------------------------


test momentaBubbleW
  integer :: i, D_S, m, n, k
  complex(dp) :: l1(3,3,2,dimP), l2(2,2,2,dimP), lAux(4,dimP), lEps(dimP), den(2,2)
  complex(dp) :: x, y

do i = 1,2
do D_S = 4, 6

call getBubbleMomentaSE(D_S, pa1, i, l1, l2, lAux, lEps, x, y)

do m = 1, 3
do n = 1, 3

  den = getDenominatorsSE(l1(m,n,1,:), pa1)
  assert_equal_within(abs(den(i, 1)), zero, limitAssert)
  assert_equal_within(abs(den(i, 2)), zero, limitAssert)
  den = getDenominatorsSE(l1(m,n,2,:), pa1)
  assert_equal_within(abs(den(i, 1)), zero, limitAssert)
  assert_equal_within(abs(den(i, 2)), zero, limitAssert)
end do
end do

do m = 1, 2
  do n = 1, 2
    den = getDenominatorsSE(l2(m,n,1,:), pa1)
    assert_equal_within(abs(den(i, 1)), zero, limitAssert)
    assert_equal_within(abs(den(i, 2)), zero, limitAssert)
    den = getDenominatorsSE(l2(m,n,2,:), pa1)
    assert_equal_within(abs(den(i, 1)), zero, limitAssert)
    assert_equal_within(abs(den(i, 2)), zero, limitAssert)
end do
end do

do m = 1, 4
  den = getDenominatorsSE(lAux(m,:), pa1)
  assert_equal_within(abs(den(i, 1)), zero, limitAssert)
  assert_equal_within(abs(den(i, 2)), zero, limitAssert)
end do

if (D_S > 4) then
den = getDenominatorsSE(lEps, pa1)
assert_equal_within(abs(den(i, 1)), zero, limitAssert)
assert_equal_within(abs(den(i, 2)), zero, limitAssert)
end if

end do
end do

end test momentaBubbleW
!------------------------------------------------------------------------------------------------------------

test bubbleNumeratorFunctionW
  integer :: i, D_S, m, n, k
  complex(dp) :: numFunc, bubbleFunc
  complex(dp) :: l1(3,3,2,dimP), l2(2,2,2,dimP), lAux(4,dimP), lEps(dimP), den(2,2)
  complex(dp) :: x, y

do D_S = 4,6
do i = 2,2
call getBubbleMomentaSE(D_S, pa1, i, l1, l2, lAux, lEps, x, y)

do m = 1,3
do n = 1,3
  do k = 1,2
numFunc = numeratorSE(pa1, l1(m, n, k, :), i, D_S)
bubbleFunc = B_SE(D_S, l1(m, n, k, :), pa1, i)
assert_equal_within(abs(numFunc/bubbleFunc), 1, limitAssert)
end do
end do
end do

do m = 1,2
do n = 1,2
  do k = 1,2
  numFunc = numeratorSE(pa1, l2(m, n, k, :), i, D_S)
  bubbleFunc = B_SE(D_S, l2(m, n, k, :), pa1, i)
  assert_equal_within(abs(numFunc/bubbleFunc), 1, limitAssert)
end do
end do
end do

do m = 1,4
  numFunc = numeratorSE(pa1, lAux(m, :), i, D_S)
  bubbleFunc = B_SE(D_S, lAux(m, :), pa1, i)
  assert_equal_within(abs(numFunc/bubbleFunc), 1, limitAssert)
end do

if (D_S > 4) then
  numFunc = numeratorSE(pa1, lEps, i, D_S)
  bubbleFunc = B_SE(D_S, lEps, pa1, i)
  assert_equal_within(abs(numFunc/bubbleFunc), 1, limitAssert)
end if


end do
end do

end test bubbleNumeratorFunctionW

!------------------------------------------------------------------------------------------------------------

test bubbleNumeratorFunctionH
  integer :: i, D_S, m, n, k
  complex(dp) :: numFunc, bubbleFunc
  complex(dp) :: l1(3,3,2,dimP), l2(2,2,2,dimP), lAux(4,dimP), lEps(dimP), den(2,2)
  complex(dp) :: x, y

do D_S = 4,6
do i = 1,2
call getBubbleMomentaSE(D_S, paH, i, l1, l2, lAux, lEps, x, y)

do m = 1,3
do n = 1,3
  do k = 1,2
numFunc = numeratorSE(paH, l1(m, n, k, :), i, D_S)
bubbleFunc = B_SE(D_S, l1(m, n, k, :), paH, i)
assert_equal_within(abs(numFunc/bubbleFunc), 1, limitAssert)
end do
end do
end do

do m = 1,2
do n = 1,2
  do k = 1,2
  numFunc = numeratorSE(paH, l2(m, n, k, :), i, D_S)
  bubbleFunc = B_SE(D_S, l2(m, n, k, :), paH, i)
  assert_equal_within(abs(numFunc/bubbleFunc), 1, limitAssert)
end do
end do
end do

do m = 1,4
  numFunc = numeratorSE(paH, lAux(m, :), i, D_S)
  bubbleFunc = B_SE(D_S, lAux(m, :), paH, i)
  assert_equal_within(abs(numFunc/bubbleFunc), 1, limitAssert)
end do

if (D_S > 4) then
  numFunc = numeratorSE(paH, lEps, i, D_S)
  bubbleFunc = B_SE(D_S, lEps, paH, i)
  assert_equal_within(abs(numFunc/bubbleFunc), 1, limitAssert)
end if


end do
end do

end test bubbleNumeratorFunctionH

!------------------------------------------------------------------------------------------------------------

test singleCutMomentaW
  integer :: i, D_S, m
  complex(dp) :: numFunc(4), singleCutFunc
  complex(dp) :: lAux(7,dimP), den(2,2)
  complex(dp) :: x, y

do D_S = 4,6
do i = 1,2
lAux = getTadpoleMomentaSE(i, D_S)

do m = 1,6
den = getDenominatorsSE(lAux(m,:), pa1)
assert_equal_within(abs(den(i, 1)), zero, limitAssert)
assert_false(abs(den(i, 2)) < limitAssert)
end do

if (D_S > 4) then
  den = getDenominatorsSE(lAux(7,:), pa1)
  assert_equal_within(abs(den(i, 1)), zero, limitAssert)
  assert_false(abs(den(i, 2)) < limitAssert)
end if


end do
end do

end test singleCutMomentaW

!------------------------------------------------------------------------------------------------------------

test singleCutMomentaH
  integer :: i, D_S, m
  complex(dp) :: numFunc(4), singleCutFunc
  complex(dp) :: lAux(7,dimP), den(2,2)
  complex(dp) :: x, y

do D_S = 4,6
do i = 1,2
lAux = getTadpoleMomentaSE(i, D_S)

do m = 1,6
den = getDenominatorsSE(lAux(m,:), paH)
assert_equal_within(abs(den(i, 1)), zero, limitAssert)
assert_false(abs(den(i, 2)) < limitAssert)
end do

if (D_S > 4) then
  den = getDenominatorsSE(lAux(7,:), paH)
  assert_equal_within(abs(den(i, 1)), zero, limitAssert)
  assert_false(abs(den(i, 2)) < limitAssert)
end if


end do
end do

end test singleCutMomentaH

!------------------------------------------------------------------------------------------------------------

test singleCutNumeratorFunctionH
  integer :: i, D_S, m
  complex(dp) :: numFunc(4), numFuncPlusp(4), singleCutFunc
  complex(dp) :: lAux(7,dimP), den(2,2), denPlusp(2,2), numSum
  complex(dp) :: x, y

do D_S = 4,6
do i = 1,2
lAux = getTadpoleMomentaSE(i, D_S)

do m = 1,7
den = getDenominatorsSE(lAux(m,:), paH)  
denPlusp = getDenominatorsSE(lAux(m,:) + paH(1,:), paH) 

numFunc(i) = numeratorSE(paH, lAux(m, :), i, D_S)
numFunc(i+2) = numeratorSE(paH, lAux(m, :), i+2, D_S)
numFuncPlusp(i) = numeratorSE(paH, lAux(m, :) + paH(1,:), i, D_S)
numFuncPlusp(i+2) = numeratorSE(paH, lAux(m, :) + paH(1,:), i+2, D_S)

numSum = numFunc(i)/den(i,2) + numFuncPlusp(i)/denPlusp(i,1) + numFunc(i+2)

!numFunc = numeratorSE(paH, lAux(m, :), i, D_S)
singleCutFunc = getTadpoleAmpsSE(D_S, lAux(m,:), paH, i, .true.)

! !print *, 'num func 1 = ', numFunc(1)
! print *, 'num func 1 = ', numFunc(i)/den(i,2)
! print *, 'num func 1 plus P = ', numFuncPlusp(i)/denPlusp(i,1)
! print *, '--------'
! !print *, 'num func 3 = ', numFunc(3)
! print *, 'num func 3 = ', numFunc(i+2)
! print *, 'diagram', i, 'single cut function'
! print *, singleCutFunc
! print *, 'numSum = ', numSum
! print *, 'quotionet = ', numSum/singleCutFunc
assert_equal_within(abs(numSum/singleCutFunc), 1, limitAssertHigh)
end do
end do
end do

end test singleCutNumeratorFunctionH



!------------------------------------------------------------------------------------------------------------

test singleCutNumeratorFunctionWDiag2
  integer :: D_S, m
  complex(dp) :: numFunc(4), numFuncPlusp(4), numFuncMinusL(4), singleCutFunc
  complex(dp) :: lAux(7,dimP), den(2,2), denPlusp(2,2), denMinusL(2,2), numSum
  complex(dp) :: x, y

do D_S = 4,6
lAux = getTadpoleMomentaSE(2, D_S)

do m = 1,7
den = getDenominatorsSE(lAux(m,:), pa1)  
denMinusL = getDenominatorsSE(- lAux(m,:), pa1)

numFunc(2) = numeratorSE(pa1, lAux(m, :), 2, D_S)
numFunc(4) = numeratorSE(pa1, lAux(m, :), 4, D_S)
numFuncMinusL(2) = numeratorSE(pa1, - lAux(m, :), 2, D_S)

numSum = numFunc(2)/den(2,2) + numFuncMinusL(2)/denMinusL(2,2) + numFunc(4)

!numFunc = numeratorSE(pa1, lAux(m, :), i, D_S)
singleCutFunc = getTadpoleAmpsSE(D_S, lAux(m,:), pa1, 2, .true.)

! print *, 'num func 1 = ', numFunc(i)/den(i,2)
! print *, 'num func 1 plus P = ', numFuncPlusp(i)/denPlusp(i,1)
! print *, 'num func 1 minus P = ', numFuncMinusL(i)/denMinusL(i,2)
! print *, '--------'
! print *, 'num func 3 = ', numFunc(i+2)
! print *, 'diagram', i, 'single cut function'
! print *, singleCutFunc
! print *, 'numSum = ', numSum
! print *, 'quotionet = ', numSum/singleCutFunc
assert_equal_within(abs(numSum/singleCutFunc), 1, limitAssert)
end do

end do

end test singleCutNumeratorFunctionWDiag2

!------------------------------------------------------------------------------------------------------------

! test singleCutNumeratorFunctionWDiag1
!   integer :: D_S, m, i
!   complex(dp) :: numFunc(4), numFuncMinusL(4), numFuncPlusP(4), numFuncPlusPMinus(4), numFuncMinusP(4), numFuncMinusPMinus(4)
!   complex(dp) :: singleCutFunc, lAux(7,dimP), numSum
!   complex(dp) :: den(2,2), denPlusp(2,2), denMinusL(2,2), denPlusPMinus(2,2), denMinusP(2,2), denMinusPMinus(2,2)
!   complex(dp) :: x, y

! do D_S = 4,4
! lAux = getTadpoleMomentaSE(1, D_S)

! do m = 1,1
! den(:,:) = getDenominatorsSE(lAux(m,:), pa1)  
! denMinusL(:,:) = getDenominatorsSE(- lAux(m,:), pa1)
! denPlusP(:,:) = getDenominatorsSE(lAux(m,:) + pa1(1,:), pa1)
! denPlusPMinus(:,:) = getDenominatorsSE( - lAux(m,:) - pa1(1,:), pa1)  
! denMinusP(:,:) = getDenominatorsSE(lAux(m,:) - pa1(1,:), pa1)
! denMinusPMinus(:,:) = getDenominatorsSE( - lAux(m,:) + pa1(1,:), pa1)  

! do i = 1,2
! numFunc(i) = numeratorSE(pa1, lAux(m, :), i, D_S)
! numFuncMinusL(i) = numeratorSE(pa1, - lAux(m, :), i, D_S)
! numFuncPlusP(i) = numeratorSE(pa1, lAux(m, :) + pa1(1,:), i, D_S)
! numFuncPlusPMinus(i) = numeratorSE(pa1, - lAux(m, :) - pa1(1,:), i, D_S)
! numFuncMinusP(i) = numeratorSE(pa1, lAux(m, :) - pa1(1,:), i, D_S)
! numFuncMinusPMinus(i) = numeratorSE(pa1, - lAux(m, :) + pa1(1,:), i, D_S)
! end do
! numFunc(3) = numeratorSE(pa1, lAux(m, :), 3, D_S)

! numSum = numFunc(1)/den(1,2) + numFuncMinusL(1)/denMinusL(1,2) + &
!       numFunc(2)/den(2,2) + numFuncMinusL(2)/denMinusL(2,2) + numFunc(3)

! !numFunc = numeratorSE(pa1, lAux(m, :), i, D_S)
! singleCutFunc = getTadpoleAmpsSE(D_S, lAux(m,:), pa1, 1, .true.)

! ! print *, 'num func 1 = ', numFunc(1)/den(1,1,2)
! ! ! print *, 'num func 1 minus L = ', numFuncMinusL(1)/denMinusL(1,1,2)
! ! print *, 'den check : ', den(2,2) - (sq(lAux(m,:)-pa1(1,:)) - mhsq)
! ! print *, 'den = ', den(2,2)
! ! print *, 'should be = ', (sq(lAux(m,:)-pa1(1,:)) - mhsq)
! ! print *, '--------'
! ! print *, 'num func 2 = ', numFunc(2)/den(2,1)
! ! print *, 'num func 2 minus L = ', numFuncMinusL(2)/denMinusL(2,1)
! ! print *, 'num func 2 Plus P = ', numFuncPlusP(2)/denPlusP(2,1)
! ! print *, 'num func 2 Plus P Minus= ', numFuncPlusPMinus(2)/denPlusPMinus(2,1)
! ! print *, 'num func 2 Minus P = ', numFuncMinusP(2)/denMinusP(2,1)
! ! print *, 'num func 2 Minus P Minus= ', numFuncMinusPMinus(2)/denMinusPMinus(2,1)
! ! print *, '--------'
! !print *, 'num func 4 = ', numFunc(4)
! ! print *, 'diagram 1, single cut function'
! ! print *, singleCutFunc
! ! print *, 'numSum = ', numSum
! ! print *, 'quotionet = ', numSum/singleCutFunc
! assert_equal_within(abs(numSum/singleCutFunc), 1, limitAssert)
! end do

! end do

! end test singleCutNumeratorFunctionWDiag1


end test_suite
