module mod_selfEnergy

  use mod_types
  use mod_consts_dp
  use common_def
  
  use mod_auxfunctions     
  use mod_kinematics
  use mod_amplitudes

  use mod_diagramKinematicsSE
  use mod_ampSumSE
  use mod_numeratorFunctionsSE
  
implicit none

public :: B_SE, getBubbleCoefficients_SE, getBubbleCoefficients0_SE
public :: A_SE, getTadpoleCoefficients_SE, getTadpoleCoefficients0_SE
public :: finalSE

logical :: demandOnShellExternal = .false., fromScratch = .true., testSE = .false.
integer :: unitOffsetC = 30, unitOffsetB = 33, unitOffsetA = 36


contains


!---------------------------------------------------------
! double cuts 
! --------------------------------------------------------

! double cut function depending on dimension D_S, momentum l, particle higgs or W
! no need to subtract higher order coefficients
function B_SE(D_S, l, pa, i)
  integer, intent(in) :: i, D_S
  complex(dp), intent(in) :: pa(3,dimP), l(dimP)
  complex(dp) :: B_SE
  complex(dp) :: num

B_SE = ci**2 * getBubbleAmpsSE(D_S, l, pa, i, demandOnShellExternal)

!print *, 'B = ', B_SE
num = d2W(pa, l)
!print *, 'numerator = ', num
end function B_SE

! ten double cut coefficients for dimension D_S, diagram i, particle pa (higgs or W)
! reduction is performed with ten properly chosen momenta l
function getBubbleCoefficients_SE(D_S, pa, i)
  integer, intent(in) :: i, D_S
  integer :: n, m, i_ind, unitInt
  complex(dp), intent(in) ::  pa(3,dimP)
  complex(dp) :: getBubbleCoefficients_SE(10), Bcoeff(10), B1(3), B2(2), B3(4), Beps, auxB
  complex(dp) :: x1, q(dimP), lt_sq, vec(dimP,dimP), V2(dimP)
  complex(dp) :: t, x, y
  complex(dp) :: l1(3,3,2,dimP), l2(2,2,2,dimP), l_Aux(4,dimP), l_Eps(dimP)
  character*5 :: strDim, strDiag, strPart
  character*70 :: documentName
  logical :: fileExists

getBubbleCoefficients_SE(:) = (/czero,czero,czero,czero,czero,czero,czero,czero,czero,czero/)

call getBubbleKinematicsSE(pa, i, q, x1, lt_sq, vec)

B1(:) = czero
l1(:,:,:,:) = czero
l2(:,:,:,:) = czero
l_Aux(:,:) = czero
l_Eps(:) = czero

call getBubbleMomentaSE(D_S, pa, i, l1, l2, l_Aux, l_Eps, x, y)

do m = 1,3
  do n = 0,2
  t = exp(two*pi*ci * n / three)  
  auxB = (B_SE(D_S, l1(m,n+1,1,:), pa, i) + B_SE(D_S, l1(m,n+1,2,:), pa, i))/ two
  B1(m) = B1(m) + (one/three) * auxB * t**(m-1) !corresponds to 1: b_0(eff), 3: b_1, 2: b_4(eff) 
end do
end do
  
B2(:) = czero
do m = 1,2
  do n = 0,1
  t = exp(2*pi*ci * n / two)
  auxB = (B_SE(D_S, l2(m,n+1,1,:), pa, i) - B_SE(D_S, l2(m,n+1,2,:), pa, i)) / (two * sqrt(lt_sq - t**2))
  B2(m) = B2(m) + (one/two) * auxB * t**(m-1) !corresponds to b_2, b_6    
end do
end do

Bcoeff(1) = B1(3)
Bcoeff(2) = B2(1)
Bcoeff(6) = B2(2)

B3(:) = czero
do m= 1,4
  B3(m) =  B_SE(D_S, l_Aux(m,:), pa, i)
end do
  
Bcoeff(3) = ( (B3(1) - B3(3))/two - x*Bcoeff(1) )/ y
Bcoeff(8) = ( (B3(1) - B3(2))/two - x*Bcoeff(1) ) / (x*y)
Bcoeff(5) = ( B1(1) + Bcoeff(3)*y + x*y * Bcoeff(8) + x* Bcoeff(1) + (x**two - y**two)*B1(2) - B3(1) ) / (three*y**two)
Bcoeff(4) = B1(2) + Bcoeff(5)
Bcoeff(10) = B1(1) - lt_sq * Bcoeff(5)
Bcoeff(7) = (B3(4) + (x**two - y**two) * Bcoeff(5) + Bcoeff(4)*x**2 - Bcoeff(3)*x - y*Bcoeff(2) - Bcoeff(10) ) / (x*y)

Bcoeff(9) = czero
!access b9
if (D_S > 4) then
  Beps = B_SE(D_S, l_Eps, pa, i)
  !print *, 'Beps = ', Beps
  !print *, 'B0 = ', Bcoeff(10)
  !Bcoeff(9) = (Beps - Bcoeff(4)*x**two - Bcoeff(1)*x - Bcoeff(10)) / y**two
  Bcoeff(9) = (Beps - Bcoeff(10))/lt_sq
end if

do m = 1,10
  if (abs(REAL(Bcoeff(m))) < limitCoeff ) then
    Bcoeff(m) = cmplx(zero,AIMAG(Bcoeff(m)),kind=dp)
  end if
  if (abs(AIMAG(Bcoeff(m))) < limitCoeff ) then
    Bcoeff(m) = cmplx(REAL(Bcoeff(m)),zero,kind=dp)
  end if
end do

getBubbleCoefficients_SE(:) = Bcoeff(:)

if (pa(3,1) .ne. 0) then
  strPart = "W"
else
  strPart = "H"
end if

! write coefficients in file
write (strDim, '(I1)') D_S
write (strDiag, '(I1)') i
unitInt = unitOffsetB+D_S-4
documentName = trim('diagramsSE/coefficientsSE/bubbleCoefficient_'//trim(strPart)//'_'//trim(strDiag)//'_'//trim(strDim)//'.dat')

INQUIRE(FILE=documentName, EXIST=fileExists)
!print *, 'file exists? ', fileExists
if (.NOT. fileExists .and. .NOT. testSE) then
  open(unit = unitInt, file = documentName, status='new')
  write(unitInt, FMT, advance='no') Bcoeff(:)
  close(unit = unitInt)
end if

end function getBubbleCoefficients_SE

function getBubbleCoefficients0_SE(D_S, pa, i)
  integer, intent(in) :: i, D_S
  complex(dp), intent(in) ::  pa(3,dimP)
  complex(dp) :: bubbleCoefficients(10)
  complex(dp) :: getBubbleCoefficients0_SE
  bubbleCoefficients(:) = czero
  bubbleCoefficients(:) = getBubbleCoefficients_SE(D_S, pa, i)
  getBubbleCoefficients0_SE = bubbleCoefficients(10)
end function getBubbleCoefficients0_SE

! returns the B_ij function for any momentum l. required for subtraction scheme for single cut coefficients
function getBubbleFunction_SE(D_S, l, pa, i)
  integer, intent(in) :: i, D_S
  complex(dp), intent(in) :: l(dimP), pa(3,dimP)
  complex(dp) :: getBubbleFunction_SE, b_Func, bCoeff(10), v(dimP,dimP), q(dimP), x1, lt_sq
  integer :: m, unitInt
  complex(dp) :: l_zero(dimP), diag(2,14)
  character*5 :: strDim, strDiag, strPart
  character*70 :: documentName
  logical :: fileExists, isW

call getBubbleKinematicsSE(pa, i, q, x1, lt_sq, v)
bCoeff = getBubbleCoefficients_SE(D_S, pa, i)

do m = 1,dimP
l_zero(m) = czero
end do

isW = isParticleW(pa)
strPart = "H"
if (isW) strPart = "W"

diag = diagramsSE(l_zero, pa(1,:), isW)

! check if file exists
write (strDim, '(I1)') D_S
write (strDiag, '(I1)') i
unitInt = 2
documentName = trim('diagramsSE/coefficientsSE/bubbleCoefficient_'//trim(strPart)//'_'//trim(strDiag)//'_'//trim(strDim)//'.dat')
!print*, 'inquiring existence of B file: ', documentName
INQUIRE(FILE=documentName, EXIST=fileExists)

! if so: getTriangleCoefficients_i from file
if (fileExists .and. (fromScratch .eqv. .false.)) then
  !print*, 'B coefficient file found, no re-calculation'
  open(unit = unitInt, file = trim(documentName), status='old')
  do m = 1,10
    read(unitInt, FMT, advance='no') bCoeff(:)
  end do
  close(unit = unitInt)

! if not: get from reduction
else
  !print*, 'B coefficient file not found, start calculation'
  bCoeff = getBubbleCoefficients_SE(D_S, pa, i)
end if

b_Func = bCoeff(10) + bCoeff(1)*sc(l, v(2,:)) + bCoeff(2)*sc(l, v(3,:)) + bCoeff(3)*sc(l, v(4,:))
b_Func = b_Func + bCoeff(4)*(sc(l, v(2,:))**2 - sc(l,v(4,:))**2) + bCoeff(5)*(sc(l, v(3,:))**2 - sc(l,v(4,:))**2)
b_Func = b_Func + bCoeff(6)*sc(l, v(2,:))*sc(l, v(3,:)) + bCoeff(7)*sc(l, v(3,:))*sc(l, v(4,:))
b_Func = b_Func + bCoeff(8)*sc(l, v(2,:))*sc(l, v(4,:)) + bCoeff(9)*sc(l, v(5,:))**2

getBubbleFunction_SE = b_Func
end function getBubbleFunction_SE

! ! !---------------------------------------------------------
! ! ! single cuts 
! ! ! --------------------------------------------------------

! single cut function depending on dimension D_S, momentum l, particles higgs, w1,w2, diagram i, cut jSingle
! need to subtract triangle and bubble coefficients adequately
function A_SE(D_S, l, pa, i)
  integer, intent(in) :: i, D_S
  complex(dp), intent(in) :: pa(3,dimP), l(dimP)
  complex(dp) :: d_l(2,2), d_lPp(2,2), d_lMl(2,2), d_lMp(2,2)
  complex(dp) :: A_SE
  complex(dp) :: doubleB(2), doubleB_Pp(2), doubleB_Ml(2), doubleB_Mp(2)
  integer :: j, k

d_l = getDenominatorsSE(l, pa)
d_lPp = getDenominatorsSE(l+pa(1,:), pa)
d_lMp = getDenominatorsSE(l-pa(1,:), pa)
d_lMl = getDenominatorsSE(-l, pa)

A_SE = ci * getTadpoleAmpsSE(D_S, l, pa, i, demandOnShellExternal)

do k = 1,2
doubleB(k) = getBubbleFunction_SE(D_S, l, pa, k)
doubleB_Pp(k) = getBubbleFunction_SE(D_S, l+pa(1,:), pa, k)
doubleB_Mp(k) = getBubbleFunction_SE(D_S, l-pa(1,:), pa, k)
doubleB_Ml(k) = getBubbleFunction_SE(D_S, -l, pa, k)
end do

if (pa(3,1) .ne. 0 .and. i == 2) then
  A_SE = A_SE - doubleB(2) / d_l(2,2) - doubleB_Ml(2) / d_lMl(2,2)
else if (pa(3,1) .ne. 0 .and. i == 1) then
  A_SE = A_SE - doubleB(1) / d_l(1,2) - doubleB_Ml(1) / d_lMl(1,2) - doubleB_Pp(2) / d_lPp(2,1) - doubleB_Mp(2) / d_lMp(2,1) 
else
  A_SE = A_SE - doubleB(i) / d_l(i,2) - doubleB_Pp(i) / d_lPp(i,1) 
end if

end function A_SE

! five single cut coefficients for dimension D_S, diagram i
! reduction is performed with seven properly chosen momenta l
function getTadpoleCoefficients_SE(D_S, pa, i)
  integer, intent(in) :: i, D_S
  complex(dp), intent(in) ::  pa(3,dimP)
  complex(dp) :: getTadpoleCoefficients_SE(5), A(10), l_sq, diag(2,14), lAux(7,dimP)
  integer :: k, unitInt
  character*5 :: strDim, strDiag, strPart
  character*70 :: documentName
  logical :: fileExists

getTadpoleCoefficients_SE(:) = (/czero,czero,czero,czero,czero/)

if (i == 1) then
  l_sq = mwsq
else if (i == 2) then
  l_sq = mhsq
end if

lAux = getTadpoleMomentaSE(i, D_S)

do k = 1,7
A(k) = A_SE(D_S, lAux(k,:), pa, i)
end do

! 5 corresponds to 0
getTadpoleCoefficients_SE(2) = (A(1) - A(2)) / sqrt(two * l_sq)
getTadpoleCoefficients_SE(3) = (A(3) - A(4)) / sqrt(two * l_sq)
getTadpoleCoefficients_SE(4) = (A(5) - A(6)) / sqrt(two * l_sq)

getTadpoleCoefficients_SE(5) = ( (A(1) - A(7)*sqrt(three/two)) + getTadpoleCoefficients_SE(3)*sqrt(l_sq/two) ) &
      / (one - sqrt(three/two))
getTadpoleCoefficients_SE(1) = (A(1) - getTadpoleCoefficients_SE(5))*sqrt(two/l_sq) - getTadpoleCoefficients_SE(2)

do k = 1,5
  if (abs(REAL(getTadpoleCoefficients_SE(k))) < limitCoeff ) then
    getTadpoleCoefficients_SE(k) = cmplx(zero,AIMAG(getTadpoleCoefficients_SE(k)),kind=dp)
  end if
  if (abs(AIMAG(getTadpoleCoefficients_SE(k))) < limitCoeff ) then
    getTadpoleCoefficients_SE(k) = cmplx(REAL(getTadpoleCoefficients_SE(k)),zero,kind=dp)
  end if
end do

if (pa(3,1) .ne. 0) then
  strPart = "W"
else
  strPart = "H"
end if

do k = 1,5
  if (abs(REAL(getTadpoleCoefficients_SE(k))) < limitCoeff ) then
    getTadpoleCoefficients_SE(k) = cmplx(zero,AIMAG(getTadpoleCoefficients_SE(k)),kind=dp)
  end if
  if (abs(AIMAG(getTadpoleCoefficients_SE(k))) < limitCoeff ) then
    getTadpoleCoefficients_SE(k) = cmplx(REAL(getTadpoleCoefficients_SE(k)),zero,kind=dp)
  end if
end do

! write coefficients in file
write (strDim, '(I1)') D_S
write (strDiag, '(I1)') i
unitInt = 3

documentName = trim('diagramsSE/coefficientsSE/singleCutCoefficient_'//trim(strPart)//'_'//trim(strDiag)//'_'//trim(strDim)//'.dat')

INQUIRE(FILE=documentName, EXIST=fileExists)
if (.NOT. fileExists ) then
  open(unit = unitInt, file = documentName, status='new')
  write(unitInt, FMT, advance='no') getTadpoleCoefficients_SE(:)
  close(unit = unitInt)
end if

end function getTadpoleCoefficients_SE

function getTadpoleCoefficients0_SE(D_S, pa, i)
  integer, intent(in) :: i, D_S
  complex(dp), intent(in) ::  pa(3,dimP)
  complex(dp) :: singleCutCoefficients(5)
  complex(dp) :: getTadpoleCoefficients0_SE
  singleCutCoefficients(:) = czero
  singleCutCoefficients(:) = getTadpoleCoefficients_SE(D_S, pa, i)
  getTadpoleCoefficients0_SE = singleCutCoefficients(5)
end function getTadpoleCoefficients0_SE



function finalSE(pa)
  complex(dp), intent(in) ::  pa(3,dimP)
  complex(dp) :: finalSE(4), bubbleCoefficients(10), singleCutCoefficients(5)
  character*70 :: documentName
  integer :: D_S, i

finalSE(:) = czero
!contains 6 numbers, b0, b7 for B_mh, B_mw, a0 for A_mh, A_mw
if (pa(3,1) .ne. 0) then
  documentName = trim('diagramsSE/coefficientsSE/finalSE_W.dat')
else
  documentName = trim('diagramsSE/coefficientsSE/finalSE_H.dat')
end if

singleCutCoefficients(:) = getTadpoleCoefficients_SE(4, pa, 1)
finalSE(3) = singleCutCoefficients(5)
singleCutCoefficients(:) = getTadpoleCoefficients_SE(4, pa, 2)
finalSE(4) = singleCutCoefficients(5)

bubbleCoefficients = two * getBubbleCoefficients_SE(5, pa, 1) - getBubbleCoefficients_SE(6, pa, 1)
finalSE(1) = bubbleCoefficients(10)
bubbleCoefficients = two * getBubbleCoefficients_SE(5, pa, 2) - getBubbleCoefficients_SE(6, pa, 2)
finalSE(2) = bubbleCoefficients(10)


open(unit = 1, file = trim(documentName), status='replace')
write(1, FMT, advance='no') finalSE(1)
write(1, FMT, advance='no') finalSE(2)
write(1, FMT, advance='no') finalSE(3)
write(1, FMT, advance='no') finalSE(4)
close(1)

end function finalSE

end module