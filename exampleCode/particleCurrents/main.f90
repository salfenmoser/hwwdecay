  use mod_types
  use mod_consts_dp
  use common_def
  
  use mod_auxfunctions     
  use mod_kinematics
  use mod_vectorCurrents
  use mod_amplitudes

  
implicit none

! define all variables
complex(dp) :: h(3,dimP), w_1(3,dimP), w_2(3, dimP), w_3(3,dimP), w_4(3, dimP), w_5(3, dimP)    ! one higgs and five W bosons
integer :: ep1, ep2, ep3, ep4, ep5, c1, c2, c3, c4, c5                                          ! color and polarization states ranging from 1 to 3
complex(dp) :: pol1(6,dimE), pol2(6,dimE), pol3(6,dimE), pol4(6,dimE), pol5(6,dimE)             ! polarization state matrices
real(dp) :: E                                                                                   ! process energy
complex(dp) :: p_3n(3,4), p_4n(4,4), p_5n(5,4)                                                  ! momentum matrix of participating particles for 3, 4 and 5 currents

complex(dp) :: vectorCurrent(dimP), scalarCurrent                                               ! result variables

!---------------------------------------------------------------------

! ----------- 3 PARTICLE CURRENT: HWW

! define process energy
E = 300._dp

! define color and polarization states
! every configuration where not ep1=ep2 or not c1=c2 vanishes
ep1 = 1
ep2 = 1

c1 = 1
c2 = 1

! get particle matrices, particlesHWW implemented in kinematics module
call particlesHWW(ep1, ep2, c1, c2, h, w_1, w_2)

! get vector current, implemented in vectorCurrents module
vectorCurrent = HW2(w_2, w_1)
print *, '---------------------------HWW CURRENTS--------------------------------'
print *, 'vector Current of HWW process:'
print *, vectorCurrent

! get scalar current, implemented in scalarCurrents module
! scalar Currents module recognizes type of particle
scalarCurrent = J3(h, w_1, w_2)
print *, 'scalar Current of HWW process:'
print *, scalarCurrent
print *, '-----------------------------------------------------------------------'

!call particlesHWW(ep1, ep2, c1, c2, h, w_1, w_2)


! ----------- 4 PARTICLE CURRENT: 4W

! define process energy
E = 500._dp

! define color and polarization states
! 1,2,2,1 + 1,2,2,1 is example for possible internal W, or 4W vertex
! 1,1,1,1 + 1,1,1,1 is example for internal higgs
! 1,2,2,1 + 1,2,3,1 vanishes because of color structure
ep1 = 1
ep2 = 2
ep3 = 2
ep4 = 1

c1 = 1
c2 = 2
c3 = 3
c4 = 1

! get momenta of particles for a physical process
! soubroutine impemented in kinematics module
! no subroutine for higgs and w scattering beyond HWW implemented
call kinematicsN(E,2,p_4n)


! construct particle matrix with remaining properties from momentum
! kinematicsN gives 4d momenta, 6d momenta required for currents and polarization
w_1(1,1:4) = p_4n(1,:)
call spinonepol_6d(w_1(1,:),pol1)
w_1(2,:) = pol1(ep1, :)
w_1(3,1) = c1

w_2(1,1:4) = p_4n(2,:)
call spinonepol_6d(w_2(1,:),pol2)
w_2(2,:) = pol2(ep2, :)
w_2(3,1) = c2

w_3(1,1:4) = p_4n(3,:)
call spinonepol_6d(w_3(1,:),pol3)
w_3(2,:) = pol3(ep3, :)
w_3(3,1) = c3

w_4(1,1:4) = p_4n(4,:)
call spinonepol_6d(w_4(1,:),pol4)
w_4(2,:) = pol4(ep4, :)
w_4(3,1) = c4

! get vector current, implemented in vectorCurrents module
vectorCurrent = W4(int(w_4(3,1)), w_1, w_2, w_3)
print *, '---------------------------4W CURRENTS---------------------------------'
print *, 'vector Current of 4W process:'
print *, vectorCurrent

! get scalar current, implemented in scalarCurrents module
! scalar Currents module recognizes type of particle
scalarCurrent = J4(w_1, w_2, w_3, w_4)
print *, 'scalar Current of 4W process:'
print *, scalarCurrent
print *, '-----------------------------------------------------------------------'

!call particlesHWW(ep1, ep2, c1, c2, h, w_1, w_2)


! ----------- 5 PARTICLE CURRENT: 5W

! define process energy
E = 500._dp

! define color and polarization states
! 1,2,2,1,1 + 1,2,1,1,3 non-vanishing
! 1,2,2,1,3 + 1,2,3,1,2 vanishes
ep1 = 1
ep2 = 2
ep3 = 2
ep4 = 1
ep5 = 1

c1 = 1
c2 = 2
c3 = 1
c4 = 1
c5 = 3

! get momenta of particles for a physical process
! soubroutine impemented in kinematics module
call kinematicsN(E,3,p_5n)

! construct particle matrix with remaining properties from momentum
! kinematicsN gives 4d momenta, 6d momenta required for currents and polarization
w_1(1,1:4) = p_5n(1,:)
call spinonepol_6d(w_1(1,:),pol1)
w_1(2,:) = pol1(ep1, :)
w_1(3,1) = c1

w_2(1,1:4) = p_5n(2,:)
call spinonepol_6d(w_2(1,:),pol2)
w_2(2,:) = pol2(ep2, :)
w_2(3,1) = c2

w_3(1,1:4) = p_5n(3,:)
call spinonepol_6d(w_3(1,:),pol3)
w_3(2,:) = pol3(ep3, :)
w_3(3,1) = c3

w_4(1,1:4) = p_5n(4,:)
call spinonepol_6d(w_4(1,:),pol4)
w_4(2,:) = pol4(ep4, :)
w_4(3,1) = c4

w_5(1,1:4) = p_5n(4,:)
call spinonepol_6d(w_5(1,:),pol5)
w_5(2,:) = pol5(ep5, :)
w_5(3,1) = c5

! get vector current, implemented in vectorCurrents module
vectorCurrent = W5(int(w_5(3,1)), w_1, w_2, w_3, w_4)
print *, '---------------------------5W CURRENTS---------------------------------'
print *, 'vector Current of 5W process:'
print *, vectorCurrent

! get scalar current, implemented in scalarCurrents module
! scalar Currents module recognizes type of particle
scalarCurrent = J5(w_1, w_2, w_3, w_4, w_5)
print *, 'scalar Current of 5W process:'
print *, scalarCurrent
print *, '-----------------------------------------------------------------------'

!call particlesHWW(ep1, ep2, c1, c2, h, w_1, w_2)


end 
