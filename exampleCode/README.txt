This folder contains some examples for code usage.
To run an example, uncomment the respective line in the makefile, where the Main file is defined (line 132 and following), and comment all others.

- particleCurrents: example on how to get numerical results of tree level particle current
- tripleCoefficients: different ways to obtain and use triple cut tripleCoefficients
- final0Coefficients: shows how the nine final master integral coefficients (only those non-vanishing after integration) are obtained
- selfEnergy: illustrates how to access coefficients of master integrals related to self energy diagrams
- waveFunctionRenormalization: shows off-shell continuation of momenta and related evaluation of self energy diagrams

General parameters and uncertainty levels, where we set certain parameters to zero, are defined in BasicDef/mod_const_dp.f90.