  use mod_types
  use mod_consts_dp
  use common_def
  
  use mod_auxfunctions     
  use mod_kinematics
  use mod_selfEnergy
  
  implicit none

complex(dp) :: h(3,dimP), w1(3,dimP), w2(3, dimP)
integer :: ep1, ep2, c1, c2
complex(dp) :: kappa(dimP), hShifted(3, dimP), w1Shifted(3, dimP), w2Shifted(3, dimP)
complex(dp) :: xi(2)
integer :: j, k

complex(dp) :: finalCoeffSE(2,2,3), selfEnDer(2,3)

! ------------ SELF ENERGY COEFFICIENT DERIVATIVES FOR HIGGS -------------------
selfEnDer(:,:) = czero
xi = [zero, one*1e-9]

! calculate shift momentum from physical h,w momenta
ep1 = 1
ep2 = 1
c1 = 1
c2 = 1
call particlesHWW(ep1, ep2, c1, c2, h, w1, w2)
call HShiftMomentum(h(1,:), w1(1,:), kappa)
hShifted = h
  
do j = 1,2
hShifted(1,:) = h(1,:) + kappa(:) * xi(j)
  
call system('rm ~/Documents/thesis/fortran/UnitarityMethodCalculation/diagramsSE/coefficientsSE/*')
finalCoeffSE(j,:,:) = finalSE(hShifted)
end do

! linear fit
! b0' of W (j=1) loop and H (j=2) loop for k=1, a0' of W (j=1) loop and H (j=2) loop for k=3
! for simplicity, second column of selfEnDer remains unused
do j = 1,2
  do k = 1,3, 2
    selfEnDer(j,k) = ( finalCoeffSE(2,j,k) - finalCoeffSE(1,j,k) ) / (two*sc(hShifted(1,:),kappa(:))*xi(2))
  end do
end do

print *, '----------------SELF ENERGY COEFFICIENT DERIVATIVES FOR HIGGS ----------------'
print *, 'derivative of b_0 of W loop (diagram 1) : ', selfEnDer(1,1)
print *, 'derivative of b_0 of H loop (diagram 2) : ', selfEnDer(2,1)
print *, 'derivative of a_0 of W loop (diagram 1) : ', selfEnDer(1,3)
print *, 'derivative of a_0 of H loop (diagram 2) : ', selfEnDer(2,3)



! ------------ SELF ENERGY COEFFICIENT DERIVATIVES FOR W -------------------
selfEnDer(:,:) = czero
xi = [zero, one*1e-9]

! construct particle matrix and shift momentum
ep1 = 1
ep2 = 1
c1 = 1
c2 = 1
call particlesHWW(ep1, ep2, c1, c2, h, w1, w2)
call WShiftMomentum(kappa)
w1Shifted = w1
  
do j = 1,2
w1Shifted(1,:) = w1(1,:) + kappa(:) * xi(j)
  
call system('rm ~/Documents/thesis/fortran/UnitarityMethodCalculation/diagramsSE/coefficientsSE/*')
finalCoeffSE(j,:,:) = finalSE(w1Shifted)
end do

! linear fit
! b0' of W (j=1) loop and HW (j=2) loop for k=1, a0' of W (j=1) loop and HW (j=2) loop for k=3
! for simplicity, second column of selfEnDer remains unused
do j = 1,2
  do k = 1,3, 2
    selfEnDer(j,k) = ( finalCoeffSE(2,j,k) - finalCoeffSE(1,j,k) ) / (two*sc(hShifted(1,:),kappa(:))*xi(2))
  end do
end do

print *, '----------------SELF ENERGY COEFFICIENT DERIVATIVES FOR W ----------------'
print *, 'derivative of b_0 of W loop (diagram 1) : ', selfEnDer(1,1)
print *, 'derivative of b_0 of WH loop (diagram 2) : ', selfEnDer(2,1)
print *, 'derivative of a_0 of W loop (diagram 1) : ', selfEnDer(1,3)
print *, 'derivative of a_0 of WH loop (diagram 2) : ', selfEnDer(2,3)

end 
