  use mod_types
  use mod_consts_dp
  use common_def
  
  use mod_auxfunctions     
  use mod_kinematics
  use mod_tadpole
  
  implicit none

complex(dp) :: h(3,dimP), w1(3,dimP), w2(3, dimP)
integer :: ep1, ep2, c1, c2
complex(dp) :: finalCoeff(9)

!---------------------------------------------------------------------


! ------------ FINAL MASTER INTEGRAL COEFFICIENTS FOR SPECIFIC POLARIZATION AND COLOR STATES -------------------
! define polarization, color
ep1 = 1
ep2 = 1
c1 = 1
c2 = 1

! particle matrices of HWW process
call particlesHWW(ep1, ep2, c1, c2, h, w1, w2)

! if necessary, deelete all intermediate previous results
call system('rm ~/Documents/thesis/fortran/UnitarityMethodCalculation/diagrams/coefficients/*')

! calculate all 10 triple cut coefficients, implementation in diagrams module, last entry is c_0
finalCoeff(:) =  czero
finalCoeff(:) = finalCoefficients(h, w1, w2)

print *, '---------------------FINAL MASTER INTEGRAL COEFFICIENTS FOR SPECIFIC POLARIZATION AND COLOR STATES-----------------------'
print *, finalCoeff(:)
print *, '-----------------------------------------------------------'

end 
