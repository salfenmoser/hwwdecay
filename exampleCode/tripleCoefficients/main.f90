  use mod_types
  use mod_consts_dp
  use common_def
  
  use mod_auxfunctions     
  use mod_kinematics
  use mod_triangle
  
  implicit none

complex(dp) :: h(3,dimP), w1(3,dimP), w2(3, dimP)
integer :: ep1, ep2, c1, c2
integer :: dim, diag
complex(dp) :: triangleCoefficients(10)

!---------------------------------------------------------------------


! ------------ TRIPLE CUT COEFFICIENTS FOR SPECIFIC POLARIZATION AND COLOR STATES, FOR GIVEN DIMENSION AND DIAGRAM -------------------
! define polarization, color, dimension, and which diagram should be investigated
ep1 = 1
ep2 = 1
c1 = 1
c2 = 1

dim = 4
diag = 1

! particle matrices of HWW process
call particlesHWW(ep1, ep2, c1, c2, h, w1, w2)

! if necessary, deelete all intermediate previous results
! call system('rm ~/Documents/thesis/fortran/UnitarityMethodCalculation/coefficients/*')

! calculate all 10 triple cut coefficients, implementation in diagrams module, last entry is c_0
triangleCoefficients(:) =  czero
triangleCoefficients(:) = getTriangleCoefficients_i(dim, h, w1, w2, diag)

print *, '---------------------TRIPLE CUT DOEFFICIENTS FOR GIVEN DIMENSION AND DIAGRAM-----------------------'
print *, 'Dimension = ', dim
print *, 'Diagram = ', diag
print *, 'Triple cut coefficients:'
print *, triangleCoefficients
print *, '-----------------------------------------------------------'


! ------------ TRIPLE CUT COEFFICIENTS FOR SPECIFIC POLARIZATION AND COLOR STATES, IN D -> 4 LIMIT FOR CERTAIN DIAGRAM -------------------
! define polarization, color and which diagram should be investigated
ep1 = 1
ep2 = 1
c1 = 1
c2 = 1

diag = 1

! particle matrices of HWW process
call particlesHWW(ep1, ep2, c1, c2, h, w1, w2)

! if necessary, deelete all intermediate previous results
! call system('rm ~/Documents/thesis/fortran/UnitarityMethodCalculation/diagrams/coefficients/*')

! calculate all 10 triple cut coefficients in 5 and 6 dimensions, take difference for 4d limit
triangleCoefficients(:) =  czero
dim = 5
triangleCoefficients(:) = two * getTriangleCoefficients_i(dim, h, w1, w2, diag)
dim = 6
triangleCoefficients(:) = triangleCoefficients(:) - getTriangleCoefficients_i(dim, h, w1, w2, diag)

print *, '---------------TRIPLE CUT DOEFFICIENTS FOR GIVEN DIAGRAM IN D->4 LIMIT FOR GIVEN PLARIZATION AND COLOR STATE----------------------'
print *, 'Diagram = ', diag
print *, 'Triple cut coefficients:'
print *, triangleCoefficients
print *, '-----------------------------------------------------------'


! ------------ TRIPLE CUT COEFFICIENTS, SUM OVER POLARIZATION AND COLOR STATES, IN D -> 4 LIMIT FOR CERTAIN DIAGRAM -------------------
! define diagram
diag = 1

triangleCoefficients(:) =  czero
do ep1 = 1,3
  do ep2 = 1,3
    do c1 = 1,3
      do c2 = 1,3

        ! particle matrices of HWW process
        call particlesHWW(ep1, ep2, c1, c2, h, w1, w2)

        ! if necessary, deelete all intermediate previous results
        call system('rm ~/Documents/thesis/fortran/UnitarityMethodCalculation/diagrams/coefficients/*')

        ! calculate all 10 triple cut coefficients in 5 and 6 dimensions, take difference for 4d limit
        dim = 5
        triangleCoefficients(:) = triangleCoefficients(:) + two * getTriangleCoefficients_i(dim, h, w1, w2, diag)
        dim = 6
        triangleCoefficients(:) = triangleCoefficients(:) - getTriangleCoefficients_i(dim, h, w1, w2, diag)

      end do
    end do
  end do
end do

print *, '-----------------TRIPLE CUT DOEFFICIENTS FOR GIVEN DIAGRAM IN D->4 LIMIT, POLARIZATION AND COLOR SUM---------------------'
print *, 'Diagram = ', diag
print *, 'Triple cut coefficients:'
print *, triangleCoefficients
print *, '-----------------------------------------------------------'

end 
