  use mod_types
  use mod_consts_dp
  use common_def
  
  use mod_auxfunctions     
  use mod_kinematics
  use mod_selfEnergy
  
  implicit none

complex(dp) :: h(3,dimP), w1(3,dimP), w2(3, dimP)
integer :: ep1, ep2, c1, c2
integer :: dim, diag
complex(dp) :: bubbleCoefficients_SE(10), finalCoeffSE(2,3)

!---------------------------------------------------------------------

! ------------ SELF ENERGY DOUBLE CUT COEFFICIENTS FOR HIGGS, FOR GIVEN DIMENSION AND DIAGRAM -------------------
! define dimension and which diagram should be investigated
dim = 4
diag = 1

! any physical configuration for higgs particle matrix is useable
h(:,:) = czero
h(1,1) = mh

! if necessary, deelete all intermediate previous results
call system('rm ~/Documents/thesis/fortran/UnitarityMethodCalculation/diagramsSE/coefficientsSE/*')

! calculate all 10 double cut coefficients, implementation in diagramsSE/selfEnergy module, last entry is b_0
bubbleCoefficients_SE(:) =  czero
bubbleCoefficients_SE(:) = getBubbleCoefficients_SE(dim, h, diag)

print *, '-----------HIGGS SELF ENERGY DOUBLE CUT DOEFFICIENTS FOR GIVEN DIMENSION AND DIAGRAM-------------'
print *, 'Dimension = ', dim
print *, 'Diagram = ', diag
print *, 'Double cut coefficients:'
print *, bubbleCoefficients_SE
print *, '-----------------------------------------------------------'


! ------------ SELF ENERGY FINAL COEFFICIENTS FOR HIGGS, D->4 LIMIT, INCLUDING RATIONAL PART COEFFICIENT -------------------

! if necessary, deelete all intermediate previous results
call system('rm ~/Documents/thesis/fortran/UnitarityMethodCalculation/diagramsSE/coefficientsSE/*')

! calculate 6 final self energy coefficients, implementation in diagramsSE/selfEnergy module
! first line is first diagram (W loop), second line is second diagram (H loop)
! entries per line : b_0, b_9, a_0
finalCoeffSE(:,:) =  czero
finalCoeffSE(:,:) = finalSE(h)

print *, '-------------SELF ENERGY FINAL COEFFICIENTS FOR HIGGS----------------'
print *, 'W Loop coefficients:'
print *, finalCoeffSE(1,:)
print *, 'H Loop coefficients:'
print *, finalCoeffSE(2,:)
print *, '-----------------------------------------------------------'


! ------------ SELF ENERGY DOUBLE CUT COEFFICIENTS FOR W, FOR GIVEN DIMENSION AND DIAGRAM -------------------
! define polarization, color, dimension, and which diagram should be investigated
ep1 = 1
ep2 = 1
c1 = 1
c2 = 1

dim = 4
diag = 1

! any physical configuration for higgs particle matrix is useable, easiest to return to HWW configuration
call particlesHWW(ep1, ep2, c1, c2, h, w1, w2)

! if necessary, deelete all intermediate previous results
call system('rm ~/Documents/thesis/fortran/UnitarityMethodCalculation/diagramsSE/coefficientsSE/*')

! calculate all 10 double cut coefficients, implementation in diagramsSE/selfEnergy module, last entry is b_0
bubbleCoefficients_SE(:) =  czero
bubbleCoefficients_SE(:) = getBubbleCoefficients_SE(dim, w1, diag)

print *, '--------------W SELF ENERGY DOUBLE CUT DOEFFICIENTS FOR GIVEN DIMENSION AND DIAGRAM----------------'
print *, 'Dimension = ', dim
print *, 'Diagram = ', diag
print *, 'Double cut coefficients:'
print *, bubbleCoefficients_SE
print *, '-----------------------------------------------------------'


! ------------ SELF ENERGY FINAL COEFFICIENTS FOR W, D->4 LIMIT, INCLUDING RATIONAL PART COEFFICIENT -------------------

! if necessary, deelete all intermediate previous results
call system('rm ~/Documents/thesis/fortran/UnitarityMethodCalculation/diagramsSE/coefficientsSE/*')

! calculate 6 final self energy coefficients, implementation in diagramsSE/selfEnergy module
! first line is first diagram (W loop), second line is second diagram (WH loop)
! entries per line : b_0, b_9, a_0
finalCoeffSE(:,:) =  czero
finalCoeffSE(:,:) = finalSE(w1)

print *, '---------------SELF ENERGY FINAL COEFFICIENTS FOR W---------------'
print *, 'W Loop coefficients:'
print *, finalCoeffSE(1,:)
print *, 'WH Loop coefficients:'
print *, finalCoeffSE(2,:)
print *, '-----------------------------------------------------------'

end 
