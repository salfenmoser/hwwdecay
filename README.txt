Unitarity method calculations for HWW one-loop scattering amplitude master integral coefficients

FOLDER STRUCTURE:

USAGE:

TESTS:
Many folders contain .fun files and a makefile for testing.
If funit is installed, tests can be run in the following way:
- go to directory of folder with respective test in terminal
- run "make test": This will copy the required files into the folder and complile and all tests in the respecive folder. Console output will show results of test suites (if mod_a.fun and mod_B.fun are test files in folder) in the form:
    mod_A test suite:
    Passed 4 of 4 possible asserts comprising 3 of 3 tests.

    mod_B test suite:
    Passed 6 of 6 possible asserts comprising 2 of 2 tests.

    ==========[ SUMMARY ]==========
    mod_A  passed
    mod_B:  passed
- run "make clean": This will delete the previously copied files and all temporary test files
- single test suite for mod_A can be run by "funit.ruby2.5 mod_A" (or respective funit version). First, respective module files need to be copied into destination folder by "make copy"

FUNIT requires the gfortran compiler. Errors can be caused by different requirements of the compiler to the code (e.g. maximum line length of 132 or 72 characters, lines starting with "c" interpreted as comments)
The ~bin/.bashrc file was adapted in the following way:
    export CXX=g++
    export CC=gcc

    export FC=gfortran
    export GEM_HOME=PATH_TO_FUNIT (x1/"user"/funit/)
    export PATH=PATH_TO_FUNIT/bin/:$PATH
    export FSFLAG=-I

INSTALL FUNIT:
If you have rubygems installed, simply run "sudo gem install funit"
Else, install rubygems first