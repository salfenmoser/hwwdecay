Here = $(PWD)
Exec = ./ewu
F95compiler = ifort
SEDcmd = sed

ObjectDir = $(Here)/objects
ModuleDir = $(Here)/modules
coefficientFileDir = $(Here)/HWW/coefficients
coefficientSEFileDir = $(Here)/SE/coefficientsSE
#coefficient3FileDir = $(Here)/coefficients3
#coefficient4FileDir = $(Here)/coefficients4
#coefficient8FileDir = $(Here)/coefficients8

config=ggvvnlo.cfg

Opt = No

ifeq ($(Opt),Yes)
   IfortOpts  = -O2 -fpp -vec-report0 -module $(ModuleDir)
else
   IfortOpts  = -O0 -fpp -nozero -debug all -check uninit -check bounds -check pointer -ftrapuv -module $(ModuleDir)
endif

#ifeq ($(Opt),Yes)
#   IfortOpts  = -B $(ModuleDir)
#else
#   IfortOpts  = -B $(ModuleDir)
#endif

WithLHAPDF = No

LibCuba=./libcuba.a
LibQCDLoop = libqcdloop.a libff.a
LHAPDF=/Users/kirill/u1/LHAPDF/lib



ifeq ($(WithLHAPDF),Yes)
   fcomp = $(F95compiler) $(IfortOpts) 
else
   fcomp = $(F95compiler) $(IfortOpts) 
endif



##################################################################################################

BasicDef = \
	./BasicDef/mod_types.f90 \
	./BasicDef/common_def.f90 \
	./BasicDef/mod_consts_dp.f90 \

BasicDefObj = \
	$(ObjectDir)/mod_types.o \
	$(ObjectDir)/common_def.o \
	$(ObjectDir)/mod_consts_dp.o \

AuxFunctions = \
	./AuxFunctions/cpolylog_main.F \
	./AuxFunctions/auxfunctions.f90 \

AuxFunctionsObj = \
	$(ObjectDir)/cpolylog_main.o \
	$(ObjectDir)/auxfunctions.o \

	
Kinematics = \
	./kinematics/mod_kinematics.f90 \

KinematicsObj = \
	$(ObjectDir)/mod_kinematics.o \
	
Currents = \
	./Currents/mod_vertices.f90 \
	./Currents/mod_propagators.f90 \
	./Currents/mod_vectorCurrents.f90 \
	./Currents/mod_currentMatrices.f90 \
	./Currents/mod_amplitudes.f90 \

CurrentsObj = \
	$(ObjectDir)/mod_vertices.o \
	$(ObjectDir)/mod_propagators.o \
	$(ObjectDir)/mod_vectorCurrents.o \
	$(ObjectDir)/mod_currentMatrices.o \
	$(ObjectDir)/mod_amplitudes.o \

HWW = \
	./HWW/mod_diagramKinematics.f90 \
	./HWW/mod_ampSum.f90 \
	./HWW/mod_triangle.f90 \
	./HWW/mod_bubble.f90 \
	./HWW/mod_tadpole.f90 \

HWWObj = \
	$(ObjectDir)/mod_diagramKinematics.o \
	$(ObjectDir)/mod_ampSum.o \
	$(ObjectDir)/mod_triangle.o \
	$(ObjectDir)/mod_bubble.o \
	$(ObjectDir)/mod_tadpole.o \

SE = \
	./SE/mod_diagramKinematicsSE.f90 \
	./SE/mod_ampSumSE.f90 \
	./SE/mod_selfEnergy.f90 \

SEObj = \
	$(ObjectDir)/mod_diagramKinematicsSE.o \
	$(ObjectDir)/mod_ampSumSE.o \
	$(ObjectDir)/mod_selfEnergy.o \

HHWW = \
	./HHWW/mod_diagKinHHWW.f90 \
	./HHWW/mod_ampSumHHWW.f90 \
	./HHWW/mod_box.f90 \

HHWWObj = \
	$(ObjectDir)/mod_diagKinHHWW.o \
	$(ObjectDir)/mod_ampSumHHWW.o \
	$(ObjectDir)/mod_box.o \

SelfEnergy = \
	./selfEnergyByShift/mod_bubbleCutSelfEnergy.f90 \

SelfEnergyObj = \
	$(ObjectDir)/mod_bubbleCutSelfEnergy.o \

NumRes = \
	./numericalResults/mod_numericalResults.f90 \

NumResObj = \
	$(ObjectDir)/mod_numericalResults.o \

NumeratorFunctions = \
	./numeratorFunctions/mod_numeratorFunctions.f90 \
	./numeratorFunctions/mod_numeratorFunctions_SE.f90 \

NumeratorFunctionsObj = \
	$(ObjectDir)/mod_numeratorFunctions.o \
	$(ObjectDir)/mod_numeratorFunctions_SE.o \

Renormalization = \
	./renormalization/mod_renSE.f90 \
	./renormalization/mod_renHWW.f90 \
	./renormalization/mod_renormConst.f90 \
	./renormalization/mod_renormalization.f90 \

RenormalizationObj = \
	$(ObjectDir)/mod_renSE.o \
	$(ObjectDir)/mod_renHWW.o \
	$(ObjectDir)/mod_renormConst.o \
	$(ObjectDir)/mod_renormalization.o \
	
Main = \
	main.f90 \
	#./exampleCode/waveFunctionRenormalization/main.f90 \
	#./exampleCode/selfEnergy/main.f90 \
	#./exampleCode/final0Coefficients/main.f90 \
	#./exampleCode/tripleCoefficients/main.f90 \
	#./exampleCode/particleCurrents/main.f90 \
	
	
MainObj = \
	$(ObjectDir)/main.o \

######################################################################

OURCODEBASIC = $(BasicDefObj) $(AuxFunctionsObj) $(KinematicsObj) $(CurrentsObj) $(NumeratorFunctionsObj) $(NumResObj) $(HWWObj) $(SelfEnergyObj) $(SEObj) $(RenormalizationObj) $(HHWWObj)


OURCODEMAIN =  $(MainObj)

OURCODEAMPS = $(AmpObj) $(AmpHObj) $(OppObj) $(RatCombObj) $(RatObj) $(AmpRObj)

OURCODELIM = $(SingLimitsObj)

OURCODESEC = $(SectorsLOObj) $(SectorsNLOVObj) $(SectorsNLORObj)

OURCODE = $(OURCODEBASIC) $(OURCODEAMPS) $(OURCODELIM) $(OURCODESEC) $(OURCODECHECK) $(OURCODEMAIN)

OTHERCODE = $(LibQCDLoop)

ALLMCFM = $(OURCODE) 

######################################################################

ewu: $(ALLMCFM) $(EnvironmentDep) 
	@echo " linking"
	@echo " executable file is " $(Exec)
	@echo " "
	$(fcomp) -o $(Exec) $(ALLMCFM)
	if [ -f *.o ]; then mv *.o $(ObjectDir)/.; fi

$(MainObj): $(Main) $(config)
	@echo " compiling main program"
	$(fcomp) -c $(Main) 
	mv *.o $(ObjectDir)/.

$(BasicDefObj): $(BasicDef) 
	@echo " compiling basic files program"
	$(fcomp) -c $(BasicDef)
	mv *.o $(ObjectDir)/.

$(AuxFunctionsObj): $(AuxFunctions) 
	@echo " compiling AuxFunctions program"
	$(fcomp) -c $(AuxFunctions)
	mv *.o $(ObjectDir)/.

$(KinematicsObj): $(Kinematics) 
	@echo " compiling kinematics program"
	$(fcomp) -c $(Kinematics)
	mv *.o $(ObjectDir)/.

$(CurrentsObj): $(Currents) 
	@echo " compiling programs related to current calculation"
	$(fcomp) -c $(Currents)
	mv *.o $(ObjectDir)/.

$(HWWObj): $(HWW) 
	@echo " compiling programs related to HWW evaluation"
	$(fcomp) -c $(HWW)
	mv *.o $(ObjectDir)/.

$(SEObj): $(SE) 
	@echo " compiling programs related to SE evaluation"
	$(fcomp) -c $(SE)
	mv *.o $(ObjectDir)/.

$(SelfEnergyObj): $(SelfEnergy) 
	@echo " compiling self energy program"
	$(fcomp) -c $(SelfEnergy)
	mv *.o $(ObjectDir)/.

$(HHWWObj): $(HHWW) 
	@echo " compiling HHWW program"
	$(fcomp) -c $(HHWW)
	mv *.o $(ObjectDir)/.

$(NumResObj): $(NumRes) 
	@echo " compiling numerical results"
	$(fcomp) -c $(NumRes)
	mv *.o $(ObjectDir)/.

$(RenormalizationObj): $(Renormalization) 
	@echo " compiling renormalizationSE object"
	$(fcomp) -c $(Renormalization)
	mv *.o $(ObjectDir)/.

$(NumeratorFunctionsObj): $(NumeratorFunctions) 
	@echo " compiling numerator functions object"
	$(fcomp) -c $(NumeratorFunctions)
	mv *.o $(ObjectDir)/.

	######################################################################

clean:
	rm -f *~
	rm -f ./BasicDef/*~
	rm -f ./AuxFunctions/*~

remove:
	rm -f $(Exec)
	rm -f $(ModuleDir)/*
	rm -f $(ObjectDir)/*
	rm -f $(coefficientFileDir)/*
	rm -f $(coefficientSEFileDir)/*
	# rm -f $(coefficient3FileDir)/*
	# rm -f $(coefficient4FileDir)/*
	# rm -f $(coefficient8FileDir)/*
