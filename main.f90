use mod_types
use mod_consts_dp
use common_def
use mod_kinematics
use mod_triangle
use mod_bubble
use mod_tadpole

implicit none

complex(dp) :: h(3,dimP), w1(3,dimP), w2(3, dimP)
integer :: ep1, ep2, c1, c2, i, D
complex(dp) :: cCoeff(10), bCoeff(10), aCoeff(5)
complex(dp) :: finalCoeff(11)

ep1 = 1
ep2 = 1
c1 = 1
c2 = 1

call particlesHWW(ep1, ep2, c1, c2, h, w1, w2)

! computation of full list of polynomial coefficients
do D = 4,6
 print *, 'dimension: ', D

 do i = 1,6
   cCoeff = getTriangleCoefficients_i(D, h, w1, w2, i, .false., c_test())
   print *, 'c coefficients for diagram', i, cCoeff(:)   
   bCoeff(:) = getBubbleCoefficients_ij(D, h, w1, w2, i, 1, .false., b_Test())
   print *, 'b coefficients for cut 1 for diagram', i, bCoeff(:)
   if (i == 1 .or. i == 3) then
     bCoeff(:) = getBubbleCoefficients_ij(D, h, w1, w2, i, 2, .false., b_Test())
     print *, 'b coefficients for cut 2 for diagram', i, bCoeff(:)
     aCoeff(:) = getTadpoleCoefficients_ij(D, h, w1, w2, i, 1, .false., a_Test())
     print *, 'a coefficients for diagram', i, aCoeff(:)
   end if
 end do
end do

! calculation of c_0, b_0, a_0 coefficients of scalar master integrals
finalCoeff(:) = finalCoefficients(h, w1, w2)
print *, 'final coefficients of scalar master integrals: ', finalCoeff

end