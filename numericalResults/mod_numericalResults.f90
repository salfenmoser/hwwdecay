module mod_numericalResults

    use mod_types
    use mod_consts_dp
    use common_def
    use mod_auxfunctions
    
    use mod_vertices
    
  implicit none

  public :: HHWWCut2, polFactor
  public :: C_mWmWmW, C_mHmWmH, C_mWmHmW
  !public :: B_kmWmW, B_kmHmW, B_pmWmW, B_pmHmH

  public ::beta_HH

  contains

function HHWWCut2(w1, w2, l)
    complex(dp), intent(in) :: w1(3,dimP), w2(3,dimP), l(dimP)
    complex(dp) :: HHWWCut2
    HHWWCut2 = V_hw2(int(w1(3,1)),int(w2(3,1)))*sc(w2(2,:), w1(2,:)) * (-ci/(sq(l+w1(1,:)) - mwsq)) * (sc(w2(2,:), w1(2,:)) - sc(l,w1(1,:))*sc(l,w2(1,:))/mwsq)
end function HHWWCut2

function polFactor(r,s)
    integer, intent(in) :: r, s
    complex(dp) :: polFactor(2)
    polFactor(1) = -delta(r,s) + (delta(3,r)*delta(3,s)*mhsq/two/mwsq)
    polFactor(2) = delta(3,r)*delta(3,s)*(mhsq**two/four/mwsq - mhsq)
end function polFactor

function C_mWmWmW(r, s, d)
    integer, intent(in) :: r, s, d
    complex(dp) :: C_mWmWmW, polF(2)
    C_mWmWmW = czero
    polF = polFactor(r,s)

    C_mWmWmW = polF(1) * -two*g_Coupling**three * mw * ( (five*d - 11._dp)*mhsq**two + (81._dp-40._dp*d)*mhsq*mwsq + six*(12._dp*d-23._dp)*mwsq**two) / ((d-two)*(mhsq - four*mwsq))
    C_mWmWmW = C_mWmWmW + polF(2) * (two*g_Coupling**three * mw * (-two*(four*d**two - 127._dp*d + 234)*mhsq*mwsq**two + two*(four*d - 9._dp)*mhsq**three + (176._dp-85._dp*d)*mhsq**two*mwsq + 24._dp*(7._dp-four*d)*mwsq**three) ) / ((d-2)*mhsq*(mhsq-four*mwsq)**two)
end function C_mWmWmW

function C_mHmWmH(r, s, d)
    integer, intent(in) :: r, s, d
    complex(dp) :: C_mHmWmH, polF(2)
    C_mHmWmH = czero
    polF = polFactor(r,s)

    C_mHmWmH = polF(1) * 12._dp*g_Coupling**three*mhsq*(-(d+one)*mhsq*mwsq + four*(d-two)*mwsq**two + mhsq**two) / ((d-two)*mw*(mhsq-four*mwsq))
    C_mHmWmH = C_mHmWmH + polF(2) * (-12._dp*g_Coupling**three*mhsq*(mhsq-two*mwsq)*(d*(mhsq-two*mwsq)-two*mwsq)) / ((d-two)*mw*(mhsq-four*mwsq)**two)
end function C_mHmWmH

function C_mWmHmW(r, s, d)
    integer, intent(in) :: r, s, d
    complex(dp) :: C_mWmHmW, polF(2)
    C_mWmHmW = czero
    polF = polFactor(r,s)

    C_mWmHmW = polF(1) * 8._dp*g_Coupling**three* (-(d+two)*mhsq*mwsq**two + four*(d-two)*mwsq**three + mhsq**three) / ((d-two)*mw*(mhsq-four*mwsq))
    C_mWmHmW = C_mWmHmW + polF(2) * (-four*g_Coupling**three*(mhsq - two*mwsq) * ((d+two)*mhsq**two + 8._dp*(d-two)*mhsq*mwsq - 16._dp*mwsq**two ) ) / ((d-two)*mw*(mhsq-four*mwsq)**two)
end function C_mWmHmW

function beta_HH()
    complex(dp) :: beta_HH
    beta_HH = 9._dp*mhsq**two/8._dp/mwsq
end function
  
end module
  