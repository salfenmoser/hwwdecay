use mod_auxfunctions
use mod_consts_dp

test_suite mod_kinematics

  real(dp) :: ECM = 250
  complex(dp) :: Mom_4d(4,4)
  complex(dp) :: P_4d(4,dimP)
  complex(dp) :: e5Pol1_4d(dimP,dimE), e5Pol2_4d(dimP,dimE), e5Pol3_4d(dimP,dimE), e5Pol4_4d(dimP,dimE)  
  complex(dp) :: P_5d(4,dimP), e5Pol1(dimP,dimE), e5Pol2(dimP,dimE), e5Pol3(dimP,dimE), e5Pol4(dimP,dimE)  

setup

call kinematicsN(ECM, 2, Mom_4d)

P_4d(1,1:4) = Mom_4d(1,:)
P_4d(2,1:4) = Mom_4d(2,:)
P_4d(3,1:4) = Mom_4d(3,:)
P_4d(4,1:4) = Mom_4d(4,:)

P_4d(1,5:6) = (/czero, czero/)
P_4d(2,5:6) = (/czero, czero/)
P_4d(3,5:6) = (/czero, czero/)
P_4d(4,5:6) = (/czero, czero/)

call spinonepol_6dMomentum(P_4d(1,:), e5Pol1_4d)
call spinonepol_6dMomentum(P_4d(2,:), e5Pol2_4d)
call spinonepol_6dMomentum(P_4d(3,:), e5Pol3_4d)
call spinonepol_6dMomentum(P_4d(4,:), e5Pol4_4d)

P_5d(1,1:4) = Mom_4d(1,:)
P_5d(2,1:4) = Mom_4d(2,:)
P_5d(3,1:4) = Mom_4d(3,:)
P_5d(4,1:4) = Mom_4d(4,:)

P_5d(1,5) = cmplx(0,0,kind=dp)
P_5d(2,5) = cmplx(10,0,kind=dp)
P_5d(3,5) = cmplx(0,10,kind=dp)
P_5d(4,5) = cmplx(10,10,kind=dp)

P_5d(1,6) = czero
P_5d(2,6) = czero
P_5d(3,6) = czero
P_5d(4,6) = czero

P_5d(1,:) = P_5d(1,:) * (mw/sqrt(sc(P_5d(1,:),P_5d(1,:))))
P_5d(2,:) = P_5d(2,:) * (mw/sqrt(sc(P_5d(2,:),P_5d(2,:))))
P_5d(3,:) = P_5d(3,:) * (mw/sqrt(sc(P_5d(3,:),P_5d(3,:))))
P_5d(4,:) = P_5d(4,:) * (mw/sqrt(sc(P_5d(4,:),P_5d(4,:))))

call spinonepol_6dMomentum(P_5d(1,:), e5Pol1)
call spinonepol_6dMomentum(P_5d(2,:), e5Pol2)
call spinonepol_6dMomentum(P_5d(3,:), e5Pol3)
call spinonepol_6dMomentum(P_5d(4,:), e5Pol4)

end setup

teardown
  ! This code runs immediately after each test
end teardown

test orthogonalToMomentum
  integer :: k
do k = 1,dimP-1
  assert_equal_within(abs(sc(P_5d(1,:), e5Pol1(k,:))), zero, limitAssert)
  assert_equal_within(abs(sc(P_5d(2,:), e5Pol2(k,:))), zero, limitAssert)
  assert_equal_within(abs(sc(P_5d(3,:), e5Pol3(k,:))), zero, limitAssert)
  assert_equal_within(abs(sc(P_5d(4,:), e5Pol4(k,:))), zero, limitAssert)
end do
end test orthogonalToMomentum

test orthonormalPolVec
  integer :: k, m
do k = 1,dimP
  assert_equal_within(abs(sc(e5Pol1(k,:), e5Pol1(k,:))), one, limitAssert)
  assert_equal_within(abs(sc(e5Pol2(k,:), e5Pol2(k,:))), one, limitAssert)
  assert_equal_within(abs(sc(e5Pol3(k,:), e5Pol3(k,:))), one, limitAssert)
  assert_equal_within(abs(sc(e5Pol4(k,:), e5Pol4(k,:))), one, limitAssert)

  do m = k+1, dimP
    assert_equal_within(abs(sc(e5Pol1(k,:), e5Pol1(m,:))), zero, limitAssert)
    assert_equal_within(abs(sc(e5Pol2(k,:), e5Pol2(m,:))), zero, limitAssert)
    assert_equal_within(abs(sc(e5Pol3(k,:), e5Pol3(m,:))), zero, limitAssert)
    assert_equal_within(abs(sc(e5Pol4(k,:), e5Pol4(m,:))), zero, limitAssert)
  end do
end do
end test orthogonalPolVec

test sumGivesMetricTensor
  integer:: mu, nu, r
  complex(dp) :: negMet1, negMet2, negMet3, negMet4, polSum
  
do mu = 1, dimP
  do nu = 1, dimP
    negMet1 = czero
    negMet2 = czero
    negMet3 = czero
    negMet4 = czero
    
    do r = 1, dimP-1
      negMet1 = negMet1 + e5Pol1(r, mu)*e5Pol1(r, nu)
      negMet2 = negMet2 + e5Pol2(r, mu)*e5Pol2(r, nu)
      negMet3 = negMet3 + e5Pol3(r, mu)*e5Pol3(r, nu)
      negMet4 = negMet4 + e5Pol4(r, mu)*e5Pol4(r, nu)
    end do
    
    if (mu == 1 .and. mu == nu) then
      polSum = negMet1 + cone - P_5d(1,mu)*P_5d(1,nu)/mwsq 
      assert_equal_within( abs(polSum) , zero, limitAssert)

      polSum = negMet2 + cone - P_5d(2,mu)*P_5d(2,nu)/mwsq 
      assert_equal_within( abs(polSum) , zero, limitAssert)

      polSum = negMet3 + cone - P_5d(3,mu)*P_5d(3,nu)/mwsq 
      assert_equal_within( abs(polSum) , zero, limitAssert)

      polSum = negMet4 + cone - P_5d(4,mu)*P_5d(4,nu)/mwsq 
      assert_equal_within( abs(polSum) , zero, limitAssert)

    else if (mu == nu) then
      polSum = negMet1 - cone - P_5d(1,mu)*P_5d(1,nu)/mwsq 
      assert_equal_within( abs(polSum) , zero, limitAssert)

      polSum = negMet2 - cone - P_5d(2,mu)*P_5d(2,nu)/mwsq 
      assert_equal_within( abs(polSum) , zero, limitAssert)

      polSum = negMet3 - cone - P_5d(3,mu)*P_5d(3,nu)/mwsq 
      assert_equal_within( abs(polSum) , zero, limitAssert)

      polSum = negMet4 - cone - P_5d(4,mu)*P_5d(4,nu)/mwsq 
      assert_equal_within( abs(polSum) , zero, limitAssert)

    else if (mu /= nu) then
      polSum = negMet1 - P_5d(1,mu)*P_5d(1,nu)/mwsq
      assert_equal_within( abs(polSum) , zero, limitAssert)

      polSum = negMet2 - P_5d(2,mu)*P_5d(2,nu)/mwsq
      assert_equal_within( abs(polSum) , zero, limitAssert)

      polSum = negMet3 - P_5d(3,mu)*P_5d(3,nu)/mwsq
      assert_equal_within( abs(polSum) , zero, limitAssert)

      polSum = negMet4 - P_5d(4,mu)*P_5d(4,nu)/mwsq
      assert_equal_within( abs(polSum) , zero, limitAssert)
    end if
  
  end do
end do

end test sumGivesMetricTensor

test sumGivesMetricTensor4d
  integer:: mu, nu, r
  complex(dp) :: negMet1, negMet2, negMet3, negMet4, polSum
  
do mu = 1, 4
  do nu = 1, 4
    negMet1 = czero
    negMet2 = czero
    negMet3 = czero
    negMet4 = czero
    
    do r = 1, 3
      negMet1 = negMet1 + e5Pol1_4d(r, mu)*e5Pol1_4d(r, nu)
      negMet2 = negMet2 + e5Pol2_4d(r, mu)*e5Pol2_4d(r, nu)
      negMet3 = negMet3 + e5Pol3_4d(r, mu)*e5Pol3_4d(r, nu)
      negMet4 = negMet4 + e5Pol4_4d(r, mu)*e5Pol4_4d(r, nu)
    end do
    
    if (mu == 1 .and. mu == nu) then
      polSum = negMet1 + cone - P_4d(1,mu)*P_4d(1,nu)/mwsq 
      assert_equal_within( abs(polSum) , zero, limitAssert)

      polSum = negMet2 + cone - P_4d(2,mu)*P_4d(2,nu)/mwsq 
      assert_equal_within( abs(polSum) , zero, limitAssert)

      polSum = negMet3 + cone - P_4d(3,mu)*P_4d(3,nu)/mwsq 
      assert_equal_within( abs(polSum) , zero, limitAssert)

      polSum = negMet4 + cone - P_4d(4,mu)*P_4d(4,nu)/mwsq 
      assert_equal_within( abs(polSum) , zero, limitAssert)

    else if (mu == nu) then
      polSum = negMet1 - cone - P_4d(1,mu)*P_4d(1,nu)/mwsq 
      assert_equal_within( abs(polSum) , zero, limitAssert)

      polSum = negMet2 - cone - P_4d(2,mu)*P_4d(2,nu)/mwsq 
      assert_equal_within( abs(polSum) , zero, limitAssert)

      polSum = negMet3 - cone - P_4d(3,mu)*P_4d(3,nu)/mwsq 
      assert_equal_within( abs(polSum) , zero, limitAssert)

      polSum = negMet4 - cone - P_4d(4,mu)*P_4d(4,nu)/mwsq 
      assert_equal_within( abs(polSum) , zero, limitAssert)
    
    else if (mu /= nu) then
      polSum = negMet1 - P_4d(1,mu)*P_4d(1,nu)/mwsq
      assert_equal_within( abs(polSum) , zero, limitAssert)

      polSum = negMet2 - P_4d(2,mu)*P_4d(2,nu)/mwsq
      assert_equal_within( abs(polSum) , zero, limitAssert)

      polSum = negMet3 - P_4d(3,mu)*P_4d(3,nu)/mwsq
      assert_equal_within( abs(polSum) , zero, limitAssert)

      polSum = negMet4 - P_4d(4,mu)*P_4d(4,nu)/mwsq
      assert_equal_within( abs(polSum) , zero, limitAssert)
    end if
  
  end do
end do

end test sumGivesMetricTensor4d

end test_suite
