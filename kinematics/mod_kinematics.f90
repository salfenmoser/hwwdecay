module mod_kinematics

use mod_types
use mod_consts_dp
use common_def

use mod_auxfunctions

  implicit none
  public :: kinematicsN, kinematics, kinematicsHWW, kinematicsHHWW, particlesHWW, particlesHHWW
  public :: spinonepol, spinonepol_6d
  public :: HShiftMomentum, WShiftMomentum

contains

! creates physical momenta for a scattering process of N vector bosons in four dimensions
subroutine kinematicsN(energy,N,pout)  ! energy is the CM energy
  real(dp), intent(in) :: energy
  integer :: N
  complex(dp) :: pout(N+2,4)
  real(dp) :: E1, vp1
  real(dp) :: p(N+2,4), vp3, E3, pdec(4), harvest(3*(N-1)), mdecsq, mdec1sq, x1, x2, x3, theta3, phi3
  real(dp) :: p3aux(4), p3(4)
  integer :: i

   pout = czero
   p = zero

   call random_number(harvest)
   call random_number(harvest)
   call random_number(harvest)
   call random_number(harvest)

!-- First check if enough energy for N-2 bosons
   if (energy .lt. abs(N*mw)) then
      print *, 'not enough energy'
      stop
   endif

! Get the momenta of two colliding particles in the lab frame
  E1 = energy/two
  vp1 = sqrt(E1**2 - mw**2)
  p(1,:) = (/E1,zero,zero,vp1/)
  p(2,:) = (/E1,zero,zero,-vp1/)

!  Now consider a particle with momentum P = (E1,zero) decaying to (N-2) partons
   pdec  = p(1,:) + p(2,:)
   mdecsq = scr(pdec,pdec)

!  generate the invariant mass of pdec - p3
   x1 = harvest(1)
   theta3 = Pi*harvest(2)
   phi3 = two*Pi*harvest(3)

   if (N.eq.2) then
   mdec1sq = mwsq
   else
   mdec1sq = ( (N-1)*mw )**2 + x1* ( ( sqrt(mdecsq) - mw)**2 - ( (N-1)*mw )**2 )
   endif

   E3 = ( mdecsq - mdec1sq + mwsq)/two/sqrt(mdecsq)
   vp3 = sqrt(E3**2 - mwsq)
   p3aux = (/E3,vp3*sin(theta3)*cos(phi3),vp3*sin(theta3)*sin(phi3),vp3*cos(theta3)/)
   p(3,:) = p3aux

!  now; continue
   pdec = p(1,:) + p(2,:) - p(3,:)
   mdecsq = scr(pdec,pdec)

   do i=2,N-1

   x1 = harvest(3*i-2)
   theta3  = Pi*harvest(3*i-1)
   phi3 = two*Pi*harvest(3*i)
  
   if (i.eq.N-1) then
   mdec1sq = mwsq
   else
   mdec1sq = ( (N-i)*mw )**2 + x1* ( ( sqrt(mdecsq) - mw)**2 - ( (N-i)*mw )**2 )
   endif

   E3 = ( mdecsq - mdec1sq + mwsq)/two/sqrt(mdecsq)
   vp3 = sqrt(E3**2 - mwsq)
   p3aux = (/E3,vp3*sin(theta3)*cos(phi3),vp3*sin(theta3)*sin(phi3),vp3*cos(theta3)/)

   call boost(sqrt(mdecsq),pdec,p3aux,p3)

   p(i+2,:) = p3
   pdec = pdec - p3
   mdecsq = scr(pdec,pdec)

   enddo

   p(N+2,:) = pdec
   pout(:,:) = cmplx(p(:,:),zero,kind=dp)
  return
  end subroutine kinematicsN

! creates physical momenta for a scattering process of 4 vector bosons in four dimensions
subroutine kinematics(energy,theta,phi,p)  ! energy is the CM energy
  real(dp), intent(in) :: energy, theta, phi
  complex(dp), intent(out) :: p(4,4)
  real(dp) :: E1, vp1
  complex(dp) :: cE1, cvp1

  E1 = energy/two
  vp1 = sqrt(E1**2 - mw**2)

  cE1 = cmplx(E1,zero,kind=dp)
  cvp1 = cmplx(vp1,zero,kind=dp)

  p(1,:) = (/cE1,czero,czero,cvp1/)
  p(2,:) = (/cE1,czero,czero,-cvp1/)
  p(3,:) = (/cE1,cvp1*sin(theta)*cos(phi),cvp1*sin(theta)*sin(phi),cvp1*cos(theta)/)
  p(4,:) = (/cE1,-cvp1*sin(theta)*cos(phi),-cvp1*sin(theta)*sin(phi),-cvp1*cos(theta)/)

  return
end subroutine

! creates kinematics for HWW scattering process
subroutine kinematicsHWW(p)  ! returns momenta of H, W1, W2 for decay process
  complex(dp), intent(out) :: p(3,dimP) !momenta of H, W1, W2 in this order
  complex(dp) :: pH(dimP), p1(dimP), p2(dimP)
  integer :: l

pH(:) = czero
p1(:) = czero
p2(:) = czero

pH(1) = cmplx(mh,zero,kind=dp)

p1(1) = - pH(1)/two
p1(4) = sqrt(p1(1)**two - mwsq)
p2(1) = - pH(1)/two
p2(4) = -sqrt(p2(1)**two- mwsq)

p(1,:) = pH
p(2,:) = p1
p(3,:) = p2
  
  return
end subroutine

! creates particle matrices for HWW scattering process
subroutine particlesHWW(ep1, ep2, c1, c2, h, w1, w2)
  integer, intent(in) :: ep1, ep2, c1, c2
  complex(dp), intent(out) :: h(3,dimP), w1(3, dimP), w2(3, dimP)
  complex(dp) :: p(3, dimP), ep(dimP, dimE)
  integer :: k, l


h(:,:) = czero
w1(:,:) = czero
w2(:,:) = czero

call kinematicsHWW(p)
h(1,:) = p(1,:)
w1(1,:) = p(2,:)
w2(1,:) = p(3,:)

call spinonepol_6d(w1(1,:), ep)
w1(2,:) = ep(ep1, :)
call spinonepol_6d(w2(1,:), ep)
w2(2,:) = ep(ep2, :)

w1(3,1) = c1
w2(3,1) = c2

return
end subroutine particlesHWW

! creates kinematics for HHWW scattering process
subroutine kinematicsHHWW(p)  ! returns momenta of H, W1, W2 for decay process
  complex(dp), intent(out) :: p(4,dimP) !momenta of H, W1, W2 in this order
  complex(dp) :: pH1(dimP), pH2(dimP), pW1(dimP), pW2(dimP)
  integer :: l

pH1(:) = czero
pH2(:) = czero
pW1(:) = czero
pW2(:) = czero

pH1(1) = cmplx(sqrt(two)*mh,zero,kind=dp)
pH1(4) = cmplx(mh,zero,kind=dp)
pH2(1) = cmplx(sqrt(two)*mh,zero,kind=dp)
pH2(4) = cmplx(-mh,zero,kind=dp)

pW1(1) = - pH1(1)
pW1(3) = sqrt(pW1(1)**two - mwsq)
pW2(1) = - pH2(1)
pW2(3) = -sqrt(pW2(1)**two- mwsq)

p(1,:) = pH1
p(2,:) = pH2
p(3,:) = pW1
p(4,:) = pW2

  return
end subroutine

! creates particle matrices for HWW scattering process
subroutine particlesHHWW(ep1, ep2, c1, c2, h1, h2, w1, w2)
  integer, intent(in) :: ep1, ep2, c1, c2
  complex(dp), intent(out) :: h1(3,dimP), h2(3,dimP), w1(3, dimP), w2(3, dimP)
  complex(dp) :: p(4, dimP), ep(dimP, dimE)
  integer :: k, l


h1(:,:) = czero
h2(:,:) = czero
w1(:,:) = czero
w2(:,:) = czero

call kinematicsHHWW(p)
h1(1,:) = p(1,:)
h2(1,:) = p(2,:)
w1(1,:) = p(3,:)
w2(1,:) = p(4,:)

call spinonepol_6d(w1(1,:), ep)
w1(2,:) = ep(ep1, :)
call spinonepol_6d(w2(1,:), ep)
w2(2,:) = ep(ep2, :)

w1(3,1) = c1
w2(3,1) = c2

return
end subroutine particlesHHWW

! creates the shift momentum for off-shell considerations of higgs boson
subroutine HShiftMomentum(h, w1, kappa)
  complex(dp), intent(in) :: h(dimP), w1(dimP)
  complex(dp), intent(out) :: kappa(dimP)

kappa(:) = czero
  ! currently third entry of W momentum is set to zero.
if (abs(w1(3)) < limitMomentum) then
  kappa(1) = cone
  kappa(2) = czero
  kappa(4) = (w1(1) - w1(2))/w1(4)
  kappa(3) = sqrt(kappa(1)**2 - kappa(2)**2 - kappa(4)**2)
end if
end subroutine HShiftMomentum

! creates the shift momentum for off-shell considerations of w boson
subroutine WShiftMomentum(kappa)
  complex(dp), intent(out) :: kappa(dimP)
kappa(:) = czero
kappa(3) = cone
kappa(4) = ci
end subroutine WShiftMomentum

! creates three physical polarization vectors and fourth unphysical polarization vector for spin one particle in four dimensions
subroutine spinonepol(p,ep)
  complex(dp), intent(in) :: p(4)
  complex(dp), intent(out) :: ep(3,4)
  complex(dp) :: ap, anorm, ap2, ep1(4), ep2(4)

  ap = sqrt(p(2)**2+p(3)**2+p(4)**2)
  ap2 = sqrt(p(2)**2+p(3)**2)
  anorm = p(1)/ap

  if ( abs(p(2)).eq.zero .and. abs(p(3)).eq.zero ) then
  ep(1,:) = one/sqrt2*(/czero,cone,ci,czero/)
  ep(2,:) = one/sqrt2*(/czero,cone,-ci,czero/)
  ep(3,:) = one/mw*(/ ap, czero,czero,p(4)*anorm/)

  else
  ep(1,:) = one/sqrt2*(/czero,  p(4)*p(2)/ap/ap2 - ci*p(3)/ap2, p(4)*p(3)/ap/ap2 + ci*p(2)/ap2,-ap2/ap/)
  ep(2,:) = one/sqrt2*(/czero,  p(4)*p(2)/ap/ap2 + ci*p(3)/ap2, p(4)*p(3)/ap/ap2 - ci*p(2)/ap2,-ap2/ap/)
  ep(3,:) = one/mw*(/ ap, p(2)*anorm, p(3)*anorm, p(4)*anorm /)
  endif
  
ep1 = ci*(ep(1,:) - ep(2,:))/sqrt(sc(ep(1,:) - ep(2,:),ep(1,:) - ep(2,:)))
ep2 = ci*(ep(1,:) + ep(2,:))/sqrt(sc(ep(1,:) + ep(2,:),ep(1,:) + ep(2,:)))

ep(1,:) = ep1
ep(2,:) = ep2

  return
end subroutine

!creates physical (1-5) and unphysical (6) polarization states for spin one particle in six dimensions
subroutine spinonepol_6d(p,ep)
  complex(dp), intent(in) :: p(6)
  complex(dp), intent(out) :: ep(6,dimE)
  integer :: k, l
  complex(dp) :: ap, anorm, ap2, apVec, eps1(dimE), eps2(dimE), mass
  complex(dp) ::  a, b, c, d, e

ep(:,:) = czero
if (abs(p(6)) > limitMomentum) then
print *, 'space time dimension of momentum not equal 5'
return
end if

ap = sqrt(p(2)**2+p(3)**2+p(4)**2+p(5)**2)
ap2 = sqrt(p(2)**2+p(3)**2)
apVec = sqrt(p(2)**2 + p(3)**2 + p(4)**2)
anorm = p(1)/ap
mass = mw

! warning should be commented out if off-shell configurations are considered
if (abs(sc(p(:),p(:)) - mwsq) > limitMomentum) then
  !print *, 'WARNING: TRYING TO CALCULATE OFF-SHELL POLARIZATION VECTOR'
  mass = sqrt(sq(p(:)))
end if
  
if ( abs(p(2)) == zero .and. abs(p(3)) == zero ) then
  ep(1,2:3) = one/sqrt2*(/cone,ci/)
  ep(2,2:3) = one/sqrt2*(/cone,-ci/)
  ep(3,1:5) = one/mass*(/ ap, czero,czero,p(4)*anorm, p(5)*anorm/)
  ep(4,4:5) = (/p(5)/ap,-p(4)/ap/)
else
  ep(1,2:4) = one/sqrt2*(/p(4)*p(2)/ap/ap2 - ci*p(3)/ap2, p(4)*p(3)/ap/ap2 + ci*p(2)/ap2, -ap2/ap/)
  ep(2,2:4) = one/sqrt2*(/p(4)*p(2)/ap/ap2 + ci*p(3)/ap2, p(4)*p(3)/ap/ap2 - ci*p(2)/ap2, -ap2/ap/)
  ep(3,1:5) = one/mass*(/ap, p(2)*anorm, p(3)*anorm, p(4)*anorm, p(5)*anorm/)
  if (p(2) /= czero) then
    e = apVec / ap
    b = - p(5) * p(2) / apVec / ap
    a = czero
    c = b * p(3)/p(2)
    d = b * p(4)/p(2)
  else if (p(2) == czero ) then
    e = apVec / ap
    a = czero
    b = czero
    c = - p(5) * p(3) / apVec / ap
    d = c * p(4)/p(3)
  end if
  ep(4,1:5) = (/a, b, c, d, e /)
end if

ep(5,6) = cone
ep(6,:) = p(:)/mass

eps1 = ci*(ep(1,:) - ep(2,:))/sqrt(sc(ep(1,:) - ep(2,:),ep(1,:) - ep(2,:)))
eps2 = ci*(ep(1,:) + ep(2,:))/sqrt(sc(ep(1,:) + ep(2,:),ep(1,:) + ep(2,:)))
ep(1,:) = eps1
ep(2,:) = eps2

return
end subroutine spinonepol_6d

end module
