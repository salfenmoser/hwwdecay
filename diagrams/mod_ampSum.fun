use mod_auxfunctions
use mod_consts_dp
use mod_kinematics
use mod_vertices


test_suite mod_ampSum

  complex(dp) :: P3(3,dimP)

  complex(dp) :: pa1(3,dimP), pa2(3,dimP), paH(3,dimP)
  integer :: pol1, pol2, c1, c2, D_S
  

setup

pol1 = 1
pol2 = 1

c1 = 2
c2 = 2

call particlesHWW(pol1, pol2, c1, c2, paH, pa1, pa2)

end setup

teardown
  ! This code runs immediately after each test
end teardown

test givesZeroForImproperChoicesIfDemanded
  complex(dp) :: l(dimP), tripleCutCurrents, doubleCutCurrents, singleCutCurrents
  integer :: i, j

l = four * paH(1,:)

do D_S = 4,6
do i = 1,6
tripleCutCurrents = getTripleCurrents(D_S, l, paH, pa1, pa2, i, .true.)
assert_equal_within(abs(tripleCutCurrents), zero, limitAssert)
do j = 1,3
  doubleCutCurrents = czero
  doubleCutCurrents = getDoubleCurrents(D_S, l, paH, pa1, pa2, i, j, .true.)
  assert_equal_within(abs(doubleCutCurrents), zero, limitAssert)
end do
end do
end do

do D_S = 4,6
do i = 1,6
do j = 1,3
  singleCutCurrents = getSingleCurrents(D_S, l, paH, pa1, pa2, i, j, .true.)
  assert_equal_within(abs(singleCutCurrents), zero, limitAssert) 
end do
end do
end do

end test givesZeroForImproperChoicesIfDemanded


test givesNotZeroForImproperChoicesIfNotDemanded
  complex(dp) :: l(dimP), tripleCutCurrents, doubleCutCurrents, singleCutCurrents
  integer :: i, j

l = four * paH(1,:)

do D_S = 4,6
do i = 1,6
tripleCutCurrents = getTripleCurrents(D_S, l, paH, pa1, pa2, i, .false.)
assert_false(abs(tripleCutCurrents) < 1e-10)
do j = 1,3
  doubleCutCurrents = getDoubleCurrents(D_S, l, paH, pa1, pa2, i, j, .false.)
  assert_false(abs(doubleCutCurrents) < 1e-10)
end do
end do
end do

do D_S = 4,6
do i = 1,6
do j = 1,3
  singleCutCurrents = getSingleCurrents(D_S, l, paH, pa1, pa2, i, j, .false.)
  assert_false(abs(singleCutCurrents) < 1e-10)
end do
end do
end do
end test givesNotZeroForImproperChoicesIfNotDemanded

test tripleCutMomentumIndependet
complex(dp) :: l(dimP), tripleCutCurrents
integer :: i, j

do D_S = 4,6
l(:) = czero
do i = 3,4
tripleCutCurrents = getTripleCurrents(D_S, l, paH, pa1, pa2, i, .false.)
assert_equal_within(abs(tripleCutCurrents), abs(V_hw2(1,1)**two * V_h3()), limitAssert)
end do

l = pa1(1,:)
do i = 5, 6
tripleCutCurrents = getTripleCurrents(D_S, l, paH, pa1, pa2, i, .false.)
assert_equal_within(abs(tripleCutCurrents), abs(V_hw2(1,1)**three), limitAssert)
end do
end do
end test tripleCutMomentumIndependet



end test_suite
