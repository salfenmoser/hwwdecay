module mod_diagramsSingle

  use mod_types
  use mod_consts_dp
  use common_def
  
  use mod_auxfunctions     
  use mod_kinematics
  use mod_amplitudes

  use mod_diagramKinematics
  use mod_ampSum
  use mod_diagramsTriple
  use mod_diagramsDouble
  
implicit none

public :: A_ij, getSingleCutCoefficients_ij, getSingleCutCoefficient0_ij, finalCoefficients

integer :: unitOffsetA = 50

contains


! single cut function depending on dimension D_S, momentum l, particles higgs, w1,w2, diagram i, cut jSingle
! need to subtract triangle and bubble coefficients adequately
function A_ij(D_S, l, pa1, pa2, pa3, i, jSingle)
  integer, intent(in) :: i, jSingle, D_S
  complex(dp), intent(in) :: pa1(3,dimP), pa2(3,dimP), pa3(3,dimP), l(dimP)
  complex(dp) :: d_l(6,3), d_lM1(6,3), d_lPp(6,3), d_lM2(6,3)
  complex(dp) :: A_ij, singleCurrents
  complex(dp) :: doubleB(6,3), doubleB_Pp(6,3), doubleB_M1(6,3), doubleB_M2(6,3), tripleC(6), tripleC_M1(6), tripleC_M2(6), tripleC_Pp(6)
  integer :: j, k

d_l = getDenominators(l, pa1, pa2, pa3)
d_lM1 = getDenominators(l-pa2(1,:), pa1, pa2, pa3)
d_lM2 = getDenominators(l-pa3(1,:), pa1, pa2, pa3)
d_lPp = getDenominators(l+pa1(1,:), pa1, pa2, pa3)

singleCurrents = getSingleCurrents(D_S, l, pa1, pa2, pa3, i, jSingle, demandOnShellExternal)

do k = 1,6
tripleC(k) = getTripleCutFunction_i(D_S, l, pa1, pa2, pa3, k)
tripleC_M1(k) = getTripleCutFunction_i(D_S, l-pa2(1,:), pa1, pa2, pa3, k)
tripleC_M2(k) = getTripleCutFunction_i(D_S, l-pa3(1,:), pa1, pa2, pa3, k)
tripleC_Pp(k) = getTripleCutFunction_i(D_S, l+pa1(1,:), pa1, pa2, pa3, k)
end do

do j = 1,2
do k = 1,6
doubleB(k,j) = getDoubleCutFunction_ij(D_S, l, pa1, pa2, pa3, k, j)
doubleB_Pp(k,j) = getDoubleCutFunction_ij(D_S, l+pa1(1,:), pa1, pa2, pa3, k, j)
doubleB_M1(k,j) = getDoubleCutFunction_ij(D_S, l-pa2(1,:), pa1, pa2, pa3, k, j)
doubleB_M2(k,j) = getDoubleCutFunction_ij(D_S, l-pa3(1,:), pa1, pa2, pa3, k, j)

end do
end do

A_ij = czero

if (jSingle == 1 .and. i == 1) then
A_ij = singleCurrents
!print *, 'before subtraction: A_ij = ', A_ij
!substract first all c coefficients
A_ij = A_ij + tripleC(1)/d_l(1,2)/d_l(1,3) + tripleC_M1(1)/d_lM1(1,1)/d_lM1(1,3) + tripleC_Pp(1)/d_lPp(1,1)/d_lPp(1,2)
A_ij = A_ij + tripleC(2)/d_l(2,2)/d_l(2,3) + tripleC_M2(2)/d_lM2(2,1)/d_lM2(2,3) + tripleC_Pp(2)/d_lPp(2,1)/d_lPp(2,2)

A_ij = A_ij + tripleC_M1(3)/d_lM1(3,1)/d_lM1(3,3)
A_ij = A_ij + tripleC_M2(4)/d_lM2(4,1)/d_lM2(4,3)

A_ij = A_ij + tripleC(5)/d_l(5,2)/d_l(5,3) + tripleC_Pp(5)/d_lPp(5,1)/d_lPp(5,2)
A_ij = A_ij + tripleC(6)/d_l(6,2)/d_l(6,3) + tripleC_Pp(6)/d_lPp(6,1)/d_lPp(6,2)
!print *, 'after C subtraction: A_ij = ', A_ij

!substract pink, light blue, dark blue, brown, skin, yellow, green b coefficients
A_ij = A_ij  + (- ci * doubleB(1,1) / d_l(1,2) - ci * doubleB_M1(1,1) / d_lM1(1,1) )
A_ij = A_ij  + (- ci * doubleB(2,1) / d_l(2,2) - ci * doubleB_M2(2,1) / d_lM2(2,1) ) 

A_ij = A_ij + (- ci * doubleB_M1(3,1) / d_lM1(3,1) )
A_ij = A_ij + (- ci * doubleB_M2(4,1) / d_lM2(4,1) )

A_ij = A_ij + - ci * doubleB(5,1) / d_l(5,2)
A_ij = A_ij + - ci * doubleB(6,1) / d_l(6,2)

A_ij = A_ij + (- ci * doubleB(1,2) / d_l(1,3) - ci * doubleB_Pp(1,2) / d_lPp(1,1) )
!print *, 'after B subtraction: A_ij = ', A_ij

else if (jSingle == 1 .and. i == 3) then
A_ij = singleCurrents
!print *, 'A_ij without subtraction:', A_ij

!substract first all c coefficients
A_ij = A_ij + ( tripleC(3)/d_l(3,2)/d_l(3,3) + tripleC_Pp(3)/d_lPp(3,1)/d_lPp(3,2) )
A_ij = A_ij + ( tripleC(4)/d_l(4,2)/d_l(4,3) + tripleC_Pp(4)/d_lPp(4,1)/d_lPp(4,2) )
A_ij = A_ij + tripleC_M1(5)/d_lM1(5,1)/d_lM1(5,3)
A_ij = A_ij + tripleC_M2(6)/d_lM2(6,1)/d_lM2(6,3)
! print *, 'A_ij after C subtraction:', A_ij

!substract dark blue, brown, skin, yellow, red b coefficients
A_ij = A_ij - ci * doubleB(3,1)/d_l(3,2)
A_ij = A_ij - ci * doubleB(4,1)/d_l(4,2)
A_ij = A_ij - ci * doubleB_M1(5,1)/d_lM1(5,1)
A_ij = A_ij - ci * doubleB_M2(6,1)/d_lM2(6,1)

A_ij = A_ij - ci * doubleB(3,2)/d_l(3,3) - ci * doubleB_Pp(3,2)/d_lPp(3,1)

!print *, 'A_ij final:', A_ij

end if

end function A_ij


! five single cut coefficients for dimension D_S, diagram i, cut jSingle
! reduction is performed with seven properly chosen momenta l
function getSingleCutCoefficients_ij(D_S, pa1, pa2, pa3, i, jSingle)
  integer, intent(in) :: i, jSingle, D_S
  complex(dp), intent(in) ::  pa1(3,dimP), pa2(3,dimP), pa3(3,dimP)
  complex(dp) :: getSingleCutCoefficients_ij(5), l(dimP), v(dimP, dimP), lAux(10, dimP), A(10), l_sq, diag(6,21)
  complex(dp) :: den(6,3)
  integer :: k, unitInt
  character*5 :: strDim, strDiag, strCut
  character*70 :: documentName
  logical :: fileExists

getSingleCutCoefficients_ij(:) = (/czero,czero,czero,czero,czero/)

if ( jSingle == 3 .or. jSingle == 2) then
else if ( i /= 1 .and. i /= 3 ) then
else  
  
do k = 1, dimP
v(k,k) = ci
end do 
v(1,1) = cone

getSingleCutCoefficients_ij = (/czero,czero,czero,czero,czero/)
if (i == 1) then
  l_sq = mwsq
else if (i == 3) then
  l_sq = mhsq
end if

lAux(1,:) = sqrt(l_sq/two) * (v(1,:) + v(2,:))
lAux(2,:) = sqrt(l_sq/two) * (v(1,:) - v(2,:))

lAux(3,:) = sqrt(l_sq/two) * (v(1,:) + v(3,:))
lAux(4,:) = sqrt(l_sq/two) * (v(1,:) - v(3,:))

lAux(5,:) = sqrt(l_sq/two) * (v(1,:) + v(4,:))
lAux(6,:) = sqrt(l_sq/two) * (v(1,:) - v(4,:))

lAux(7,:) = sqrt(l_sq/three) * (v(1,:) + v(2,:) + v(3,:))

do k = 1,7
A(k) = A_ij(D_S, lAux(k,:), pa1, pa2, pa3, i, jSingle)
!print *, 'A(',k,') = ', A(k)
end do

! 5 corresponds to 0
getSingleCutCoefficients_ij(2) = (A(1) - A(2)) / sqrt(two * l_sq)
getSingleCutCoefficients_ij(3) = (A(3) - A(4)) / sqrt(two * l_sq)
getSingleCutCoefficients_ij(4) = (A(5) - A(6)) / sqrt(two * l_sq)

getSingleCutCoefficients_ij(5) = ( (A(1) - A(7)*sqrt(three/two)) + getSingleCutCoefficients_ij(3)*sqrt(l_sq/two) ) / (one - sqrt(three/two))
getSingleCutCoefficients_ij(1) = (A(1) - getSingleCutCoefficients_ij(5))*sqrt(two/l_sq) - getSingleCutCoefficients_ij(2)

! write coefficients in file
write (strDim, '(I1)') D_S
write (strDiag, '(I1)') i
write (strCut, '(I1)') jSingle
unitInt = unitOffsetA+D_S-4

documentName = trim('diagrams/coefficients/singleCutCoefficient_'//trim(strDiag)//'_'//trim(strCut)//'_'//trim(strDim)//'.dat')

INQUIRE(FILE=documentName, EXIST=fileExists)
if (.NOT. fileExists ) then
  open(unit = unitInt, file = documentName, status='new')
  write(unitInt, FMT, advance='no') getSingleCutCoefficients_ij(:)
end if

end if
end function getSingleCutCoefficients_ij

function getSingleCutCoefficient0_ij(D_S, pa1, pa2, pa3, i, jSingle)
  integer, intent(in) :: i, jSingle, D_S
  complex(dp), intent(in) ::  pa1(3,dimP), pa2(3,dimP), pa3(3,dimP)
  complex(dp) :: singleCutCoefficients(5)
  complex(dp) :: getSingleCutCoefficient0_ij
singleCutCoefficients(:) = czero
singleCutCoefficients(:) = getSingleCutCoefficients_ij(D_S, pa1, pa2, pa3, i, jSingle)
getSingleCutCoefficient0_ij = singleCutCoefficients(5)
end function getSingleCutCoefficient0_ij

! returns final master integral coefficients in 4 dimensional limit
function finalCoefficients(pa1, pa2, pa3)
  complex(dp), intent(in) :: pa1(3,dimP), pa2(3,dimP), pa3(3,dimP)
  complex(dp) :: finalCoefficients(9)
  complex(dp) :: tripleCutCoefficients(10), doubleCutCoefficients(10), singleCutCoefficients(5)
  character*70 :: documentName
  integer :: D_S, i
  logical :: fileExists

finalCoefficients(:) = czero
tripleCutCoefficients(:) = czero
doubleCutCoefficients(:) = czero
singleCutCoefficients(:) = czero
documentName = trim('diagrams/coefficients/finalCoefficients.dat')

! check if file exists, if so, take from file
INQUIRE(FILE=documentName, EXIST=fileExists)
if (fileExists .and. (fromScratch .eqv. .false.)) then
  open(unit = 1, file = trim(documentName), status='old')
  do i = 1,9
    read(1, *) finalCoefficients(i)
  end do
  close(unit = 1)

! if not: calculate
else

print *, 'get triple cut coefficients'
do i = 1,3
do D_S = 5,6
  tripleCutCoefficients(:) = czero
  tripleCutCoefficients(:) = getTripleCutCoefficients_i(D_S, pa1, pa2, pa3, (2*i-1), .false., cTest() )
  if (D_S == 5) then
    finalCoefficients(i) = finalCoefficients(i) + two*tripleCutCoefficients(10)
  else if (D_S == 6) then
    finalCoefficients(i) = finalCoefficients(i) - tripleCutCoefficients(10)
  end if  
end do
end do

print *, 'get double and single cut coefficients, diag 1, cut 1'
do D_S = 5,6
doubleCutCoefficients(:) = czero
singleCutCoefficients(:) = czero

doubleCutCoefficients(:) = getDoubleCutCoefficients_ij(D_S, pa1, pa2, pa3, 1, 1, .false., b_Test())
!singleCutCoefficients(:) = getSingleCutCoefficients_ij(D_S, pa1, pa2, pa3, 1, 1)

if (D_S == 5) then
  finalCoefficients(4) = finalCoefficients(4) + two*doubleCutCoefficients(10)
  finalCoefficients(8) = finalCoefficients(8) + two*singleCutCoefficients(5)
else if (D_S == 6) then
  finalCoefficients(4) = finalCoefficients(4) - doubleCutCoefficients(10)
  finalCoefficients(8) = finalCoefficients(8) - singleCutCoefficients(5)
end if  
end do

print *, 'get double and single cut coefficients, diag 3, cut 1'
do D_S = 5,6
doubleCutCoefficients(:) = czero
singleCutCoefficients(:) = czero

doubleCutCoefficients(:) = getDoubleCutCoefficients_ij(D_S, pa1, pa2, pa3, 3, 1, .false., b_Test())
singleCutCoefficients(:) = getSingleCutCoefficients_ij(D_S, pa1, pa2, pa3, 3, 1)
  
if (D_S == 5) then
  finalCoefficients(5) = finalCoefficients(5) + two*doubleCutCoefficients(10)
  finalCoefficients(9) = finalCoefficients(9) + two*singleCutCoefficients(5)
else if (D_S == 6) then
  finalCoefficients(5) = finalCoefficients(5) - doubleCutCoefficients(10)
  finalCoefficients(9) = finalCoefficients(9) - singleCutCoefficients(5)
end if  
end do

print *, 'get double cut coefficients, diag 1, cut 2'
do D_S = 5,6
doubleCutCoefficients(:) = czero
doubleCutCoefficients(:) = getDoubleCutCoefficients_ij(D_S, pa1, pa2, pa3, 1, 2, .false., b_Test())
    
if (D_S == 5) then
  finalCoefficients(6) = finalCoefficients(6) + two*doubleCutCoefficients(10)
else if (D_S == 6) then
  finalCoefficients(6) = finalCoefficients(6) - doubleCutCoefficients(10)
end if  
end do

print *, 'get double cut coefficients, diag 3, cut 2'
do D_S = 5,6
doubleCutCoefficients(:) = czero
doubleCutCoefficients(:) = getDoubleCutCoefficients_ij(D_S, pa1, pa2, pa3, 3, 2, .false., b_Test())
      
if (D_S == 5) then
  finalCoefficients(7) = finalCoefficients(7) + two*doubleCutCoefficients(10)
else if (D_S == 6) then
  finalCoefficients(7) = finalCoefficients(7) - doubleCutCoefficients(10)
end if  
end do

! write into file
open(unit = 1, file = trim(documentName), status='replace')
write(1, FMT, advance='no') finalCoefficients(1)
write(1, FMT, advance='no') finalCoefficients(2)
write(1, FMT, advance='no') finalCoefficients(3)
write(1, FMT, advance='no') finalCoefficients(4)
write(1, FMT, advance='no') finalCoefficients(5)
write(1, FMT, advance='no') finalCoefficients(6)
write(1, FMT, advance='no') finalCoefficients(7)
write(1, FMT, advance='no') finalCoefficients(8)
write(1, FMT, advance='no') finalCoefficients(9)
close(1)

end if

end function finalCoefficients


end module