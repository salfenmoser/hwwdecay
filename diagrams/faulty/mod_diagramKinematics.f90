module mod_diagramKinematics

    use mod_types
    use mod_consts_dp
    use common_def
    
    use mod_auxfunctions     
    use mod_kinematics
    use mod_vertices
    
    implicit none
    
  public :: diagramsHWW, getDenominators, getTripleCutKinematics, getDoubleCutKinematics

  
  contains

! contains 6 rows, each row represents one diagram;
! each row contains propagator momentum and mass for the three propagators
! row 1,2: WWW loop, row 3,4: HWH loop, row 5,6: WHW loop
! entry 1-6:momentum of first propagator, entry 7: mass squared of first propagator, etc.
function diagramsHWW(l,p,k1,k2)
  integer :: i
  complex(dp), intent(in) :: l(dimP), p(dimP), k1(dimP), k2(dimP)
  complex(dp) ::  diagramsHWW(6,21)

  do i = 1,5, 2
    diagramsHWW(i,1:6) = l
    diagramsHWW(i,8:13) = l + k1
    diagramsHWW(i,15:20) = l - p
    diagramsHWW(i,7) = mwsq
    diagramsHWW(i,14) = mwsq
    diagramsHWW(i,21) = mwsq
  end do

diagramsHWW(3,7) = mhsq
diagramsHWW(3,21) = mhsq
diagramsHWW(5,14) = mhsq
  
  do i = 2,6, 2
    diagramsHWW(i,1:6) = l
    diagramsHWW(i,8:13) = l + k2
    diagramsHWW(i,15:20) = l - p
    diagramsHWW(i,7) = mwsq
    diagramsHWW(i,14) = mwsq
    diagramsHWW(i,21) = mwsq
  end do

diagramsHWW(4,7) = mhsq
diagramsHWW(4,21) = mhsq
diagramsHWW(6,14) = mhsq

end function diagramsHWW

! returns the three denominators of the six different diagrams
function getDenominators(l, part1, part2, part3)
  integer :: i, k
  complex(dp), intent(in) :: l(dimP), part1(3,dimP), part2(3,dimP), part3(3,dimP)
  complex(dp) :: getDenominators(6,3)
  complex(dp) :: diag(6,21)

diag = diagramsHWW(l, part1(1,:), part2(1,:), part3(1,:))
do i = 1,6
getDenominators(i,1) = sc(diag(i,1:6), diag(i,1:6)) - diag(i,7)
getDenominators(i,2) = sc(diag(i,8:13), diag(i,8:13)) - diag(i,14)
getDenominators(i,3) = sc(diag(i,15:20), diag(i,15:20)) - diag(i,21)
end do
end function getDenominators
  
! construction of Van Neerven-Vermaseren basis from particle 1,2,3, for diagram i for triple cut
! v contains 6d basis, v(1), v(2) parallel to k1,k2; v(3), v(4) in transversal space
! V3 and lt_sq required for construction of momenta
subroutine getTripleCutKinematics(part1, part2, part3, i, v, V3, lt_sq)
  integer, intent(in) :: i
  complex(dp), intent(in) ::  part1(3,dimP), part2(3,dimP), part3(3,dimP)
  complex(dp), intent(out) :: lt_sq, v(dimP,dimP), V3(dimP)
  complex(dp) :: diag(6,21), l(dimP), vAux(4,4)

l(:) = czero
diag = diagramsHWW(l, part1(1,:),part2(1,:),part3(1,:))
    
! construct physical and transverse space to get l_transverse, symmetric in part2,part3
! since give2to4vect is symmetric in part2, part3, no need to differentiate between diagrams
! could also use give2to4vect(1,2) and shift all indices in diag(i,:) by -4 or give2to4vect(3,1) and shift by +4
if (MOD(i,2) == 1) then
  call give2to4vect(part2(1,1:4),part3(1,1:4),vAux)
  call extendBasisD(vAux, v)
   
else if (MOD(i,2) == 0) then
  call give2to4vect(part3(1,1:4),part2(1,1:4),vAux)
  call extendBasisD(vAux, v)
end if

V3(:) = - 0.5_dp * ( sq(diag(i,8:13))-sq(diag(i,1:6)) - diag(i,14) + diag(i,7) ) * v(1,:)
V3(:) = V3(:) - 0.5_dp * ( sq(diag(i,15:20)) - sq(diag(i,8:13)) - diag(i,21) + diag(i,14) ) * v(2,:)
lt_sq = diag(i,7) - sc(V3(:),V3(:))
return
end subroutine getTripleCutKinematics


! construction of Van Neerven-Vermaseren basis from particle 1,2,3, for diagram i, cut j for double cut
! v contains 6d basis, v(1) parallel to k1 (diag 1,3,5) or k2 (diag 2,4,6); v(2), v(3), v(4) in transversal space
! x1 and lt_sq required for construction of momenta
subroutine getDoubleCutKinematics(part1, part2, part3, i, j, q, x1, lt_sq, v)
  integer, intent(in) :: i, j ! correspond to diag, cut
  complex(dp), intent(in) :: part1(3,dimP), part2(3,dimP), part3(3,dimP)
  complex(dp), intent(out) :: q(dimP), x1, lt_sq, v(dimP,dimP)
  complex(dp) :: l(dimP), diag(6,21), vAux(4,4)

l(:) = czero

if (j == 1) then
  diag = diagramsHWW(l, part1(1,:),part2(1,:),part3(1,:))
  q(:) = diag(i, 8:13) - diag(i, 1:6)
  x1 = ( diag(i,14) - diag(i,7) - sq(q) ) / (two * sq(q) )
  lt_sq = diag(i, 7) -  x1**two * sq(q)
      
else if (j == 2) then
  diag = diagramsHWW(l + part1(1,:), part1(1,:),part2(1,:),part3(1,:))
  q(:) = diag(i, 1:6) - diag(i, 15:20)
  x1 = ( diag(i,7) - diag(i,21) - sq(q) ) / (two * sq(q) )
  lt_sq = diag(i, 21) -  x1**two * sq(q)
  
! else if (j == 3) then
!   q(:) = diag(i, 15:20) - diag(i, 8:13)
!   x1 = ( sc(diag(i,8:13), diag(i,8:13)) - diag(i,14) - sc(diag(i,15:20), diag(i,15:20)) + diag(i,21) ) / (two * sc(q, q) )    
!   lt_sq = diag(i,14) - diag(i,21) -  sc(diag(i,8:13), diag(i,8:13)) + sc(diag(i,15:20), diag(i,15:20)) - three * x1**2 * sc(q,q)
end if

call give1to4vect(q(1:4), vAux)
call extendBasisD(vAux, v)
    
return
end subroutine getDoubleCutKinematics  
  
end module
  