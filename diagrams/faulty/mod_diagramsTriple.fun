use mod_auxfunctions
use mod_consts_dp
use mod_kinematics


test_suite mod_diagramsTriple
  complex(dp) :: pa1(3,dimP), pa2(3,dimP), paH(3,dimP)
  integer :: pol1, pol2, c1, c2
  
setup

pol1 = 1
pol2 = 1
c1 = 2
c2 = 2
call particlesHWW(pol1, pol2, c1, c2, paH, pa1, pa2)
end setup

teardown

end teardown


test CReduction
integer :: D_S, i, k
complex(dp) :: c(10), tripleCutCoeff(10)

c(:) = czero
tripleCutCoeff(:) = czero

c(1) = one
c(2) = two
c(3) = -one/two
c(4) = czero
c(5) = three/four
c(6) = ci
c(7) = -ci
c(8) = four*ci
c(9) = -five
c(10) = mh

do D_S = 4,6
do i = 1,6
  tripleCutCoeff(:) = czero
tripleCutCoeff = getTripleCutCoefficients_i(D_S, paH, pa1, pa2, i, .true., c)

do k = 1,6
assert_equal_within(REAL(tripleCutCoeff(k)), REAL(c(k)), limitAssert)
assert_equal_within(aimag(tripleCutCoeff(k)), AIMAG(c(k)), limitAssert)
end do

if (D_S > 4) then
do k = 8,9
assert_equal_within(REAL(tripleCutCoeff(k)), REAL(c(k)), limitAssert)
assert_equal_within(aimag(tripleCutCoeff(k)), AIMAG(c(k)), limitAssert)
end do
end if

assert_equal_within(REAL(tripleCutCoeff(10)), REAL(c(10)), limitAssert)
assert_equal_within(aimag(tripleCutCoeff(10)), AIMAG(c(10)), limitAssert)

end do
end do

end test CReduction

end test_suite