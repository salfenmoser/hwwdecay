module mod_diagramsTriple

  use mod_types
  use mod_consts_dp
  use common_def
  
  use mod_auxfunctions     
  use mod_kinematics
  use mod_amplitudes

  use mod_diagramKinematics
  use mod_ampSum
  
implicit none

public :: C_ijk, getTripleCutCoefficients_i, getTripleCutCoefficient0_i, getTripleCutFunction_i
public :: cTest

logical :: demandOnShellExternal = .false., fromScratch = .false.
integer :: unitOffsetC = 30

contains

!---------------------------------------------------------
! triple cuts 
! --------------------------------------------------------

function cTest()
  complex(dp) :: cTest(10)
  cTest(:) = czero
end function cTest

function C_ijkTest(D_S, l, pa1, pa2, pa3, i, c)
  integer, intent(in) :: D_S, i
  complex(dp), intent(in) ::  l(dimP), pa1(3,dimP), pa2(3,dimP), pa3(3,dimP), c(10)
  complex(dp) :: lt_sq, v(dimP,dimP), V3(dimP), l_eps(10,dimP)
  complex(dp) C_ijkTest

C_ijkTest = czero
call getTripleCutKinematics(pa1, pa2, pa3, i, v, V3, lt_sq)
if (D_S == 4) then
  v(5,:) = czero
end if

C_ijkTest = c(10) + c(1)*sc(l,v(3,:)) + c(2)*sc(l,v(4,:)) + c(3)*(sc(l,v(3,:))**two - sc(l,v(4,:))**two)
C_ijkTest = C_ijkTest + c(4)*sc(l,v(3,:))*sc(l,v(4,:)) + c(5)*sc(l,v(3,:))**three + c(6)*sc(l,v(4,:))**three 
C_ijkTest = C_ijkTest + c(7)*sc(l,v(5,:))**two + c(8)*(sc(l,v(5,:))**two)*sc(l,v(3,:)) + c(9)*(sc(l,v(5,:))**two)*sc(l,v(4,:))

end function C_ijkTest

! triple cut function depending on dimension D_S, momentum l, particles higgs, w1,w2, diagram i
! nothing to subtract from higher order cuts
function C_ijk(D_S, l, pa1, pa2, pa3, i, test, testC)
  integer, intent(in) :: i, D_S
  complex(dp), intent(in) :: pa1(3,dimP), pa2(3,dimP), pa3(3,dimP), l(dimP), testC(10)
  logical, intent(in) :: test
  complex(dp) :: C_ijk
if (test) then
  C_ijk = C_ijkTest(D_S, l, pa1, pa2, pa3, i, testC)
else
  C_ijk = getTripleCurrents(D_S, l, pa1, pa2, pa3, i, demandOnShellExternal)
end if
end function C_ijk


! ten triple cut coefficients for dimension D_S, diagram i
! reduction is performed with ten properly chosen momenta l
function getTripleCutCoefficients_i(D_S, pa1, pa2, pa3, i, test, testC)
  integer, intent(in) :: D_S, i
  complex(dp), intent(in) ::  pa1(3,dimP), pa2(3,dimP), pa3(3,dimP), testC(10)
  logical, intent(in) :: test
  integer :: m, n, unitInt
  complex(dp) :: getTripleCutCoefficients_i(10), coeff(10), auxC(7), t, C, C_eps(10)
  complex(dp) :: lt_sq, v(dimP,dimP), V3(dimP), l(dimP), l_eps(10,dimP)
  character*5 :: strDim, strDiag
  character*70 :: documentName
  logical :: fileExists

coeff(:) = czero
getTripleCutCoefficients_i(:) = czero
C = czero
C_eps(:) = czero

call getTripleCutKinematics(pa1, pa2, pa3, i, v, V3, lt_sq)

if (D_S == 4) then
  v(5,:) = czero
end if

!m=1,...7 corresponds to auxC -3, ... +3
do m = 1,7
auxC(m) =  czero
  do n = 0,6
    t = czero
    t = exp(two*pi*ci*n/7._dp)
    l(:) = V3 + sqrt(lt_sq)*(REAL(t)*v(3,:) + AIMAG(t)*v(4,:))
    C = C_ijk(D_S, l, pa1, pa2, pa3, i, test, testC)
    auxC(m) = auxC(m) + (one/7.0_dp) * C * t**(-(m-4))
  end do
end do
  
! coeff(10) corresponds to c_0
coeff(10) = auxC(4)
coeff(5) = four * (auxC(7) + auxC(1)) / lt_sq**(three/two)
coeff(6) = - four * ci * (auxC(7) - auxC(1)) / lt_sq**(three/two)
coeff(3) = (auxC(6) + auxC(2)) / lt_sq
coeff(4) = 2 * ci * (auxC(6) - auxC(2)) / lt_sq
coeff(1) = (auxC(5) + auxC(3) - three*lt_sq**(three/two)*coeff(5)/four) / sqrt(lt_sq)
coeff(2) = ci * (auxC(5) - auxC(3) + three*ci*lt_sq**(three/two)*coeff(6)/four) / sqrt(lt_sq)
  
! access c7,8,9
if (D_S .NE. 4) then
  l_eps(4,:) = V3 + sqrt(lt_sq/two)*(v(3,:) + v(5,:))
  l_eps(5,:) = V3 + sqrt(lt_sq/two)*(- v(3,:) + v(5,:))
  l_eps(6,:) = V3 + sqrt(lt_sq/two)*(v(4,:) + v(5,:))
  l_eps(7,:) = V3 + sqrt(lt_sq/two)*( - v(4,:) + v(5,:))

  C_eps(4) = C_ijk(D_S, l_eps(4,:), pa1, pa2, pa3, i, test, testC)
  C_eps(5) = C_ijk(D_S, l_eps(5,:), pa1, pa2, pa3, i, test, testC)
  C_eps(6) = C_ijk(D_S, l_eps(6,:), pa1, pa2, pa3, i, test, testC)
  C_eps(7) = C_ijk(D_S, l_eps(7,:), pa1, pa2, pa3, i, test, testC)

  coeff(7) = (C_eps(4) + C_eps(5) - two*coeff(10) - lt_sq*coeff(3))/lt_sq
  coeff(8) = C_eps(4) - coeff(10) - coeff(1)*sqrt(lt_sq/two) - coeff(3)*(lt_sq/two) - coeff(5)*(lt_sq/two)**(three/two)
  coeff(8) = ( coeff(8) - coeff(7)*(lt_sq/two) ) * (two/lt_sq)**(three/two)
  coeff(9) = C_eps(6) - coeff(10) - coeff(2)*sqrt(lt_sq/two) + coeff(3)*(lt_sq/two) - coeff(6)*(lt_sq/two)**(three/two)
  coeff(9) = ( coeff(9) - coeff(7)*(lt_sq/two) ) * (two/lt_sq)**(three/two)
else
  coeff(7) = czero
  coeff(8) = czero
  coeff(9) = czero
end if

! set to zero if smaller than limit value
do m = 1,10
  if (abs(REAL(coeff(m))) < limitCoeff ) then
    coeff(m) = cmplx(zero,AIMAG(coeff(m)),kind=dp)
  end if
  if (abs(AIMAG(coeff(m))) < limitCoeff ) then
    coeff(m) = cmplx(REAL(coeff(m)),zero,kind=dp)
  end if
  end do

getTripleCutCoefficients_i(:) = coeff(:)

if (.Not. test) then
! write coefficients in file, if not done yet
write (strDim, '(I1)') D_S
write (strDiag, '(I1)') i
unitInt = unitOffsetC+D_S-4

documentName = trim('diagrams/coefficients/tripleCutCoefficient_'//trim(strDiag)//'_'//trim(strDim)//'.dat')

INQUIRE(FILE=documentName, EXIST=fileExists)
if (.NOT. fileExists ) then
  open(unit = unitInt, file = documentName, status='new')
  write(unitInt, FMT, advance='no') coeff(:)
  close(unit = unitInt)
end if
end if

end function getTripleCutCoefficients_i

function getTripleCutCoefficient0_i(D_S, pa1, pa2, pa3, i)
  integer, intent(in) :: i, D_S
  complex(dp), intent(in) ::  pa1(3,dimP), pa2(3,dimP), pa3(3,dimP)
  complex(dp) :: tripleCutCoefficients(10)
  complex(dp) :: getTripleCutCoefficient0_i
tripleCutCoefficients(:) = czero
tripleCutCoefficients(:) = getTripleCutCoefficients_i(D_S, pa1, pa2, pa3, i, .false., cTest())
getTripleCutCoefficient0_i = tripleCutCoefficients(10)
end function getTripleCutCoefficient0_i

! returns the C_ijk function for any momentum l. required for subtraction scheme for double cut coefficients
function getTripleCutFunction_i(D_S, l, pa1, pa2, pa3, i)
  integer, intent(in) :: i, D_S
  complex(dp), intent(in) :: l(dimP), pa1(3,dimP), pa2(3,dimP), pa3(3,dimP)
  complex(dp) :: getTripleCutFunction_i, c_Func, c_i(10)
  real(dp) :: c_iREAL(10), c_iIMAG(10)
  complex(dp) :: v(dimP,dimP), V3(dimP), lt_sq
  integer :: m, unitInt
  character*5 :: strDim, strDiag
  character*70 :: documentName
  logical :: fileExists

call getTripleCutKinematics(pa1, pa2, pa3, i, v, V3, lt_sq)
if (D_S == 4) then
  v(5,:) = czero
end if

! check if file exists
write (strDim, '(I1)') D_S
write (strDiag, '(I1)') i
unitInt = unitOffsetC+D_S-4
documentName = trim('diagrams/coefficients/tripleCutCoefficient_'//trim(strDiag)//'_'//trim(strDim)//'.dat')
!print*, 'inquiring existence of C file: ', documentName
INQUIRE(FILE=documentName, EXIST=fileExists)

! if so: getTripleCutCoefficients_i from file
if (fileExists .and. (fromScratch .eqv. .false.)) then
  !print*, documentName, 'coefficient file found, no re-calculation'
  open(unit = unitInt, file = documentName, action='read', status='old')
  do m = 1,10
    c_i(m) = czero
    read(unitInt, *) c_iREAL(m)
    read(unitInt, *) c_iIMAG(m)
    c_i(m) = c_iREAL(m) + ci*c_iIMAG(m)
  end do
  close(unit = unitInt)

! if not: get from reduction
else
  !print*, documentName, 'coefficient file not found, start calculation'
  c_i = getTripleCutCoefficients_i(D_S, pa1, pa2, pa3, i, .false., cTest())
end if

c_Func = c_i(10) + c_i(1)*sc(l,v(3,:)) + c_i(2)*sc(l,v(4,:)) + c_i(3)*(sc(l,v(3,:))**two - sc(l,v(4,:))**two)
c_Func = c_Func + c_i(4)*sc(l,v(3,:))*sc(l, v(4,:)) + c_i(5)*sc(l,v(3,:))**three + c_i(6)*sc(l,v(4,:))**three
c_Func = c_Func + c_i(7)*sc(l,v(5,:))**two + c_i(8)*sc(l,v(3,:))*sc(l, v(5,:))**two + c_i(9)*sc(l,v(4,:))*sc(l, v(5,:))**two

getTripleCutFunction_i = c_Func
end function getTripleCutFunction_i

end module