module mod_diagrams

  use mod_types
  use mod_consts_dp
  use common_def
  
  use mod_auxfunctions     
  use mod_kinematics
  use mod_amplitudes

  use mod_diagramKinematics
  use mod_ampSum

  use mod_numeratorFunctions
  
implicit none

public :: C_ijk, getTripleCutCoefficients_i, getTripleCutFunction_i, getTripleCutCoefficient0_i
public :: B_ij, getDoubleCutCoefficients_ij, getDoubleCutFunction_ij, getDoubleCutCoefficient0_ij
public :: A_ij, getSingleCutCoefficients_ij, getSingleCutCoefficient0_ij
public :: finalCoefficients

logical :: demandOnShellExternal = .false., fromScratch = .false.
integer :: unitOffsetC = 30, unitOffsetB = 33, unitOffsetA = 36


contains

!---------------------------------------------------------
! triple cuts 
! --------------------------------------------------------

! triple cut function depending on dimension D_S, momentum l, particles higgs, w1,w2, diagram i
! nothing to subtract from higher order cuts
function C_ijk(D_S, l, pa1, pa2, pa3, i)
  integer, intent(in) :: i, D_S
  complex(dp), intent(in) :: pa1(3,dimP), pa2(3,dimP), pa3(3,dimP), l(dimP)
  complex(dp) :: C_ijk
C_ijk = getTripleCurrents(D_S, l, pa1, pa2, pa3, i, demandOnShellExternal)
end function C_ijk

! ten triple cut coefficients for dimension D_S, diagram i
! reduction is performed with ten properly chosen momenta l
function getTripleCutCoefficients_i(D_S, pa1, pa2, pa3, i)
  integer, intent(in) :: D_S, i
  complex(dp), intent(in) ::  pa1(3,dimP), pa2(3,dimP), pa3(3,dimP)
  integer :: m, n, unitInt
  complex(dp) :: getTripleCutCoefficients_i(10), coeff(10), auxC(7), t, C, C_eps(4)
  complex(dp) :: lt_sq, v(dimP,dimP), V3(dimP), l4(7,7,dimP), l_eps(4,dimP)
  character*5 :: strDim, strDiag
  character*70 :: documentName
  logical :: fileExists

l4(:,:,:) = czero
l4 = getTripleCutMomenta4D(D_S, pa1, pa2, pa3, i)

!m=1,...7 corresponds to auxC -3, ... +3
auxC(:) =  czero
do m = 1,7
  do n = 0,6
    t = exp(two*pi*ci*n/7._dp)
    C = C_ijk(D_S, l4(m,n+1,:), pa1, pa2, pa3, i)
    auxC(m) = auxC(m) + (one/7.0_dp) * C * t**(-(m-4))
  end do
end do
  
! coeff(10) corresponds to c_0
coeff(10) = auxC(4)
coeff(5) = four * (auxC(7) + auxC(1)) / lt_sq**(three/two)
coeff(6) = - four * ci * (auxC(7) - auxC(1)) / lt_sq**(three/two)
coeff(3) = (auxC(6) + auxC(2)) / lt_sq
coeff(4) = 2 * ci * (auxC(6) - auxC(2)) / lt_sq
coeff(1) = (auxC(5) + auxC(3) - three*lt_sq**(three/two)*coeff(5)/four) / sqrt(lt_sq)
coeff(2) = ci * (auxC(5) - auxC(3) + three*ci*lt_sq**(three/two)*coeff(6)/four) / sqrt(lt_sq)
  
! access c7,8,9
l_eps(:,:) = czero
if (D_S > 4) then
l_eps = getTripleCutMomenta5_6D(D_S, pa1, pa2, pa3, i)

C_eps(1) = C_ijk(D_S, l_eps(1,:), pa1, pa2, pa3, i)
C_eps(2) = C_ijk(D_S, l_eps(2,:), pa1, pa2, pa3, i)
C_eps(3) = C_ijk(D_S, l_eps(3,:), pa1, pa2, pa3, i)
C_eps(4) = C_ijk(D_S, l_eps(4,:), pa1, pa2, pa3, i)

coeff(7) = (C_eps(1) + C_eps(2) - two*coeff(10) - lt_sq*coeff(3))/lt_sq
coeff(8) = (C_eps(1) - C_eps(2) - sqrt(two*lt_sq)*coeff(1) - sqrt(lt_sq**three / two)*coeff(5))*sqrt(two/lt_sq**three)
coeff(9) = (C_eps(3) - C_eps(4) - sqrt(two*lt_sq)*coeff(2) - sqrt(lt_sq**three / two)*coeff(6))*sqrt(two/lt_sq**three)

else
  coeff(7) = czero
  coeff(8) = czero
  coeff(9) = czero
end if

! set to zero if smaller than limit value
do m = 1,10
  if (abs(REAL(coeff(m))) < limitCoeff ) then
    coeff(m) = cmplx(zero,AIMAG(coeff(m)),kind=dp)
  end if
  if (abs(AIMAG(coeff(m))) < limitCoeff ) then
    coeff(m) = cmplx(REAL(coeff(m)),zero,kind=dp)
  end if
  end do

getTripleCutCoefficients_i(:) = coeff(:)

! write coefficients in file, if not done yet
write (strDim, '(I1)') D_S
write (strDiag, '(I1)') i
unitInt = unitOffsetC+D_S-4

documentName = trim('diagrams/coefficients/tripleCutCoefficient_'//trim(strDiag)//'_'//trim(strDim)//'.dat')

INQUIRE(FILE=documentName, EXIST=fileExists)
if (.NOT. fileExists ) then
  open(unit = unitInt, file = documentName, status='new')
  write(unitInt, FMT, advance='no') coeff(:)
  close(unit = unitInt)
end if
end function getTripleCutCoefficients_i


function getTripleCutCoefficient0_i(D_S, pa1, pa2, pa3, i)
  integer, intent(in) :: i, D_S
  complex(dp), intent(in) ::  pa1(3,dimP), pa2(3,dimP), pa3(3,dimP)
  complex(dp) :: tripleCutCoefficients(10)
  complex(dp) :: getTripleCutCoefficient0_i
tripleCutCoefficients(:) = czero
tripleCutCoefficients(:) = getTripleCutCoefficients_i(D_S, pa1, pa2, pa3, i)
getTripleCutCoefficient0_i = tripleCutCoefficients(10)
end function getTripleCutCoefficient0_i


! returns the C_ijk function for any momentum l. required for subtraction scheme for double cut coefficients
function getTripleCutFunction_i(D_S, l, pa1, pa2, pa3, i)
  integer, intent(in) :: i, D_S
  complex(dp), intent(in) :: l(dimP), pa1(3,dimP), pa2(3,dimP), pa3(3,dimP)
  complex(dp) :: getTripleCutFunction_i, c_Func, c_i(10)
  real(dp) :: c_iREAL(10), c_iIMAG(10)
  complex(dp) :: v(dimP,dimP), V3(dimP), lt_sq
  integer :: m, unitInt
  character*5 :: strDim, strDiag
  character*70 :: documentName
  logical :: fileExists

call getTripleCutKinematics(pa1, pa2, pa3, i, v, V3, lt_sq)
if (D_S == 4) then
  v(5,:) = czero
end if

! check if file exists
write (strDim, '(I1)') D_S
write (strDiag, '(I1)') i
unitInt = unitOffsetC+D_S-4
documentName = trim('diagrams/coefficients/tripleCutCoefficient_'//trim(strDiag)//'_'//trim(strDim)//'.dat')
!print*, 'inquiring existence of C file: ', documentName
INQUIRE(FILE=documentName, EXIST=fileExists)

! if so: getTripleCutCoefficients_i from file
if (fileExists .and. (fromScratch .eqv. .false.)) then
  !print*, documentName, 'coefficient file found, no re-calculation'
  open(unit = unitInt, file = documentName, action='read', status='old')
  do m = 1,10
    c_i(m) = czero
    read(unitInt, *) c_iREAL(m)
    read(unitInt, *) c_iIMAG(m)
    c_i(m) = c_iREAL(m) + ci*c_iIMAG(m)
  end do
  close(unit = unitInt)

! if not: get from reduction
else
  !print*, documentName, 'coefficient file not found, start calculation'
  c_i = getTripleCutCoefficients_i(D_S, pa1, pa2, pa3, i)
end if

c_Func = c_i(10) + c_i(1)*sc(l, v(3,:)) + c_i(2)*sc(l, v(4,:)) + c_i(3)*(sc(l, v(3,:))**two - sc(l,v(4,:))**two)
c_Func = c_Func + c_i(4)*sc(l, v(3,:))*sc(l, v(4,:)) + c_i(5)*sc(l, v(3,:))**three + c_i(6)*sc(l, v(4,:))**three
c_Func = c_Func + c_i(7)*sc(l, v(5,:))**two + c_i(8)*sc(l, v(3,:))*sc(l, v(5,:))**two + c_i(9)*sc(l, v(4,:))*sc(l, v(5,:))**two

getTripleCutFunction_i = c_Func
end function getTripleCutFunction_i


!---------------------------------------------------------
! double cuts 
! --------------------------------------------------------

! double cut function depending on dimension D_S, momentum l, particles higgs, w1,w2, diagram i, cut j
! need to subtract triangle coefficients adequately
function B_ij(D_S, l, pa1, pa2, pa3, i, j)
  integer, intent(in) :: i, j, D_S
  complex(dp), intent(in) :: pa1(3,dimP), pa2(3,dimP), pa3(3,dimP), l(dimP)
  complex(dp) :: B_ij, d_l(6,3), d_lM2(6,3), d_lM1(6,3), doubleCurrents, tripleC(6), tripleC_shiftedByK1(6), tripleC_shiftedByK2(6)
  integer :: k
  complex(dp) :: lt_sq, v(dimP,dimP), V3(dimP), x1, q(dimP)

d_l = getDenominators(l, pa1, pa2, pa3)
d_lM2 = getDenominators(l-pa3(1,:), pa1, pa2, pa3)
d_lM1 = getDenominators(l-pa2(1,:), pa1, pa2, pa3)

doubleCurrents = getDoubleCurrents(D_S, l, pa1, pa2, pa3, i, j, demandOnShellExternal)
B_ij = czero

! first cut: d0=d1=0
if (j == 1) then

  !subtraction prescription depends on structure, how diagrams add up
if (i == 1) then
  tripleC(i) = getTripleCutFunction_i(D_S, l, pa1, pa2, pa3, i)
  tripleC_shiftedByK2(i+1) = getTripleCutFunction_i(D_S, l - pa3(1,:) , pa1, pa2, pa3, i+1)
  B_ij = doubleCurrents - ci * tripleC(i)/d_l(i,3) - ci * tripleC_shiftedByK2(i+1)/d_lM2(i+1,1)
else if (i == 3) then
  tripleC(i) = getTripleCutFunction_i(D_S, l , pa1, pa2, pa3, i)
  tripleC_shiftedByK2(i+3) = getTripleCutFunction_i(D_S, l - pa3(1,:) , pa1, pa2, pa3, i+3)
  B_ij = doubleCurrents - ci * tripleC(i)/d_l(i,3) - ci * tripleC_shiftedByK2(i+3)/d_lM2(i+3,1)
else if (i == 5) then
  tripleC(i) = getTripleCutFunction_i(D_S, l , pa1, pa2, pa3, i)
  tripleC_shiftedByK2(i-1) = getTripleCutFunction_i(D_S, l - pa3(1,:) , pa1, pa2, pa3, i-1)
  B_ij = doubleCurrents - ci * tripleC(i)/d_l(i,3) - ci * tripleC_shiftedByK2(i-1)/d_lM2(i-1,1)

else if (i == 2) then
  tripleC(i) = getTripleCutFunction_i(D_S, l , pa1, pa2, pa3, i)
  tripleC_shiftedByK1(i-1) = getTripleCutFunction_i(D_S, l - pa2(1,:) , pa1, pa2, pa3, i-1)
  B_ij = doubleCurrents - ci * tripleC(i)/d_l(i,3) - ci * tripleC_shiftedByK1(i-1)/d_lM1(i-1,1)
else if (i == 4) then
  tripleC(i) = getTripleCutFunction_i(D_S, l , pa1, pa2, pa3, i)
  tripleC_shiftedByK1(i+1) = getTripleCutFunction_i(D_S, l - pa2(1,:) , pa1, pa2, pa3, i+1)
  B_ij = doubleCurrents - ci * tripleC(i)/d_l(i,3) - ci * tripleC_shiftedByK1(i+1)/d_lM1(i+1,1)
else if (i == 6) then
  tripleC(i) = getTripleCutFunction_i(D_S, l , pa1, pa2, pa3, i)
  tripleC_shiftedByK1(i-3) = getTripleCutFunction_i(D_S, l - pa2(1,:) , pa1, pa2, pa3, i-3)
  B_ij = doubleCurrents - ci * tripleC(i)/d_l(i,3) - ci * tripleC_shiftedByK1(i-3)/d_lM1(i-3,1) 
end if

! second cut: d0=d2=0
else if (j == 2) then
  do k = 1,6
  tripleC(k) = getTripleCutFunction_i(D_S, l, pa1, pa2, pa3, k)
  end do

  if (i == 1) then
    B_ij = doubleCurrents - ci * tripleC(1)/d_l(1,2) - ci * tripleC(2)/d_l(2,2) 
    B_ij = B_ij - ci * tripleC(5)/d_l(5,2) - ci * tripleC(6)/d_l(6,2)
  else if (i == 3) then
    B_ij = doubleCurrents - ci*tripleC(3)/d_l(3,2) - ci*tripleC(4)/d_l(4,2)
  end if
end if

end function B_ij

! ten double cut coefficients for dimension D_S, diagram i, cut j
! reduction is performed with ten properly chosen momenta l
function getDoubleCutCoefficients_ij(D_S, pa1, pa2, pa3, i, j)
  integer, intent(in) :: i, j, D_S
  integer :: n, m, i_ind, unitInt
  complex(dp), intent(in) ::  pa1(3,dimP), pa2(3,dimP), pa3(3,dimP)
  complex(dp) :: getDoubleCutCoefficients_ij(10), Bcoeff(10), B1(3), B2(2), auxB, B3(4), BEps
  complex(dp) :: x1, q(dimP), lt_sq, vec(dimP,dimP), V2(dimP)
  complex(dp) :: t, x, y
  complex(dp) :: l1(3,3,2,dimP), l2(2,2,2,dimP), l_Aux(4,dimP), l_Eps(dimP)
  character*5 :: strDim, strDiag, strCut
  character*70 :: documentName
  logical :: fileExists

getDoubleCutCoefficients_ij(:) = (/czero,czero,czero,czero,czero,czero,czero,czero,czero,czero/)

if ( j == 3 ) then
else if ( j == 2 .and. (i /= 1 .and. i /= 3) ) then
else

call getDoubleCutKinematics(pa1, pa2, pa3, i, j, q, x1, lt_sq, vec)

B1(:) = czero
l1(:,:,:,:) = czero
l2(:,:,:,:) = czero
l_Aux(:,:) = czero
l_Eps(:) = czero

call getDoubleCutMomenta(D_S, pa1, pa2, pa3, i, j, l1, l2, l_Aux, l_Eps, x, y)

do m = 1,3
  do n = 0,2
  t = exp(2*pi*ci * n / three)  
  auxB = (B_ij(D_S, l1(m,n+1,1,:), pa1, pa2, pa3, i, j) + B_ij(D_S, l1(m,n+1,2,:), pa1, pa2, pa3, i, j))/ two
  B1(m) = B1(m) + (one/three) * auxB * t**(m-1) !corresponds to 1: b_0(eff), 3: b_1, 2: b_4(eff)   
end do
end do
  
B2(:) = czero
do m = 1,2
  do n = 0,1
  t = exp(2*pi*ci * n / two)
  auxB = (B_ij(D_S, l2(m,n+1,1,:), pa1, pa2, pa3, i, j) - B_ij(D_S, l2(m,n+1,1,:), pa1, pa2, pa3, i, j)) &
           / (two * sqrt(lt_sq - t**2))
  B2(m) = B2(m) + (one/two) * auxB * t**(m-1) !corresponds to b_2, b_6    
end do
end do

Bcoeff(1) = B1(3)
Bcoeff(2) = B2(1)
Bcoeff(6) = B2(2)

B3(:) = czero
do m= 1,4
  B3(m) =  B_ij(D_S, l_Aux(m,:), pa1, pa2, pa3, i, j)
end do
  
Bcoeff(3) = ( (B3(1) - B3(3))/two - x*Bcoeff(1) )/ y
Bcoeff(8) = ( (B3(1) - B3(2))/two - x*Bcoeff(1) ) / (x*y)
Bcoeff(5) = ( B1(1) + Bcoeff(3)*y + x*y * Bcoeff(8) + x* Bcoeff(1) + (x**two - y**two)*B1(2) - B3(1) ) / (three*y**two)
Bcoeff(4) = B1(2) + Bcoeff(5)
Bcoeff(10) = B1(1) - lt_sq * Bcoeff(5)
Bcoeff(7) = (B3(4) + (x**two - y**two) * Bcoeff(5) + Bcoeff(4)*x**2 - Bcoeff(3)*x - y*Bcoeff(2) - Bcoeff(10) ) / (x*y)

Bcoeff(9) = czero
!access b9
if (D_S > 4) then
  Beps = B_ij(D_S, l_Aux(m,:), pa1, pa2, pa3, i, j)
  Bcoeff(9) = (Beps - Bcoeff(4)*x**two - Bcoeff(1)*x - Bcoeff(10)) / y**two
end if

do m = 1,10
  if (abs(REAL(Bcoeff(m))) < limitCoeff ) then
    Bcoeff(m) = cmplx(zero,AIMAG(Bcoeff(m)),kind=dp)
  end if
  if (abs(AIMAG(Bcoeff(m))) < limitCoeff ) then
    Bcoeff(m) = cmplx(REAL(Bcoeff(m)),zero,kind=dp)
  end if
end do

getDoubleCutCoefficients_ij(:) = Bcoeff(:)

! write coefficients in file
write (strDim, '(I1)') D_S
write (strDiag, '(I1)') i
write (strCut, '(I1)') j
unitInt = unitOffsetB+D_S-4
documentName = trim('diagrams/coefficients/doubleCutCoefficient_'//trim(strDiag)//'_'//trim(strCut)//'_'//trim(strDim)//'.dat')

INQUIRE(FILE=documentName, EXIST=fileExists)
if (.NOT. fileExists ) then
  open(unit = unitInt, file = documentName, status='new')
  write(unitInt, *) Bcoeff(:)
  close(unit = unitInt)
end if

end if
end function getDoubleCutCoefficients_ij


function getDoubleCutCoefficient0_ij(D_S, pa1, pa2, pa3, i, j)
  integer, intent(in) :: i, j, D_S
  complex(dp), intent(in) ::  pa1(3,dimP), pa2(3,dimP), pa3(3,dimP)
  complex(dp) :: doubleCutCoefficients(10)
  complex(dp) :: getDoubleCutCoefficient0_ij
doubleCutCoefficients(:) = czero
doubleCutCoefficients(:) = getDoubleCutCoefficients_ij(D_S, pa1, pa2, pa3, i, j)
getDoubleCutCoefficient0_ij = doubleCutCoefficients(10)
end function getDoubleCutCoefficient0_ij


! returns the B_ij function for any momentum l. required for subtraction scheme for single cut coefficients
function getDoubleCutFunction_ij(D_S, l, pa1, pa2, pa3, i, j)
  integer, intent(in) :: i, j, D_S
  complex(dp), intent(in) :: l(dimP), pa1(3,dimP), pa2(3,dimP), pa3(3,dimP)
  complex(dp) :: getDoubleCutFunction_ij, b_Func, bCoeff_ij(10), v(dimP,dimP), q(dimP), x1, lt_sq
  integer :: m, unitInt
  complex(dp) :: den(6,3), l_eff(dimP), den_eff, l_zero(dimP), alpha1, diag(6,21)
  character*5 :: strDim, strDiag, strCut
  character*70 :: documentName
  logical :: fileExists, sqDen

sqDen = .false.

call getDoubleCutKinematics(pa1, pa2, pa3, i, j, q, x1, lt_sq, v)
bCoeff_ij = getDoubleCutCoefficients_ij(D_S, pa1, pa2, pa3, i, j)

do m = 1,dimP
l_zero(m) = czero
end do

diag = diagramsHWW(l_zero, pa1(1,:), pa2(1,:), pa3(1,:))

! check if file exists
write (strDim, '(I1)') D_S
write (strDiag, '(I1)') i
write (strCut, '(I0)') j
unitInt = unitOffsetB+D_S-4
documentName = trim('diagrams/coefficients/doubleCutCoefficient_'//trim(strDiag)//'_'//trim(strCut)//'_'//trim(strDim)//'.dat')
!print*, 'inquiring existence of B file: ', documentName
INQUIRE(FILE=documentName, EXIST=fileExists)

! if so: getTripleCutCoefficients_i from file
if (fileExists .and. (fromScratch .eqv. .false.)) then
  !print*, 'B coefficient file found, no re-calculation'
  open(unit = unitInt, file = trim(documentName), status='old')
  do m = 1,10
    read(unitInt, *) bCoeff_ij(m)
  end do
  close(unit = unitInt)

! if not: get from reduction
else
  !print*, 'B coefficient file not found, start calculation'
  bCoeff_ij = getDoubleCutCoefficients_ij(D_S, pa1, pa2, pa3, i, j)
end if

den = getDenominators(l, pa1, pa2, pa3)

if (j == 1) then

b_Func = bCoeff_ij(10) + bCoeff_ij(1)*sc(l, v(2,:)) + bCoeff_ij(2)*sc(l, v(3,:)) + bCoeff_ij(3)*sc(l, v(4,:))
b_Func = b_Func + bCoeff_ij(4)*(sc(l, v(2,:))**2 - sc(l,v(4,:))**2) + bCoeff_ij(5)*(sc(l, v(3,:))**2 - sc(l,v(4,:))**2)
b_Func = b_Func + bCoeff_ij(6)*sc(l, v(2,:))*sc(l, v(3,:)) + bCoeff_ij(7)*sc(l, v(3,:))*sc(l, v(4,:))
b_Func = b_Func + bCoeff_ij(8)*sc(l, v(2,:))*sc(l, v(4,:)) + bCoeff_ij(9)*sc(l, v(5,:))**2

! in case of squared denominator contribution
if (sqDen) then
  !print *, 'bFunc before: ', b_Func
  den = getDenominators(l, pa1, pa2, pa3)
  alpha1 = bCoeff_ij(1)/( -two * sc(v(2,:), diag(i,1:6) - diag(i,15:20)) )
  b_Func = b_Func - alpha1 * (den(i,2) - den(i,1)) * sc(v(1,:),diag(i,1:6) - diag(i,15:20))
  !print *, 'bFunc after: ', b_Func
end if 

else if (j == 2) then
b_Func = bCoeff_ij(10) + bCoeff_ij(1)*sc(l, v(2,:)) + bCoeff_ij(2)*sc(l, v(3,:)) + bCoeff_ij(3)*sc(l, v(4,:))
b_Func = b_Func + bCoeff_ij(4)*(sc(l, v(2,:))**2 - sc(l,v(4,:))**2) + bCoeff_ij(5)*(sc(l, v(3,:))**2 - sc(l,v(4,:))**2)
b_Func = b_Func + bCoeff_ij(6)*sc(l, v(2,:))*sc(l, v(3,:)) + bCoeff_ij(7)*sc(l, v(3,:))*sc(l, v(4,:))
b_Func = b_Func + bCoeff_ij(8)*sc(l, v(2,:))*sc(l, v(4,:)) + bCoeff_ij(9)*sc(l, v(5,:))**2


if (sqDen) then
  !print *, 'bFunc before: ', b_Func
  den = getDenominators(l, pa1, pa2, pa3)
  alpha1 = bCoeff_ij(1)/( -two * sc(v(2,:), diag(i,8:13) - diag(i,1:6)) )
  b_Func = b_Func - alpha1 * (den(i,1) - den(i,3)) * sc(v(1,:),diag(i,8:13) - diag(i,1:6))
  !print *, 'bFunc after: ', b_Func
end if

! else if (j == 3) then
! b_Func = bCoeff_ij(10) + bCoeff_ij(1)*sc(l, v(2,:)) + bCoeff_ij(2)*sc(l, v(3,:)) + bCoeff_ij(3)*sc(l, v(4,:))
! b_Func = b_Func + bCoeff_ij(4)*(sc(l, v(2,:))**2 - sc(l,v(4,:))**2) + bCoeff_ij(5)*(sc(l, v(3,:))**2 - sc(l,v(4,:))**2)
! b_Func = b_Func + bCoeff_ij(6)*sc(l, v(2,:))*sc(l, v(3,:)) + bCoeff_ij(7)*sc(l, v(3,:))*sc(l, v(4,:))
! b_Func = b_Func + bCoeff_ij(8)*sc(l, v(2,:))*sc(l, v(4,:)) + bCoeff_ij(9)*sc(l, v(5,:))**2

! if (sqDen) then
!   den = getDenominators(l, pa1, pa2, pa3)
!   alpha1 = bCoeff_ij(1)/( -two * sc(v(2,:), diag(i,15:20) - diag(i,8:13)) )
!   b_Func = b_Func - alpha1 * (den(i,3) - den(i,2)) * sc(v(1,:),diag(i,15:20) - diag(i,8:13))
! end if 

end if

getDoubleCutFunction_ij = b_Func

end function getDoubleCutFunction_ij

! !---------------------------------------------------------
! ! single cuts 
! ! --------------------------------------------------------

! single cut function depending on dimension D_S, momentum l, particles higgs, w1,w2, diagram i, cut jSingle
! need to subtract triangle and bubble coefficients adequately
function A_ij(D_S, l, pa1, pa2, pa3, i, jSingle)
  integer, intent(in) :: i, jSingle, D_S
  complex(dp), intent(in) :: pa1(3,dimP), pa2(3,dimP), pa3(3,dimP), l(dimP)
  complex(dp) :: d_l(6,3), d_lM1(6,3), d_lPp(6,3), d_lM2(6,3)
  complex(dp) :: A_ij, singleCurrents
  complex(dp) :: doubleB(6,3), doubleB_Pp(6,3), doubleB_M1(6,3), doubleB_M2(6,3)
  complex(dp) :: tripleC(6), tripleC_M1(6), tripleC_M2(6), tripleC_Pp(6)
  integer :: j, k

d_l = getDenominators(l, pa1, pa2, pa3)
d_lM1 = getDenominators(l-pa2(1,:), pa1, pa2, pa3)
d_lM2 = getDenominators(l-pa3(1,:), pa1, pa2, pa3)
d_lPp = getDenominators(l+pa1(1,:), pa1, pa2, pa3)

singleCurrents = getSingleCurrents(D_S, l, pa1, pa2, pa3, i, jSingle, demandOnShellExternal)

do k = 1,6
tripleC(k) = getTripleCutFunction_i(D_S, l, pa1, pa2, pa3, k)
tripleC_M1(k) = getTripleCutFunction_i(D_S, l-pa2(1,:), pa1, pa2, pa3, k)
tripleC_M2(k) = getTripleCutFunction_i(D_S, l-pa3(1,:), pa1, pa2, pa3, k)
tripleC_Pp(k) = getTripleCutFunction_i(D_S, l+pa1(1,:), pa1, pa2, pa3, k)
end do

do j = 1,2
do k = 1,6
doubleB(k,j) = getDoubleCutFunction_ij(D_S, l, pa1, pa2, pa3, k, j)
doubleB_Pp(k,j) = getDoubleCutFunction_ij(D_S, l+pa1(1,:), pa1, pa2, pa3, k, j)
doubleB_M1(k,j) = getDoubleCutFunction_ij(D_S, l-pa2(1,:), pa1, pa2, pa3, k, j)
doubleB_M2(k,j) = getDoubleCutFunction_ij(D_S, l-pa3(1,:), pa1, pa2, pa3, k, j)

end do
end do

A_ij = czero

if (jSingle == 1 .and. i == 1) then
A_ij = singleCurrents
!print *, 'before subtraction: A_ij = ', A_ij
!substract first all c coefficients
A_ij = A_ij + tripleC(1)/d_l(1,2)/d_l(1,3) + tripleC_M1(1)/d_lM1(1,1)/d_lM1(1,3) + tripleC_Pp(1)/d_lPp(1,1)/d_lPp(1,2)
A_ij = A_ij + tripleC(2)/d_l(2,2)/d_l(2,3) + tripleC_M2(2)/d_lM2(2,1)/d_lM2(2,3) + tripleC_Pp(2)/d_lPp(2,1)/d_lPp(2,2)

A_ij = A_ij + tripleC_M1(3)/d_lM1(3,1)/d_lM1(3,3)
A_ij = A_ij + tripleC_M2(4)/d_lM2(4,1)/d_lM2(4,3)

A_ij = A_ij + tripleC(5)/d_l(5,2)/d_l(5,3) + tripleC_Pp(5)/d_lPp(5,1)/d_lPp(5,2)
A_ij = A_ij + tripleC(6)/d_l(6,2)/d_l(6,3) + tripleC_Pp(6)/d_lPp(6,1)/d_lPp(6,2)
!print *, 'after C subtraction: A_ij = ', A_ij

!substract pink, light blue, dark blue, brown, skin, yellow, green b coefficients
A_ij = A_ij  + (- ci * doubleB(1,1) / d_l(1,2) - ci * doubleB_M1(1,1) / d_lM1(1,1) )
A_ij = A_ij  + (- ci * doubleB(2,1) / d_l(2,2) - ci * doubleB_M2(2,1) / d_lM2(2,1) ) 

A_ij = A_ij + (- ci * doubleB_M1(3,1) / d_lM1(3,1) )
A_ij = A_ij + (- ci * doubleB_M2(4,1) / d_lM2(4,1) )

A_ij = A_ij + (- ci * doubleB(5,1)) / d_l(5,2)
A_ij = A_ij + (- ci * doubleB(6,1)) / d_l(6,2)

A_ij = A_ij + (- ci * doubleB(1,2) / d_l(1,3) - ci * doubleB_Pp(1,2) / d_lPp(1,1) )
!print *, 'after B subtraction: A_ij = ', A_ij

else if (jSingle == 1 .and. i == 3) then
A_ij = singleCurrents
!print *, 'A_ij without subtraction:', A_ij

!substract first all c coefficients
A_ij = A_ij + ( tripleC(3)/d_l(3,2)/d_l(3,3) + tripleC_Pp(3)/d_lPp(3,1)/d_lPp(3,2) )
A_ij = A_ij + ( tripleC(4)/d_l(4,2)/d_l(4,3) + tripleC_Pp(4)/d_lPp(4,1)/d_lPp(4,2) )
A_ij = A_ij + tripleC_M1(5)/d_lM1(5,1)/d_lM1(5,3)
A_ij = A_ij + tripleC_M2(6)/d_lM2(6,1)/d_lM2(6,3)
! print *, 'A_ij after C subtraction:', A_ij

!substract dark blue, brown, skin, yellow, red b coefficients
A_ij = A_ij - ci * doubleB(3,1)/d_l(3,2)
A_ij = A_ij - ci * doubleB(4,1)/d_l(4,2)
A_ij = A_ij - ci * doubleB_M1(5,1)/d_lM1(5,1)
A_ij = A_ij - ci * doubleB_M2(6,1)/d_lM2(6,1)

A_ij = A_ij - ci * doubleB(3,2)/d_l(3,3) - ci * doubleB_Pp(3,2)/d_lPp(3,1)

!print *, 'A_ij final:', A_ij

end if

end function A_ij

! five single cut coefficients for dimension D_S, diagram i, cut jSingle
! reduction is performed with seven properly chosen momenta l
function getSingleCutCoefficients_ij(D_S, pa1, pa2, pa3, i, jSingle)
  integer, intent(in) :: i, jSingle, D_S
  complex(dp), intent(in) ::  pa1(3,dimP), pa2(3,dimP), pa3(3,dimP)
  complex(dp) :: getSingleCutCoefficients_ij(5), l(dimP), v(dimP, dimP), lAux(10, dimP), A(10), l_sq, diag(6,21)
  complex(dp) :: den(6,3)
  integer :: k, unitInt
  character*5 :: strDim, strDiag, strCut
  character*70 :: documentName
  logical :: fileExists

getSingleCutCoefficients_ij(:) = (/czero,czero,czero,czero,czero/)

if ( jSingle == 3 .or. jSingle == 2) then
else if ( i /= 1 .and. i /= 3 ) then
else  
  
do k = 1, dimP
v(k,k) = ci
end do 
v(1,1) = cone

getSingleCutCoefficients_ij = (/czero,czero,czero,czero,czero/)
if (i == 1) then
  l_sq = mwsq
else if (i == 3) then
  l_sq = mhsq
end if

lAux(1,:) = sqrt(l_sq/two) * (v(1,:) + v(2,:))
lAux(2,:) = sqrt(l_sq/two) * (v(1,:) - v(2,:))

lAux(3,:) = sqrt(l_sq/two) * (v(1,:) + v(3,:))
lAux(4,:) = sqrt(l_sq/two) * (v(1,:) - v(3,:))

lAux(5,:) = sqrt(l_sq/two) * (v(1,:) + v(4,:))
lAux(6,:) = sqrt(l_sq/two) * (v(1,:) - v(4,:))

lAux(7,:) = sqrt(l_sq/three) * (v(1,:) + v(2,:) + v(3,:))

do k = 1,7
A(k) = A_ij(D_S, lAux(k,:), pa1, pa2, pa3, i, jSingle)
!print *, 'A(',k,') = ', A(k)
end do

! 5 corresponds to 0
getSingleCutCoefficients_ij(2) = (A(1) - A(2)) / sqrt(two * l_sq)
getSingleCutCoefficients_ij(3) = (A(3) - A(4)) / sqrt(two * l_sq)
getSingleCutCoefficients_ij(4) = (A(5) - A(6)) / sqrt(two * l_sq)

getSingleCutCoefficients_ij(5) = ( (A(1) - A(7)*sqrt(three/two)) + getSingleCutCoefficients_ij(3)*sqrt(l_sq/two) ) &
  / (one - sqrt(three/two))
getSingleCutCoefficients_ij(1) = (A(1) - getSingleCutCoefficients_ij(5))*sqrt(two/l_sq) - getSingleCutCoefficients_ij(2)

! write coefficients in file
write (strDim, '(I1)') D_S
write (strDiag, '(I1)') i
write (strCut, '(I1)') jSingle
unitInt = unitOffsetA+D_S-4

documentName = trim('diagrams/coefficients/singleCutCoefficient_'//trim(strDiag)//'_'//trim(strCut)//'_'//trim(strDim)//'.dat')

INQUIRE(FILE=documentName, EXIST=fileExists)
if (.NOT. fileExists ) then
  open(unit = unitInt, file = documentName, status='new')
  write(unitInt, FMT, advance='no') getSingleCutCoefficients_ij(:)
end if

end if
end function getSingleCutCoefficients_ij


function getSingleCutCoefficient0_ij(D_S, pa1, pa2, pa3, i, jSingle)
  integer, intent(in) :: i, jSingle, D_S
  complex(dp), intent(in) ::  pa1(3,dimP), pa2(3,dimP), pa3(3,dimP)
  complex(dp) :: singleCutCoefficients(5)
  complex(dp) :: getSingleCutCoefficient0_ij
singleCutCoefficients(:) = czero
singleCutCoefficients(:) = getSingleCutCoefficients_ij(D_S, pa1, pa2, pa3, i, jSingle)
getSingleCutCoefficient0_ij = singleCutCoefficients(5)
end function getSingleCutCoefficient0_ij

! returns final master integral coefficients in 4 dimensional limit
function finalCoefficients(pa1, pa2, pa3)
  complex(dp), intent(in) :: pa1(3,dimP), pa2(3,dimP), pa3(3,dimP)
  complex(dp) :: finalCoefficients(9)
  complex(dp) :: tripleCutCoefficients(10), doubleCutCoefficients(10), singleCutCoefficients(5)
  character*70 :: documentName
  integer :: D_S, i
  logical :: fileExists

finalCoefficients(:) = czero
documentName = trim('diagrams/coefficients/finalCoefficients.dat')

! check if file exists, if so, take from file
INQUIRE(FILE=documentName, EXIST=fileExists)
if (fileExists .and. (fromScratch .eqv. .false.)) then
  open(unit = 1, file = trim(documentName), status='old')
  do i = 1,9
    read(1, *) finalCoefficients(i)
  end do
  close(unit = 1)

! if not: calculate
else

print *, 'get triple cut coefficients'
do i = 1,3
do D_S = 5,6   
  tripleCutCoefficients(:) = getTripleCutCoefficients_i(D_S, pa1, pa2, pa3, (2*i-1) )
  if (D_S == 5) then
    finalCoefficients(i) = finalCoefficients(i) + two*tripleCutCoefficients(10)
  else if (D_S == 6) then
    finalCoefficients(i) = finalCoefficients(i) - tripleCutCoefficients(10)
  end if  
end do
end do

print *, 'get double and single cut coefficients, diag 1, cut 1'
do D_S = 5,6
doubleCutCoefficients(:) = getDoubleCutCoefficients_ij(D_S, pa1, pa2, pa3, 1, 1)
singleCutCoefficients(:) = getSingleCutCoefficients_ij(D_S, pa1, pa2, pa3, 1, 1)

if (D_S == 5) then
  finalCoefficients(4) = finalCoefficients(4) + two*doubleCutCoefficients(10)
  finalCoefficients(8) = finalCoefficients(8) + two*singleCutCoefficients(5)
else if (D_S == 6) then
  finalCoefficients(4) = finalCoefficients(4) - doubleCutCoefficients(10)
  finalCoefficients(8) = finalCoefficients(8) - singleCutCoefficients(5)
end if  
end do

print *, 'get double and single cut coefficients, diag 3, cut 1'
do D_S = 5,6
doubleCutCoefficients(:) = getDoubleCutCoefficients_ij(D_S, pa1, pa2, pa3, 3, 1)
singleCutCoefficients(:) = getSingleCutCoefficients_ij(D_S, pa1, pa2, pa3, 3, 1)
  
if (D_S == 5) then
  finalCoefficients(5) = finalCoefficients(5) + two*doubleCutCoefficients(10)
  finalCoefficients(9) = finalCoefficients(9) + two*singleCutCoefficients(5)
else if (D_S == 6) then
  finalCoefficients(5) = finalCoefficients(5) - doubleCutCoefficients(10)
  finalCoefficients(9) = finalCoefficients(9) - singleCutCoefficients(5)
end if  
end do

print *, 'get double cut coefficients, diag 1, cut 2'
do D_S = 5,6
doubleCutCoefficients(:) = getDoubleCutCoefficients_ij(D_S, pa1, pa2, pa3, 1, 2)
    
if (D_S == 5) then
  finalCoefficients(6) = finalCoefficients(6) + two*doubleCutCoefficients(10)
else if (D_S == 6) then
  finalCoefficients(6) = finalCoefficients(6) - doubleCutCoefficients(10)
end if  
end do

print *, 'get double cut coefficients, diag 3, cut 2'
do D_S = 5,6
doubleCutCoefficients(:) = getDoubleCutCoefficients_ij(D_S, pa1, pa2, pa3, 3, 2)
      
if (D_S == 5) then
  finalCoefficients(7) = finalCoefficients(7) + two*doubleCutCoefficients(10)
else if (D_S == 6) then
  finalCoefficients(7) = finalCoefficients(7) - doubleCutCoefficients(10)
end if  
end do

! write into file
open(unit = 1, file = trim(documentName), status='replace')
write(1, FMT, advance='no') finalCoefficients(1)
write(1, FMT, advance='no') finalCoefficients(2)
write(1, FMT, advance='no') finalCoefficients(3)
write(1, FMT, advance='no') finalCoefficients(4)
write(1, FMT, advance='no') finalCoefficients(5)
write(1, FMT, advance='no') finalCoefficients(6)
write(1, FMT, advance='no') finalCoefficients(7)
write(1, FMT, advance='no') finalCoefficients(8)
write(1, FMT, advance='no') finalCoefficients(9)
close(1)

end if

end function finalCoefficients

end module