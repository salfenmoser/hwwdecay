use mod_auxfunctions
use mod_consts_dp
use mod_kinematics


test_suite mod_diagramsDouble
  complex(dp) :: pa1(3,dimP), pa2(3,dimP), paH(3,dimP)
  integer :: pol1, pol2, c1, c2
  
setup

pol1 = 1
pol2 = 1
c1 = 2
c2 = 2
call particlesHWW(pol1, pol2, c1, c2, paH, pa1, pa2)
end setup

teardown

end teardown


test BReduction
integer :: D_S, i, j, k
complex(dp) :: b(10), doubleCutCoeff(10)

b(:) = czero
doubleCutCoeff(:) = czero

b(1) = one - ci
b(2) = two + two*ci
b(3) = -one/two
b(4) = czero
b(5) = three/four
b(6) = ci
b(7) = -ci
b(8) = four*ci
b(9) = -five
b(10) = mh

do D_S = 4,6
do i = 1,6
j = 1
doubleCutCoeff(:) = czero
doubleCutCoeff = getDoubleCutCoefficients_ij(D_S, paH, pa1, pa2, i, j, .true., b)
print *, doubleCutCoeff(1:2)
print *, '--------'

do k = 1, 8
assert_equal_within(REAL(doubleCutCoeff(k)), REAL(b(k)), limitAssert)
assert_equal_within(aimag(doubleCutCoeff(k)), AIMAG(b(k)), limitAssert)
end do

if (D_S > 4) then
assert_equal_within(REAL(doubleCutCoeff(9)), REAL(b(9)), limitAssert)
assert_equal_within(aimag(doubleCutCoeff(9)), AIMAG(b(9)), limitAssert)
end if

assert_equal_within(REAL(doubleCutCoeff(10)), REAL(b(10)), limitAssert)
assert_equal_within(aimag(doubleCutCoeff(10)), AIMAG(b(10)), limitAssert)

end do
end do

do D_S = 4,6
do i = 1,3, 2
j = 2
doubleCutCoeff(:) = czero
doubleCutCoeff = getDoubleCutCoefficients_ij(D_S, paH, pa1, pa2, i, j, .true., b)
  
do k = 1, 8
assert_equal_within(REAL(doubleCutCoeff(k)), REAL(b(k)), limitAssert)
assert_equal_within(aimag(doubleCutCoeff(k)), AIMAG(b(k)), limitAssert)
end do
  
if (D_S > 4) then
assert_equal_within(REAL(doubleCutCoeff(9)), REAL(b(9)), limitAssert)
assert_equal_within(aimag(doubleCutCoeff(9)), AIMAG(b(9)), limitAssert)
end if
  
assert_equal_within(REAL(doubleCutCoeff(10)), REAL(b(10)), limitAssert)
assert_equal_within(aimag(doubleCutCoeff(10)), AIMAG(b(10)), limitAssert)
  
end do
end do

end test BReduction

end test_suite