use mod_auxfunctions
use mod_consts_dp
use mod_kinematics
use mod_vertices
use mod_numeratorFunctions


test_suite mod_diagramsSingle
  complex(dp) :: pa1(3,dimP), pa2(3,dimP), paH(3,dimP)
  integer :: pol1, pol2, c1, c2

!------------------------------------------------------------------------------------------------------------
setup

pol1 = 3
pol2 = 3
c1 = 1
c2 = 1
call particlesHWW(pol1, pol2, c1, c2, paH, pa1, pa2)

end setup
!------------------------------------------------------------------------------------------------------------

teardown
  ! This code runs immediately after each test
end teardown

!----------------------------------------------------------------------------------
test AReduction
integer :: D_S, i, jSingle, k
complex(dp) :: a(5), singleCutCoeff(5)

a(:) = czero
singleCutCoeff(:) = czero

a(1) = one - ci
a(2) = two + two*ci
a(3) = -one/two
a(4) = czero
a(5) = mh

do D_S = 4,6
do i = 1,3, 2
jSingle = 1
singleCutCoeff(:) = czero
singleCutCoeff = getSingleCutCoefficients_ij(D_S, paH, pa1, pa2, i, jSingle, .true., a)

do k = 1, 5
assert_equal_within(REAL(singleCutCoeff(k)), REAL(a(k)), limitAssert)
assert_equal_within(aimag(singleCutCoeff(k)), AIMAG(a(k)), limitAssert)
end do

end do
end do

end test AReduction
!-------------------------------------------------------------------------------------------------

test momentaSingleCut
  integer :: i, j, D_S, m, n, k
  complex(dp) ::lAux(7,dimP), den(6,3)

do i = 1,3, 2
do D_S = 4,6

j = 1
lAux = getSingleCutMomenta(i)

do m = 1, 7
  den = getDenominators(lAux(m,:), paH, pa1, pa2)
  assert_equal_within(abs(den(i, 1)), zero, limitAssert)
  assert_false(abs(den(i, 2)) < limitAssert)
  assert_false(abs(den(i, 3)) < limitAssert)
end do

end do
end do

end test momentaSingleCut
! !------------------------------------------------------------------------------------------------------------

!------------------------------------------------------------------------------------------------------------

test singleCutNumFunc
  integer :: D_S, j, m, n, k, i
  complex(dp) :: numFunc(3,12), numFuncMink1(6,12), numFuncMink2(3,12), numFuncPlusP(3,12), singleCutFunc(3)
  complex(dp) :: numSum(3), den(3,6,3), denMink1(3,6,3), denMink2(3,6,3), denPlusP(3,6,3)
  complex(dp) :: lAux(7,dimP)

do D_S = 4,4

do m = 1,7
numSum(:) = czero
numFunc(:,:) = czero
singleCutFunc(:) = czero
  
do i = 3,3, 2
  lAux = getSingleCutMomenta(i)
  den(i,:,:) = getDenominators(lAux(m,:), paH, pa1, pa2)
  denMink1(i,:,:) = getDenominators(lAux(m,:) - pa1(1,:), paH, pa1, pa2)
  denMink2(i,:,:) = getDenominators(lAux(m,:) - pa2(1,:), paH, pa1, pa2)
  denPlusP(i,:,:) = getDenominators(lAux(m,:) + paH(1,:), paH, pa1, pa2)
  
  singleCutFunc(i) = getSingleCurrents(D_S, lAux(m,:), paH, pa1, pa2, i, 1, .true.)
    
  do j = 3,12
  numFunc(i,j) = numerator(paH, pa1, pa2, lAux(m,:), j, D_S)
  numFuncMink2(i,j) = numerator(paH, pa1, pa2, lAux(m,:) - pa2(1,:), j, D_S)
  numFuncMink1(i,j) = numerator(paH, pa1, pa2, lAux(m,:) - pa1(1,:), j, D_S)
  numFuncPlusP(i,j) = numerator(paH, pa1, pa2, lAux(m,:) + paH(1,:), j, D_S)
  end do
    
end do

! print *, singleCutFunc(3)
! print *, '--------------------------'
! print *, numFunc(3,8)/den(3,3,3)
! print *, numFuncPlusP(3,8)/denPlusP(3,3,1)
! print *, '--------------------------'
! print *, '--------------------------'
! print *, numFunc(3,9)/den(3,3,2)
! print *, numFuncMink1(3,11)/denMink1(3,5,1)
! print *, '--------------------------'
! print *, numFunc(3,3)/den(3,3,2)/den(3,3,3)
! print *, numFuncPlusP(3,4)/denPlusP(3,4,1)/denPlusP(3,4,2)
! print *, '--------------------------'
! print *, numFuncMink1(3,5)/denMink1(3,5,1)/denMink1(3,5,3)
! print *, numFuncMink2(3,6)/denMink2(3,6,1)/denMink2(3,6,3)
! print *, '--------------------------'
! print *, '--------------------------'
! print *, numFunc(3,10)/den(3,4,2)   ! matches to 4_2
! print *, numFuncMink2(3,12)/denMink2(3,6,1)   ! matches to 4_3
! print *, '--------------------------'
! print *, '--------------------------'
! print *, numFuncPlusP(3,3)/denPlusP(3,3,1)/denPlusP(3,3,2)
! print *, numFunc(3,4)/den(3,4,2)/den(3,4,3)
! print *, '--------------------------'

numSum(3) = numFunc(3,8)/den(3,3,3) + numFuncPlusP(3,8)/denPlusP(3,3,1) + numFunc(3,9)/den(3,3,2) + &
    numFuncMink1(3,11)/denMink1(3,5,1) + numFunc(3,3)/den(3,3,2)/den(3,3,3) + numFuncPlusP(3,4)/denPlusP(3,4,1)/denPlusP(3,4,2) +&
    numFuncMink1(3,5)/denMink1(3,5,1)/denMink1(3,5,3) + numFuncMink2(3,6)/denMink2(3,6,1)/denMink2(3,6,3) +&
    numFunc(3,10)/den(3,4,2) + numFuncMink2(3,12)/denMink2(3,6,1) + numFuncPlusP(3,3)/denPlusP(3,3,1)/denPlusP(3,3,2) +&
    numFunc(3,4)/den(3,4,2)/den(3,4,3)

if (abs(singleCutFunc(3)) > limitAssert) then
  assert_equal_within(abs(numSum(3)/singleCutFunc(3)), 1, limitAssert)
end if

! numSum(3) = numFunc(3,3)/den(3,3,3) + numFuncMink2(3,6)/denMink2(3,6,1) + numFunc(3,9)
! assert_equal_within(abs(numSum(3)/singleCutFunc(3)), 1, limitAssert)
    
! numSum(4) = numFunc(4,4)/den(4,4,3) + numFuncMink1(4,5)/denMink1(4,5,1) + numFunc(4,10)
! assert_equal_within(abs(numSum(4)/singleCutFunc(4)), 1, limitAssert)
  
! numSum(5) = numFunc(5,5)/den(5,5,3) + numFuncMink2(5,4)/denMink2(5,4,1) + numFunc(5,11)
! assert_equal_within(abs(numSum(5)/singleCutFunc(5)), 1, limitAssert)
    
! numSum(6) = numFunc(6,6)/den(6,6,3) + numFuncMink1(6,3)/denMink1(6,3,1) + numFunc(6,12)
! assert_equal_within(abs(numSum(6)/singleCutFunc(6)), 1, limitAssert)
end do


end do

end test singleCutNumFunc


!------------------------------------------------------------------------------------------------------------

end test_suite
